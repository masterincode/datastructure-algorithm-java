package com.udemy.timbuchalka.dataStructure.stack;

import com.udemy.timbuchalka.dataStructure.linkedlist.singly.Employee;
import com.udemy.timbuchalka.dataStructure.linkedlist.singly.EmployeeNode;

public class StackUsingSinglyLinkedList {

    EmployeeNode head;
    EmployeeNode tail;
    int length;

    public static void main(String[] args) {
        StackUsingSinglyLinkedList employeeSinglyLinkedList = new StackUsingSinglyLinkedList();
        employeeSinglyLinkedList.push(new Employee("Virat","Kohli"));
        employeeSinglyLinkedList.push(new Employee("MS","Dhoni"));
        employeeSinglyLinkedList.push(new Employee("Sachin","Tendulkar"));
        System.out.println(employeeSinglyLinkedList.head);
        System.out.println("Peek: "+employeeSinglyLinkedList.peek());
        System.out.println("Pop: "+employeeSinglyLinkedList.pop());
        System.out.println("Peek: "+employeeSinglyLinkedList.peek());
        System.out.println("Pop: "+employeeSinglyLinkedList.pop());
        System.out.println("Peek: "+employeeSinglyLinkedList.peek());
        System.out.println("Pop: "+employeeSinglyLinkedList.pop());
        System.out.println("Peek: "+employeeSinglyLinkedList.peek());
        System.out.println("Pop: "+employeeSinglyLinkedList.pop());
        System.out.println(employeeSinglyLinkedList.head);
    }

    public void push(Employee employee) {
        EmployeeNode node = new EmployeeNode(employee);
        if(head == null) {
            head = node;
            tail = node;
        } else {
            tail.next = node;
            tail = node;
        }
        length++;
    }

    public Employee pop() {
        if(tail == null) {
            return null;
        } else {
            EmployeeNode currentNode = head;
            EmployeeNode removedNode = tail;
            if(head == tail) {
                head = null;
                tail = null;
            } else {
                while (currentNode.next != null) {
                    if(currentNode.next.next == null) {
                        tail = currentNode;
                        tail.next = null;
                        break;
                    } else {
                        currentNode = currentNode.next;
                    }
                }
            }
            length--;
            return removedNode.value;
        }
    }

    public Employee peek() {
        if (head == null) {
            return null;
        } else {
            return tail.value;
        }
    }
}
