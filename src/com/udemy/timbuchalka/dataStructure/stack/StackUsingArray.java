package com.udemy.timbuchalka.dataStructure.stack;

import com.udemy.timbuchalka.dataStructure.linkedlist.singly.Employee;

public class StackUsingArray {

    private Employee[] stack;
    private int top;

    public static void main(String[] args) {
        StackUsingArray stackUsingArray = new StackUsingArray(5);
        stackUsingArray.push(new Employee("Virat","Kohli"));
        stackUsingArray.push(new Employee("MS","Dhoni"));
        stackUsingArray.push(new Employee("Sachin","Tendulkar"));
        for(Employee employee: stackUsingArray.stack)
            System.out.println(employee);
        System.out.println();
        System.out.println("Pop: "+stackUsingArray.pop());
        System.out.println("Pop: "+stackUsingArray.pop());
        System.out.println("Pop: "+stackUsingArray.pop());
        System.out.println("Pop: "+stackUsingArray.pop());
        System.out.println("After Pop we have data !");
        for(Employee employee: stackUsingArray.stack)
            System.out.println(employee);
        stackUsingArray.push(new Employee("Hardik","Pandya"));
        stackUsingArray.push(new Employee("Krunal","Pandya"));
        System.out.println(stackUsingArray.peek());
    }

    public StackUsingArray(int capacity){
        stack = new Employee[capacity];
        top = -1;
    }

    //Time Complexity: O(n) due to copying of array logic
    public void push(Employee employee) {
        if(top == stack.length) {
            Employee[] newArray = new Employee[2 * stack.length];
            System.arraycopy(stack,0,newArray,0,stack.length);
            stack = newArray;
        }
        stack[++top] = employee;
    }

    public Employee pop(){
        if(top == -1) {
            return null;
        } else {
            Employee removedData = stack[top];
            stack[top] = null;
            top--;
            return removedData;
        }
    }
    public Employee peek() {
        if(top == -1) {
            return null;
        } else {
            return stack[top];
        }
    }
}
