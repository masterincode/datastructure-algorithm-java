package com.udemy.timbuchalka.dataStructure.trees.binaryHeaps;

public class Heap {

    private int[] heap;
    private int size;

    public static void main(String[] args) {
        Heap heapmMainMethod = new Heap(10);
        heapmMainMethod.insert(80);
        heapmMainMethod.insert(75);
        heapmMainMethod.insert(60);
        heapmMainMethod.insert(68);
        heapmMainMethod.insert(55);
        heapmMainMethod.insert(40);
        heapmMainMethod.insert(52);
        heapmMainMethod.insert(67);
        heapmMainMethod.traverseHeap();
        System.out.println();
        System.out.println(heapmMainMethod.peek());

    }

    public Heap(int capacity){
        heap = new int[capacity];
    }

    public void insert(int value) {
        if(isFull()) {
            throw new IndexOutOfBoundsException("Heap is full");
        }
        heap[size++] = value;
        fixHeapAbove(size);
    }

    public int peek() {
        if(isEmpty())
            throw new IndexOutOfBoundsException("Heap is Empty");
        return heap[0];
    }

    private boolean isEmpty(){
        return size == 0;
    }

    private void fixHeapAbove(int index) {
        int newValue = heap[index];
        //Excluding root node && Is newValue of children is greater than its parent node
        while(index > 0 && newValue > heap[getParent(index)]){
            heap[index] = heap[getParent(index)]; // Assigning Parent node value to child node
            index = getParent(index); // Getting the Parent index
        }
        heap[index] = newValue; // Assigning new value[Greater] to Parent node
    }

    private void traverseHeap() {
        for (int value: heap) {
          if(value != 0)
            System.out.printf(value+", ");
        }

    }

    private boolean isFull() {
        return size == heap.length;
    }

    private int getParent(int index) {
        return (index - 1)/2;
    }
}
