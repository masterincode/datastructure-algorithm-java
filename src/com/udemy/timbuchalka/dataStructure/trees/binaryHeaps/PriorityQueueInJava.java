package com.udemy.timbuchalka.dataStructure.trees.binaryHeaps;

import java.util.PriorityQueue;

public class PriorityQueueInJava {

    public static void main(String[] args) {

        //Java has implemented using min heap i.e Number with lower value will have highest priority
        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>((num1, num2) -> (num1 > num2) ? 1 : (num1 < num2) ? -1 : 0);
        priorityQueue.add(25);
        priorityQueue.add(-22);
        priorityQueue.add(1343);
        priorityQueue.add(54);
        priorityQueue.add(0);
        priorityQueue.add(-562);
        priorityQueue.add(429);
        System.out.println(priorityQueue);

        System.out.println(priorityQueue.peek());
        System.out.println(priorityQueue.remove()); //it always delete root value
        System.out.println(priorityQueue.peek());
        System.out.println(priorityQueue.poll()); //it always delete root value
        System.out.println(priorityQueue.peek());
        System.out.println(priorityQueue.remove(54));
        System.out.println(priorityQueue.peek());
        priorityQueue.add(-1);
        priorityQueue.add(10);
        priorityQueue.add(100);
        System.out.println(priorityQueue.peek());
        System.out.println(priorityQueue);


    }
}
