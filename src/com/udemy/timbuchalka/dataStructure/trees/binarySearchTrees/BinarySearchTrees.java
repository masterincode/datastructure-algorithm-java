package com.udemy.timbuchalka.dataStructure.trees.binarySearchTrees;


public class BinarySearchTrees {

    private TreeNode root;

    public static void main(String[] args) {
        BinarySearchTrees binarySearchTrees = new BinarySearchTrees();
        binarySearchTrees.push(25);
        binarySearchTrees.push(20);
        binarySearchTrees.push(15);
        binarySearchTrees.push(27);
        binarySearchTrees.push(30);
        binarySearchTrees.push(29);
        binarySearchTrees.push(26);
        binarySearchTrees.push(22);
        binarySearchTrees.push(32);
        binarySearchTrees.push(17);
        binarySearchTrees.traverse();
        System.out.println();
        System.out.println("Find: "+binarySearchTrees.find(27));
        System.out.println("Find: "+binarySearchTrees.find(17));
        System.out.println("Find: "+binarySearchTrees.find(88));
        System.out.println();
        System.out.println("Min: "+binarySearchTrees.minimumValue());
        System.out.println("Max: "+binarySearchTrees.maximumValue());
        System.out.println();
        binarySearchTrees.traverse();
        System.out.println();
        binarySearchTrees.delete(25);
        binarySearchTrees.traverse();
        System.out.println();



    }

    public void push(int value) {
        if(root == null) {
            root = new TreeNode(value);
        } else {
            root.insert(value);
        }
    }

    public TreeNode find(int value) {
        if(root != null) {
           return root.get(value);
        }
        return null;
    }

    public int minimumValue() {
        if(root == null) {
            return Integer.MIN_VALUE;
        } else {
            return root.min();
        }
    }

    public int maximumValue() {
        if(root == null) {
            return Integer.MAX_VALUE;
        } else {
            return root.max();
        }
    }
    public void delete(int value) {
        root = delete(root, value);
    }

    public TreeNode delete(TreeNode subTreeNode, int value) {
        if(subTreeNode == null) {
            return null;
        }
        if(value < subTreeNode.data) {
          subTreeNode.left = delete(subTreeNode.left, value);
        } else if(value > subTreeNode.data){
          subTreeNode.right = delete(subTreeNode.right, value);
        } else {

            //Case 1 and 2: When node has 0 and 1 child(ren)
            if(subTreeNode.left == null) {
                return subTreeNode.right;
            } else if(subTreeNode.right == null) {
                return subTreeNode.left;
            }
            //Case 3: When Nodez has 2 Children

            //Replace the value in the subTreeNode with the smallest value from the right subTreeNode
            subTreeNode.data = subTreeNode.right.min();

            //Delete the node that has the smallest value in the right subTreeNode
            subTreeNode.right = delete(subTreeNode.right, subTreeNode.data);
        }
        return subTreeNode;
    }

    // Traversing In-Order
    public void traverse() {
       if(root != null) {
           root.traverseInOrder();
       }
    }




}
