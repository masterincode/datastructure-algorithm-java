package com.udemy.timbuchalka.dataStructure.linkedlist.doubly;


import com.udemy.timbuchalka.dataStructure.linkedlist.singly.Employee;

public class EmployeeDoublyLinkedList {

    private EmployeeNode head;
    private EmployeeNode tail;
    private int size;

    public static void main(String[] args) {
        EmployeeDoublyLinkedList employeeDoublyLinkedList = new EmployeeDoublyLinkedList();
        employeeDoublyLinkedList.addToFront(new Employee("Virat","Kohli"));
        employeeDoublyLinkedList.addToFront(new Employee("MS","Dhoni"));
        employeeDoublyLinkedList.addToFront(new Employee("Sachin","Tendulkar"));
        employeeDoublyLinkedList.traverse();
        System.out.println();
        employeeDoublyLinkedList.addToEnd(new Employee("Bill","End"));
        employeeDoublyLinkedList.traverse();
        System.out.println();
        employeeDoublyLinkedList.removeFromFront();
        employeeDoublyLinkedList.traverse();
        System.out.println();
        employeeDoublyLinkedList.removeFromEnd();
        employeeDoublyLinkedList.traverse();
        System.out.println();

    }

    public void addToFront(Employee employee) {
        EmployeeNode node = new EmployeeNode(employee);
        node.next = head;
        if(head == null) {
            tail = node;
        } else {
            head.previous = node;
        }
        head = node;
        size++;
    }

    public void addToEnd(Employee employee){
        EmployeeNode node = new EmployeeNode(employee);
        if(head == null) {
            head = node;
            tail = node;
        } else {
            tail.next = node;
            node.previous = tail;
            tail = node;
        }
        size++;
    }

    public void traverse() {
        EmployeeNode currentNode = head;
        System.out.print("Head ->");
        while(currentNode != null) {
            System.out.print(currentNode.value);
            System.out.print("<---->");
            currentNode = currentNode.next;
        }
        System.out.println("null");
    }

    public EmployeeNode removeFromFront(){
        if(isEmpty()) {
            return null;
        }
        EmployeeNode removedNode = head;
        if(head.next == null) {
            tail = null;
        } else {
            head.next.previous = null;
        }
        head = head.next;
        size--;
        removedNode.next = null;
        return removedNode;
    }

    public EmployeeNode removeFromEnd() {
        if(isEmpty()) {
            return null;
        }
        EmployeeNode removedNode = tail;
        if(tail.previous == null) {
            head = null;
        } else {
            tail.previous.next = null;
        }
        tail = tail.previous;
        size--;
        removedNode.previous = null;
        return removedNode;
    }

    public int getSize() {
        return size;
    }

    public boolean isEmpty() {
        return head == null;
    }
}
