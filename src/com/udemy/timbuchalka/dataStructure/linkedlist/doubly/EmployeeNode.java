package com.udemy.timbuchalka.dataStructure.linkedlist.doubly;

import com.udemy.timbuchalka.dataStructure.linkedlist.singly.Employee;
import lombok.Data;

@Data
public class EmployeeNode {

    public Employee value;
    public EmployeeNode next;
    public EmployeeNode previous;

    public EmployeeNode(Employee employee) {
        this.value = employee;
    }
}
