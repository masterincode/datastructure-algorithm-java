package com.udemy.timbuchalka.dataStructure.linkedlist.singly;

public class EmployeeSinglyLinkedList {

    private EmployeeNode head;
    private int size;
    public static void main(String[] args) {
        EmployeeSinglyLinkedList employeeSinglyLinkedList = new EmployeeSinglyLinkedList();
        employeeSinglyLinkedList.addToFront(new Employee("Virat","Kohli"));
        employeeSinglyLinkedList.addToFront(new Employee("MS","Dhoni"));
        employeeSinglyLinkedList.addToFront(new Employee("Sachin","Tendulkar"));
        for (int i=0 ; i<= employeeSinglyLinkedList.size; i++) {
            System.out.println(employeeSinglyLinkedList.head);
            employeeSinglyLinkedList.removeFromFront();
        }
    }

    public void addToFront(Employee employee) {
        EmployeeNode node = new EmployeeNode(employee);
        node.next = head;
        head = node;
        size++;
    }

    public EmployeeNode removeFromFront(){
        if(isEmpty()) {
            return null;
        }
        EmployeeNode oldHead = head;
        head = head.next;
        oldHead.next = null;
        return oldHead;
    }

    public int getSize() {
        return size;
    }

    public boolean isEmpty() {
        return head == null;
    }
}
