package com.udemy.timbuchalka.dataStructure.linkedlist.singly;

import lombok.Data;

@Data
public class EmployeeNode {

    public Employee value;
    public EmployeeNode next;

    public EmployeeNode(Employee employee) {
        this.value = employee;
    }
}
