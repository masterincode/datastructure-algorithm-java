package com.udemy.timbuchalka.dataStructure.queues;

import com.udemy.timbuchalka.dataStructure.linkedlist.singly.Employee;
import com.udemy.timbuchalka.dataStructure.linkedlist.singly.EmployeeNode;

public class QueuesUsingSinglyLinkedList {

    EmployeeNode first;
    EmployeeNode last;
    int length;

    public static void main(String[] args) {
        QueuesUsingSinglyLinkedList queuesUsingSinglyLinkedList = new QueuesUsingSinglyLinkedList();
        queuesUsingSinglyLinkedList.add(new Employee("Virat","Kohli"));
        queuesUsingSinglyLinkedList.add(new Employee("MS","Dhoni"));
        queuesUsingSinglyLinkedList.add(new Employee("Sachin","Tendulkar"));
        System.out.println(queuesUsingSinglyLinkedList.first);
        System.out.println();
        System.out.println("Peek: "+queuesUsingSinglyLinkedList.peek());
        System.out.println("Remove: "+queuesUsingSinglyLinkedList.remove());
        System.out.println("Peek: "+queuesUsingSinglyLinkedList.peek());
        System.out.println(queuesUsingSinglyLinkedList.first);
        System.out.println("Remove: "+queuesUsingSinglyLinkedList.remove());
        System.out.println("Peek: "+queuesUsingSinglyLinkedList.peek());
        System.out.println(queuesUsingSinglyLinkedList.first);
        System.out.println("Remove: "+queuesUsingSinglyLinkedList.remove());
        System.out.println("Peek: "+queuesUsingSinglyLinkedList.peek());
        System.out.println(queuesUsingSinglyLinkedList.first);
        System.out.println("Remove: "+queuesUsingSinglyLinkedList.remove());
        System.out.println("Peek: "+queuesUsingSinglyLinkedList.peek());
        System.out.println(queuesUsingSinglyLinkedList.first);
    }

    //Enqueue: Add element at the end of the queue [Linked List PUSH Method]
    public void add(Employee employee){
        EmployeeNode node = new EmployeeNode(employee);
        if(first == null) {
            first = node;
            last = node;
        } else {
           last.next = node;
           last = node;
        }
        length++;
    }

    //Dequeue : Remove element from start of the queue
    public Employee remove() {
        if (first == null) {
            return null;
        } else {
            EmployeeNode removedNode = first;
            first = first.next;
            removedNode.next = null;
            length--;
            return removedNode.value;
        }
    }

    public Employee peek() {
        if (first == null) {
            return null;
        } else {
            return first.value;
        }
    }
}
