package com.udemy.timbuchalka.dataStructure.queues;

import com.udemy.timbuchalka.dataStructure.linkedlist.singly.Employee;

public class QueuesUsingArray {

    private Employee[] queue;
    private int front;
    private int back;

    public static void main(String[] args) {
        QueuesUsingArray queuesUsingArray = new QueuesUsingArray(5);
        queuesUsingArray.add(new Employee("Virat","Kohli"));
        queuesUsingArray.add(new Employee("MS","Dhoni"));
        queuesUsingArray.add(new Employee("Sachin","Tendulkar"));
        for (Employee employee: queuesUsingArray.queue) {
            System.out.println(employee);
        }
        System.out.println();
        System.out.println("Peek: "+queuesUsingArray.peek());
        System.out.println("Remove: "+queuesUsingArray.remove());
        System.out.println("Peek: "+queuesUsingArray.peek());
        System.out.println("Remove: "+queuesUsingArray.remove());
        System.out.println("Peek: "+queuesUsingArray.peek());
        System.out.println("Remove: "+queuesUsingArray.remove());
        System.out.println("Peek: "+queuesUsingArray.peek());
        System.out.println("Remove: "+queuesUsingArray.remove());
        System.out.println("Peek: "+queuesUsingArray.peek());
    }

    public QueuesUsingArray(int capacity) {
        queue = new Employee[capacity];
    }

    public void add(Employee employee) {
        if(back == queue.length) {
            Employee[] newArray = new Employee[2 * queue.length];
            System.arraycopy(queue,0,newArray,0,queue.length);
            queue = newArray;
        }
        queue[back] = employee;
        back++;
    }

    public Employee remove() {
        if(front >= back){
            return null;
        } else {
            Employee removedNode = queue[front];
            front++;
            return removedNode;
        }
    }

    public Employee peek() {
        if(front == 0 && back == 0) {
            return  null;
        } else {
            return queue[front];
        }
    }
}
