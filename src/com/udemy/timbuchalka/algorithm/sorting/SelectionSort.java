package com.udemy.timbuchalka.algorithm.sorting;

public class SelectionSort {

    public static void main(String[] args) {
        int[] array =  {20,10,40,50,30};
        int lastUnsortedIndex = array.length-1;
        int largestElementIndex = 0;
        int tmp = 0;

        for(int i = lastUnsortedIndex; i > 0; i--) {
            for(int j = 1; j <= lastUnsortedIndex; j++ ) {
                if(array[largestElementIndex] < array[j]) {
                    largestElementIndex = j;
                }
            }
            tmp = array[lastUnsortedIndex];
            array[lastUnsortedIndex] = array[largestElementIndex];
            array[largestElementIndex] = tmp;
            lastUnsortedIndex--;
            largestElementIndex = 0;
        }
        for (int v: array)
        System.out.println(v);
    }
}
