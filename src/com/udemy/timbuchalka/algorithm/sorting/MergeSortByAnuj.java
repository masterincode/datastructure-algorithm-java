package com.udemy.timbuchalka.algorithm.sorting;

public class MergeSortByAnuj {

    public static void main(String[] args) {
          int[] arr = {9,4,7,6,3,1,5};
          mergesort(arr,0, arr.length-1);
          for (int v: arr) {
              System.out.printf(v+", ");
          }
    }

    private static void mergesort(int[] arr, int left , int right) {

        if(left < right) {
            int mid = (left + right) / 2;
            mergesort(arr, left, mid);
            mergesort(arr,mid+1, right);
            mergeArray(arr,left, mid, right);
        }
    }

    private static void mergeArray(int[] arr, int left, int mid, int right) {

       int[] tempArr = new int[arr.length];
       int i = left;
       int j = mid + 1;
       int k = left;

      while(i <= mid && j<= right) {
          if(arr[i] < arr[j]) {
              tempArr[k] = arr[i];
              i++;
          } else {
              tempArr[k] = arr[j];
              j++;
          }
          k++;
      }

      if( i > mid) {
          while(j <= right) {
              tempArr[k] = arr[j];
              j++;
              k++;
          }
      } else {
           while(i <= mid) {
                tempArr[k] = arr[i];
                i++;
                k++;
           }
      }

      for(k = left; k<= right ; k++) {
          arr[k] = tempArr[k];
      }
    }
}
