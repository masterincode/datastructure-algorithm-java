package com.udemy.timbuchalka.algorithm.sorting;

public class MergeSort {

    public static void main(String[] args) {
        int[] array =  {20,35,-15,7,55,1,-22};
        int[] output = divideArray(array);
        for(int i: output)
            System.out.printf(i+", ");
    }

    public static int[] divideArray(int[]  parentArray){
        if(parentArray.length == 1) {
            return parentArray;
        }
        int firstIndex = 0;
        int lastIndex = parentArray.length;
        int middleIndex = (firstIndex+lastIndex)/2;
        int[] leftSubArray = new int[middleIndex];
        int[] rightSubArray = new int[parentArray.length - middleIndex];
        int rightIndex = 0;
        for(int i =0; i<parentArray.length; i++) {
            if(i< middleIndex) {
                leftSubArray[i] = parentArray[i];
                if(i == middleIndex - 1) {
                   leftSubArray = divideArray(leftSubArray);
                }
            } else {
                rightSubArray[rightIndex] = parentArray[i];
                rightIndex++;
                if(i == parentArray.length - 1) {
                   rightSubArray = divideArray(rightSubArray);
                }
            }
        }
        return mergeTwoArrays(leftSubArray, leftSubArray.length, rightSubArray, rightSubArray.length);
    }

    public static int[] mergeTwoArrays(int[] arr1, int n,  int[] arr2, int m) {
        int[] resultArray =  new int[n+m];
        int i = 0; int j = 0; int k = 0;

        while(i < n && j < m) {
            if(arr1[i] < arr2[j]) {
               resultArray[k] = arr1[i];
               i++;
            } else {
                resultArray[k] = arr2[j];
                j++;
            }
            k++;
        }

        while(i < n) {
            resultArray[k] = arr1[i];
            i++;
            k++;
        }

        while(j < m) {
            resultArray[k] = arr2[j];
            j++;
            k++;
        }
        return resultArray;
    }

}
/*                      {20,35,-15,7,55,1,-22}

*                {20,35,-15}             {7,55,1,-22}
*
*             {20}      {35,-15}       {7,55}      {1,-22}
*
*                     {35}   {-15}   {7}  {55}    {1}  {-22}
*
* */