package com.udemy.timbuchalka.algorithm.sorting;

public class InsertionSort {

    public static void main(String[] args) {
//        int[] array =  {20,35,-15,7,55,1,-22};
//        int[] array =  {10,60,20,50,30,40};
        int[] array =  {20,3,-15,7,-55,1,22};
        int newElement = 0;
        int temp;
        int decreaseIndex;

        //firstUnSortedIndex will move from Left to right [Unsorted array]
        for(int firstUnSortedIndex = 1; firstUnSortedIndex < array.length; firstUnSortedIndex++) {
            decreaseIndex =  firstUnSortedIndex;
            newElement = array[firstUnSortedIndex];

            //i will move from right to left [Sorted array]
            for (int i = firstUnSortedIndex-1; i >= 0 ; i--) {
                if(newElement < array[i]) {
                    temp = newElement;
                    array[decreaseIndex--] = array[i];
                    array[i] = temp;
                }
            }
        }
        for (int v: array)
            System.out.print(v+", ");
    }
}
