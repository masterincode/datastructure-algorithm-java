package com.udemy.coltsteele.dataStructure.sorting;

public class QuickSortAlgorithm {

    /* Reference: https://www.baeldung.com/java-quicksort
       Find All Sorting Algorithms https://github.com/eugenp/tutorials/tree/master/algorithms-sorting
       Time Complexity:
       Best : O (n Log n)
       Worst : O( n^2 )
    */
    public static final int[] arr = {4, 6, 9, 1, 2, 5};

    public static void main(String[] args) {
        new QuickSortAlgorithm().quickSort(arr, 0, arr.length - 1);
        for (int i : arr)
            System.out.println(i);
    }

    public void quickSort(int[] arr, int left, int right) {
        if (left < right) {
            int partitionIndex = partition(arr, left, right);

            quickSort(arr, left, partitionIndex - 1);
            quickSort(arr, partitionIndex + 1, right);
        }
    }

    public int partition(int[] arr, int left, int right) {
        //Taking array last element as pivot element
        int pivotElement = arr[right];
        int pivotIndex = (left - 1);
        for (int j = left; j < right; j++) {
            if (arr[j] <= pivotElement) {
                pivotIndex++;
                int swapTemp = arr[pivotIndex];
                arr[pivotIndex] = arr[j];
                arr[j] = swapTemp;
            }
        }
        int swapTemp = arr[pivotIndex + 1];
        arr[pivotIndex + 1] = arr[right];
        arr[right] = swapTemp;
        return pivotIndex + 1;
    }
}
