package com.udemy.coltsteele.dataStructure.sorting;

public class InsertionSortAlgorithm {


    public static void main(String[] args) {
        int[] arr = {6,3,1,8,7,2,4};
        insertionSort(arr);
        for(int i: arr)
            System.out.println(i);
    }
    public static void insertionSort(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            int key = arr[i];
            int j = i - 1;
            while (j >= 0 && arr[j] > key) {
                arr[j + 1] = arr[j];
                j = j - 1;
            }
            arr[j + 1] = key;
        }
    }
}
