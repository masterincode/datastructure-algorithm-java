package com.udemy.coltsteele.dataStructure.trees.binaryHeaps;

import java.util.ArrayList;
import java.util.List;

public class MaxBinaryHeap {

    public List<Integer> values = new ArrayList<>();

    // Time complexity: O (log N)
    public List<Integer> insert(Integer param) {
        values.add(param);
        return this.bubbleUp();
    }

    public List<Integer> bubbleUp() {
         int index = this.values.size() - 1;
         int childValue  = this.values.get(index);
         while(index > 0) {
               int parentIndex = Math.round((index - 1)/2);
               int parentValue = this.values.get(parentIndex);
               if(childValue <= parentValue)
                   break;
               this.values.set(parentIndex,childValue);
               this.values.set(index,parentValue);
               index = parentIndex;
         }
         return this.values;
    }

    // Time complexity: O (log N)
    public int extractMax(){
        if(this.values.size() > 0)
        {
            int max = this.values.get(0);
            int end = this.values.remove(this.values.size() -1);
            if(this.values.size() > 0) {
                this.values.set(0, end);
                this.sinkDown();
                return max;
              }
        }
       return 0;
    }

    public void sinkDown() {
        int index = 0;
        int length = this.values.size();
        int element = this.values.get(0);
        while(true) {
            int leftChildIndex = 2 * index + 1;
            int rightChildIndex = 2 * index + 2;
            int leftChild = 0, rightChild;
            int swap = 0;
            if(leftChildIndex < length) {
                leftChild = this.values.get(leftChildIndex);
                if(leftChild > element) {
                    swap = leftChildIndex;
                }
            }
            if(rightChildIndex < length) {
                rightChild = this.values.get(rightChildIndex);
                if( (swap == 0 && rightChild > element) || (swap != 0 && rightChild > leftChild)) {
                    swap = rightChildIndex;
                }
            }
            if(swap == 0)
                break;
            this.values.set(index, this.values.get(swap));
            this.values.set(swap, element);
            index = swap;
        }
    }
}