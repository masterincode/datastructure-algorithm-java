package com.udemy.coltsteele.datastructure.trees.binaryHeaps;

import java.util.Arrays;
import java.util.Scanner;

public class PriorityQueueMainMethod {
    
    public static void main(String args[]) {
        
        PriorityQueue priorityQueue = new PriorityQueue();
        startPriorityQueue(priorityQueue);
    }
    
    public static void startPriorityQueue(PriorityQueue priorityQueue) {

        System.out.println();
        System.out.println("Select the operation need to be perform.");
        System.out.println("1. Enqueue Method   2. Dequeue Max Priority     3. Auto Insert Data");
        System.out.println();
        Scanner operationType = new Scanner(System.in);
        Scanner userInput = new Scanner(System.in);
        int operationNumber = operationType.nextInt();
        switch (operationNumber) {

            case 1:     System.out.println("Enter value to Enqueue");
                        String args1 = userInput.nextLine();
                        System.out.println("Enter priority level to Enqueue");
                        int args2   = userInput.nextInt();
                        System.out.println(priorityQueue.enqueue(args1, args2));
                        startPriorityQueue(priorityQueue);
                        break;

            case 2 :    System.out.println(priorityQueue.dequeue());
                        System.out.println(priorityQueue.values);
                        startPriorityQueue(priorityQueue);
                        break;

            case 3 :    Node arr [] = { new Node("common cold",5),
                                        new Node("gunshot wound",1),
                                        new Node("high fever",4),
                                        new Node("broken arm",2),
                                        new Node("glass inside foot",3)};
                        Arrays.stream(arr).forEach(res -> {
                            priorityQueue.enqueue(res.val, res.priority);
                        });
                        startPriorityQueue(priorityQueue);
                        break;

            default:    startPriorityQueue(priorityQueue);
                        break;
        }
        
    }
}
