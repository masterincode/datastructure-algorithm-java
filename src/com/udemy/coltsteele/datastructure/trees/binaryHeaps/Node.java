package com.udemy.coltsteele.datastructure.trees.binaryHeaps;

public class Node {
    public String val;
    public int priority;

    public Node(String val, int priority) {
        this.val = val;
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "Node{" +
                "val='" + val + '\'' +
                ", priority=" + priority +
                '}';
    }
}
