package com.udemy.coltsteele.datastructure.trees.binaryHeaps;

import java.util.ArrayList;
import java.util.List;

public class PriorityQueue {

    public List<Node> values = new ArrayList<>();

    // Time complexity: O (log N)
    public List<Node> enqueue(String val , int priority) {
        Node newNode = new Node(val,priority);
        values.add(newNode);
        return this.bubbleUp();
    }

    public List<Node> bubbleUp() {
        int index = this.values.size() - 1;
        Node childValue  = this.values.get(index);
        while(index > 0) {
            int parentIndex = Math.round((index - 1)/2);
            Node parentValue = this.values.get(parentIndex);
            if(childValue.priority >= parentValue.priority)
                break;
            this.values.set(parentIndex,childValue);
            this.values.set(index,parentValue);
            index = parentIndex;
        }
        return this.values;
    }

    // Time complexity: O (log N)
    public Node dequeue(){
        if(this.values.size() > 0)
        {
            Node min = this.values.get(0);
            Node end = this.values.remove(this.values.size() -1);
            if(this.values.size() > 0) {
                this.values.set(0, end);
                this.sinkDown();
                return min;
            }
        }
        return null;
    }

    public void sinkDown() {
        int index = 0;
        int length = this.values.size();
        Node element = this.values.get(0);
        while(true) {
            int leftChildIndex = 2 * index + 1;
            int rightChildIndex = 2 * index + 2;
            Node leftChild = null, rightChild = null;
            int swap = 0;
            if(leftChildIndex < length) {
                leftChild = this.values.get(leftChildIndex);
                if(leftChild.priority < element.priority) {
                    swap = leftChildIndex;
                }
            }
            if(rightChildIndex < length) {
                rightChild = this.values.get(rightChildIndex);
                if( (swap == 0 && rightChild.priority < element.priority) || (swap != 0 && rightChild.priority < leftChild.priority)) {
                    swap = rightChildIndex;
                }
            }
            if(swap == 0)
                break;
            this.values.set(index, this.values.get(swap));
            this.values.set(swap, element);
            index = swap;
        }
    }
}
