package com.udemy.coltsteele.dataStructure.trees.binaryHeaps;

import java.util.Arrays;
import java.util.Scanner;

public class MaxBinaryHeapMainMethod {

    public static void main(String args[]) {
        MaxBinaryHeap maxBinaryHeap = new MaxBinaryHeap();
        startMaxBinaryHeap(maxBinaryHeap);
    }

    public static void startMaxBinaryHeap(MaxBinaryHeap maxBinaryHeap) {
        System.out.println();
        System.out.println("Select the operation need to be perform.");
        System.out.println("1. Insert Method   2. Extract Max     3. Auto Insert Data");
        System.out.println();
        Scanner operationType = new Scanner(System.in);
        Scanner userInput = new Scanner(System.in);
        int operationNumber = operationType.nextInt();
        switch (operationNumber) {

            case 1:     System.out.println("Enter data to Insert");
                        System.out.println(maxBinaryHeap.insert(userInput.nextInt()));
                        startMaxBinaryHeap(maxBinaryHeap);
                        break;

            case 2 :    System.out.println(maxBinaryHeap.extractMax());
                        System.out.println(maxBinaryHeap.values);
                        startMaxBinaryHeap(maxBinaryHeap);
                        break;

            case 3 :    Integer arr [] = {41,39,33,18,27,12};
                        Arrays.stream(arr).forEach(value -> {
                            maxBinaryHeap.insert(value);
                        });
                        startMaxBinaryHeap(maxBinaryHeap);
                        break;

            default:
                break;
        }
    }
}
