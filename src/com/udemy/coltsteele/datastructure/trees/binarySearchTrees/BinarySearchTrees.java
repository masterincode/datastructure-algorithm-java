package com.udemy.coltsteele.datastructure.trees.binarySearchTrees;

import java.util.*;

public class BinarySearchTrees {

    public Node root;

    //Time Complexity : O (log n)
    public boolean insert(int value) {
        Node newNode = new Node(value);
        if(this.root == null) {
            this.root = newNode;
        }
        Node current = this.root;
        while(true){
            if(value == current.value) return false;
            if(value < current.value){
                if(current.left == null){
                    current.left = newNode;
                    return true;
                }
                current = current.left;
            } else {
                if(current.right == null){
                    current.right = newNode;
                    return true;
                }
                current = current.right;
            }
        }
    }

    //Time Complexity : O (log n)
    public boolean find(int value) {
        if(this.root == null) return false;
        Node current = this.root;
        boolean found = false;
        while(current != null && !found){
            if(value < current.value){
                current = current.left;
            } else if(value > current.value){
                current = current.right;
            } else {
                found = true;
            }
        }
        if(!found) return false;
        return true;
    }

    //Reference: https://www.baeldung.com/java-binary-tree
    public void delete(int value) {
        root = deleteRecursive(root, value);
    }

    private Node deleteRecursive(Node current, int value) {
        if (current == null) {
            return null;
        }
        if (value == current.value) {
        // 1. A node has no children
            if (current.left == null && current.right == null) {
                return null;
            }
        // 2. A node has one child
            if (current.right == null) {
                return current.left;
            }
            if (current.left == null) {
                return current.right;
            }

        // 3. A node has two children
            int smallestValue = findSmallestValue(current.right);
            current.value = smallestValue;
            current.right = deleteRecursive(current.right, smallestValue);
            return current;
        }
        if (value < current.value) {
            current.left = deleteRecursive(current.left, value);
            return current;
        }
        current.right = deleteRecursive(current.right, value);
        return current;
    }

    private int findSmallestValue(Node root) {
        return root.left == null ? root.value : findSmallestValue(root.left);
    }

    //====================Level Order Traversal (BFS)=================================
    public List<Integer> breadthFirstSearch() {
        Queue<Node> queue = new LinkedList<>();
        List<Integer> data = new ArrayList<>();
        Node node = this.root;
        queue.add(node);
        while(!queue.isEmpty()) {
            node = queue.poll();
            data.add(node.value);
            if(node.left != null)
                queue.add(node.left);
            if(node.right != null)
                queue.add(node.right);
        }
        return data;
    }

    //====================Inorder DFS=================================
    public List<Integer> inOrderDFS(Node root){
        List<Integer> dfsOutput = new ArrayList<>();
        inOrderTraversal(root, dfsOutput);
        return dfsOutput;
    }

    public void inOrderTraversal(Node root, List<Integer> dfsOutput){
        if(root == null)
            return;
        inOrderTraversal(root.left, dfsOutput);
        dfsOutput.add(root.value);
        inOrderTraversal(root.right, dfsOutput);
    }

    //====================Preorder DFS=================================
    public List<Integer> preOrderDFS(Node root){
        List<Integer> dfsOutput = new ArrayList<>();
        preOrderTraversal(root, dfsOutput);
        return dfsOutput;
    }

    public void preOrderTraversal(Node root, List<Integer> dfsOutput){
        if(root == null)
            return;
        dfsOutput.add(root.value);
        preOrderTraversal(root.left, dfsOutput);
        preOrderTraversal(root.right, dfsOutput);
    }

    //====================Postorder DFS=================================
    public List<Integer> postOrderDFS(Node root){
        List<Integer> dfsOutput = new ArrayList<>();
        postOrderTraversal(root, dfsOutput);
        return dfsOutput;
    }

    public void postOrderTraversal(Node root, List<Integer> dfsOutput){
        if(root == null)
            return;
        postOrderTraversal(root.left, dfsOutput);
        postOrderTraversal(root.right, dfsOutput);
        dfsOutput.add(root.value);
    }

    @Override
    public String toString() {
        return "BinarySearchTrees{" +
                "root=" + root +
                '}';
    }
}
