package com.udemy.coltsteele.datastructure.trees.binarySearchTrees;

import java.util.Arrays;
import java.util.Scanner;

public class BinarySearchTreesMainMethod {

    public static void main(String args[]) {
        BinarySearchTrees binarySearchTrees = new BinarySearchTrees();
        startBinarySearchTrees(binarySearchTrees);
    }

    public static void startBinarySearchTrees(BinarySearchTrees binarySearchTrees) {
        System.out.println();
        System.out.println("Select the operation need to be perform.");
        System.out.println("1. Insert Method    2. Find Method   3. Delete          4. BFS Method");
        System.out.println("5. InOrder DFS      6. PreOrder DFS  7. PostOrder DFS   8. Automatic Insert" );
        System.out.println();
        Scanner operationType = new Scanner(System.in);
        Scanner userInput = new Scanner(System.in);
        int operationNumber = operationType.nextInt();
        switch (operationNumber) {

            case 1:
                System.out.println("Enter data to Insert");
                System.out.println(binarySearchTrees.insert(userInput.nextInt()));
                System.out.println(binarySearchTrees);
                startBinarySearchTrees(binarySearchTrees);
                break;

            case 2:
                System.out.println("Enter data to find ??");
                System.out.println(binarySearchTrees.find(userInput.nextInt()));
                startBinarySearchTrees(binarySearchTrees);
                break;

            case 3:
                System.out.println("Enter data to Delete element ??");
                binarySearchTrees.delete(userInput.nextInt());
                startBinarySearchTrees(binarySearchTrees);
                break;

            case 4:
                System.out.println(binarySearchTrees.breadthFirstSearch());
                startBinarySearchTrees(binarySearchTrees);
                break;

            case 5:
                Integer arr1[] = {50, 60, 65, 55, 40, 35, 45};
                Arrays.stream(arr1).forEach(value -> {
                    binarySearchTrees.insert(value);
                });
                System.out.println("InOrder: "+binarySearchTrees.inOrderDFS(binarySearchTrees.root));
                startBinarySearchTrees(binarySearchTrees);
                break;

            case 6:
                Integer arr2[] = {50, 60, 65, 55, 40, 35, 45};
                Arrays.stream(arr2).forEach(value -> {
                    binarySearchTrees.insert(value);
                });
                System.out.println("PreOrder: "+binarySearchTrees.preOrderDFS(binarySearchTrees.root));
                startBinarySearchTrees(binarySearchTrees);
                break;

            case 7:
                Integer arr3[] = {50, 60, 65, 55, 40, 35, 45};
                Arrays.stream(arr3).forEach(value -> {
                    binarySearchTrees.insert(value);
                });
                System.out.println("PostOrder: "+binarySearchTrees.postOrderDFS(binarySearchTrees.root));
                startBinarySearchTrees(binarySearchTrees);
                break;

            case 8:
                Integer arr4[] = {50, 60, 65, 55, 40, 35, 45};
                Arrays.stream(arr4).forEach(value -> {
                    binarySearchTrees.insert(value);
                });
                startBinarySearchTrees(binarySearchTrees);
                break;

            default:
                break;
        }
    }
}
