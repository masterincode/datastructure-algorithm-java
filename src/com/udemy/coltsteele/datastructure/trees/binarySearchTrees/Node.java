package com.udemy.coltsteele.datastructure.trees.binarySearchTrees;

public class Node {

    public Node right;
    public Node left;
    public int value;

    public Node(int value) {
        this.value = value;
        this.right = null;
        this.left  = null;
    }

    @Override
    public String toString() {
        return "Node{" +
                "right=" + right +
                ", left=" + left +
                ", value=" + value +
                '}';
    }
}
