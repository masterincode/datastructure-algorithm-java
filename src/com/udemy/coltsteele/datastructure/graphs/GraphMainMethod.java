package com.udemy.coltsteele.datastructure.graphs;

import java.util.Arrays;
import java.util.Scanner;

public class GraphMainMethod {

    public static void main(String[] args) {
        Graph graph = new Graph();
        startGraph(graph);
    }

    public static void startGraph(Graph graph) {
        System.out.println();
        System.out.println("Select the operation need to be perform.");
        System.out.println("1. Add Vertex            2. Add Edges           3.Remove Edge");
        System.out.println("4. Remove Vertex         5. Recursive DFS       6. Iterative DFS");
        System.out.println("7. Breadth First Search  8. Auto Insert City    9. Auto Insert Alphabet");
        System.out.println();
        Scanner operationType = new Scanner(System.in);
        Scanner userInput = new Scanner(System.in);
        int operationNumber = operationType.nextInt();
        switch (operationNumber) {

            case 1:     System.out.println("Enter the Vertex Name !!");
                        graph.addVertex(userInput.nextLine());
                        graph.traverse();
                        startGraph(graph);
                        break;

            case 2:     System.out.println("Enter the First Vertex !!");
                        String vertex1 = userInput.nextLine();
                        System.out.println("Enter the Second Vertex !!");
                        String vertex2 = userInput.nextLine();
                        graph.addEdge(vertex1,vertex2);
                        graph.traverse();
                        startGraph(graph);
                        break;

            case 3:     System.out.println("Enter the First Vertex to be removed !!");
                        String vertex3 = userInput.nextLine();
                        System.out.println("Enter the Second Vertex to be removed !!");
                        String vertex4 = userInput.nextLine();
                        graph.removeEdge(vertex3,vertex4);
                        graph.traverse();
                        startGraph(graph);
                        break;

            case 4:     System.out.println("Enter the Vertex that to be removed");
                        graph.removeVertex(userInput.nextLine());
                        graph.traverse();
                        startGraph(graph);
                        break;

            case 5:     System.out.println("Enter the Vertex for Recursive DFS");
                        graph.recursiveDFS(userInput.nextLine());
                        System.out.println("Visted List : "+graph.visitedNodeList);
                        System.out.println("Flag : "+graph.visitedNodeMapping);
                        startGraph(graph);
                        break;

            case 6:     System.out.println("Enter the Vertex for Iterative DFS");
                        graph.iterativeDFS(userInput.nextLine());
                        System.out.println("Visted List : "+graph.visitedNodeList);
                        System.out.println("Flag : "+graph.visitedNodeMapping);
                        startGraph(graph);
                        break;

            case 7:     System.out.println("Enter the Vertex for Breadth First Search");
                        graph.breadthFirstSearchBFS(userInput.nextLine());
                        System.out.println("Visted List : "+graph.visitedNodeList);
                        System.out.println("Flag : "+graph.visitedNodeMapping);
                        startGraph(graph);
                        break;

            case 8 :    String arr [] = {"Delhi","Patna","Mumbai", "Kolkatta"};
                        Arrays.stream(arr).forEach(value -> {
                            graph.addVertex(value);
                        });
                        graph.addEdge("Delhi","Patna");
                        graph.addEdge("Patna","Mumbai");
                        graph.addEdge("Delhi","Mumbai");
                        graph.addEdge("Delhi","Chennai");
                        graph.addEdge("Chennai","Patna");
                        graph.addEdge("Chennai","Mumbai");
                        graph.traverse();
                        startGraph(graph);
                        break;

            case 9 :   String arr1 [] = {"A","B","C", "D", "E", "F"};
                       Arrays.stream(arr1).forEach(value -> {
                         graph.addVertex(value);
                       });
                       graph.addEdge("A","B");
                       graph.addEdge("A","C");
                       graph.addEdge("B","D");
                       graph.addEdge("C","E");
                       graph.addEdge("D","E");
                       graph.addEdge("D","F");
                       graph.addEdge("E","F");
                       graph.traverse();
                       startGraph(graph);
                       break;
            default:
                break;
        }
    }
}