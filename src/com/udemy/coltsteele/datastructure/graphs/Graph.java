package com.udemy.coltsteele.datastructure.graphs;

import java.util.*;

public class Graph {

    public Map<String,List<String>> adjacencyMap = new HashMap<>();
    public Map<String, Boolean> visitedNodeMapping = new HashMap<>();
    public List<String> visitedNodeList = new ArrayList<>();

    public void addVertex(String key) {
        adjacencyMap.putIfAbsent(key, new ArrayList<>());
    }

    public void addEdge(String vertex1, String vertex2) {
        if(adjacencyMap.containsKey(vertex1)) {
            List<String> list = adjacencyMap.get(vertex1);
            if(!list.contains(vertex2)) {
                list.add(vertex2);
                adjacencyMap.replace(vertex1,list);
            }
        }
        if(adjacencyMap.containsKey(vertex2)) {
            List<String> list = adjacencyMap.get(vertex2);
            if(!list.contains(vertex1)) {
                list.add(vertex1);
                adjacencyMap.replace(vertex2,list);
            }
        }
    }

    public void removeVertex(String vertex) {
        if(adjacencyMap.containsKey(vertex)) {
            adjacencyMap.remove(vertex);
        }
        adjacencyMap.forEach( (key, value) -> {
            List<String> list = adjacencyMap.get(key);
            if(list.contains(vertex)) {
                list.remove(vertex);
            }
        });
    }

    public void removeEdge(String vertex1, String vertex2) {
        if(adjacencyMap.containsKey(vertex1)) {
            List<String> list = adjacencyMap.get(vertex1);
            if(list.contains(vertex2)) {
                list.remove(vertex2);
            }
        }
        if(adjacencyMap.containsKey(vertex2)) {
            List<String> list = adjacencyMap.get(vertex2);
            if(list.contains(vertex1)) {
                list.remove(vertex1);
            }
        }
    }

    public void recursiveDFS(String vertex) {
        if(!visitedNodeList.contains(vertex))
            visitedNodeList.add(vertex);
        if(!visitedNodeMapping.containsKey(vertex)) {
            visitedNodeMapping.put(vertex,true);
        }
        if(adjacencyMap.containsKey(vertex)) {
            List<String> list = adjacencyMap.get(vertex);
            if(!list.isEmpty()) {
                list.forEach( node -> {
                    if(!visitedNodeMapping.containsKey(node)) {
                        visitedNodeMapping.put(node,true);
                        recursiveDFS(node);
                    }
                });
            }
        }
    }

    public void iterativeDFS(String vertex) {
        if(!visitedNodeList.contains(vertex))
            visitedNodeList.add(vertex);
        visitedNodeMapping.put(vertex,true);
        adjacencyMap.forEach( (key, value) -> {
            if(!value.isEmpty()) {
                value.forEach( node -> {
                    visitedNodeMapping.put(node,true);
                    if(!visitedNodeList.contains(node))
                        visitedNodeList.add(node);
                });
            }
        });
    }

    public void breadthFirstSearchBFS(String vertex) {
        Queue<String> queue = new LinkedList<>();
        queue.add(vertex);
        String currentVertex;
        if(!visitedNodeMapping.containsKey(vertex))
            visitedNodeMapping.put(vertex,true);
        while (!queue.isEmpty()) {
             currentVertex = queue.poll();
             visitedNodeList.add(currentVertex);
             adjacencyMap.forEach( (key, value) -> {
                if(!value.isEmpty()) {
                    value.forEach( node -> {
                        if(!visitedNodeMapping.containsKey(node)) {
                            visitedNodeMapping.put(node,true);
                            queue.add(node);
                        }
                    });
                }
            });
        }
    }

    public void traverse() {
        System.out.println();
        adjacencyMap.forEach( (key, value) -> {
            System.out.println(key+": "+value);
        });
    }
}
