package com.udemy.coltsteele.datastructure.stack;

import java.util.Scanner;

public class StackMainMethod {

    public static void main(String args[]) {
        Stack stack =  new Stack();
        startStack(stack);
    }

    public static void startStack(Stack stack) {
        System.out.println();
        System.out.println("Select the operation need to be perform.");
        System.out.println("1. Push Method       2. Pop Method");
        System.out.println();
        Scanner operationType = new Scanner(System.in);
        Scanner userInput = new Scanner(System.in);
        int operationNumber = operationType.nextInt();
        switch (operationNumber) {

            case 1 :   System.out.println("Enter data for Push Method");
                       System.out.println(stack.push(userInput.nextLine()));
                       stack.traverse();
                       startStack(stack);
                       break;

            case 2:    System.out.println(stack.pop());
                       stack.traverse();
                       startStack(stack);
                       break;

            default:   stack.traverse();
                       startStack(stack);
                       break;
        }
    }

}
