package com.udemy.coltsteele.datastructure.stack;


import com.udemy.coltsteele.datastructure.linkedList.singly.Node;

public class Stack {

    public Node first;
    public Node last;
    public int size;

    //Time Complexity : O (1) we are using addFirst() of Singly lInked List
    public int push(Object value){
        Node newNode = new Node(value);
        if(this.first == null ){
            this.first = newNode;
            this.last = newNode;
        } else {
            Node oldFirst = this.first;
            newNode.next = oldFirst;
            this.first = newNode;
        }
        this.size++;
        return this.size;
    }

    //Time Complexity : O (1) we are using removeFirst() of Singly lInked List
    public Object pop() {
        if(this.first == null)
            return null;
        if(this.first == this.last) {
            this.last = null;
        }
        Node removeFirst = this.first;
        this.first = this.first.next;
        this.size--;
        return removeFirst.value;
    }

    public void traverse() {
        Node currentNode = this.first;
        if(currentNode == null) {
            System.out.println("No Data Found!!");
        }
        while(currentNode != null) {
            System.out.println(currentNode.toString());
            currentNode = currentNode.next;
        }
    }

    @Override
    public String toString() {
        return "Stack{" +
                "first=" + first +
                ", last=" + last +
                ", size=" + size +
                '}';
    }
}
