package com.udemy.coltsteele.datastructure.linkedList.singly;

public class SinglyLinkedList {

    public Node head;
    public Node tail;
    public int length;

    //Shifting
    public Object removeFirst() {
       if(this.head == null ) {
           return null;
       } else {
           Node temp = this.head;
           this.head = this.head.next;
           length--;
           if(this.length == 0) {
               this.tail = null;
           }
           return temp.value;
       }
    }

    //Unshifting
    public void addFirst(Object value) {
        Node newNode = new Node(value);
        if(this.head == null) {
            this.head = newNode;
            this.tail = this.head;
        } else {
            newNode.next = this.head;
            this.head = newNode;
            length++;
        }
    }

    public void push(Object argument) {
        Node newNode = new Node(argument);
        if(this.head == null) {
           this.head = newNode;
           this.tail = this.head;
        } else {
            this.tail.next =  newNode;
            this.tail = newNode; // Update tail position
        }
        this.length ++;
    }

    public Object pop() {
        if(this.head == null) {
            System.out.println("Both Head and Tail are null");
            return null;
        } else {
            Node currentNode = this.head;
            Node newTail = currentNode;
            while(currentNode.next != null) {
                newTail = currentNode;
                currentNode = currentNode.next;
            }
            this.tail = newTail;
            this.tail.next = null;
            length--;
            if(this.length == 0) {
                this.head = null;
                this.tail = null;
            }
            return currentNode.value;
        }
    }

    public Object get(int index) {
       return this.getCurrentNode(index).value;
    }

    public Node getCurrentNode(int index) {
        int counter = 0;
        Node currentNode = this.head;
        if(index <= -1 || index > this.length - 1 ) {
            return null;
        } else {
            while(counter != index) {
                currentNode = currentNode.next;
                counter++;
            }
            return currentNode;
        }
    }

    //Time Complexity : O (1)
    public boolean insert(int index, Object value){
        if(index < 0  || index > this.length) {
            return false;
        } else if(index == this.length) {
           this.push(value);
        } else if(index == 0) {
            this.addFirst(value);
        } else {
            Node previousNode = this.getCurrentNode(index-1);
            Node temp = previousNode.next;
            Node newNode = new Node(value);
            previousNode.next = newNode;
            newNode.next = temp;
        }
        this.length++;
        return true;
    }

    //Time Complexity : O (1)
    public boolean set(int index, Object value) {
        Node currentNode = this.getCurrentNode(index);
        if(currentNode == null) {
            return false;
        } else {
            currentNode.value = value;
            return true;
        }
    }

    //Time Complexity : O (n)
    public Object remove(int index) {
        if(index < 0  || index > this.length) {
            return null;
        } else if(index == this.length-1) {
          return this.pop();
        } else if(index == 0) {
           return this.removeFirst();
        } else {
            Node previousNode = this.getCurrentNode(index-1);
            Node currentNode = previousNode.next;
            previousNode.next = currentNode.next;
            length--;
            return currentNode.value;
        }
    }

    public void reverse() {
        // Swap Head and Tail
        Node node = this.head;
        this.head = this.tail;
        this.tail = node;

        Node nextNode, prevNode = null;
        for(int i=0; i < this.length ; i++){
            nextNode  = node.next;
            node.next = prevNode;
            prevNode  = node;
            node      = nextNode;
        }
    }

    public void traverse() {
        Node currentNode = this.head;
        if(currentNode == null) {
            System.out.println("No Data Found!!");
        }
        while(currentNode != null) {
            System.out.println(currentNode);
           currentNode = currentNode.next;
        }
    }

    @Override
    public String toString() {
        return "SinglyLinkedList{" +
                "head=" + head +
                ", tail=" + tail +
                ", length=" + length +
                '}';
    }
}
