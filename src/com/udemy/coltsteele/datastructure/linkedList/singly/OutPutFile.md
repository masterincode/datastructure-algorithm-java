##  PUSH Method

- Node{value='Balmukund', next=Node{value='Virat Kohli', next=Node{value='MS Dhoni', next=Node{value='Sachin', next=null}}}}
- Node{value='Virat Kohli', next=Node{value='MS Dhoni', next=Node{value='Sachin', next=null}}}
- Node{value='MS Dhoni', next=Node{value='Sachin', next=null}}
- Node{value='Sachin', next=null}

## Traverse

- Node{value='Balmukund', next=Node{value='Virat Kohli', next=Node{value='MS Dhoni', next=Node{value='Sachin', next=null}}}}
- Node{value='Virat Kohli', next=Node{value='MS Dhoni', next=Node{value='Sachin', next=null}}}
- Node{value='MS Dhoni', next=Node{value='Sachin', next=null}}
- Node{value='Sachin', next=null}

## POP Method
- POP Method :: Sachin
- POP Method :: MS Dhoni
- POP Method :: Virat Kohli
- POP Method :: Balmukund
- POP Method :: null
## Traverse After POP Method
- SinglyLinkedList{head=null, tail=null, length=0}

## Add First
- Node{value='Khushi', next=Node{value='Virat Kohli', next=Node{value='MS Dhoni', next=Node{value='Sachin', next=null}}}}
- Node{value='Virat Kohli', next=Node{value='MS Dhoni', next=Node{value='Sachin', next=null}}}
- Node{value='MS Dhoni', next=Node{value='Sachin', next=null}}
- Node{value='Sachin', next=null}

## Get Method
- Get Method::Khushi
- Get Method::MS Dhoni
- Get Method::null