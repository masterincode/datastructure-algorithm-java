package com.udemy.coltsteele.datastructure.linkedList.singly;

public class Node {

    public Object value;
    public Node next;
    public Node(Object value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Node{" +
                "value='" + value + '\'' +
                ", next=" + next +
                '}';
    }

//    Node first = new Node("My");
//    first.next = new Node("Name");
//    first.next.next = new Node("is");
//    first.next.next.next = new Node("BMS");
}
