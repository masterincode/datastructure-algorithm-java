package com.udemy.coltsteele.datastructure.linkedList.singly;
import java.util.Scanner;

public class SinglyLinkedListMainMethod {

    public static void main(String args[]) {
        SinglyLinkedList singlyLinkedList = new SinglyLinkedList();
        startSinglyLinkedList(singlyLinkedList);
    }

    public static void startSinglyLinkedList(SinglyLinkedList singlyLinkedList) {
        System.out.println();
        System.out.println("Select the operation need to be perform.");
        System.out.println("1. Push Method           2. Pop Method     3. Add First Method");
        System.out.println("4. Remove First Method   5. Get Method     6. Set Method");
        System.out.println("7. Insert Method         8. Remove Method  9. Reverse Method  10. Traverse Method");
        System.out.println();
        Scanner operationType = new Scanner(System.in);
        Scanner userInput = new Scanner(System.in);
        int operationNumber = operationType.nextInt();
        switch (operationNumber) {

            case 1 :   System.out.println("Enter data for Push Method");
                       singlyLinkedList.push(userInput.nextLine());
                       singlyLinkedList.traverse();
                       startSinglyLinkedList(singlyLinkedList);
                       break;

            case 2 :   singlyLinkedList.pop();
                       singlyLinkedList.traverse();
                       startSinglyLinkedList(singlyLinkedList);
                       break;

            case 3 :   System.out.println("Enter data for Add First Method");
                       singlyLinkedList.addFirst(userInput.nextLine());
                       singlyLinkedList.traverse();
                       startSinglyLinkedList(singlyLinkedList);
                       break;

            case 4 :   singlyLinkedList.removeFirst();
                       singlyLinkedList.traverse();
                       startSinglyLinkedList(singlyLinkedList);
                       break;

            case 5 :    System.out.println("Enter Index for Get Method");
                        int index = userInput.nextInt();
                        System.out.println("At Index "+index+" Value is "+singlyLinkedList.get(index));
                        startSinglyLinkedList(singlyLinkedList);
                        break;

            case 6 :    System.out.println("Enter the Value for Set Method");
                        String param1 = userInput.nextLine();
                        System.out.println("Enter Index for Set Method");
                        int index1 = userInput.nextInt();
                        singlyLinkedList.set(index1,param1);
                        singlyLinkedList.traverse();
                        startSinglyLinkedList(singlyLinkedList);

            case 7 :    System.out.println("Enter the Value for Insert Method");
                        String param2 = userInput.nextLine();
                        System.out.println("Enter Index for Insert Method");
                        int index2 = userInput.nextInt();
                        singlyLinkedList.insert(index2,param2);
                        singlyLinkedList.traverse();
                        startSinglyLinkedList(singlyLinkedList);

            case 8:     System.out.println("Enter the Index to remove Element");
                        System.out.println(singlyLinkedList.remove(userInput.nextInt()));
                        singlyLinkedList.traverse();
                        startSinglyLinkedList(singlyLinkedList);

            case 9 :    singlyLinkedList.reverse();
                        singlyLinkedList.traverse();
                        startSinglyLinkedList(singlyLinkedList);

            default:    singlyLinkedList.traverse();
                        startSinglyLinkedList(singlyLinkedList);
                        break;
        }

    }


}
