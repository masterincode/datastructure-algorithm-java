package com.udemy.coltsteele.datastructure.linkedList.doubly;

import java.util.Scanner;

public class DoublyLinkedListMainMethod {

    public static void main(String args[]) {
          DoublyLinkedList doublyLinkedList = new DoublyLinkedList();
          startDoublyLinkedList(doublyLinkedList);
    }

    public static void startDoublyLinkedList(DoublyLinkedList doublyLinkedList) {
        System.out.println();
        System.out.println("Select the operation need to be perform.");
        System.out.println("1. Push Method       2. Pop Method     3. Remove First");
        System.out.println("4. Add First Method  5. Get Method     6. Set Method");
        System.out.println("7. Insert Method     8. Remove Method");
        System.out.println();
        Scanner operationType = new Scanner(System.in);
        Scanner userInput = new Scanner(System.in);
        int operationNumber = operationType.nextInt();
        switch (operationNumber) {

            case 1:     System.out.println("Enter data for Push Method");
                        doublyLinkedList.push(userInput.nextLine());
                        doublyLinkedList.traverse();
                        startDoublyLinkedList(doublyLinkedList);
                        break;

            case 2 :    doublyLinkedList.pop();
                        doublyLinkedList.traverse();
                        startDoublyLinkedList(doublyLinkedList);
                        break;

            case 3:     doublyLinkedList.removeFirst();
                        doublyLinkedList.traverse();
                        startDoublyLinkedList(doublyLinkedList);
                        break;

            case 4:     System.out.println("Enter data for Add First Method");
                        doublyLinkedList.addFirst(userInput.nextLine());
                        doublyLinkedList.traverse();
                        startDoublyLinkedList(doublyLinkedList);
                        break;

            case 5:     System.out.println("Enter index to find element");
                        System.out.println("Get ::"+doublyLinkedList.get(userInput.nextInt()));
                        doublyLinkedList.traverse();
                        startDoublyLinkedList(doublyLinkedList);
                        break;

            case 6:     System.out.println("Enter value to be replace");
                        String arg1 = userInput.nextLine();
                        System.out.println("Enter index to find element");
                        int param1  = userInput.nextInt();
                        doublyLinkedList.set(param1,arg1);
                        doublyLinkedList.traverse();
                        startDoublyLinkedList(doublyLinkedList);
                        break;

            case 7 :    System.out.println("Enter value to be insert");
                        String arg2 = userInput.nextLine();
                        System.out.println("Enter index at which value to be insert");
                        int param2  = userInput.nextInt();
                        doublyLinkedList.insert(param2,arg2);
                        doublyLinkedList.traverse();
                        startDoublyLinkedList(doublyLinkedList);
                        break;

            case 8:     System.out.println("Enter index at which value to be remove");
                        System.out.println("Remove::"+doublyLinkedList.remove(userInput.nextInt()));
                        doublyLinkedList.traverse();
                        startDoublyLinkedList(doublyLinkedList);
                        break;

            default:    doublyLinkedList.traverse();
                        startDoublyLinkedList(doublyLinkedList);
                        break;
        }
    }
}
