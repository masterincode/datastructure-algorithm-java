package com.udemy.coltsteele.datastructure.linkedList.doubly;

public class DoublyLinkedList {

    public Node head;
    public Node tail;
    public int length;

    public boolean push(Object value) {
        Node newNode = new Node(value);
        if(this.length == 0) {
            this.head = newNode;
            this.tail = newNode;
        } else {
            this.tail.next = newNode;
            newNode.previous = this.tail;
            this.tail = newNode;
        }
        this.length++;
        return true;
    }

    public Object pop() {
        Node currentTail = this.tail;
        if(this.head == null) {
            System.out.println("No data for Pop Operation !!!");
            return null;
        } else if(this.length == 1) {
            this.head = null;
            this.tail = null;
        } else {
            this.tail = currentTail.previous;
            this.tail.next = null;
        }
        this.length--;
        return currentTail.value;
    }

    //Shifting
    public Object removeFirst() {
        Node oldHead = this.head;
        if(this.head == null) {
            System.out.println("No data for Remove First Operation !!!");
            return null;
        } else if(this.length == 1) {
            this.head = null;
            this.tail = null;
        } else {
            this.head = oldHead.next;
            this.head.previous = null;
            oldHead.next = null;
        }
        this.length--;
        return oldHead.value;
    }

    //Unshifting
    public boolean addFirst(Object value) {
        Node newNode =  new Node(value);
        if(this.length == 0) {
            this.head = newNode;
            this.tail = newNode;
        } else {
            this.head.previous = newNode;
            newNode.next = this.head;
            this.head = newNode;
        }
        this.length++;
        return true;
    }

    //Time Complexity : O (N)
    public Object get(int index) {
        return this.getCurrentNode(index).value;
    }

    public boolean set(int index, Object value) {
        Node currentNode = this.getCurrentNode(index);
        if(currentNode == null) {
            return false;
        } else {
            currentNode.value = value;
            return true;
        }
    }


    public Node getCurrentNode(int index) {
        if (index < 0 || index >= this.length) {
            return null;
        } else {
            int counter = 0;
            Node currentHead = this.head;
            Node currentTail = this.tail;
            if (index <= this.length / 2) {
                while(counter != index) {
                    currentHead = currentHead.next;
                    counter++;
                }
                System.out.println("--Got from first half--");
                return currentHead;
            } else {
                counter = this.length - 1;
                while(counter != index) {
                    currentTail = currentTail.previous;
                    counter--;
                }
                System.out.println("--Got from Second half--");
                return currentTail;
            }
        }
    }

    //Time Complexity : O (1)
    public boolean insert(int index,Object value) {
        Node newNode = new Node(value);
        Node beforeNode = this.getCurrentNode(index-1);
        Node afterNode = this.getCurrentNode(index);
        if(index < 0 || index > this.length) {
            return false;
        } else if(index == 0) {
            return this.addFirst(value);
        } else if(index == this.length) {
           return this.push(value);
        } else {
            beforeNode.next = newNode;
            newNode.previous = beforeNode;
            newNode.next = afterNode;
            afterNode.previous = newNode;
            this.length++;
            return true;
        }
    }

    //Time Complexity : O (1)
    public Object remove(int index) {
        Node deleteNode = this.getCurrentNode(index);
        if(index < 0 || index >= this.length) {
            return null;
        } else if(index == 0){
           return this.removeFirst();
        } else if(index == this.length-1) {
            return this.pop();
        } else {
            Node beforeNode = deleteNode.previous;
            Node afterNode  = deleteNode.next;
            beforeNode.next = afterNode;
            afterNode.previous = beforeNode;
            deleteNode.next = null;
            deleteNode.previous = null;
        }
        this.length--;
        return deleteNode.value;
    }

    public void traverse() {
        Node currentNode = this.head;
        if(currentNode == null) {
            System.out.println("No Data Found!!");
        }
        while(currentNode != null) {
            System.out.println(currentNode.toString());
            currentNode = currentNode.next;
        }
    }

    @Override
    public String toString() {
        return "DoublyLinkedList{" +
                "head=" + head +
                ", tail=" + tail +
                ", length=" + length +
                '}';
    }
}
