package com.udemy.coltsteele.datastructure.linkedList.doubly;

public class Node {

    public Object value;
    public Node next;
    public Node previous;

    public Node(Object value) {
        this.value = value;
    }
}

