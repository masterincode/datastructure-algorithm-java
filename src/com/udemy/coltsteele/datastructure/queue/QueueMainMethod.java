package com.udemy.coltsteele.datastructure.queue;

import java.util.Scanner;

public class QueueMainMethod {

    public static void main(String args[]) {
        Queue queue =  new Queue();
        startQueue(queue);
    }

    public static void startQueue(Queue queue) {
        System.out.println();
        System.out.println("Select the operation need to be perform.");
        System.out.println("1. Enqueue Method       2. Dequeue Method");
        System.out.println();
        Scanner operationType = new Scanner(System.in);
        Scanner userInput = new Scanner(System.in);
        int operationNumber = operationType.nextInt();
        switch (operationNumber) {

            case 1 :    System.out.println("Enter data for Enqueue Method");
                        System.out.println(queue.enqueue(userInput.nextLine()));
                        queue.traverse();
                        startQueue(queue);
                        break;

            case 2:     System.out.println(queue.dequeue());
                        queue.traverse();
                        startQueue(queue);
                        break;

            default:    queue.traverse();
                        startQueue(queue);
                        break;
        }
    }
}
