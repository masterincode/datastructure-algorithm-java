package com.udemy.coltsteele.algorithms.dijkstra;

import java.util.LinkedList;

public class PriorityQueue {

    public LinkedList<Node> values = new LinkedList<>();

    public void enqueue(String val, String priority) {
        this.values.push(new Node(val,priority));
        this.values.sort(( node1, node2) -> {
               int first  = (int) node1.val.charAt(0);
               int second = (int) node2.val.charAt(0);
               if(first > second)
                   return 1;
               else if (first < second)
                   return -1;
               else
                   return 0;
        });
    }

    public String dequeue() {
        return this.values.pop().val;
    }

    @Override
    public String toString() {
        return "PriorityQueue{" +
                "values=" + values +
                "}" + '\n';
    }
}
