package com.udemy.coltsteele.algorithms.dijkstra;

import java.util.Scanner;

public class DijkstraAlgoMainMethod {

    public static void main(String[] args) {
        WeightedGraph weightedGraph =  new WeightedGraph();
        startDijkstraAlgorithm(weightedGraph);
    }

    public static void startDijkstraAlgorithm(WeightedGraph weightedGraph) {
        System.out.println();
        System.out.println("Select the operation need to be perform.");
        System.out.println("1. Dijkstra Method           2.  Dijkstra New Logic                3. ");
        System.out.println("11. Auto Insert Add Vertex   12. Auto Insert Add Edge");
        System.out.println();
        Scanner operationType = new Scanner(System.in);
        Scanner userInput = new Scanner(System.in);
        int operationNumber = operationType.nextInt();
        switch (operationNumber) {

            case 1 :
//                        System.out.println("Enter the starting vertex.");
//                        String start = userInput.nextLine();
//                        System.out.println("Enter the finishing vertex.");
//                        String finish = userInput.nextLine();
                        weightedGraph.dijkstra("A","B");
                        startDijkstraAlgorithm(weightedGraph);
                        break;

            case 2:     weightedGraph.dijkstraNewLogic("A","E");
                        startDijkstraAlgorithm(weightedGraph);
                        break;

            case 11 :   weightedGraph.addVertex("A");
                        weightedGraph.addVertex("B");
                        weightedGraph.addVertex("C");
                        weightedGraph.addVertex("D");
                        weightedGraph.addVertex("E");
                        weightedGraph.addVertex("F");
                        weightedGraph.traverse();
                        startDijkstraAlgorithm(weightedGraph);
                        break;

            case 12 :   weightedGraph.addEdge("A","B",4);
                        weightedGraph.addEdge("A","C",2);
                        weightedGraph.addEdge("B","E",3);
                        weightedGraph.addEdge("C","D",2);
                        weightedGraph.addEdge("C","F",4);
                        weightedGraph.addEdge("D","E",3);
                        weightedGraph.addEdge("D","F",1);
                        weightedGraph.addEdge("E","F",1);
                        weightedGraph.traverse();
                        startDijkstraAlgorithm(weightedGraph);
                        break;

            default:    weightedGraph.traverse();
                        startDijkstraAlgorithm(weightedGraph);
                        break;
         }
        }
}
