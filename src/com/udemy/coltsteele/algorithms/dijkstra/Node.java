package com.udemy.coltsteele.algorithms.dijkstra;

public class Node {
    public String val;
    public String priority;

    public Node(String val, String priority) {
        this.val = val;
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "Node{" +
                "val='" + val + '\'' +
                ", priority=" + priority +
                '}';
    }
}
