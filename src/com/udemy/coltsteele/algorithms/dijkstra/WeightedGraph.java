package com.udemy.coltsteele.algorithms.dijkstra;

import java.util.*;

public class WeightedGraph {

    public Map<String, List<Vertex>> adjacencyList = new HashMap<>();

    public void addVertex(String vertex) {
         if(!adjacencyList.containsKey(vertex))
             adjacencyList.put(vertex, new ArrayList<>());
    }

    public void addEdge(String vertex1,String vertex2, int weight) {
        List<Vertex> vertexlist;
        if(adjacencyList.containsKey(vertex1)) {
            vertexlist = adjacencyList.get(vertex1);
            vertexlist.add(new Vertex(vertex2,weight));
            adjacencyList.put(vertex1, vertexlist);
        }
        if(adjacencyList.containsKey(vertex2)) {
            vertexlist = adjacencyList.get(vertex2);
            vertexlist.add(new Vertex(vertex1,weight));
            adjacencyList.put(vertex2, vertexlist);
        }
    }

    public void dijkstraNewLogic(String start, String finish) {

        Map<String, Double> distance = new HashMap<>();
        Map<String, Boolean> visited = new HashMap<>();
        Queue<String> unVisited = new LinkedList<>();

        adjacencyList.forEach( (key, value) -> {
            unVisited.add(key);
            visited.put(key,false);
            if(key.equalsIgnoreCase("A"))
                distance.put(key,0.0);
            else
               distance.put(key,Double.POSITIVE_INFINITY);
        });
        System.out.println(distance);
//        System.out.println(visited);
//        System.out.println(unVisited);

        while(!unVisited.isEmpty()) {
            String currentVisited = unVisited.poll();
            System.out.println("*************Current Node : "+currentVisited+" *************************"+visited);
            visited.put(currentVisited, true);
            double minWeight;
            for(Vertex neighbourNode : adjacencyList.get(currentVisited)) {
//                System.out.println("Visted till Now::"+visited);
                if(!visited.get(neighbourNode.node)) {
                    System.out.println("Current Node : "+currentVisited+ " Value : "+ distance.get(currentVisited)+"  NeighbourNode ------>"+neighbourNode.node+ "  Value : "+neighbourNode.weight);
                    minWeight = Math.min((double) distance.get(neighbourNode.node),distance.get(currentVisited) + neighbourNode.weight);
                    distance.put(neighbourNode.node, minWeight);
                    System.out.println(distance);
                }
                System.out.println("-------------------------------------------------------------------------------------");
            }
        }
    }

    public void dijkstra(String start, String finish) {
        PriorityQueue nodes = new PriorityQueue();
        Map<String,Integer> distances = new HashMap<>();
        Map<String,String> previous = new HashMap<>();
        String smallest;
        //Preparing Initial state
        adjacencyList.forEach( (vertex, vertexList) -> {
            if(vertex.equalsIgnoreCase(start)) {
                distances.put(vertex,0);
                nodes.enqueue(vertex, "0");
            } else {
                distances.put(vertex, -1);
                nodes.enqueue(vertex, "Infinity");
            }
            previous.put(vertex, null);
        });
        System.out.println(distances);
        System.out.println(nodes.toString());
        while(nodes.values.size() != 0) {
            smallest = nodes.dequeue();
//            System.out.println("Smallest::"+smallest);
            if(smallest.equalsIgnoreCase(finish)) {
//                System.out.println("Distance : "+distances);
//                System.out.println("Previous : "+previous);
            }
            if(smallest != null || !distances.get(smallest).toString().equalsIgnoreCase("Infinity")) {
                int candidate;
                String nextNeighbour;
                int neighbourIndex = 0;
                for(Vertex neighbour : adjacencyList.get(smallest)) {
//                    System.out.println("neighbourIndex ->"+neighbourIndex);
                    nextNeighbour = neighbour.node;
//                    System.out.println("neighbour ->"+neighbour);
                    try {
                    // Calculate new distance to neighbour node
                    if(distances.get(smallest).toString().equalsIgnoreCase("Infinity"))
                       candidate = neighbour.weight;
//                    else
//                        candidate = Integer.parseInt(distances.get(smallest).toString()) + neighbour.weight;
//
//                    if(distances.get(nextNeighbour).toString().equalsIgnoreCase("Infinity"))
//                        nextNeighbour = 0;
//                    else
//                        nextNeighbour = Integer.parseInt(distances.get(nextNeighbour).toString());
//
//                        if(candidate < distances.get(nextNeighbour)) {
//                            //updating new smallest distance to neighbour
//                            distances.put(nextNode, candidate);
//                            //updating previous - How we got to neighbour
//                            previous.put(nextNode,smallest);
//                            //enqueue in priority queue with new priority
//                            nodes.enqueue(nextNode,String.valueOf(candidate));
//                        }
                    } catch(Exception e) {
                        System.out.println("Exception -> "+e.getMessage());
                    }
                    neighbourIndex++;
                }
            }
        }
    }

    public void traverse() {
        if(!adjacencyList.isEmpty()) {
            adjacencyList.forEach( (key,value) -> System.out.println(key+" : "+value));
        }
    }

    @Override
    public String toString() {
        return "WeightedGraph{" +
                "adjacencyList=" + adjacencyList +
                '}';
    }
}
