package com.udemy.coltsteele.algorithms.dijkstra;

public class Vertex {

    public String node;
    public int weight;

    public Vertex(String node, int weight) {
        this.node = node;
        this.weight = weight;
    }

    public Vertex() {
        super();
    }

    @Override
    public String toString() {
        return "Vertex{" +
                "node='" + node + '\'' +
                ", weight=" + weight +
                '}';
    }
}
