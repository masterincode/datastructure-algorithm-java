package com.geeksForGeeks.algorithms.dijkstra;

import java.util.*;

public class DijkstraAlgorithm {

    //Drvier Code written here
    public static void main(String[] args) {

        int V = 5;
        int source = 0;

        // Adjacency list representation of the
        // connected edges
        List<List<Node>> adjacent = new ArrayList<>();

        // Initialize list for every node
        for (int i = 0; i < V; i++) {
            adjacent.add(new ArrayList<>());
        }
        // Inputs for the DPQ graph
        adjacent.get(0).add(new Node(1, 9));
        adjacent.get(0).add(new Node(2, 6));
        adjacent.get(0).add(new Node(3, 5));
        adjacent.get(0).add(new Node(4, 3));

        adjacent.get(2).add(new Node(1, 2));
        adjacent.get(2).add(new Node(3, 4));

        // Calculate the single source shortest path
        DijkstrasAlgorithmUsingPriorityQueue dpq = new DijkstrasAlgorithmUsingPriorityQueue(V);
        dpq.dijkstra(adjacent, source);

        // Print the shortest path to all the nodes
        // from the source node
        System.out.println("The shorted path from node :");
        for (int i = 0; i < dpq.distance.length; i++)
            System.out.println(source + " to " + i + " is "
                    + dpq.distance[i]);

    }
}
