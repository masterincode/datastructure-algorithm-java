package com.geeksForGeeks.algorithms.dijkstra;

import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;

public class DijkstrasAlgorithmUsingPriorityQueue {

    public int distance[];
    private Set<Integer> settled;
    private PriorityQueue<Node> priorityQueue;
    private int numberOfVertex; // Number of vertices
    List<List<Node>> adjacent;

    //constructor
    public DijkstrasAlgorithmUsingPriorityQueue(int numberOfVertex)
    {
        this.numberOfVertex = numberOfVertex;
        distance = new int[numberOfVertex];
        settled = new HashSet<Integer>();
        priorityQueue = new PriorityQueue<Node>(numberOfVertex, new Node());
    }

    // Function for Dijkstra's Algorithm
    public void dijkstra(List<List<Node>> adjacent, int src)
    {
        this.adjacent = adjacent;
        for (int i = 0; i < numberOfVertex; i++)
            distance[i] = Integer.MAX_VALUE;

        // Add source node to the priority queue
        priorityQueue.add(new Node(src, 0));

        // Distance to the source is 0
        distance[src] = 0;
        while (settled.size() != numberOfVertex) {

            // remove the minimum distance node
            // from the priority queue
            int u = priorityQueue.remove().node;

            // adding the node whose distance is
            // finalized
            settled.add(u);
            e_Neighbours(u);
        }
    }

    // Function to process all the neighbours
    // of the passed node
    private void e_Neighbours(int u)
    {
        int edgeDistance = -1;
        int newDistance = -1;

        // All the neighbors of v
        for (int i = 0; i < adjacent.get(u).size(); i++) {
            Node v = adjacent.get(u).get(i);

            // If current node hasn't already been processed
            if (!settled.contains(v.node)) {
                edgeDistance = v.cost;
                newDistance = distance[u] + edgeDistance;

                // If new distance is cheaper in cost
                if (newDistance < distance[v.node])
                    distance[v.node] = newDistance;

                // Add the current node to the queue
                priorityQueue.add(new Node(v.node, distance[v.node]));
            }
        }
    }
}
