package com.geeksForGeeks.problems.linkedlist;

public class Node {

    public int data;
    public Node next;

    public Node(int d) {
        data = d;
        next = null;
    }

    @Override
    public String toString() {
        return "Node{" +
                "data=" + data +
                ", next=" + next +
                '}';
    }
}
