package com.geeksForGeeks.problems.linkedlist.medium;

import com.geeksForGeeks.problems.linkedlist.Node;

public class GivenLinkedListOf0s1sAnd2sSortIt {

    // This is only applicable when you have 0s, 1s and 2s
    public Node segregate(Node head)
    {
        if(head == null) return head;

        Node zeroHead = new Node(-1);
        Node zeroTail = zeroHead;

        Node oneHead = new Node(-1);
        Node oneTail = oneHead;

        Node twoHead = new Node(-1);
        Node twoTail = twoHead;

        Node current = head;
        Node result = null;

        while(current != null) {
            int data = current.data;
            switch(data) {
                case 0:zeroTail.next = new Node(data);
                    zeroTail = zeroTail.next;
                    break;
                case 1:oneTail.next = new Node(data);
                    oneTail = oneTail.next;
                    break;
                case 2:twoTail.next = new Node(data);
                    twoTail = twoTail.next;
                    break;
                default: break;
            }
            current = current.next;
        }
        // After putting this Validation all these condition can work
        /* 0s 1s 2s
           0s 1s
           0s 2s
           1s 2s
        * */
        if(zeroHead.next != null) {
            result = zeroHead.next;
        }
        if(oneHead.next != null) {
            zeroTail.next = oneHead.next;
        }
        if(oneHead.next == null) {
            zeroTail.next = twoHead.next;
        }
        if(twoHead.next != null) {
            oneTail.next = twoHead.next;
        }
        return result;
    }
}
