package com.geeksForGeeks.problems.linkedlist.medium;

import com.geeksForGeeks.problems.linkedlist.Node;

public class ReverseLinkedListInGroupsOfGivenSize {

    //https://www.geeksforgeeks.org/reverse-a-list-in-groups-of-given-size/
    public Node reverse(Node node, int k)
    {
        if(node == null) return null;
        Node current = node;
        Node newHead = null;
        Node prev = null;
        Node next = null;
        int count = 0;
        while (count < k && current != null)
        {
            next = current.next;
            current.next = prev;
            prev = current;
            current = next;
            count++;
        }
        if (next != null)
            node.next = reverse(next, k);
        return prev;
    }
}