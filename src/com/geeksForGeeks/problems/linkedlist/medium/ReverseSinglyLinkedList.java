package com.geeksForGeeks.problems.linkedlist.medium;

import com.geeksForGeeks.problems.linkedlist.Node;

public class ReverseSinglyLinkedList {

    public static void main(String[] args) {
        Node first = new Node(10);
        first.next = new Node(20);
        first.next.next = new Node(30);
        first.next.next.next = new Node(40);
        first.next.next.next.next = new Node(50);

        System.out.println(first);
        System.out.println("After Reverse !!!");
        System.out.println(reverseLinkedList(first));
    }

    //Explanation video https://www.youtube.com/watch?v=jY-EUKXYT20
    public static Node reverseLinkedList(Node head) {
        if(head == null) return null;
        Node current = head;
        Node previous = null;
        Node next = null;
        while(current != null) {
            next = current.next;
            current.next = previous;
            previous = current;
            current = next;
        }
        return previous;
    }
}
