package com.geeksForGeeks.problems.linkedlist.medium;

import com.geeksForGeeks.problems.linkedlist.Node;

import java.util.HashMap;
import java.util.Map;

/* Reference URLs
* https://www.geeksforgeeks.org/detect-and-remove-loop-in-a-linked-list/
* https://www.codesdope.com/blog/article/detect-and-remove-loop-in-a-linked-list/
* */
public class RemoveLoopFromLinkedList {

    public static void main(String[] args) {
        Node first = new Node(10);
        first.next = new Node(20);
        first.next.next = new Node(30);
        first.next.next.next = first;
        System.out.println(removeLoopUsingHashing(first));
    }

    //Floyd’s Cycle detection algorithm.
    //Method 2 (Better Solution)
    public static void removeLoopGFG(Node node){
        Node slow = node, fast = node;
        while (slow != null && fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
            if (slow == fast) {
                removeLoop(slow, node);
            }
        }
    }

    public static void removeLoop(Node loop, Node head)
    {
        Node ptr1 = loop;
        Node ptr2 = loop;

        // Count the number of nodes in loop
        int k = 1, i;
        while (ptr1.next != ptr2) {
            ptr1 = ptr1.next;
            k++;
        }
        // Fix one pointer to head
        ptr1 = head;
        // And the other pointer to k nodes after head
        ptr2 = head;
        for (i = 0; i < k; i++) {
            ptr2 = ptr2.next;
        }

        /*  Move both pointers at the same pace,
         they will meet at loop starting node */
        while (ptr2 != ptr1) {
            ptr1 = ptr1.next;
            ptr2 = ptr2.next;
        }

        // Get pointer to the last node
        while (ptr2.next != ptr1) {
            ptr2 = ptr2.next;
        }

        /* Set the next node of the loop ending node
         to fix the loop */
        ptr2.next = null;
    }

    //My Logic To detect Loop and remove Loop
    public static Node removeLoopUsingHashing(Node head){
        Node fast = head;
        Node slow = null;
        Map<Integer,Integer> map = new HashMap<>();
        while(fast.next != null) {
            if(map.get(fast.data) == null) {
                map.put(fast.data, 1);
            } else {
                slow.next = null;
                break;
            }
            slow = fast;
            fast = fast.next;
        }
        return head;
    }
}
