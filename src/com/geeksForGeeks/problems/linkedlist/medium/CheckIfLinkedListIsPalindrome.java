package com.geeksForGeeks.problems.linkedlist.medium;

import com.geeksForGeeks.problems.linkedlist.Node;

import java.util.Stack;

public class CheckIfLinkedListIsPalindrome {

    public boolean isPalindrome(Node head)
    {
        Node slow = head;
        boolean ispalin = true;
        Stack<Integer> stack = new Stack<>();
        while (slow != null) {
            stack.push(slow.data);
            slow = slow.next;
        }
        while (head != null) {
            int i = stack.pop();
            if (head.data == i) {
                ispalin = true;
            }
            else {
                ispalin = false;
                break;
            }
            head = head.next;
        }
        return ispalin;
    }
}
