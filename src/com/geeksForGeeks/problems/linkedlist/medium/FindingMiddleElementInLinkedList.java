package com.geeksForGeeks.problems.linkedlist.medium;

import com.geeksForGeeks.problems.linkedlist.Node;

public class FindingMiddleElementInLinkedList {

//  Brute Force Approach
    public int getMiddleBruteForceApproach(Node head)
    {
        if(head == null) return 0;
        Node newHead = head;
        int counter = 0;
        int result = 0;
        while(head != null) {
            head = head.next;
            counter++;
        }
        for(int i=1; i <= (counter/2+1) ;i++) {
            result = newHead.data;
            newHead = newHead.next;
        }
        return result;
    }

    //Optimised Approach
    public int getMiddleOptimisedApproach(Node head)
    {
        if(head == null) return 0;
        Node slow = head;
        Node fast = head.next;
        while(fast != null) {
            fast = fast.next;
            if(fast != null) {
                fast = fast.next;
            }
            slow = slow.next;
        }
        return slow.data;
    }
}