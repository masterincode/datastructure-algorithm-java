package com.geeksForGeeks.problems.linkedlist.medium;

import com.geeksForGeeks.problems.linkedlist.Node;

public class AddTwoNumbersRepresentedByLinkedLists {

    static Node addTwoLists(Node first, Node second) {
        first = reverse(first);       // reversing lists
        second = reverse(second);     // to simplify addition
        Node sum = null;
        int carry = 0;
        while( first!=null || second!=null || carry>0 )
        {
            int newVal = carry;
            if(first!=null) newVal+=first.data;
            if(second!=null) newVal+=second.data;
            // newly value for sumList is sum of carry and respective
            // digits in the 2 lists
            carry = newVal / 10;        // updating carry
            newVal = newVal % 10;       // making sure newVal is <10
            Node newNode = new Node(newVal);
            newNode.next = sum;
            sum = newNode;
            if(first!=null) first = first.next;       // moving to next node
            if(second!=null) second = second.next;
        }
        return sum;
    }

    static Node reverse(Node head) {
        if(head == null)
            return head;
        Node current = head;
        Node prev = null;
        Node next = null;
        while(current != null) {
            next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        return prev;
    }
}