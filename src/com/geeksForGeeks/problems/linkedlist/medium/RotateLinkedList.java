package com.geeksForGeeks.problems.linkedlist.medium;

import com.geeksForGeeks.problems.linkedlist.Node;

public class RotateLinkedList {

//    Expected Time Complexity: O(N).
//    Expected Auxiliary Space: O(1).
//    Implementation URL https://www.geeksforgeeks.org/rotate-a-linked-list/
    public Node rotate(Node head, int k) {
        if (k == 0)
            return head;

        Node current = head;
        int count = 1;
        while (count < k && current != null) {
            current = current.next;
            count++;
        }
        Node kthNode = current;
        while (current.next != null)
            current = current.next;

        current.next = head;
        head = kthNode.next;
        kthNode.next = null;
        return head;
    }
}