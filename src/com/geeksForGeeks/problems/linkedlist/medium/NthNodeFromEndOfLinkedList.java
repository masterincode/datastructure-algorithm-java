package com.geeksForGeeks.problems.linkedlist.medium;

import com.geeksForGeeks.problems.linkedlist.Node;

public class NthNodeFromEndOfLinkedList {

    public int getNthFromLast(Node head, int n)
    {
        if(head == null || head.next == null)
            return -1;
        if(n == 0)
            return -1;

        int counter = 0;
        Node node = head;
        while(node != null) {
            node = node.next;
            counter++;
        }
        counter = counter - n;
        if(counter < 0 )
            return -1;
        int count = 1;
        while(head.next != null && count <= counter) {
            head = head.next;
            count++;
        }
        return head.data;
    }
}
