package com.geeksForGeeks.problems.linkedlist.medium;

public class FlatteningLinkedList {

    public  Node flatten(Node root)
    {
        // Base Cases
        if (root == null || root.next == null)
            return root;

        // recur for list on next
        root.next = flatten(root.next);

        // now merge
        root = merge(root, root.next);

        // return the root
        // it will be in turn merged with its left
        return root;
    }

    // An utility function to merge two sorted linked lists
    Node merge(Node a, Node b)
    {
        // if first linked list is empty then second
        // is the answer
        if (a == null)     return b;

        // if second linked list is empty then first
        // is the result
        if (b == null)      return a;

        // compare the data members of the two linked lists
        // and put the larger one in the result
        Node result;

        if (a.data < b.data)
        {
            result = a;
            result.bottom =  merge(a.bottom, b);
        }

        else
        {
            result = b;
            result.bottom = merge(a, b.bottom);
        }

        result.next = null;
        return result;
    }

    class Node {
        int data;
        Node next, bottom;
        Node(int data) {
            this.data = data;
            next = null;
            bottom = null;
        }
    }
}
