package com.geeksForGeeks.problems.linkedlist.easy;

import com.geeksForGeeks.problems.linkedlist.Node;

import java.util.HashMap;
import java.util.Map;

public class DetectLoopInLinkedList {

    public static void main(String[] args) {
        Node first = new Node(10);
        first.next = new Node(20);
        Node connector = first.next;
        first.next.next = new Node(30);
        first.next.next.next = new Node(40);
        first.next.next.next.next = new Node(50);
        first.next.next.next.next.next = connector;
        traverse(first);
    }

    // https://www.youtube.com/watch?v=354J83hX7RI
    // Brute force Approach using Hashing
    // Time Complexity: O(N)
    // Space Complexity: O(N)
    public boolean detectLoopBruteForceApproach(Node head){

        if(head == null) return false;
        Map<Integer,Integer> map = new HashMap<>();
        while(head != null) {
            if(map.get(head.data) == null) {
                map.put(head.data,1);
            } else {
                return true;
            }
            head = head.next;
        }
        return false;
    }

    // Optimised Approach
    // Time Complexity: O(N)
    // Space Complexity: O(1)
    public boolean detectLoopOptimisedApproach(Node head){

        if(head == null) return false;
        Node slow = head;
        Node fast = head.next;
        while(fast.next != null && fast.next.next != null) {
            slow = slow.next;
            fast = fast.next.next;
            if(fast == slow)
                return true;
        }
        return false;
    }

    public static void traverse(Node head) {
        int counter = 0;
        while (head !=null && counter <=10) {
            System.out.println(head.data);
            head = head.next;
            counter++;
        }
    }

}
