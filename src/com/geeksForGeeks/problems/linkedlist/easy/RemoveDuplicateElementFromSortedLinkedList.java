package com.geeksForGeeks.problems.linkedlist.easy;

import com.geeksForGeeks.problems.linkedlist.Node;

public class RemoveDuplicateElementFromSortedLinkedList {

    public static void main(String[] args) {
        Node first = new Node(10);
        first.next = new Node(20);
        first.next.next = new Node(20);
        first.next.next.next = new Node(40);
        first.next.next.next.next = new Node(50);

        System.out.println(removeDuplicates(first));
    }

    public static Node removeDuplicates(Node head)
    {
        // Your code here
        Node currentNode = head;
        Node nextNode = null;
        while(currentNode.next != null) {
            nextNode = currentNode.next;
            if(currentNode.data == nextNode.data)
            {
                nextNode = nextNode.next;
                currentNode.next = nextNode;
            } else {
                currentNode = currentNode.next;
            }

        }
        return head;
    }

}
