package com.geeksForGeeks.problems.linkedlist.easy;

import com.geeksForGeeks.problems.linkedlist.Node;

public class DeleteWithoutHeadPointer {

    public static void main(String[] args) {
        Node first = new Node(10);
        first.next = new Node(20);
        first.next.next = new Node(30);
        first.next.next.next = new Node(40);

        Node current = first;
        while (current != null) {
            System.out.println(current);
            current = current.next;
        }
        System.out.println();
        deleteNode(first.next);
        System.out.println();
        while (first != null) {
            System.out.println(first);
            first = first.next;
        }
    }

    public static void deleteNode(Node deleteNode)
    {
        Node nextNode = deleteNode.next;
        deleteNode.data = nextNode.data;
        deleteNode.next = nextNode.next;
    }
}