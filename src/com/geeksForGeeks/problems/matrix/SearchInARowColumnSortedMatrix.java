package com.geeksForGeeks.problems.matrix;

public class SearchInARowColumnSortedMatrix {

    public static void main(String[] args) {
        int row = 3; int column = 3; int searchItem = 69;
        int[][] matrix = {{ 3, 30, 38},
                          {36, 43, 60},
                          {40, 51, 69}};
//                        i,j
        System.out.println(search(matrix,row,column,searchItem));
    }

    //Function to search a given number in row-column sorted matrix.
    public  static boolean search(int[][] matrix, int row, int column, int searchItem)
    {
        int i = row - 1, j = 0;
        while (i >= 0 && j < column)
        {
            //if given number is found, we return true.
            if (matrix[i][j] == searchItem)
                return true;

            //if current element is greater than given number, we
            //decrease i pointer else we increase j pointer.
            if (matrix[i][j] < searchItem)
                j = j + 1;  //j++;
            else
                i = i - 1;  //i--;
        }
        //if we reach here, it means given number is not
        //present in matrix so we return false.
        return false;
    }
}
