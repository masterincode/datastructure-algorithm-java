package com.geeksForGeeks.problems.arrays.medium;

public class MinimumNumberOfJumps {

    public static void main(String[] args) {
        int[] arr = {1, 4, 3, 2, 6, 7};
        int arr1[] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
        MinimumNumberOfJumps minimumNumberOfJumps = new MinimumNumberOfJumps();
        System.out.println("No Of Jumps: "+minimumNumberOfJumps.minJumpsOptimisedApproach(arr));
    }

    //BruteForceApproach  Note: Time Limit Exceeded
    //Time Complexity: O(n/2)
    //Space Complexity: O(1)
    public int minJumpsBruteForceApproach(int[] arr){
        int length = arr.length;
        if(length == 0 || length == 1)
            return 0;
        int jumpBy = 0;
        int value = 0;
        int jumpCount = 0;
        int index = 0;

        while(index < length) {
            if(index >= length-1) {
                break;
            }
            value = arr[index];
            jumpBy = index + value;
            index = jumpBy;
            jumpCount++;
        }
        return jumpCount;
    }

    //OptimisedApproach
    //Time Complexity: O(n)
    //Space Complexity: O(1)
    public int minJumpsOptimisedApproach(int[] arr) {
        if (arr.length <= 1)
            return 0;

        // Return -1 if not possible to jump
        if (arr[0] == 0)
            return -1;

        // initialization
        int maxReach = arr[0];
        int step = arr[0];
        int jump = 1;

        // Start traversing array
        for (int i = 1; i < arr.length; i++) {
            // Check if we have reached
            // the end of the array
            if (i == arr.length - 1)
                return jump;

            // updating maxReach
            maxReach = Math.max(maxReach, i + arr[i]);

            // we use a step to get to the current index
            step--;

            // If no further steps left
            if (step == 0) {
                // we must have used a jump
                jump++;

                // Check if the current index/position or lesser index is the maximum reach point
                // from the previous indexes
                if (i >= maxReach)
                    return -1;

                // re-initialize the steps to the amount of steps to reach maxReach from position i.
                step = maxReach - i;
            }
        }

        return -1;
    }
}
