package com.geeksForGeeks.problems.arrays.medium;

import com.udemy.coltsteele.dataStructure.sorting.QuickSortAlgorithm;

import java.util.Arrays;

public class KthSmallestElement {

    public static void main(String[] args) {
        int[] arr = {7, 10, 4, 3, 20, 15};
        System.out.println(kthSmallestUsingQuickSort(arr, 0, arr.length - 1, 3));
    }


    //BruteForceApproach
    //Time Complexity: O(n Log n)
    //Space Complexity: O(1)
    public static int kthSmallestBruteForceApproachUsingSorting(int[] arr, int leftIndex, int rightIndex, int k) {
        int length = arr.length;
        if (length == 0)
            return -1;
        if (length == 1 && k == 1)
            return arr[0];
        Arrays.sort(arr); //O(n Log n)
        return arr[k - 1];
    }

    //BruteForceApproach
    //Time Complexity: O(n Log n)
    //Space Complexity: O(1)
    public static int kthSmallestUsingQuickSort(int[] arr, int leftIndex, int rightIndex, int k) {
        // If k is smaller than number of elements in array
        if (k > 0 && k <= rightIndex - leftIndex + 1) {
            // Partition the array around last element and get position of pivot element in sorted array
            int pivotIndex = new QuickSortAlgorithm().partition(arr, leftIndex, rightIndex);

            // If position is same as k
            if (pivotIndex - leftIndex == k - 1)
                return arr[pivotIndex];

            // If position is more, recur for left subarray
            if (pivotIndex - leftIndex > k - 1)
                return kthSmallestUsingQuickSort(arr, leftIndex, pivotIndex - 1, k);

            // Else recur for right subarray
            return kthSmallestUsingQuickSort(arr, pivotIndex + 1, rightIndex, k - pivotIndex + leftIndex - 1);
        }
        // If k is more than number of elements in array
        return Integer.MAX_VALUE;
    }

}
