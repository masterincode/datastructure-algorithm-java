package com.geeksForGeeks.problems.arrays.medium;

public class MinimizeTheHeights {
    //https://practice.geeksforgeeks.org/problems/minimize-the-heights3351/1#
    //Note: Submit is still pending
    int getMinDiff(int[] arr, int n, int k) {
        if( n == 0 ||  n==1)
            return 0;

        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;

        for(int i=0; i < arr.length; i++) {
            if(arr[i] < 0) {
                arr[i] = arr[i] + k;
            }
            if( (arr[i]-k) <=0) {
                arr[i] = arr[i] + k;
            } else {
                arr[i] = arr[i] - k;
            }
            System.out.println("arr[i]="+arr[i]);
            min = Math.min(min, arr[i]);
            System.out.println("min="+min);
            max = Math.max(max, arr[i]);
            System.out.println("max="+max);
        }
        return max-min;
    }

}
