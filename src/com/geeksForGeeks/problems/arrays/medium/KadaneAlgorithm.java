package com.geeksForGeeks.problems.arrays.medium;

public class KadaneAlgorithm {

    public static void main(String[] args) {
        int N = 5;
        int[] Arr = {1,2,3,-2,5};
        System.out.println(maxSubarraySum(Arr, N));
    }

    //OptimisedApproach
    //Time Complexity: O(n)
    //Space Complexity: O(1)
    public static int maxSubarraySum(int[] arr, int n) {
        if (n == 0)
            return 0;
        if (n == 1)
            return arr[0];
        int sum = 0;
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < arr.length; i++) {
            sum = sum + arr[i];
            if (sum > max)
                max = sum;
            if (sum < 0)
                sum = 0;
        }
        return max;
    }

}
