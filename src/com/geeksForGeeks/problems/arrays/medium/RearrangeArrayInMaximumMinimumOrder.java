package com.geeksForGeeks.problems.arrays.medium;

public class RearrangeArrayInMaximumMinimumOrder {

    // Driver code
    public static void main(String args[]) {
        int arr[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        int n = arr.length;

        System.out.println("Original Array");
        for (int i = 0; i < n; i++)
            System.out.print(arr[i] + " ");

        rearrange(arr, n);

        System.out.print("\nModified Array\n");
        for (int i = 0; i < n; i++)
            System.out.print(arr[i] + " ");
    }

    //OptimisedApproach
    //Time Complexity: O(n)
    //Space Complexity: O(1)
    //URL: https://practice.geeksforgeeks.org/problems/-rearrange-array-alternately-1587115620/1#
    public static void rearrange(int arr[], int n) {
        // initialize index of first minimum and first
        // maximum element
        int maxIndex = n - 1, minIndex = 0;
        // store maximum element of array
        int maxElement = arr[maxIndex] + 1;
        // traverse array elements
        for (int i = 0; i < n; i++) {
            // at even index : we have to put
            // maximum element
            if (i % 2 == 0) {
                arr[i] = arr[i] + (arr[maxIndex] % maxElement) * maxElement;
                maxIndex--;
            }
            // at odd index : we have to put minimum element
            else {
                arr[i] = arr[i] + (arr[minIndex] % maxElement) * maxElement;
                minIndex++;
            }
        }
        // array elements back to it's original form
        for (int i = 0; i < n; i++)
            arr[i] = arr[i] / maxElement;
    }
}