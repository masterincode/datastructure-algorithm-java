package com.geeksForGeeks.problems.arrays.easy;

public class PeakElement {

    /*References:
    https://leetcode.com/problems/find-peak-element/
    https://practice.geeksforgeeks.org/problems/peak-element/1/?company[]=Visa&company[]=Visa&page=1&query=company[]Visapage1company[]Visa#
    * */
    public static void main(String[] args) {
        int N = 7;
        int[] arr = {1,2,1,3,5,6,4};
        System.out.println(peakElement(arr, N));
    }

    //Optmised Approach
    //Time Complexity: O(log N) Whenever there is Binary Search
    //Space Complexity: O(1)
    public static int peakElement(int[] arr,int n)
    {
        int firstIndex = 0;
        int lastIndex = arr.length-1;
        int middleIndex = (firstIndex+lastIndex) / 2;

        while(firstIndex<lastIndex) {  //Note: here we have only < [Less Than] not <= [Less Than Equals]
            if(arr[middleIndex] < arr[middleIndex+1]) //Compare Middle Element with it's Right Element
                firstIndex = middleIndex+1;
            else
                lastIndex = middleIndex;

            middleIndex = (firstIndex+lastIndex) / 2;
        }
        return firstIndex;
    }
}
