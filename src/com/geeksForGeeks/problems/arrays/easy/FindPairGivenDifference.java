package com.geeksForGeeks.problems.arrays.easy;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class FindPairGivenDifference {

    public static void main(String[] args) throws IOException {

        int[] arr1 = {5, 20, 3, 2, 5, 80};
        int n1 = 78;
        int[] arr2 = {90, 70, 20, 80, 50};
        int n2 = 45;
        System.out.println(findPairBruteForceApproach(arr1, arr1.length, n1));
        System.out.println(findPairOptimisedApproach(arr2, arr2.length, n2));
    }

    //BruteForceApproach
    //Time Complexity: O(n^2)
    //Space Complexity: O(1)
    public static boolean findPairBruteForceApproach(int[] arr, int size, int n)
    {
        if(size == 0) {
            return false;
        }
        boolean flag = false;
        int value = 0;
        for(int i = 0; i < size; i++) {
            value = arr[i] + n;
            for(int j = i+1; j < size; j++) {
                if(arr[j] == value) {
                    flag = true;
                    return flag;
                }
            }
        }
        return flag;
    }

    //Optmised Approach
    //Time Complexity: O(n)
    //Space Complexity: O(n)
    public static boolean findPairOptimisedApproach(int[] arr, int size, int n)
    {
        //Set contains() method has Time Complexity of O(1).
        Set<Integer> set =  new HashSet<>();
        for(int value: arr) {
            set.add(value);
        }
        for(int i = 0 ; i <arr.length; i++) {
            if(set.contains(arr[i]+n)) { // uses internally map.containsKey(o);
                return true;
            }
        }
        return false;
    }
}
