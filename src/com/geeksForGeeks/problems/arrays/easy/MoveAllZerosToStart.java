package com.geeksForGeeks.problems.arrays.easy;

public class MoveAllZerosToStart {

    public static void main(String[] args) {
        int[] arr1 = {1, -2, 0, 4, -3, 0, 5, 0, 0, 3, 2};
        MoveAllZerosToStart moveAllZerosToStart = new MoveAllZerosToStart();
        System.out.println("=====================BruteForceApproach======================");
        for (int v : moveAllZerosToStart.moveZeroesBruteForceApproach(arr1))
            System.out.println(v);
        System.out.println("=====================OptimisedApproach======================");
        for (int v : moveAllZerosToStart.moveZeroesOptimisedApproach(arr1))
            System.out.println(v);
    }

    //BruteForceApproach
    //Time Complexity: O(n)
    //Space Complexity: O(n)
    public int[] moveZeroesBruteForceApproach(int[] nums) {
        if (nums.length == 0)
            return nums;
        int[] newArray = new int[nums.length];
        int index = nums.length - 1;
        for (int i = nums.length - 1; i >= 0; i--) {
            if (nums[i] != 0) {
                newArray[index] = nums[i];
                index--;
            }
        }
        return newArray;
    }

    //OptimisedApproach
    //Time Complexity: O(n)
    //Space Complexity: O(1)  Note: Only space complexity is improved
    public int[] moveZeroesOptimisedApproach(int[] nums) {
        if (nums.length == 0)
            return nums;
        int[] newArray = new int[nums.length];
        int index = nums.length - 1;
        for (int i = nums.length - 1; i >= 0; i--) {
            if (nums[i] != 0) {
                nums[index] = nums[i];
                index--;
            }
        }
        for (int i = index; i >= 0; i--) {
            nums[i] = 0;
        }
        return nums;
    }
}
