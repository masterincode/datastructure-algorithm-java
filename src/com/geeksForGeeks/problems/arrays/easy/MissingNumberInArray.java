package com.geeksForGeeks.problems.arrays.easy;

public class MissingNumberInArray {

    public static void main(String[] args) {
        int n = 10;
        int[] array = {1,2,3,4,5,6,7,8,10};
        System.out.println(missingNumber(array, n));
    }

//    Expected Time Complexity: O(N)
//    Expected Auxiliary Space: O(1)
    public static int missingNumber(int[] array, int n) {

        if(array.length == 0 && n ==0)
            return 0;
        int totalSum = (n*(n+1))/2;
        int sum = 0;
        for(int value: array) {
            sum = sum + value;
        }
        return totalSum - sum;
    }
}
