package com.geeksForGeeks.problems.arrays.easy;

public class SortAnArrayof0s1sAnd2sWithUsingSortingAlgorithm {

//    Approach 1: Arrays.sort();   Time complexity: O(n log n)
//    Approach 2: Count No of 0, 1 and 2 and then put into array accordingly. Time complexity: O(n)
//    Approach 3: Use 3 pointers Low, Mid and High [Dutch National Flag Alogrithm]

    //OptimisedApproach
    //Time Complexity: O(n)
    //Space Complexity: O(1)
    public static void sort012(int a[], int n) {
        int low = 0;
        int high = n - 1;
        int mid = 0, temp = 0;
        while (mid <= high) {
            switch (a[mid]) {
                case 0: {
                    temp = a[low];
                    a[low] = a[mid];
                    a[mid] = temp;
                    low++;
                    mid++;
                    break;
                }
                case 1:
                    mid++;
                    break;
                case 2: {
                    temp = a[mid];
                    a[mid] = a[high];
                    a[high] = temp;
                    high--;
                    break;
                }
            }
        }
    }
}
