package com.geeksForGeeks.problems.arrays.easy;

import java.util.ArrayList;

public class SubarrayWithGivenSum {

    public static void main(String[] args) {
        int N = 5;
        int S = 12;
        int[] A = {1,2,3,7,5};
        System.out.println(subArraySumBruteForceApproach(A,N,S));
        System.out.println(subArraySumOptimisedApproach(A,N,S));
    }

    //BruteForceApproach
    //Time Complexity: O(n^2)
    //Space Complexity: O(1)
    public static ArrayList<Integer> subArraySumBruteForceApproach(int[] arr, int n, int s)
    {
        ArrayList<Integer> list = new ArrayList<>();
        int sum;
        for(int i=0; i<n; i++ ) {
            sum = arr[i];
            for(int j=i+1; j<n; j++) {
                sum = sum + arr[j];
                if(sum == s) {
                    list.add(++i);
                    list.add(++j);
                    return list;
                }
            }
        }
        return list;
    }

    //Optmised Approach
    //Time Complexity: O(n)
    //Space Complexity: O(1)
    public static ArrayList<Integer> subArraySumOptimisedApproach(int[] arr, int n, int s)
    {
        int start = 0;
        int last;
        boolean flag=false;
        int currsum=0;
        ArrayList<Integer> res = new ArrayList<>();

        //iterating over the array.
        for(int i=0;i<n;i++)
        {
            //storing sum upto current element.
            currsum = currsum + arr[i];

            //checking if current sum is greater than or equal to given number.
            if(currsum>=s)
            {
                last=i;
                //we start from starting index till current index and do the
                //excluding part while s(given number) < currsum.
                while(s<currsum && start<last)
                {
                    //subtracting the element from left i.e., arr[start]
                    currsum = currsum - arr[start];
                    ++start;
                }

                //now if current sum becomes equal to given number, we store
                //the starting and ending index for the subarray.
                if(currsum==s)
                {
                    res.add(start + 1);
                    res.add(last + 1);

                    //flag is set to true since subarray exists.
                    flag = true;
                    break;
                }
            }
        }
        //if no subarray is found, we store -1 in result else the found indexes.
        if (flag==false) {
            res.add(-1);
        }
        //returning the result.
        return res;
    }
}
