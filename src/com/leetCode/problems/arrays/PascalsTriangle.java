package com.leetCode.problems.arrays;

import java.util.ArrayList;
import java.util.List;

public class PascalsTriangle {

    //OptimisedApproach
    //Time Complexity: O(n^2)
    //Space Complexity: O(n^2)
    //Runtime : 0 ms
    //Memory : 36.9 MB
    public List<List<Integer>> generate(int numRows) {
        if (numRows == 0)
            return new ArrayList<>();
        List<List<Integer>> list = new ArrayList<>();
        List<Integer> row = null;

        for (int i = 0; i < numRows; i++) {
            row = new ArrayList<>();
            for (int j = 0; j <= i; j++) {
                if (j == 0 || j == i) {
                    row.add(1);
                } else {
                    row.add(sum(j, i - 1, list));
                }
            }
            list.add(row);
        }
        return list;
    }

    public int sum(int index, int row, List<List<Integer>> list) {
        List<Integer> prevRow = list.get(row);
        int sum = 0;
        for (int k = index - 1; k <= index; k++) {
            sum = sum + prevRow.get(k);
        }
        return sum;
    }
}
