package com.leetCode.problems.arrays;

public class MergeSortedArray {

    public static void main(String[] args) {
//        int[] nums1 = {0,0,0,0,0,0};
//        int m       = 0;
//        int[] nums2 = {1,2,3,4,5};
//        int n       = 5;

        int[] nums1 = {1,2,3,0,0,0};
        int m       = 3;
        int[] nums2 = {2,5,6};
        int n       = 3;
        MergeSortedArray mergeSortedArray = new MergeSortedArray();
        mergeSortedArray.mergeOptimisedApproach(nums1,m,nums2,n);
    }

    //BruteForceApproach  Note: Not able to cover above test case
    //Time Complexity: O(n^2)
    //Space Complexity: O(1)
    public void mergeBruteForceApproach(int[] nums1, int m, int[] nums2, int n) {
        if( (m==1 && n==0) || (m==0 && n==0) )
            return;
        if( m==0 && n==1){
            nums1[0] = nums2[0];
            return;
        }
        int temp = 0;
        for(int i=0; i< m; i++) {
            for(int j=0; j< n; j++) {
                if(nums1[i] > nums2[j]) {
                    temp = nums1[i];
                    nums1[i] = nums2[j];
                    nums2[j] = temp;
                }
            }
        }

        for(int k=m; k < nums1.length; k++) {
            nums1[k] = nums2[k-n];
        }

        for( int v: nums1 )
            System.out.println(v);
    }

    //OptimisedApproach
    //Time Complexity: O(n)
    //Space Complexity: O(1)
    //Runtime : 1 ms
    //Memory : 51.9 MB
    public void mergeOptimisedApproach(int[] nums1, int m, int[] nums2, int n) {

        int i=m-1;
        int j=n-1;
        int k = m+n-1;
        while(i >=0 && j>=0)
        {
            if(nums1[i] > nums2[j])
                nums1[k--] = nums1[i--];
            else
                nums1[k--] = nums2[j--];
        }
        while(j>=0) {
            nums1[k--] = nums2[j--];
        }
        for( int v: nums1 )
            System.out.println(v);
    }


}
