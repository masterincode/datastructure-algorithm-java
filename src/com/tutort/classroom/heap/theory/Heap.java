package com.tutort.classroom.heap.theory;

public class Heap {
    public int count;
    public int capacity;
    public int[] arr;
    public int heapType;

    public Heap(int capacity, int heapType) {
        this.capacity = capacity;
        this.arr = new int[capacity];
        this.heapType = heapType;
        this.count = 0;
    }
}
