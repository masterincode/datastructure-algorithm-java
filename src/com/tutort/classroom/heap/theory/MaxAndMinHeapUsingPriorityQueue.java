package com.tutort.classroom.heap.theory;

import java.util.Arrays;
import java.util.Collections;
import java.util.PriorityQueue;

public class MaxAndMinHeapUsingPriorityQueue {

    public static void main(String[] args) {
        System.out.println("====Build Min Heap====");
        minHeapUsingPriorityQueue();
        System.out.println();
        System.out.println("====Build Max Heap====");
        maxHeapUsingPriorityQueue();
    }

    public static void minHeapUsingPriorityQueue() {
        PriorityQueue<Integer> minPQ = new PriorityQueue<>();
        int[] arr = new int[]{20, 19, 17, 18, 15, 16, 13, 11, 14, 12};
        for (int value : arr) {
            minPQ.add(value);
        }
        System.out.println("Before: " + Arrays.toString(arr));
        System.out.println("After : " + minPQ);
    }

    public static void maxHeapUsingPriorityQueue() {
//        Method 1:
        PriorityQueue<Integer> maxPQ1 = new PriorityQueue<>(Collections.reverseOrder());
//        Method 2:
        PriorityQueue<Integer> maxPQ2 = new PriorityQueue<>((a, b) -> b - a);
//        Method 3:
        PriorityQueue<Integer> maxPQ3 = new PriorityQueue<>((a, b) -> b.compareTo(a));

        int[] arr = new int[]{11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
        for (int value : arr) {
            maxPQ1.add(value);
            maxPQ2.add(value);
            maxPQ3.add(value);
        }
        System.out.println("Before:   " + Arrays.toString(arr));
        System.out.println("Method 1: " + maxPQ1);
        System.out.println("Method 2: " + maxPQ2);
        System.out.println("Method 3: " + maxPQ3);
    }
}
