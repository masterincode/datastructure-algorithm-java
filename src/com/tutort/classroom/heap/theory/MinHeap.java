package com.tutort.classroom.heap.theory;

import java.util.Arrays;

public class MinHeap extends PriorityQueue {

    public static void main(String[] args) {
        MinHeap minHeap = new MinHeap();
        int[] arr1 = {7, 5, 6, 4, 2, 1, 3};

        //TODO: 1. Build Min Heap [Time Complexity: O(n)]
        System.out.println("====Build Min Heap====");
        System.out.println("Before: " + Arrays.toString(arr1));
        Heap heap1 = new Heap(arr1.length, 1);
        minHeap.buildMinHeap(heap1, arr1, arr1.length);
        System.out.println("After:  " + Arrays.toString(heap1.arr));

        //TODO: 2. Percolate Down of Min Heap [Time Complexity: O(log n)]
        System.out.println("====Percolate Down of Min Heap====");
        Heap heap2 = new Heap(10, 1);
        heap2.arr = new int[]{20, 19, 17, 18, 15, 16, 13, 11, 14, 12};
        heap2.count = 10;

        System.out.println("Before: " + Arrays.toString(heap2.arr));
        for (int i = (heap2.count - 2) / 2; i >= 0; i--) {
            minHeap.percolateDownOfMinHeap(heap2, i);
        }
        System.out.println("After:  " + Arrays.toString(heap2.arr));

        //TODO: 3. Delete Node Index [Time Complexity: O(log n)]
        System.out.println("====Delete Node Index====");
        System.out.println("Before: " + Arrays.toString(heap2.arr));
        minHeap.deleteMinHeap(heap2, 0);
        System.out.println("After:  " + Arrays.toString(heap2.arr));
        minHeap.deleteMinHeap(heap2, 0);
        System.out.println("After:  " + Arrays.toString(heap2.arr));

        //TODO: 4. Min Heap using Insert function [Time Complexity: O(n log n)]
        System.out.println("====Min Heap using Insert function====");
        Heap heap3 = new Heap(7, 1);
        for (int data : arr1)
            minHeap.insert(heap3, data);
        System.out.println("After:  " + Arrays.toString(heap3.arr));

    }

    //TODO: Create Heap from Given array using Percolate Down
    // Time Complexity: O(n)
    // Min Heap  => Ascending Priority Queue
    public void buildMinHeap(Heap heap, int arr[], int n) {
        while (n > heap.capacity)
            resizeHeap(heap);
        for (int i = 0; i < n; i++) {
            heap.arr[i] = arr[i];
            heap.count++;
        }
        for (int i = (heap.count - 2) / 2; i >= 0; i--) {
            this.percolateDownOfMinHeap(heap, i);
        }
    }

    //TODO: Comparison of violated node with its child nodes
    //nodeIndex is the index of the element which is violating the heap property
    // Time Complexity: O(log n)
    public void percolateDownOfMinHeap(Heap heap, int nodeIndex) {
        int minIndex;
        int leftIndex = findLeft(heap, nodeIndex);
        int rightIndex = findRight(heap, nodeIndex);
        if (leftIndex != -1 && heap.arr[leftIndex] < heap.arr[nodeIndex])
            minIndex = leftIndex;
        else
            minIndex = nodeIndex;

        if (rightIndex != -1 && heap.arr[rightIndex] < heap.arr[minIndex])
            minIndex = rightIndex;

        if (minIndex != nodeIndex) {
            //Swapping
            int temp = heap.arr[nodeIndex];
            heap.arr[nodeIndex] = heap.arr[minIndex];
            heap.arr[minIndex] = temp;

            percolateDownOfMinHeap(heap, minIndex);
        }
        return;
    }

    // Time Complexity: O (log n)
    public int deleteMinHeap(Heap heap, int nodeIndex) {
        int deleteElement;
        if (heap.count == 0 || nodeIndex >= heap.count)
            return -1;
        deleteElement = heap.arr[nodeIndex];
        // copy the last element from heap to the position in which we have to delete
        heap.arr[nodeIndex] = heap.arr[heap.count - 1];
        heap.arr[heap.count - 1] = 0;
        heap.count--;
        // reheapify the tree
        percolateDownOfMinHeap(heap, nodeIndex);
        return deleteElement;
    }

    //TODO: Build Heap using insert + Percolate Up
    // Time Complexity: O(n log n)
    public int insert(Heap heap, int data) {
        int i;
        if (heap.count == heap.capacity)
            resizeHeap(heap);
        heap.count++;
        i = heap.count - 1;
        while (i >= 0 && data > heap.arr[(i - 1) / 2]) {  // heap->arr[findParent(i)]
            heap.arr[i] = heap.arr[(i - 1) / 2];
            i = (i - 1) / 2;
        }
        heap.arr[i] = data;
        return i;
    }
}
