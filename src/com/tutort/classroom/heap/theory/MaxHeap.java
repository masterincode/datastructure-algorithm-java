package com.tutort.classroom.heap.theory;

import java.util.Arrays;

public class MaxHeap extends PriorityQueue {

    public static void main(String[] args) {
        MaxHeap maxHeap = new MaxHeap();
        int[] arr1 = {1, 2, 3, 4, 5, 6, 7};

        //TODO: 1. Build Max Heap [Time Complexity: O(n)]
        System.out.println("====Build Max Heap====");
        System.out.println("Before: " + Arrays.toString(arr1));
        Heap heap1 = new Heap(arr1.length, 1);
        maxHeap.buildMaxHeap(heap1, arr1, arr1.length);
        System.out.println("After:  " + Arrays.toString(heap1.arr));

        //TODO: 2. Percolate Down of Max Heap [Time Complexity: O(log n)]
        System.out.println("====Percolate Down of Max Heap====");
        Heap heap2 = new Heap(10, 1);
        heap2.arr = new int[]{11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
        heap2.count = 10;

        System.out.println("Before: " + Arrays.toString(heap2.arr));
        for (int i = (heap2.count - 2) / 2; i >= 0; i--) {
            maxHeap.percolateDownOfMaxHeap(heap2, i);
        }
        System.out.println("After:  " + Arrays.toString(heap2.arr));

        //TODO: 3. Delete Node Index [Time Complexity: O(log n)]
        System.out.println("====Delete Node Index====");
        System.out.println("Before: " + Arrays.toString(heap2.arr));
        maxHeap.deleteMaxHeap(heap2, 0);
        System.out.println("After:  " + Arrays.toString(heap2.arr));
        maxHeap.deleteMaxHeap(heap2, 0);
        System.out.println("After:  " + Arrays.toString(heap2.arr));

        //TODO: 4. Max Heap using Insert function [Time Complexity: O(n log n)]
        System.out.println("====Max Heap using Insert function====");
        Heap heap3 = new Heap(7, 1);
        for (int data : arr1)
            maxHeap.insert(heap3, data);
        System.out.println("After:  " + Arrays.toString(heap3.arr));
    }

    //TODO: Create Heap from Given array using Percolate Down
    // Time Complexity: O(n)
    // Max Heap  => Descending Priority Queue
    public void buildMaxHeap(Heap heap, int arr[], int n) {
        while (n > heap.capacity)
            resizeHeap(heap);
        for (int i = 0; i < n; i++) {
            heap.arr[i] = arr[i];
            heap.count++;
        }
        for (int i = (heap.count - 2) / 2; i >= 0; i--) {
            this.percolateDownOfMaxHeap(heap, i);
        }
    }

    //TODO: Comparison of violated node with its child nodes
    // nodeIndex is the index of the element which is violating the heap property
    // Time Complexity: O(log n)
    public void percolateDownOfMaxHeap(Heap heap, int nodeIndex) {
        int maxIndex;
        int leftIndex = findLeft(heap, nodeIndex);
        int rightIndex = findRight(heap, nodeIndex);
        if (leftIndex != -1 && heap.arr[leftIndex] > heap.arr[nodeIndex])
            maxIndex = leftIndex;
        else
            maxIndex = nodeIndex;

        if (rightIndex != -1 && heap.arr[rightIndex] > heap.arr[maxIndex])
            maxIndex = rightIndex;

        if (maxIndex != nodeIndex) {
            //Swapping
            int temp = heap.arr[nodeIndex];
            heap.arr[nodeIndex] = heap.arr[maxIndex];
            heap.arr[maxIndex] = temp;

            percolateDownOfMaxHeap(heap, maxIndex);
        }
        return;
    }

    // Time Complexity: O (log n)
    public int deleteMaxHeap(Heap heap, int nodeIndex) {
        int deleteElement;
        if (heap.count == 0 || nodeIndex >= heap.count)
            return -1;
        deleteElement = heap.arr[nodeIndex];
        // copy the last element from heap to the position in which we have to delete
        heap.arr[nodeIndex] = heap.arr[heap.count - 1];
        heap.arr[heap.count - 1] = 0;
        heap.count--;
        // reheapify the tree
        percolateDownOfMaxHeap(heap, nodeIndex);
        return deleteElement;
    }

    //TODO: Build Heap using insert + Percolate Up
    // Time Complexity: O(n log n)
    public int insert(Heap heap, int data) {
        int i;
        if (heap.count == heap.capacity)
            resizeHeap(heap);
        heap.count++;
        i = heap.count - 1;
        while (i >= 0 && data > heap.arr[(i - 1) / 2]) {  // heap->arr[findParent(i)]
            heap.arr[i] = heap.arr[(i - 1) / 2];
            i = (i - 1) / 2;
        }
        heap.arr[i] = data;
        return i;
    }
}
