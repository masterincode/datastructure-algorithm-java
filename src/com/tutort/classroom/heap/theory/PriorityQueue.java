package com.tutort.classroom.heap.theory;

import java.util.Arrays;

public class PriorityQueue {

    // Time Complexity: O(1)
    public int getMax(Heap h) {
        if (h.count == 0)
            return -1;

        return h.arr[0]; // return element
    }

    // Time Complexity: O(1)
    public int getMin(Heap h) {
        if (h.count == 0)
            return -1;
        return h.arr[0]; // return element
    }

    // Time Complexity: O(1)
    public int findParent(int i) {
        int parentIndex = (i - 1) / 2;
        if (parentIndex < 0)
            return -1;
        return parentIndex; // return parent index
    }

    // Time Complexity: O(1)
    public int findLeft(Heap heap, int i) {
        int leftIndex = 2 * i + 1;
        if (leftIndex > heap.count - 1) {
            return -1;
        }
        return leftIndex; // return left child index
    }

    // Time Complexity: O(1)
    public int findRight(Heap heap, int i) {
        int rightIndex = 2 * i + 2;
        if (rightIndex > heap.count - 1) {
            return -1;
        }
        return rightIndex; // return right child index
    }

    public void resizeHeap(Heap heap) {
        int[] resizedArray = Arrays.copyOf(heap.arr, 10);
        heap.arr = resizedArray;
    }
}
