package com.tutort.classroom.heap.problems;


import com.tutort.LeetCodeURLConstant;

import java.util.PriorityQueue;

public class MinimumCostOfRopes {

    public static void main(String[] args) {
        long[] arr = new long[]{4, 2, 7, 6, 9};
        minCost(arr, arr.length);
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.MINIMUM_COST_OF_ROPES);
    }

    public static long minCost(long arr[], int n)
    {
        PriorityQueue<Long> minHeap = new PriorityQueue<>();
        for (long value: arr) {
            minHeap.add(value);
        }
        long answer = 0;
        while (minHeap.size() != 1) {
            long first = minHeap.poll();
            long sec = minHeap.poll();
            long sum = first + sec;
            answer = answer + sum;
            minHeap.add(sum);
        }
        return answer;
    }
}
