package com.tutort.classroom.heap.problems;

import java.util.Collections;
import java.util.PriorityQueue;

public class KthSmallestElementInAnArray {

    public static void main(String[] args) {
        int[] arr = {3,2,1,5,6,4};
        System.out.println(findKthSmallest(arr, 3));
    }

    public static int findKthSmallest(int[] nums, int k) {

        PriorityQueue<Integer> maxHeapPQ = new PriorityQueue<>(Collections.reverseOrder());
        for(int value : nums) {
            maxHeapPQ.add(value);
            if (maxHeapPQ.size() > k) {
                maxHeapPQ.poll();
            }
        }
        return maxHeapPQ.peek();
    }
}
