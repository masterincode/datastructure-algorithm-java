package com.tutort.classroom.heap.problems;

import com.tutort.LeetCodeURLConstant;

import java.util.PriorityQueue;

public class KthLargestElementInAnArray {

    public static void main(String[] args) {
        int[] arr = {3, 2, 3, 1, 2, 4, 5, 5, 6};
        System.out.println(findKthLargest(arr, 4));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.KTH_LARGEST_ELEMENT_IN_AN_ARRAY);
    }

    public static int findKthLargest(int[] nums, int k) {

        PriorityQueue<Integer> minHeapPQ = new PriorityQueue<>();
        for(int value : nums) {
            minHeapPQ.add(value);
            if (minHeapPQ.size() > k) {
                minHeapPQ.poll();
            }
        }
        return minHeapPQ.peek();
    }
}
