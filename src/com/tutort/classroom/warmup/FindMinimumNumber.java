package com.tutort.classroom.warmup;

import java.util.ArrayList;
import java.util.List;

public class FindMinimumNumber {

    public static void main(String[] args) {
        List<Integer[]> testCases = new ArrayList<>();
        testCases.add(new Integer[]{2,3,4,1,5});
        testCases.add(new Integer[]{});
        testCases.add(new Integer[]{2,2,2});
        testCases.add(new Integer[]{3});
        int test = 1;

        for(Integer[] testCase: testCases) {
            System.out.println("Test Cases: "+test);
            System.out.println(getMinimumNumber(testCase));
            test++;
            System.out.println();
        }
    }

    public static int getMinimumNumber(Integer[] arr) {

        if(arr.length == 0) {
            return -1;
        }
        if(arr.length == 1) {
            return arr[0];
        }
        int min = Integer.MAX_VALUE;

        for(int i = 0; i < arr.length; i++ ) {

            if(arr[i] < min) {
                min = arr[i];
            }
        }
        return min;
    }
}
