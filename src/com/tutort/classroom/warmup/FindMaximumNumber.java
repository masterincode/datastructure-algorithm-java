package com.tutort.classroom.warmup;

import java.util.ArrayList;
import java.util.List;

public class FindMaximumNumber {

    public static void main(String[] args) {
        List<Integer[]> testCases = new ArrayList<>();
        testCases.add(new Integer[]{2,3,4,1,5});
        testCases.add(new Integer[]{});
        testCases.add(new Integer[]{2,2,2});
        testCases.add(new Integer[]{3});
        int test = 1;

        for(Integer[] testCase: testCases) {
            System.out.println("Test Cases: "+test);
            System.out.println(getMaximumNumber(testCase));
            test++;
            System.out.println();
        }
    }

    public static int getMaximumNumber(Integer[] arr) {
        int max = -1;
        for(int i = 0; i < arr.length; i++) {
            if(arr[i] > max) {
                max = arr[i];
            }
        }
        return max;
    }
}
