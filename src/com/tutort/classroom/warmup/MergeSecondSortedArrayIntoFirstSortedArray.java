package com.tutort.classroom.warmup;

import com.tutort.LeetCodeURLConstant;

public class MergeSecondSortedArrayIntoFirstSortedArray {

    public static void main(String[] args) {
        int [] nums1 = {2,0};
        int m = 1;
        int[] nums2 = {1};
        int n = 1;
        merge(nums1,m,nums2,n);
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.MERGE_SORTED_ARRAY_IN_FIRST_ARRAY);
    }

    public static void merge(int[] nums1, int m, int[] nums2, int n) {
        if((m == 1 && nums1[0] == 0)) {
            return;
        }
        if((n == 1 && nums2[0] == 0)) {
            return;
        }
        if(m == 1 && n == 0) {
            return;
        }
        if(m == 0 && n == 1) {
            nums1[0] = nums2[0];
            return;
        }
        int i = m-1;
        int j = n-1;
        int k = m+n-1;

        while(i > -1 && j > -1) {
            if(nums1[i] > nums2[j]) {
                nums1[k] = nums1[i];
                i--;
                k--;
            } else {
                nums1[k] = nums2[j];
                j--;
                k--;
            }

        }
        while(i > -1) {
            nums1[k] = nums1[i];
            i--;
            k--;
        }

        while(j > -1) {
            nums1[k] = nums2[j];
            j--;
            k--;
        }
    }
}
