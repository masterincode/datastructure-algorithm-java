package com.tutort.classroom.warmup;

import java.util.ArrayList;
import java.util.List;

public class PrintArray {

    public static void main(String[] args) {

        List<Integer[]> testCases = new ArrayList<>();
        testCases.add(new Integer[]{1, 2, 3, 4});
        testCases.add(new Integer[]{});
        int test = 1;

        for(Integer[] testCase: testCases) {
            System.out.println("Test Cases: "+test);
            printArray(testCase);
            System.out.println();
            test++;
            System.out.println();
        }

    }

    public static void printArray(Integer[] arr) {
        if (arr.length == 0) {
            System.out.print(-1);
        } else {
            for (int i = 0; i < arr.length; i++) {
                System.out.print(arr[i]);
            }
        }
    }
}
