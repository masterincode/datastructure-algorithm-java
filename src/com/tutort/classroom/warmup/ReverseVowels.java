package com.tutort.classroom.warmup;

import com.tutort.LeetCodeURLConstant;

public class ReverseVowels {

    public static void main(String[] args) {

        System.out.println(reverseVowels("leetcode"));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.REVERSE_VOWELS_OF_A_STRING);
    }

    public static String reverseVowels(String s) {
        char[] arr = new char[0];
        if(s != null) {
            arr = s.toCharArray();
        }

        if (arr.length == 0) {
            return s;
        }
        if (arr.length == 1) {
            return s;
        }

        char temp;
        int i = 0;
        int j = arr.length - 1;

        while (i < j) {
            //arr[i] = a
            //arr[j] = e
            if (isVowels(arr[i]) && isVowels(arr[j])) {
                temp = arr[i];   //temp = a
                arr[i] = arr[j]; //arr[i] = f
                arr[j] = temp;   //arr[j] = a
                i++;
                j--;
            }
            if (!isVowels(arr[i])) {
                i++;
            }
            if (!isVowels(arr[j])) {
                j--;
            }
        }
        String newString = new String(arr);
        return newString;
    }

    private static  boolean isVowels(char a) {
        return (a=='a' || a=='e' ||a=='i' ||a=='o' ||a=='u' || a=='A' || a=='E' ||a=='I' ||a=='O' ||a=='U');
    }
}
