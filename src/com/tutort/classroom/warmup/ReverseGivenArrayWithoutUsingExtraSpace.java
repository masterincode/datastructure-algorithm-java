package com.tutort.classroom.warmup;

import java.util.ArrayList;
import java.util.List;

public class ReverseGivenArrayWithoutUsingExtraSpace {

    public static void main(String[] args) {
        List<Character[]> testCases = new ArrayList<>();
        testCases.add(new Character[]{'a','d','e','r','f','g'});
        testCases.add(new Character[]{'a','d','e','r','f'});
        testCases.add(new Character[]{});
        int test = 1;
        for (Character[] testCase : testCases) {
            System.out.println("Test Cases: " + test);
            reverseArray(testCase);
            for (Character value: testCase)
                System.out.print(value+",");
            test++;
            System.out.println();
        }
    }

    public static void reverseArray(Character[] arr) {

        if (arr.length == 0) {
            return;
        }

        char temp;
        int i = 0;
        int j = arr.length - 1;

        while (i < j) {
            //arr[i] = e
            //arr[j] = r
            temp = arr[i];   //temp = a
            arr[i] = arr[j]; //arr[i] = f
            arr[j] = temp;   //arr[j] = a
            i++;
            j--;
        }
    }
}
