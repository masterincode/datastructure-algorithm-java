package com.tutort.classroom.warmup;

import com.tutort.LeetCodeURLConstant;

import java.util.ArrayList;
import java.util.List;

public class FizzBuzz {

    public static void main(String[] args) {
        List<Integer> testCasesList = new ArrayList<>();
        testCasesList.add(3);
        testCasesList.add(5);
        testCasesList.add(15);
        int test = 1;

        for (Integer testCase : testCasesList) {
            System.out.println("Test Cases: " + test);
            for (String value: fizzBuzz(testCase))
                System.out.print(value+",");
            test++;
            System.out.println();
            System.out.println();
        }
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.FIZZ_BUZZ);
    }

    public static String[] fizzBuzz(Integer n) {

        String[] result = new String[n];
        for(int i = 1 ; i <= n ; i++) {
            if(i % 3 == 0 && i % 5 == 0) {
                result[i-1] = "FizzBuzz";
            } else if(i % 3 == 0) {
                result[i-1] = "Fizz";
            } else if(i % 5 == 0) {
                result[i-1] = "Buzz";
            } else {
                result[i-1] = String.valueOf(i);
            }
        }
        return result;
    }
}
