package com.tutort.classroom.warmup;

import java.util.ArrayList;
import java.util.List;

public class FindMinimumAndMaximum {

    public static void main(String[] args) {
        List<Integer[]> testCases = new ArrayList<>();
        testCases.add(new Integer[]{2,1,3,4,5,6});
        testCases.add(new Integer[]{12,11,11,4,5,6});
        testCases.add(new Integer[]{2, 2, 2});
        testCases.add(new Integer[]{5});
        int test = 1;

        for (Integer[] testCase : testCases) {
            System.out.println("Test Cases: " + test);
            for (int value: findMinAndMaxValue(testCase))
            System.out.print("["+value+"]");
            test++;
            System.out.println();
        }
    }

    public static int[] findMinAndMaxValue(Integer[] arr) {

        int[] minMax = new int[2];
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
            if (arr[i] < min)
                min = arr[i];
        }
        minMax[0]=min;
        minMax[1]=max;
        return minMax;
   }
}
