package com.tutort.classroom.warmup;

import java.util.ArrayList;
import java.util.List;

public class FindMaximumAndSecondMaximum {

    public static void main(String[] args) {
        List<Integer[]> testCases = new ArrayList<>();
        testCases.add(new Integer[]{2,1,3,4,5,6});
        testCases.add(new Integer[]{12,11,11,4,5,6});
        testCases.add(new Integer[]{2,1});
        testCases.add(new Integer[]{5,5,5,5});
        int test = 1;

        for (Integer[] testCase : testCases) {
            System.out.println("Test Cases: " + test);
            for (int value: findMaxAndSecondMax_Approach_2(testCase))
                System.out.print("["+value+"]");
            test++;
            System.out.println();
        }
    }

    //TODO: Using Shifting Mechanism
    public static int[] findMaxAndSecondMax_Approach_2(Integer[] arr) {
        int[] minAndSecondMax = new int[2];
        int max = Integer.MIN_VALUE;
        int secondMax = Integer.MIN_VALUE;
        for(int i = 0; i < arr.length; i++) {
            if(arr[i] > max) {
                secondMax = max;
                max = arr[i];
            } else if(arr[i] > secondMax) {
                secondMax = arr[i];
            }
        }
        minAndSecondMax[0] = secondMax;
        minAndSecondMax[1] = max;
        return minAndSecondMax;
    }

    //TODO: Using 2 Pointers
    public static int[] findMaxAndSecondMax_Approach_1(Integer[] arr) {

        int[] minAndSecondMax = new int[2];
        int max = Integer.MIN_VALUE;
        int secondMax = Integer.MIN_VALUE;
        int i = 0;
        int j = arr.length -1;
        while( i < j) {
            if(arr[i] < arr[j]) {
                if(arr[j] > max) {
                    max = arr[j];
                }
                if(arr[i] > secondMax) {
                    secondMax = arr[i];
                }
                i++;
            } else {
                if(arr[i] > max) {
                    max = arr[i];
                }
                if(arr[j] > secondMax && max >= arr[j]) {
                    secondMax = arr[j];
                }
                j--;
            }
        }
        minAndSecondMax[0] = secondMax;
        minAndSecondMax[1] = max;
        return minAndSecondMax;
    }
}
