package com.tutort.classroom.warmup;

public class MergeTwoSortedArrays {

    public static void main(String[] args) {
        System.out.println("Test Cases...");
        for(int a: merge2SortedArrays(new int[]{1, 3, 5, 7}, new int[]{2, 4, 6, 8, 10})) {
            System.out.print(a+",");
        }
        System.out.println();
        System.out.println("Test Cases...");
        for(int a: merge2SortedArrays(new int[]{1}, new int[]{2})) {
            System.out.print(a+",");
        }
        System.out.println();
        System.out.println("Test Cases...");
        for(int a: merge2SortedArrays(new int[]{1,1,4}, new int[]{2,5,5})) {
            System.out.print(a+",");
        }

    }

    public static int[] merge2SortedArrays(int[] arr1, int[] arr2) {
        int n = arr1.length;
        int m = arr2.length;
        int i = 0;
        int j = 0;
        int k = 0;

        int[] result = new int[n+m];

        //arr1 = [1, 3, 5, 7] , n = 4
        //        i
        //arr2 = [2, 4, 6, 8, 10] , m = 1
        //        j
        while(i < n && j < m) {
            //arr1[i] = 5
            //arr2[j] = 4

            if(arr1[i] < arr2[j]) {
                result[k] = arr1[i]; // result [1,2,3,4,5,6,7]
                i++;
            } else {
                result[k] = arr2[j]; // result [1,2,3,4,5,6,7]
                j++;
            }
            k++;
        }

        while(i < n) {
            result[k] = arr1[i];
            i++;
            k++;
        }

        while(j < m) {
            result[k] = arr2[j]; //result [1,2,3,4,5,6,7,8,10]
            j++;
            k++;
        }
        return result;
    }
}
