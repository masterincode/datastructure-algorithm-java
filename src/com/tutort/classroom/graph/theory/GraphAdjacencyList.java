package com.tutort.classroom.graph.theory;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class GraphAdjacencyList {

    public Graph graph;

    public GraphAdjacencyList(int nodes, boolean isZeroIndexBased) {
        graph = new Graph(nodes, isZeroIndexBased);
    }

    public void addEdge(int u, int v) {
        graph.adjList[u].add(v);
        graph.adjList[v].add(u);
        graph.edge++;
    }

    //=========================================================================================================
    //BFS for more than one component

//    Time Complexity : O(N+E) ,  N = Nodes , E = travelling through adjacent nodes
//    Space Complexity : O(N+E) + O(N) + O(N) ,  Space for adjacency list, visited array, queue data structure
    public void breadthFirstSearch(Graph graph) {
        int[] visited = new int[graph.vertex];
        for (int vertex = 0; vertex < graph.vertex; vertex++) {
            if (visited[vertex] == 0) {
                breadthFirstSearchUtil(graph, vertex, visited);
            }
        }
    }

    private void breadthFirstSearchUtil(Graph graph, int node, int[] visited) {
        Queue<Integer> queue = new LinkedList<>();
        queue.offer(node);
        visited[node] = 1;

        while (!queue.isEmpty()) {
            int currentNode = queue.poll();
            System.out.print(currentNode + " ");

            for (int nextNode : graph.adjList[currentNode]) {
                if (visited[nextNode] == 0) {
                    queue.offer(nextNode);
                    visited[nextNode] = 1;
                }
            }
        }
    }

    //=============Depth First Search Recursion=========================================================================

    //  Time complexity: O(N+E), Where N is the time taken for visiting N nodes and E is for travelling through adjacent nodes.
    //  Space Complexity: O(N+E) + O(N) + O(N) , Space for adjacency list, Array,Auxiliary space.
    public void depthFirstSearchRecursion(Graph graph) {
        int[] visited = new int[graph.vertex];
        for (int vertex = 0; vertex < graph.vertex; vertex++) {
            if (visited[vertex] == 0) {
                depthFirstSearchRecursionUtil(graph, vertex, visited);
            }
        }
    }

    private void depthFirstSearchRecursionUtil(Graph graph, int vertex, int[] visited) {
        visited[vertex] = 1;
        System.out.print(vertex + " ");

        for (int nextVertex : graph.adjList[vertex]) {
            if (visited[nextVertex] == 0) {
                depthFirstSearchRecursionUtil(graph, nextVertex, visited);
            }
        }
    }

    //=============Depth First Search Iterative=========================================================================
    //DFS
    public void depthFirstSearch(int s) {
        int[] visited = new int[graph.vertex];
        Stack<Integer> stack = new Stack<>();
        stack.push(s);

        while (!stack.isEmpty()) {
            int currentNode = stack.pop();

            if (visited[currentNode] == 0) {
                visited[currentNode] = 1;
                System.out.print(currentNode + " ");

                for (int nextNode : graph.adjList[currentNode]) {
                    if (visited[nextNode] == 0) {
                        stack.push(nextNode);
                    }
                }
            }
        }
    }
}
