package com.tutort.classroom.graph.theory;

import java.util.List;

public class GraphAdjacencyListMainMethod {
    public static void main(String[] args) {
        GraphAdjacencyList graphAdjacencyList = new GraphAdjacencyList(7, true);
        graphAdjacencyList.addEdge(0,1);
        graphAdjacencyList.addEdge(1,2);
        graphAdjacencyList.addEdge(2,3);
        graphAdjacencyList.addEdge(3,0);
        graphAdjacencyList.addEdge(2,4);
        graphAdjacencyList.addEdge(5,6);
        int count = 0;
        System.out.println("Vertex: "+graphAdjacencyList.graph.vertex);
        System.out.println("Edge: "+graphAdjacencyList.graph.edge);
        for (List<Integer> list : graphAdjacencyList.graph.adjList) {
            System.out.println(count++ + "->"+ list);
        }
        System.out.println();
        System.out.println("BFS Result: ");
        graphAdjacencyList.breadthFirstSearch(graphAdjacencyList.graph);
        System.out.println();
        System.out.println("DFS Result: ");
        graphAdjacencyList.depthFirstSearchRecursion(graphAdjacencyList.graph);
    }
}
