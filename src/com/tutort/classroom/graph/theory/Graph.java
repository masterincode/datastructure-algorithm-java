package com.tutort.classroom.graph.theory;

import java.util.ArrayList;

public class Graph {

    public int vertex; // total number of vertices in graph
    public int edge; // total number of edges in graph
    ArrayList<Integer>[] adjList;

    public Graph(int nodes, boolean isZeroIndexBased) {
        if(isZeroIndexBased) {
            this.vertex = nodes;
            this.adjList = new ArrayList[nodes];
        } else {
            this.vertex = nodes + 1;
            this.adjList = new ArrayList[nodes + 1];
        }
        for (int i = 0; i < vertex; i++) {
            adjList[i] =  new ArrayList<>();
        }
    }
}
