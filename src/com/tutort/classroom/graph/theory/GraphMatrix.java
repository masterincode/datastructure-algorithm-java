package com.tutort.classroom.graph.theory;

public class GraphMatrix {
    public int vertex; // total number of vertices
    public int edge; // total number of edges
    public int[][] adjMatrix;

    public GraphMatrix(int nodes) {
        this.adjMatrix =  new int[nodes][nodes];
        this.vertex = nodes;
        this.edge = 0;
    }
}