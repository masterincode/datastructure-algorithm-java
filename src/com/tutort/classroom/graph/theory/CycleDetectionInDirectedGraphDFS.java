package com.tutort.classroom.graph.theory;

import java.util.ArrayList;

public class CycleDetectionInDirectedGraphDFS {

    public static void main(String[] args) {

        int V = 6;
        ArrayList<ArrayList<Integer>> graph = new ArrayList<>(V);

        //Vertex - 0
        ArrayList<Integer> neighbors = new ArrayList<>();
        neighbors.add(1);
        graph.add(neighbors);

        //Vertex - 1
        neighbors = new ArrayList<>();
        neighbors.add(2);
        neighbors.add(5);
        graph.add(neighbors);

        //Vertex - 2
        neighbors = new ArrayList<>();
        neighbors.add(3);
        graph.add(neighbors);

        //Vertex - 3
        neighbors = new ArrayList<>();
        neighbors.add(4);
        graph.add(neighbors);

        //Vertex - 4
        neighbors = new ArrayList<>();
        neighbors.add(0);
        neighbors.add(1);
        graph.add(neighbors);

        //Vertex - 5
        neighbors = new ArrayList<>();
        graph.add(neighbors);

        if (isCyclic(V, graph))
            System.out.println("Cycle detected");
        else
            System.out.println("No cycles detected");

    }

    public static boolean isCyclic(int N, ArrayList<ArrayList<Integer>> adj) {
        int visited[] = new int[N];
        int dfsVisited[] = new int[N];

        for (int node = 0; node < N; node++) {
            if (visited[node] == 0) {
                if (checkCycle(node, adj, visited, dfsVisited))
                    return true;
            }
        }
        return false;
    }

    private static boolean checkCycle(int node, ArrayList<ArrayList<Integer>> adj, int visited[], int dfsVisited[]) {
        visited[node] = 1;
        dfsVisited[node] = 1; //

        for (int nextNode : adj.get(node)) {
            if (visited[nextNode] == 0) {
                if (checkCycle(nextNode, adj, visited, dfsVisited)) {
                    return true;
                }
            } else if (dfsVisited[nextNode] == 1) { //
                return true;
            }
        }
        dfsVisited[node] = 0; //
        return false;
    }

}
