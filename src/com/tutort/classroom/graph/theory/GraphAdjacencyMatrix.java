package com.tutort.classroom.graph.theory;

public class GraphAdjacencyMatrix {

    public GraphMatrix graph;
    public GraphAdjacencyMatrix(int nodes) {
        graph = new GraphMatrix(nodes);
    }

    public void addEdge(int u, int v) {
        graph.adjMatrix[u][v] = 1;
        graph.adjMatrix[v][u] = 1;
        graph.edge++;
    }

    //=============Depth First Search Recursion=========================================================================
    public void depthFirstSearchRecursion(GraphMatrix graph){
        int[] visited = new int[graph.vertex];
        for(int i = 0; i < graph.vertex; i++){
            if(visited[i] != 1){
                depthFirstSearchRecursionUtil(graph, i, visited);
            }
        }
    }

    private void depthFirstSearchRecursionUtil(GraphMatrix graph, int u, int[] visited) {
        visited[u] = 1;
        System.out.print(u + " ");
        for (int v = 0; v < graph.vertex; v++) {
            if(visited[v] != 1 && graph.adjMatrix[u][v] == 1){
                depthFirstSearchRecursionUtil(graph, v, visited);
            }
        }
    }
}
