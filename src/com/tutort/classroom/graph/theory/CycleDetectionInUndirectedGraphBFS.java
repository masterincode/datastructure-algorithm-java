package com.tutort.classroom.graph.theory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

class Node {
    int currentValue;
    int parentValue;

    public Node(int first, int parentValue) {
        this.currentValue = first;
        this.parentValue = parentValue;
    }
}

public class CycleDetectionInUndirectedGraphBFS {

    public static void main(String[] args) {
        GraphAdjacencyList graphAdjacencyList = new GraphAdjacencyList(11, false); // Initilise with Size
        graphAdjacencyList.addEdge(1,2);
        graphAdjacencyList.addEdge(2,4);
        graphAdjacencyList.addEdge(3,5);
        graphAdjacencyList.addEdge(5,6);
        graphAdjacencyList.addEdge(6,7);
        graphAdjacencyList.addEdge(7,8);
        graphAdjacencyList.addEdge(8,9);
        graphAdjacencyList.addEdge(9,10);
        graphAdjacencyList.addEdge(10,5);
        graphAdjacencyList.addEdge(8,11);
        System.out.println(Arrays.toString(graphAdjacencyList.graph.adjList));
        System.out.println(hasCycle(graphAdjacencyList.graph.vertex, graphAdjacencyList.graph.adjList));
    }

    public static boolean hasCycle(int V, ArrayList<Integer>[] adj) {
        int[] visited = new int[V]; //In the cas eof Graph Root vertex is 1, 1 is added vertex length in Graph class

        for (int i = 1; i < V; i++) { // Here i = 1 coz here starting index is 1 not zero
            if (visited[i] == 0) {
                if (checkForCycleUsingBFS(adj, i, visited)) {
                    return true;
                }
            }
        }
        return false;
    }

    // int vertex: Starting Node of graph component which is 1 here
    private static boolean checkForCycleUsingBFS(ArrayList<Integer>[] adj, int vertex, int[] visited) {
        Queue<Node> queue = new LinkedList<>(); //BFS Approach
        queue.add(new Node(vertex, -1));
        visited[vertex] = 1;

        while (!queue.isEmpty()) {
            int current = queue.peek().currentValue;
            int parent = queue.peek().parentValue;
            queue.remove();

            for (Integer nextNode : adj[current]) {
                if (visited[nextNode] == 0) {
                    queue.add(new Node(nextNode, current));
                    visited[nextNode] = 1;
                } else if (parent != nextNode)
                    return true;
            }
        }
        return false;
    }
}
