package com.tutort.classroom.graph.theory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class ShortestPathInUnweightedGraph {

    public static void main(String[] args) {
        GraphAdjacencyList graphAdjacencyList = new GraphAdjacencyList(9, true); // Initilise with Size
        graphAdjacencyList.addEdge(0,1);
        graphAdjacencyList.addEdge(0,3);
        graphAdjacencyList.addEdge(1,3);
        graphAdjacencyList.addEdge(3,4);
        graphAdjacencyList.addEdge(4,5);
        graphAdjacencyList.addEdge(5,6);
        graphAdjacencyList.addEdge(1,2);
        graphAdjacencyList.addEdge(2,6);
        graphAdjacencyList.addEdge(6,7);
        graphAdjacencyList.addEdge(7,8);
        graphAdjacencyList.addEdge(8,6);

        System.out.println(Arrays.toString(graphAdjacencyList.graph.adjList));
        shortestPath(graphAdjacencyList.graph.adjList,  0);
    }

    // TODO:
    //  Reference: https://www.youtube.com/watch?v=hwCWi7-bRfI&list=PLgUwDviBIf0rGEWe64KWas0Nryn7SCRWw&index=15&ab_channel=takeUforward
    private static void shortestPath(ArrayList<Integer>[] adj, int src) {
        int[] distance = new int[adj.length];
        Arrays.fill(distance, Integer.MAX_VALUE);

        Queue<Integer> queue = new LinkedList<>();
        distance[src] = 0;
        queue.add(src);

        while (!queue.isEmpty()) {
            int currentNode = queue.poll();
            for (int nextNode: adj[currentNode]) {
                if(distance[currentNode] + 1 < distance[nextNode]) {
                    distance[nextNode] = distance[currentNode] + 1;
                    queue.add(nextNode);
                }
            }
        }
        System.out.println(Arrays.toString(distance));
    }
}
