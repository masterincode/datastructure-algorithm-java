package com.tutort.classroom.graph.theory;

import java.util.ArrayList;
import java.util.Stack;

public class TopologicalSortDFS {

    public static void main(String args[]) {
        ArrayList<ArrayList<Integer>> adj = new ArrayList<>();
        int n = 6;
        for (int i = 0; i < n; i++) {
            ArrayList<Integer> arr = new ArrayList<>();
            adj.add(arr);
        }

        adj.get(5).add(2);
        adj.get(5).add(0);
        adj.get(4).add(0);
        adj.get(4).add(1);
        adj.get(2).add(3);
        adj.get(3).add(1);

        int res[] = topologicalSort(6, adj);

        System.out.println("Toposort of the given graph is:");
        for (int i = 0; i < res.length; i++) {
            System.out.print(res[i] + " ");
        }
    }

//    Time Complexity: O(N+E) ,   N = Number of node , E = Number of Edges
//    Space Complexity: O(N) + O(N) ,  Visited Array and Stack data structure. Both will be using O(N).
//    Auxiliary Space Complexity:  O(N) ,  Recursion call of DFS
    public static int[] topologicalSort(int V, ArrayList<ArrayList<Integer>> adj) {
        Stack<Integer> stack = new Stack<>();
        int[] visited = new int[V];

        for (int index = 0; index < V; index++) {
            if (visited[index] == 0) {
                topologicalSortUtil(index, visited, adj, stack);
            }
        }

        int topologicalSortResult[] = new int[V];
        int index = 0;
        while (!stack.isEmpty()) {
            topologicalSortResult[index++] = stack.pop();
        }
        return topologicalSortResult;
    }

    private static void topologicalSortUtil(int vertex, int[] visited, ArrayList<ArrayList<Integer>> adj, Stack<Integer> stack) {
        visited[vertex] = 1;
        for (Integer nextNode : adj.get(vertex)) {
            if (visited[nextNode] == 0) {
                topologicalSortUtil(nextNode, visited, adj, stack);
            }
        }
        stack.push(vertex);
    }
}
