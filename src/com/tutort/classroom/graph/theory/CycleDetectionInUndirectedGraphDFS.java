package com.tutort.classroom.graph.theory;

import java.util.ArrayList;
import java.util.Arrays;

public class CycleDetectionInUndirectedGraphDFS {

    public static void main(String[] args) {
        GraphAdjacencyList graphAdjacencyList = new GraphAdjacencyList(8, false); // Initilise with Size
        graphAdjacencyList.addEdge(1, 3);
        graphAdjacencyList.addEdge(3, 4);

        graphAdjacencyList.addEdge(2, 5);
        graphAdjacencyList.addEdge(5, 6);
        graphAdjacencyList.addEdge(6, 7);
        graphAdjacencyList.addEdge(7, 8);
        graphAdjacencyList.addEdge(8, 5);

        System.out.println(Arrays.toString(graphAdjacencyList.graph.adjList));
        System.out.println(hasCycle(graphAdjacencyList.graph.vertex, graphAdjacencyList.graph.adjList));
    }

    public static boolean hasCycle(int V, ArrayList<Integer>[] adj) {
        int[] visited = new int[V]; //In the cas eof Graph Root vertex is 1, 1 is added vertex length in Graph class

        for (int node = 1; node < V; node++) { // Here i = 1 coz here starting index is 1 not zero
            if (visited[node] == 0) {
                if (checkForCycleUsingDFS(adj, node, -1, visited)) {
                    return true;
                }
            }
        }
        return false;
    }

    private static boolean checkForCycleUsingDFS(ArrayList<Integer>[] adjList, int node, int parentNode, int[] visited) {
        visited[node] = 1;

        for (int nextNode : adjList[node]) {
            if (visited[nextNode] == 0) {
                if (checkForCycleUsingDFS(adjList, nextNode, node, visited)) {
                    return true;
                }
            } else if (parentNode != nextNode) {
                return true;
            }
        }
        return false;
    }
}
