package com.tutort.classroom.graph.theory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.PriorityQueue;
class NodeVertex {
    public int vertex;
    public int weight;

    public NodeVertex(int vertex, int weight) {
        this.vertex = vertex;
        this.weight = weight;
    }
}

public class DijkstraAlgorithm {

    public static void main(String[] args) {
        int n = 5;
        ArrayList<ArrayList<NodeVertex>> adj = new ArrayList<>();

        for (int i = 0; i < n; i++)
            adj.add(new ArrayList<>());

        adj.get(0).add(new NodeVertex(1, 2));
        adj.get(1).add(new NodeVertex(0, 2));

        adj.get(1).add(new NodeVertex(2, 4));
        adj.get(2).add(new NodeVertex(1, 4));

        adj.get(0).add(new NodeVertex(3, 1));
        adj.get(3).add(new NodeVertex(0, 1));

        adj.get(3).add(new NodeVertex(2, 3));
        adj.get(2).add(new NodeVertex(3, 3));

        adj.get(1).add(new NodeVertex(4, 5));
        adj.get(4).add(new NodeVertex(1, 5));

        adj.get(2).add(new NodeVertex(4, 1));
        adj.get(4).add(new NodeVertex(2, 1));

        shortestPath(adj, 0);

    }

    // TODO:
    //  Reference: https://www.youtube.com/watch?v=jbhuqIASjoM&list=PLgUwDviBIf0rGEWe64KWas0Nryn7SCRWw&index=17&ab_channel=takeUforward
    public static void shortestPath(ArrayList<ArrayList<NodeVertex>> adj, int src) {
        int[] distance = new int[adj.size()];
        for (int i = 0; i < adj.size(); i++) {
            distance[i] = Integer.MAX_VALUE;
        }
        distance[src] = 0;
        PriorityQueue<NodeVertex> minHeapPQ = new PriorityQueue<>((a, b) -> a.weight - b.weight);
        minHeapPQ.add(new NodeVertex(src, 0));
        while (minHeapPQ.size() > 0) {
            NodeVertex currentNode = minHeapPQ.poll();

            for(NodeVertex nextNode: adj.get(currentNode.vertex)) {

                if(distance[currentNode.vertex] + nextNode.weight < distance[nextNode.vertex]) {
                    distance[nextNode.vertex] = distance[currentNode.vertex] + nextNode.weight;
                    minHeapPQ.add(new NodeVertex(nextNode.vertex, distance[nextNode.vertex]));
                }
            }
        }
        System.out.println(Arrays.toString(distance));
    }
}
