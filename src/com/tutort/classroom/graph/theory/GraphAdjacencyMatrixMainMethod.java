package com.tutort.classroom.graph.theory;

public class GraphAdjacencyMatrixMainMethod {

    public static void main(String[] args) {
        GraphAdjacencyMatrix graphAdjacencyMatrix = new GraphAdjacencyMatrix(7);
        graphAdjacencyMatrix.addEdge(0,1);
        graphAdjacencyMatrix.addEdge(1,2);
        graphAdjacencyMatrix.addEdge(2,3);
        graphAdjacencyMatrix.addEdge(3,0);
        graphAdjacencyMatrix.addEdge(2,4);
        graphAdjacencyMatrix.addEdge(5,6);
        System.out.println("Vertex: "+graphAdjacencyMatrix.graph.vertex);
        System.out.println("Edge: "+graphAdjacencyMatrix.graph.edge);
        System.out.println();
        System.out.println("DFS Result: ");
        graphAdjacencyMatrix.depthFirstSearchRecursion(graphAdjacencyMatrix.graph);
    }
}
