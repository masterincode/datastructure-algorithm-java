package com.tutort.classroom.graph.dfs;

import com.tutort.LeetCodeURLConstant;

import java.util.*;

class Node {
    public int val;
    public List<Node> neighbors;

    public Node() {
        val = 0;
        neighbors = new ArrayList<>();
    }

    public Node(int _val) {
        val = _val;
        neighbors = new ArrayList<>();
    }

    public Node(int _val, ArrayList<Node> _neighbors) {
        val = _val;
        neighbors = _neighbors;
    }

}

public class CloneGraph {

    public static void main(String[] args) {
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.CLONE_GRAPH);
    }

    public static Node cloneGraph(Node node) {
        if (node == null)
            return null;
  // 1. Org Node, New Node
        Map<Node, Node> map = new HashMap<>();
        Queue<Node> queue = new ArrayDeque<>();

        queue.add(node);
        map.put(node, new Node(node.val, new ArrayList<>())); // { val: 1, neighbours: [] }

        while (!queue.isEmpty()) {
            Node currentNode = queue.poll();

            for (Node nextNode : currentNode.neighbors) {
                if (!map.containsKey(nextNode)) {
                    map.put(nextNode, new Node(nextNode.val, new ArrayList<>()));
                    queue.add(nextNode);
                }
                map.get(currentNode).neighbors.add(map.get(nextNode));
            }
        }

        return map.get(node);
    }

}
