package com.tutort.classroom.graph.dfs;

import com.tutort.LeetCodeURLConstant;

import java.util.ArrayList;

public class CourseSchedule {

    // TODO: CourseSchedule is almost same as Cycle detection in Directed graph using DFS,
    //  If Cycle present we cannot complete all courses, hence return false;
    //  If Cycle is not present we can complete all courses, hence return true;
    public static void main(String[] args) {
       int numCourses = 2;
       int[][] prerequisites = {{1,0}};
        System.out.println(canFinish(numCourses, prerequisites));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.COURSE_SCHEDULE);
    }

    public static boolean canFinish(int numCourses, int[][] prerequisites) {
        ArrayList<ArrayList<Integer>> adj = new ArrayList<>();

        for(int i = 0; i < numCourses; i++){
            adj.add(new ArrayList<>());
        }
        for(int[] arr : prerequisites){
            adj.get(arr[0]).add(arr[1]);
        }
        if(isCyclic(numCourses, adj)) {
            return false;
        }
        return true;
    }

//=============================  CycleDetectionInDirectedGraphDFS.java =================================================
    private static boolean isCyclic(int N, ArrayList<ArrayList<Integer>> adj) {
        int visited[] = new int[N];
        int dfsVisited[] = new int[N];

        for (int node = 0; node < N; node++) {
            if (visited[node] == 0) {
                if (checkCycle(node, adj, visited, dfsVisited))
                    return true;
            }
        }
        return false;
    }

    private static boolean checkCycle(int node, ArrayList<ArrayList<Integer>> adj, int visited[], int dfsVisited[]) {
        visited[node] = 1;
        dfsVisited[node] = 1; //

        for (int nextNode : adj.get(node)) {
            if (visited[nextNode] == 0) {
                if (checkCycle(nextNode, adj, visited, dfsVisited)) {
                    return true;
                }
            } else if (dfsVisited[nextNode] == 1) { //
                return true;
            }
        }
        dfsVisited[node] = 0; //
        return false;
    }
}
