package com.tutort.classroom.graph.dfs;

import com.tutort.LeetCodeURLConstant;

public class MaxAreaOfIsland {

    public static void main(String[] args) {
        int[][] arr = new int[][]{{1, 1, 0, 0, 0}, {1, 1, 0, 0, 0}, {0, 0, 1, 0, 0}, {0, 0, 0, 1, 1}};
        System.out.println(numIslands(arr));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.MAX_AREA_OF_ISLAND);
    }

    public static int numIslands(int[][] grid) {
        int row = grid.length;
        int col = grid[0].length;
        int maxArea = 0;

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (grid[i][j] == 1) {
                    int area = dfs(grid, row, col, i, j);
                    maxArea = Math.max(area, maxArea);
                }
            }
        }
        return maxArea;
    }

    private static int dfs(int[][] grid, int row, int col, int i, int j) {
        if (i < 0 || i > row - 1 || j < 0 || j > col - 1 || grid[i][j] != 1) {
            return 0;
        }
        grid[i][j] = 0;
        return 1 + dfs(grid, row, col, i - 1, j)
                + dfs(grid, row, col, i + 1, j)
                + dfs(grid, row, col, i, j - 1)
                + dfs(grid, row, col, i, j + 1);
    }
}
