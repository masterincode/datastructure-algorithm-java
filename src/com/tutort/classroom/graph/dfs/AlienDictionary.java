package com.tutort.classroom.graph.dfs;

import com.tutort.LeetCodeURLConstant;

import java.util.ArrayList;
import java.util.Stack;

public class AlienDictionary {

    public static void main(String[] args) {
        int N = 5, K = 4;
        String[] dict = {"baa", "abcd", "abca", "cab", "cad"};
        System.out.println(findOrder(dict, N, K));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.ALIEN_DICTIONARY);
    }

    public static String findOrder(String[] dict, int N, int K) {

        // 1. Arrays of ArrayList
        ArrayList<Integer>[] graph = new ArrayList[K]; // K is starting alphabets of standard dictionary i.e [a, b, c, d]
        for (int i = 0; i < K; i++) {
            graph[i] = new ArrayList<>(); // Initialising array with empty arraylist at each index
        }
        // 2. Iterating over dictionary
        for (int i = 0; i < N - 1; i++) {
            String word1 = dict[i]; // Picking first word
            String word2 = dict[i + 1]; // Picking second word
            int shortestWord = Math.min(word1.length(), word2.length()); // Find shortest word

            // 3. Creating Adjacency List of graph i.e. [V -> [V1, V2... ]]
            for (int j = 0; j < shortestWord; j++) {
                if (word1.charAt(j) != word2.charAt(j)) {
                    graph[word1.charAt(j) - 'a'].add(word2.charAt(j) - 'a');
                    break;
                }
            }
        }
//============ 4. Topological Sorting Algo Starts here ==========================
        Stack<String> stack = new Stack<>();
        int[] visited = new int[K];

        for (int index = 0; index < K; index++) {
            if (visited[index] == 0) {
               dfs(index, graph, visited, stack);
            }
        }

        String topologicalSortResult = "";
        while (!stack.isEmpty()) {
            topologicalSortResult = topologicalSortResult + stack.pop();
        }
        return topologicalSortResult;
    }

    private static void dfs(int node, ArrayList<Integer>[] graph, int[] visited, Stack<String> stack) {
        visited[node] = 1;
        for (int nextNode : graph[node]) {
            if (visited[nextNode] == 0) {
                dfs(nextNode, graph, visited, stack);
            }
        }
        char ch = (char) (node + 'a');
        stack.push(String.valueOf(ch));
    }
    //============================Ends Here==================================================
}
