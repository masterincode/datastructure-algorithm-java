package com.tutort.classroom.graph.dfs;

import com.tutort.LeetCodeURLConstant;

public class NumberOfIslands {

    public static void main(String[] args) {
        char[][] arr = new char[][] {{'1','1','0','0','0'},{'1','1','0','0','0'},{'0','0','1','0','0'},{'0','0','0','1','1'}};
        System.out.println(numIslands(arr));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.NUMBER_OF_ISLANDS);
    }

    public static int numIslands(char[][] grid) {
        int row = grid.length;
        int col = grid[0].length;
        int count = 0;

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (grid[i][j] == '1') {
                    dfs(grid, row, col, i, j);
                    count++;
                }
            }
        }
        return count;
    }

    private static void dfs(char[][] grid, int row, int col, int i, int j) {
        if (i < 0 || i > row - 1 || j < 0 || j > col - 1 || grid[i][j] != '1') {
            return;
        }
        grid[i][j] = '0';
        dfs(grid, row, col, i - 1, j);
        dfs(grid, row, col, i + 1, j);
        dfs(grid, row, col, i, j - 1);
        dfs(grid, row, col, i, j + 1);
    }
}
