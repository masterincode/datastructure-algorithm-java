package com.tutort.classroom.graph.bfs;

import com.tutort.LeetCodeURLConstant;

import java.util.LinkedList;
import java.util.Queue;

class Coordinates {
    int x;
    int y;
    int dis;

    Coordinates(int x, int y, int dis) {
        this.x = x;
        this.y = y;
        this.dis = dis;
    }
}

public class StepsByKnightBFS {

    public static void main(String[] args) {
        System.out.println(minStepToReachTarget(new int[]{4, 5}, new int[]{1, 1}, 6));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.STEPS_BY_KNIGHT);
    }

    public static int minStepToReachTarget(int KnightPos[], int TargetPos[], int N) {

        boolean visited[][] = new boolean[N + 1][N + 1];

        int dx[] = {-2, -2, -1, -1, 1, 1, 2, 2};
        int dy[] = {-1, 1, -2, 2, -2, 2, -1, 1};

        Queue<Coordinates> queue = new LinkedList<>();
        queue.add(new Coordinates(KnightPos[0], KnightPos[1], 0));

        while (!queue.isEmpty()) {

            Coordinates currentNode = queue.remove();

            if (currentNode.x == TargetPos[0] && currentNode.y == TargetPos[1])
                return currentNode.dis;

            for (int i = 0; i < 8; i++) {

                int x = currentNode.x + dx[i];
                int y = currentNode.y + dy[i];

                if (isValid(x, y, N) && !visited[x][y]) {
                    visited[x][y] = true;
                    queue.add(new Coordinates(x, y, currentNode.dis + 1));
                }
            }
        }
        return Integer.MAX_VALUE;
    }

    public static boolean isValid(int x, int y, int n) {

        if (x >= 1 && x <= n && y >= 1 && y <= n)
            return true;
        return false;
    }
}
