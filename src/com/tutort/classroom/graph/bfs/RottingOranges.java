package com.tutort.classroom.graph.bfs;

import com.tutort.LeetCodeURLConstant;

import java.util.LinkedList;
import java.util.Queue;

public class RottingOranges {

    public static void main(String[] args) {
        int[][] grid = {{2, 1, 1}, {1, 1, 0}, {0, 1, 1}};
        System.out.println(orangesRottingDFS(grid));
        System.out.println(orangesRottingBFS(grid));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.ROTTING_ORANGES);
    }

    //    ================================DFS=================================
    //  Time Complexity: O(M * N) --> Each cell in the grid is visited maximum twice.
    //  Space Complexity: O(M * N) --> Queue Size
    public static int orangesRottingDFS(int[][] grid) {
        int row = grid.length;
        int col = grid[0].length;

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (grid[i][j] == 2) {
                    dfs(grid, row, col, i, j, 2);
                }

            }
        }
        int minutes = 2;
        for (int[] rows : grid) {
            for (int cell : rows) {
                if (cell == 1) return -1;
                minutes = Math.max(minutes, cell);
            }
        }

        return minutes - 2;
    }

    private static void dfs(int[][] grid, int row, int col, int i, int j, int minute) {
        if (i < 0 || i > row - 1 || j < 0 || j > col - 1 || grid[i][j] == 0 || (1 < grid[i][j] && grid[i][j] < minute)) {
            return;
        } else {
            grid[i][j] = minute;
            dfs(grid, row, col, i - 1, j, minute + 1);
            dfs(grid, row, col, i + 1, j, minute + 1);
            dfs(grid, row, col, i, j - 1, minute + 1);
            dfs(grid, row, col, i, j + 1, minute + 1);
        }
    }

    //    ================================BFS=================================
    public static int orangesRottingBFS(int[][] grid) {
        if (grid == null || grid.length == 0)
            return 0;
        int rows = grid.length;
        int cols = grid[0].length;
        Queue<int[]> queue = new LinkedList<>();
        int count_fresh = 0;
        //Put the position of all rotten oranges in queue
        //count the number of fresh oranges
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (grid[i][j] == 2) {
                    queue.offer(new int[]{i, j});
                }
                if (grid[i][j] != 0) {
                    count_fresh++;
                }
            }
        }

        if (count_fresh == 0)
            return 0;
        int countMin = 0, cnt = 0;
        int dx[] = {0, 0, 1, -1};
        int dy[] = {1, -1, 0, 0};

        //bfs starting from initially rotten oranges
        while (!queue.isEmpty()) {
            int size = queue.size();
            cnt += size;
            for (int i = 0; i < size; i++) {
                int[] point = queue.poll();
                for (int j = 0; j < 4; j++) {
                    int x = point[0] + dx[j];
                    int y = point[1] + dy[j];

                    if (x < 0 || y < 0 || x >= rows || y >= cols || grid[x][y] == 0 || grid[x][y] == 2)
                        continue;

                    grid[x][y] = 2;
                    queue.offer(new int[]{x, y});
                }
            }
            if (queue.size() != 0) {
                countMin++;
            }
        }
        return count_fresh == cnt ? countMin : -1;
    }
}
