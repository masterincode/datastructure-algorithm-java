package com.tutort.classroom.graph.bfs;

import com.tutort.LeetCodeURLConstant;

import java.util.LinkedList;
import java.util.Queue;

public class SnakesAndLadders {

    public static void main(String[] args) {
        int[][] board = {{-1,-1,-1,-1,-1,-1},{-1,-1,-1,-1,-1,-1},{-1,-1,-1,-1,-1,-1},{-1,35,-1,-1,13,-1},{-1,-1,-1,-1,-1,-1},{-1,15,-1,-1,-1,-1}};
        System.out.println(snakesAndLadders(board));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.SNAKES_AND_LADDERS);
    }

    public static int snakesAndLadders(int[][] board) {
        int boardLength = board.length;
        int totalCellsInBoard = boardLength * boardLength;
        int noOfSteps = 0;
        Queue<Integer> queue = new LinkedList<>();
        boolean visited[][] = new boolean[boardLength][boardLength];

        queue.add(1); // 1. Square 1 added in queue
        visited[boardLength - 1][0] = true; // 2. Mark Square 1 as visited

        while (!queue.isEmpty()) {
            int size = queue.size();

            for (int i = 0; i < size; i++) {
                int squareNumber = queue.poll();

                // If you reach to END i.e last Square then you are done.
                if (squareNumber == totalCellsInBoard)
                    return noOfSteps;

                // Rolling Dice from 1 to 6
                for (int diceValue = 1; diceValue <= 6; diceValue++) {

                    if (diceValue + squareNumber > totalCellsInBoard)
                        break;

                    int pos[] = findCoordinates(diceValue + squareNumber, boardLength);
                    int row = pos[0];
                    int col = pos[1];

                    if (visited[row][col] == true)
                        continue;

                    visited[row][col] = true;
                    if (board[row][col] == -1) {
                        queue.add(diceValue + squareNumber);
                    } else {
                        queue.add(board[row][col]);
                    }
                }
            }
            noOfSteps++;
        }
        return -1;
    }

    public static int[] findCoordinates(int currentPosition, int boardLength) {
        int row = boardLength - (currentPosition - 1) / boardLength - 1;
        int col = (currentPosition - 1) % boardLength;
        if (row % 2 == boardLength % 2) {
            return new int[]{row, boardLength - 1 - col};
        } else {
            return new int[]{row, col};
        }

    }
}
