package com.tutort.classroom.graph.bfs;

import com.tutort.LeetCodeURLConstant;

import java.util.*;

public class WordLadderI {

    public static void main(String[] args) {
        String beginWord = "hit", endWord = "cog";
        List<String> wordList = Arrays.asList("hot","dot","dog","lot","log","cog");
        System.out.println(ladderLength(beginWord, endWord, wordList));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.WORD_LADDER_I);
    }


    // T.C and S.C details explanation: https://leetcode.com/problems/word-ladder/solution/

    // Time Complexity: O(M^2 * N)
    // Space Complexity: O(M^2 * N)
    public static int ladderLength(String beginWord, String endWord, List<String> wordList) {

        // Validation
        if (!wordList.contains(endWord))
            return 0;
        HashMap<String, Boolean> visitedMap = new HashMap<>();

        // 1. Initialising with default value. Here Node value is String due to which we opted HashMap
        for (int index = 0; index < wordList.size(); index++)
            visitedMap.put(wordList.get(index), false);

        Queue<String> queue = new LinkedList<>();

        //2. Added in queue
        queue.add(beginWord);
        visitedMap.put(beginWord, true);
        int graphLevel = 1;

        //3. While and Inside that for loop which will run depends on queue size
        while (!queue.isEmpty()) {
            int size = queue.size();

            for (int i = 0; i < size; i++) {
                String currentNode = queue.poll();
                if (currentNode.equals(endWord)) {
                    return graphLevel;
                }
                wordMatch(currentNode, visitedMap, queue);
            }
            graphLevel++;
        }
        return 0;
    }


    private static void wordMatch(String currentNode, HashMap<String, Boolean> visitedMap, Queue<String> queue) {

        for (int index = 0; index < currentNode.length(); index++) {
            char[] word = currentNode.toCharArray();

            for (int letter = 0; letter < 26; letter++) {

                char c = (char) ('a' + letter);
                word[index] = c;
                String newWord = String.valueOf(word);

                if (visitedMap.containsKey(newWord) && visitedMap.get(newWord) == false) {
                    queue.add(newWord);
                    visitedMap.put(newWord, true);
                }
            }
        }
    }

}
