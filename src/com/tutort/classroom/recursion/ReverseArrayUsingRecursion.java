package com.tutort.classroom.recursion;

import java.util.Arrays;

public class ReverseArrayUsingRecursion {

    public static void main(String[] args) {
        int[] arr1 = new int[]{1, 2, 3, 4, 5};
        int[] arr2 = new int[]{1, 2, 3, 4, 5, 6};
        reverse(arr1, 0, arr1.length - 1);
        System.out.println(Arrays.toString(arr1));
        System.out.println();
        reverse(arr2, 0, arr2.length - 1);
        System.out.println(Arrays.toString(arr2));
    }

    // TODO: Using 2 Pointers
    private static void reverse(int[] arr, int left, int right) {
        if (left >= right) {
            return;
        }
        int temp = arr[left];
        arr[left] = arr[right];
        arr[right] = temp;
        reverse(arr, left + 1, right - 1);
    }
}
