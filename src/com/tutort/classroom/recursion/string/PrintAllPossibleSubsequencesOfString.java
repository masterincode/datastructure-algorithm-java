package com.tutort.classroom.recursion.string;

import java.util.ArrayList;
import java.util.List;

public class PrintAllPossibleSubsequencesOfString {

    public static void main(String[] args) {
        printAllPossibleSubsequencesOfString("abc");
    }

    public static void printAllPossibleSubsequencesOfString(String str) {
        printAllPossibleSubsequencesOfStringUtil(str, 0, new ArrayList<>());
    }

    public static void printAllPossibleSubsequencesOfStringUtil(String str, int index, List<Character> pick) {
        if(index == str.length()) {
            System.out.println(pick);
            return;
        }
        //Include
        pick.add(str.charAt(index));
        printAllPossibleSubsequencesOfStringUtil(str, index + 1, pick);

        //Exclude
        pick.remove((Character) str.charAt(index));
        printAllPossibleSubsequencesOfStringUtil(str, index + 1, pick);
    }
}
