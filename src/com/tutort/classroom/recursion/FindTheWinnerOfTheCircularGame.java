package com.tutort.classroom.recursion;

import com.tutort.LeetCodeURLConstant;

import java.util.ArrayList;

public class FindTheWinnerOfTheCircularGame {

    public static void main(String[] args) {
        System.out.println(findTheWinner(5,2));
        System.out.println(findTheWinner(6,5));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.FIND_THE_WINNER_OF_THE_CIRCULAR_GAME);
    }


    public static int findTheWinner(int n, int k) {
        ArrayList<Integer> list = new ArrayList<>();
        for(int i = 1; i<=n; i++)
            list.add(i);
        return josephus(n, k, 0, list);
    }

    public static int josephus(int n, int k, int start, ArrayList<Integer> list){
        if(n > 1) {
            int curr = (start + k - 1 ) % n;
            list.remove(list.get(curr));
            josephus(n - 1, k, curr, list);
        }
        return list.get(0);
    }
}
