package com.tutort.classroom.recursion;

public class ReverseStringUsingRecursion {
    public static void main(String[] args) {
        String str = "123456";
        String result = "";
        System.out.println(reverse(str, 0));
    }

    public static String reverse(String str, int index) {
        if(index >= str.length())
            return "";
        return reverse(str, index + 1) + str.charAt(index);
    }
}
