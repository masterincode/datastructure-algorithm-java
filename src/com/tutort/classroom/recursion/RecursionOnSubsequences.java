package com.tutort.classroom.recursion;

import java.util.ArrayList;
import java.util.List;

public class RecursionOnSubsequences {

    public static void main(String[] args) {
        printSubsequences(new int[] {3,1,2}, 0, new ArrayList<>());
    }

    public static void printSubsequences(int[] arr, int index, List<Integer> list) {
        if(index >= arr.length) {
            System.out.println(list);
            return;
        }
        list.add(arr[index]);
        printSubsequences(arr, index + 1, list);
        list.remove((Integer) arr[index]);
        printSubsequences(arr, index + 1, list);
    }
}
