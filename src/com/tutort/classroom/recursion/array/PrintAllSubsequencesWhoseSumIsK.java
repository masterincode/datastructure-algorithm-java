package com.tutort.classroom.recursion.array;

import java.util.ArrayList;
import java.util.List;

public class PrintAllSubsequencesWhoseSumIsK {

    public static void main(String[] args) {
        print_All_Subsequences_Whose_Sum_Is_K(new int[]{1, 2, 1}, new ArrayList<>(), 0, 2, 0);
        System.out.println();
        System.out.println(printSubsequencesWhoseSumIsKApproach2(new int[]{1, 2, 1}, 2, new int[]{1, 2, 1}.length - 1 ));
        System.out.println(printSubsequencesWhoseSumIsKApproach2(new int[]{1, 2, 1}, 5, new int[]{1, 2, 1}.length - 1 ));
    }

    public static void print_All_Subsequences_Whose_Sum_Is_K(int[] arr, List<Integer> list, int sum, int targetSum, int index) {
        // Base condition
        if (index == arr.length) {
            if(sum == targetSum) {
                System.out.println(list);
                return;
            } else {
                return;
            }
        }
        // Include
        list.add(arr[index]);
        sum = sum + arr[index];
        print_All_Subsequences_Whose_Sum_Is_K(arr, list, sum, targetSum, index + 1);

        // Exclude
        list.remove((Integer) arr[index]);
        sum = sum - arr[index];
        print_All_Subsequences_Whose_Sum_Is_K(arr, list, sum, targetSum, index + 1);
    }



    public static boolean printSubsequencesWhoseSumIsKApproach2(int[] arr, int sum, int index) {
        // Base condition
        if (index < 0 ) {
            if (sum == 0) {
                return true;
            } else {
                return false;
            }
        }
        // Include
        boolean include = printSubsequencesWhoseSumIsKApproach2(arr, sum - arr[index], index - 1);

        // Exclude
        boolean exclude = printSubsequencesWhoseSumIsKApproach2(arr, sum, index - 1);
        return include || exclude;
    }
}
