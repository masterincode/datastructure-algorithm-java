package com.tutort.classroom.recursion.array;

import java.util.ArrayList;
import java.util.List;

public class PrintTotalNoWaysOfSubsequencesWhoseSumIsK {

    public static void main(String[] args) {
        System.out.println(print_Total_No_of_Ways_Subsequences_Whose_Sum_Is_K_Approach_1(new int[]{1, 2, 1}, new ArrayList<>(), 0, 2, 0));
        System.out.println();
        System.out.println(print_Total_No_of_Ways_Subsequences_Whose_Sum_Is_K_Approach_2(new int[]{1, 2, 1}, 2, new int[]{1, 2, 1}.length - 1 ));
        System.out.println(print_Total_No_of_Ways_Subsequences_Whose_Sum_Is_K_Approach_2(new int[]{1, 2, 1}, 5, new int[]{1, 2, 1}.length - 1 ));
    }

    // Starting from index = 0 to index = n - 1
    public static int print_Total_No_of_Ways_Subsequences_Whose_Sum_Is_K_Approach_1(int[] arr, List<Integer> list, int sum, int targetSum, int index) {
        // Base condition
        if (index == arr.length) {
            if(sum == targetSum) {
                System.out.println(list);
                return 1;
            } else {
                return 0;
            }
        }
        // Include
        list.add(arr[index]);
        sum = sum + arr[index];
        int include = print_Total_No_of_Ways_Subsequences_Whose_Sum_Is_K_Approach_1(arr, list, sum, targetSum, index + 1);

        // Exclude
        list.remove((Integer) arr[index]);
        sum = sum - arr[index];
        int exclude = print_Total_No_of_Ways_Subsequences_Whose_Sum_Is_K_Approach_1(arr, list, sum, targetSum, index + 1);
        //Total no of ways means add both include and exclude
        return include + exclude;
    }

    // Starting from index = n - 1 to index = 0
    public static int print_Total_No_of_Ways_Subsequences_Whose_Sum_Is_K_Approach_2(int[] arr, int sum, int index) {
        // Base condition
        if (index < 0 ) {
            if (sum == 0) {
                return 1;
            } else {
                return 0;
            }
        }
        // Include
        int include = print_Total_No_of_Ways_Subsequences_Whose_Sum_Is_K_Approach_2(arr, sum - arr[index], index - 1);

        // Exclude
        int exclude = print_Total_No_of_Ways_Subsequences_Whose_Sum_Is_K_Approach_2(arr, sum, index - 1);
        //Total no of ways means add both include and exclude
        return include + exclude;
    }
}
