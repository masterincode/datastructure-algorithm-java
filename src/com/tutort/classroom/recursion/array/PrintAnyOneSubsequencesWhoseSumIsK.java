package com.tutort.classroom.recursion.array;

import java.util.ArrayList;
import java.util.List;

public class PrintAnyOneSubsequencesWhoseSumIsK {

    public static void main(String[] args) {
        System.out.println(print_Any_One_Subsequence_Whose_Sum_Is_K(new int[]{1, 2, 1}, new ArrayList<>(), 0, 2, 0));
        System.out.println(print_Any_One_Subsequence_Whose_Sum_Is_K(new int[]{1, 2, 1}, new ArrayList<>(), 0, 6, 0));
        System.out.println();
        System.out.println(print_Any_One_Subsequence_Whose_Sum_Is_K_CodeRefactored(new int[]{1, 2, 1}, new ArrayList<>(), 0, 2, 0));
        System.out.println(print_Any_One_Subsequence_Whose_Sum_Is_K_CodeRefactored(new int[]{1, 2, 1}, new ArrayList<>(), 0, 6, 0));
    }

    public static boolean print_Any_One_Subsequence_Whose_Sum_Is_K(int[] arr, List<Integer> list, int sum, int targetSum, int index) {
        // Base condition
        if (index == arr.length) {
            if(sum == targetSum) {
                System.out.println(list);
                return true;
            } else {
                return false;
            }
        }
        // Include
        list.add(arr[index]);
        sum = sum + arr[index];
        boolean include = print_Any_One_Subsequence_Whose_Sum_Is_K(arr, list, sum, targetSum, index + 1);
        if(include) {
            return true;
        }

        // Exclude
        list.remove((Integer) arr[index]);
        sum = sum - arr[index];
        boolean exclude = print_Any_One_Subsequence_Whose_Sum_Is_K(arr, list, sum, targetSum, index + 1);
        if(exclude) {
            return true;
        }
        return false;
    }

    public static boolean print_Any_One_Subsequence_Whose_Sum_Is_K_CodeRefactored(int[] arr, List<Integer> list, int sum, int targetSum, int index) {
        // Base condition
        if (index == arr.length) {
            if(sum == targetSum) {
                System.out.println(list);
                return true;
            } else {
                return false;
            }
        }
        // Include
        list.add(arr[index]);
        boolean include = print_Any_One_Subsequence_Whose_Sum_Is_K_CodeRefactored(arr, list, sum + arr[index], targetSum, index + 1);

        // Exclude
        list.remove((Integer) arr[index]);
        boolean exclude = print_Any_One_Subsequence_Whose_Sum_Is_K_CodeRefactored(arr, list, sum, targetSum, index + 1);

        return include || exclude;
    }

}
