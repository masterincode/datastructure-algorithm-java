package com.tutort.classroom.algorithms.backtracking;

import com.tutort.LeetCodeURLConstant;

public class SudokuSolver {

    public static void main(String[] args) {

        char[][] board = {
                {'9', '5', '7', '.', '1', '3', '.', '8', '4'},
                {'4', '8', '3', '.', '5', '7', '1', '.', '6'},
                {'.', '1', '2', '.', '4', '9', '5', '3', '7'},
                {'1', '7', '.', '3', '.', '4', '9', '.', '2'},
                {'5', '.', '4', '9', '7', '.', '3', '6', '.'},
                {'3', '.', '9', '5', '.', '8', '7', '.', '1'},
                {'8', '4', '5', '7', '9', '.', '6', '1', '3'},
                {'.', '9', '1', '.', '3', '6', '.', '7', '5'},
                {'7', '.', '6', '1', '8', '5', '4', '.', '9'}
        };
        solveSudoku(board);

        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++)
                System.out.print(board[i][j] + " ");
            System.out.println();
        }
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.SUDOKU_SOLVER);
    }

    // Time Complexity: O(9(n ^ 2)), in the worst case, for each cell in the n^2 board, we have 9 possible numbers.
    // Space Complexity: O(1),       since we are refilling the given board itself, there is no extra space required, so constant space complexity.
    public static void solveSudoku(char[][] board) {
        solve(board);
    }

    public static boolean solve(char[][] board) {
        for (int row = 0; row < 9; row++) {
            for (int col = 0; col < 9; col++) {
                if (board[row][col] == '.') {

                    for (char number = '1'; number <= '9'; number++) {
                        if (isValid(board, row, col, number)) {
                            board[row][col] = number;

                            if (solve(board)) // Recursion
                                return true;
                            else
                                board[row][col] = '.'; // Backtracking [Rollback]
                        }
                    }

                    return false;
                }
            }
        }
        return true;
    }

    public static boolean isValid(char[][] board, int row, int col, char number) {
        for (int i = 0; i < 9; i++) {

            // 1. Column is fixed, row will change
            if (board[i][col] == number)
                return false;

            // 2. Row is fixed, column will change
            if (board[row][i] == number)
                return false;

            // 3. This is for 3 * 3 block
            int blockRow = 3 * (row / 3) + i / 3;
            int blockColumn = 3 * (col / 3) + i % 3;

            if (board[blockRow][blockColumn] == number)
                return false;
        }
        return true;
    }
}
