package com.tutort.classroom.algorithms.backtracking;

import com.tutort.LeetCodeURLConstant;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class NQueens {

    public static void main(String[] args) {
        System.out.println(solveNQueens(4));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.N_QUEENS);
    }

    // Time Complexity: O (N! * N)
    // Space Complexity: O(N^2)
    public static List<List<String>> solveNQueens(int n) {
        char[][] board = new char[n][n];
        for (char[] eachRow: board) {
            Arrays.fill(eachRow, '.');
        }
        List<List<String>> result = new ArrayList<>();
        dfs(0, board, result);
        return result;
    }

    private static void dfs(int column, char[][] board, List<List<String>> result) {

        // Base Case
        if (column == board.length) {
            // Prepare Possible Solutions
            result.add(preparePossibleSolution(board));
            return;
        }

        for (int row = 0; row < board.length; row++) {
            if (isSafe(board, row, column)) {
                board[row][column] = 'Q';
                dfs(column + 1, board, result);
                board[row][column] = '.';
            }
        }
    }

    private static boolean isSafe(char[][] board, int row, int col) {
        int initialRowValue = row;
        int initialColumnValue = col;

        // 1. Moving and Checking in North East (Diagonal) Direction of Chess Board
        while (row >= 0 && col >= 0) {
            if (board[row][col] == 'Q') {
                return false;
            }
            // To move diagonal Right
            row--; // Row will decrease
            col--; // Col will decrease
        }

        row = initialRowValue;
        col = initialColumnValue;

        // 2. Moving and Checking in East (Horizontal) Direction of Chess Board
        while (col >= 0) {
            if (board[row][col] == 'Q') {
                return false;
            }
            col--; // Row will remain same, only column will decrease to move in EAST direction.
        }

        row = initialRowValue;
        col = initialColumnValue;

        // 1. Moving and Checking in South East (Diagonal) Direction of Chess Board
        while (col >= 0 && row < board.length) {
            if (board[row][col] == 'Q') {
                return false;
            }
            // To move diagonal Left
            row++; // Row will increase
            col--; // Col will decrease
        }
        return true;
    }

    private static List<String> preparePossibleSolution(char[][] board) {
        List<String> res = new LinkedList<>();
        for (int i = 0; i < board.length; i++) {
            String s = new String(board[i]);
            res.add(s);
        }
        return res;
    }

}
