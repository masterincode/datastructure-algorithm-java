package com.tutort.classroom.algorithms.backtracking;

import com.tutort.LeetCodeURLConstant;

import java.util.ArrayList;
import java.util.List;

public class PermutationsOfGivenNumber {

    public static void main(String[] args) {
        System.out.println(permute(new int[]{1, 2, 3}));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.PERMUTATIONS_OF_A_GIVEN_NUMBER);
    }

    public static List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> ans = new ArrayList<>();
        dfs(0, nums, ans);
        return ans;
    }

    private static void dfs(int index, int[] nums, List<List<Integer>> ans) {

        // 1. Base case
        if (index == nums.length) {
            // copy the ds to ans
            List<Integer> ds = new ArrayList<>();
            for (int i = 0; i < nums.length; i++) {
                ds.add(nums[i]);
            }
            ans.add(new ArrayList<>(ds));
            return; //
        }
        for (int i = index; i < nums.length; i++) {
            swap(i, index, nums);
            dfs(index + 1, nums, ans);
            swap(i, index, nums);
        }
    }

    private static void swap(int i, int j, int[] nums) {
        int t = nums[i];
        nums[i] = nums[j];
        nums[j] = t;
    }
}
