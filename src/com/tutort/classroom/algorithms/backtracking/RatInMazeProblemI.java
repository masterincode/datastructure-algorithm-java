package com.tutort.classroom.algorithms.backtracking;

import com.tutort.LeetCodeURLConstant;

import java.util.ArrayList;

public class RatInMazeProblemI {

    public static void main(String[] args) {
        int n = 4;
        int[][] a = {{1, 0, 0, 0}, {1, 1, 0, 1}, {1, 1, 0, 0}, {0, 1, 1, 1}};
        System.out.println(findPath(a, n));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.RAT_IN_A_MAZE_PROBLEM_I);
    }

//    Time Complexity: O(4^(m*n)), because on every cell we need to try 4 different directions.
//    Space Complexity:  O(m*n) ,Maximum Depth of the recursion tree(auxiliary space).
    public static ArrayList<String> findPath(int[][] m, int n) {
        int[][] visited = new int[n][n];
        //                     D, L, R, U
        int[] rowDirection = {+1, 0, 0,-1};
        int[] colDirection = { 0,-1, 1, 0};
        ArrayList<String> result = new ArrayList<>();
        if (m[0][0] == 1) {
            dfs(0, 0, m, n, result, "", visited, rowDirection, colDirection);
        }
        return result;
    }

    private static void dfs(int row, int col, int[][] grid, int n, ArrayList<String> ans, String move,
                            int visited[][], int rowDirection[], int colDirection[]) {
        if (row == n - 1 && col == n - 1) {
            ans.add(move);
            return;
        }
        String dir = "DLRU";
        for (int index = 0; index < 4; index++) {
            int rowNext = row + rowDirection[index];
            int colNext = col + colDirection[index];
            if (rowNext >= 0 && colNext >= 0 && rowNext < n && colNext < n && visited[rowNext][colNext] == 0 && grid[rowNext][colNext] == 1) {

                visited[row][col] = 1;
                dfs(rowNext, colNext, grid, n, ans, move + dir.charAt(index), visited, rowDirection, colDirection);
                visited[row][col] = 0;

            }
        }
    }


}
