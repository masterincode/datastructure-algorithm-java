package com.tutort.classroom.algorithms.backtracking;

import com.tutort.LeetCodeURLConstant;

import java.util.*;

public class PermutationsOfGivenString {

    public static void main(String[] args) {
        System.out.println(find_permutation("ABB"));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.PERMUTATIONS_OF_A_GIVEN_STRING);
    }

    public static List<String> find_permutation(String S) {
        return permute(S.toCharArray());
    }

    private static List<String> permute(char[] character) {
        Set<String> ans = new TreeSet<>();
        dfs(0, character, ans);
        return  new ArrayList<>(ans);
    }

    private static void dfs(int index, char[] character, Set<String> ans) {

        // 1. Base case
        if (index == character.length) {
            // copy the ds to ans
            String result = "";
            for (int i = 0; i < character.length; i++) {
                result = result + character[i];
            }
            ans.add(result);
            return; //
        }
        for (int i = index; i < character.length; i++) {
            swap(i, index, character);
            dfs(index + 1, character, ans);
            swap(i, index, character);
        }
    }

    private static void swap(int i, int j, char[] character) {
        char ch = character[i];
        character[i] = character[j];
        character[j] = ch;
    }

}
