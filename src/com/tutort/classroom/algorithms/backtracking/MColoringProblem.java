package com.tutort.classroom.algorithms.backtracking;

import com.tutort.LeetCodeURLConstant;

import java.util.Arrays;

public class MColoringProblem {

    public static void main(String[] args) {
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.M_COLORING_PROBLEM);
    }


    public boolean graphColoring(boolean graph[][], int m, int n) {
        // int m : Number of colours
        // int n : Number of nodes
        int[] colorUsed = new int[n];
        Arrays.fill(colorUsed, -1);
        return dfs(0, graph, m, n, colorUsed);
    }

    public boolean dfs(int node, boolean graph[][], int m, int V, int[] colorUsed) {
        // if all vertices have been assigned colour then we return true.
        if (node == V) {
            return true;
        }
        for (int colour = 0; colour < m; colour++) {
            // checking if this colour can be given to a particular node.
            if (isSafe(graph, node, colour, colorUsed)) {
                // assigning colour to the node.
                colorUsed[node] = colour;

                // calling function recursively and checking if other nodes can be assigned other colours.
                if (dfs(node + 1, graph, m, V, colorUsed)) {
                    // returning true if the current allocation was successful.
                    return true;
                }
                // if not true, we backtrack and remove the colour for that node.
                colorUsed[node] = -1;
            }
        }
        // if no colour can be assigned, we return false.
        return false;
    }

    public boolean isSafe(boolean graph[][], int node, int assigncolor, int[] colorUsed) {
        for (int i = 0; i < graph.length; i++) {
            if (graph[node][i] && colorUsed[i] == assigncolor) {
                return false;
            }
        }
        // returning true if no connected node has same colour.
        return true;
    }
}
