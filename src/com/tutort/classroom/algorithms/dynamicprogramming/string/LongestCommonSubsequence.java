package com.tutort.classroom.algorithms.dynamicprogramming.string;

import com.tutort.LeetCodeURLConstant;

import java.util.Arrays;

public class LongestCommonSubsequence {

    public static void main(String[] args) {
        System.out.println(longestCommonSubsequenceBrute("abcde", "ace"));
        System.out.println(longestCommonSubsequenceBetter("abcde", "ace"));
        System.out.println(longestCommonSubsequenceOptimised("abcde", "ace"));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.LONGEST_COMMON_SUBSEQUENCE);
    }

    // TODO: Recursive Approach
    //  Time Complexity: O (2^n)
    public static int longestCommonSubsequenceBrute(String text1, String text2) {
        return longestCommonSubsequenceBruteUtil(text1, text2, text1.length() - 1, text2.length() - 1);
    }

    private static int longestCommonSubsequenceBruteUtil(String text1, String text2, int index1, int index2) {
        if (index1 < 0 || index2 < 0) {
            return 0;
        }
        // abcde  ace
        //     i    j
        if (text1.charAt(index1) == text2.charAt(index2)) {
            return 1 + longestCommonSubsequenceBruteUtil(text1, text2, index1 - 1, index2 - 1);
        }
        // abcde  ace
        //    i    j
        int pick1 = longestCommonSubsequenceBruteUtil(text1, text2, index1 - 1, index2); // abc | ac
        int pick2 = longestCommonSubsequenceBruteUtil(text1, text2, index1, index2 - 1); // abcd | a
        return Math.max(pick1, pick2);
    }

    // TODO: Recursive Approach with Memoization
    //  Time Complexity: O (N * M)
    //  Space Complexity: O(N * M) + O(N + M)
    public static int longestCommonSubsequenceBetter(String text1, String text2) {
        int[][] dp = new int[text1.length()][text2.length()];
        for (int rows[] : dp) {
            Arrays.fill(rows, -1);
        }
        return longestCommonSubsequenceBetterUtil(text1, text2, text1.length() - 1, text2.length() - 1, dp);
    }

    private static int longestCommonSubsequenceBetterUtil(String text1, String text2, int index1, int index2, int[][] dp) {
        if (index1 < 0 || index2 < 0) {
            return 0;
        }
        if (dp[index1][index2] != -1) {
            return dp[index1][index2];
        }
        if (text1.charAt(index1) == text2.charAt(index2)) {
            dp[index1][index2] = 1 + longestCommonSubsequenceBetterUtil(text1, text2, index1 - 1, index2 - 1, dp);
            return dp[index1][index2];
        } else {
            int pick1 = longestCommonSubsequenceBetterUtil(text1, text2, index1 - 1, index2, dp);
            int pick2 = longestCommonSubsequenceBetterUtil(text1, text2, index1, index2 - 1, dp);
            dp[index1][index2] = Math.max(pick1, pick2);
            return dp[index1][index2];
        }
    }

    // TODO: DP Solutions
    //  Time Complexity: O(N * M)
    //  Space Complexity: O(N * M)
    public static int longestCommonSubsequenceOptimised(String text1, String text2) {
        int n = text1.length();
        int m = text2.length();

        int dp[][] = new int[n + 1][m + 1];
        for (int rows[] : dp)
            Arrays.fill(rows, -1);

        for (int i = 0; i <= n; i++) {
            dp[i][0] = 0;
        }
        for (int i = 0; i <= m; i++) {
            dp[0][i] = 0;
        }

        for (int index1 = 1; index1 <= n; index1++) {
            for (int index2 = 1; index2 <= m; index2++) {
                if (text1.charAt(index1 - 1) == text2.charAt(index2 - 1)) {
                    dp[index1][index2] = 1 + dp[index1 - 1][index2 - 1];
                } else {
                    int pick1 = dp[index1 - 1][index2];
                    int pick2 = dp[index1][index2 - 1];
                    dp[index1][index2] = 0 + Math.max(pick1, pick2);
                }
            }
        }
        return dp[n][m];
    }
}
