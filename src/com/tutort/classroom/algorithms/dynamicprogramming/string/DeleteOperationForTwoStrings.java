package com.tutort.classroom.algorithms.dynamicprogramming.string;

import com.tutort.LeetCodeURLConstant;

import java.util.Arrays;

public class DeleteOperationForTwoStrings {

    public static void main(String[] args) {
        System.out.println(deleteOperationForTwoStringsBrute("sea", "eat"));
        System.out.println(deleteOperationForTwoStringsBetter("sea", "eat"));
        System.out.println(deleteOperationForTwoStringsOptimised("sea", "eat"));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.DELETE_OPERATION_FOR_TWO_STRINGS);
    }

    // TODO: Recursive Approach
    //  Time Complexity: O (2^n)
    public static int deleteOperationForTwoStringsBrute(String word1, String word2) {
        int n = word1.length();
        int m = word2.length();
        int ans = deleteOperationForTwoStringsBruteUtil(word1, word2, n - 1, m - 1);
        return (n + m) - (ans * 2);
    }

    private static int deleteOperationForTwoStringsBruteUtil(String word1, String word2, int index1, int index2) {
        if (index1 < 0 || index2 < 0) {
            return 0;
        }
        // abcde  ace
        //     i    j
        if (word1.charAt(index1) == word2.charAt(index2)) {
            return 1 + deleteOperationForTwoStringsBruteUtil(word1, word2, index1 - 1, index2 - 1);
        }
        // abcde  ace
        //    i    j
        int pick1 = deleteOperationForTwoStringsBruteUtil(word1, word2, index1 - 1, index2); // abc | ac
        int pick2 = deleteOperationForTwoStringsBruteUtil(word1, word2, index1, index2 - 1); // abcd | a
        return Math.max(pick1, pick2);
    }

    // TODO: Recursive Approach with Memoization
    //  Time Complexity: O (N * M)
    //  Space Complexity: O(N * M) + O(N + M)
    public static int deleteOperationForTwoStringsBetter(String word1, String word2) {
        int[][] dp = new int[word1.length()][word2.length()];
        for (int rows[] : dp) {
            Arrays.fill(rows, -1);
        }
        int n = word1.length();
        int m = word2.length();
        int ans = deleteOperationForTwoStringsBetterUtil(word1, word2, n - 1, m - 1, dp);
        return (n + m) - (ans * 2);
    }

    private static int deleteOperationForTwoStringsBetterUtil(String word1, String word2, int index1, int index2, int[][] dp) {
        if (index1 < 0 || index2 < 0) {
            return 0;
        }
        if (dp[index1][index2] != -1) {
            return dp[index1][index2];
        }
        if (word1.charAt(index1) == word2.charAt(index2)) {
            dp[index1][index2] = 1 + deleteOperationForTwoStringsBetterUtil(word1, word2, index1 - 1, index2 - 1, dp);
            return dp[index1][index2];
        } else {
            int pick1 = deleteOperationForTwoStringsBetterUtil(word1, word2, index1 - 1, index2, dp);
            int pick2 = deleteOperationForTwoStringsBetterUtil(word1, word2, index1, index2 - 1, dp);
            dp[index1][index2] = Math.max(pick1, pick2);
            return dp[index1][index2];
        }
    }

    // TODO: DP Solutions
    //  Time Complexity: O(N * M)
    //  Space Complexity: O(N * M)
    public static int deleteOperationForTwoStringsOptimised(String word1, String word2) {
        int n = word1.length();
        int m = word2.length();

        int dp[][] = new int[n + 1][m + 1];
        for (int rows[] : dp)
            Arrays.fill(rows, -1);

        for (int i = 0; i <= n; i++) {
            dp[i][0] = 0;
        }
        for (int i = 0; i <= m; i++) {
            dp[0][i] = 0;
        }

        for (int index1 = 1; index1 <= n; index1++) {
            for (int index2 = 1; index2 <= m; index2++) {
                if (word1.charAt(index1 - 1) == word2.charAt(index2 - 1)) {
                    dp[index1][index2] = 1 + dp[index1 - 1][index2 - 1];
                } else {
                    int pick1 = dp[index1 - 1][index2];
                    int pick2 = dp[index1][index2 - 1];
                    dp[index1][index2] = 0 + Math.max(pick1, pick2);
                }
            }
        }
        int len = n + m;
        return len - 2 * dp[n][m];
    }
}
