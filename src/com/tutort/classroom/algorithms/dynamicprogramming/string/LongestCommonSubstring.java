package com.tutort.classroom.algorithms.dynamicprogramming.string;

import com.tutort.LeetCodeURLConstant;

public class LongestCommonSubstring {

    public static void main(String[] args) {
        System.out.println(longestCommonSubstring("abdca","cbda"));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.LONGEST_COMMON_SUBSTRING);
    }

    public static int longestCommonSubstring(String str1, String str2) {
        int n = str1.length();
        int m = str2.length();
        int[][] dp = new int[n + 1][m + 1];
        int ans = 0;
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= m; j++) {
                if (str1.charAt(i - 1) == str2.charAt(j - 1)) {
                    int val = 1 + dp[i - 1][j - 1];
                    dp[i][j] = val;
                    ans = Math.max(ans, val);
                } else
                    dp[i][j] = 0;
            }
        }
        return ans;
    }
}
