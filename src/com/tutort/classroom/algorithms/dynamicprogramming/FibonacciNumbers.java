package com.tutort.classroom.algorithms.dynamicprogramming;

import java.util.Arrays;

public class FibonacciNumbers {

    public static void main(String[] args) {
        int n = 5;
        int[] dp = new int[n];
        Arrays.fill(dp, -1);
        System.out.println(fibonacciNumbersBrute(n - 1));
        System.out.println(fibonacciNumbersBetter1(dp, n-1));
        System.out.println(fibonacciNumbersBetter2(dp, n-1));
        System.out.println(fibonacciNumbersOptmised(n - 1));
    }

    // TODO: Recursive Approach
    //  Time Complexity: O (2^n) exponential time complexity
    public static int fibonacciNumbersBrute(int n) {
        if (n <= 1) {
            return n;
        }
        return fibonacciNumbersBrute(n - 1) + fibonacciNumbersBrute(n - 2);
    }

    // TODO: Recursive Approach using Memoization
    //  Time Complexity: O (n)
    //  Space Complexity: O (n)
    public static int fibonacciNumbersBetter1(int[] dp, int n) {
        if (n <= 1)
            return n;

        if (dp[n] != -1)
            return dp[n];

        return dp[n] = fibonacciNumbersBetter1(dp, n - 1) + fibonacciNumbersBetter1(dp, n - 2);
    }

    // TODO: DP Solutions
    //  Time Complexity: O (n)
    //  Space Complexity: O (n)
    public static int fibonacciNumbersBetter2(int[] dp, int n) {
        dp[0]= 0;
        dp[1]= 1;

        for(int i=2; i<=n; i++){
            dp[i] = dp[i-1]+ dp[i-2];
        }
        return dp[n];
    }

    // TODO: DP Solutions with Space Optimisation
    //  Time Complexity: O (n)
    //  Space Complexity: O (1)
    public static int fibonacciNumbersOptmised(int n) {
        int prev2 = 0;
        int prev = 1;

        for(int i=2; i<=n; i++){
            int cur_i = prev2+ prev;
            prev2 = prev;
            prev= cur_i;
        }
        return prev;
    }
}
