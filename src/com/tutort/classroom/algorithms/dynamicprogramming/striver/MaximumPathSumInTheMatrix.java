package com.tutort.classroom.algorithms.dynamicprogramming.striver;

import com.tutort.LeetCodeURLConstant;

import java.util.Arrays;

public class MaximumPathSumInTheMatrix {

    public static void main(String[] args) {
        int[][] matrix = {{1, 2, 10, 4}, {100, 3, 2, 1}, {1, 1, 20, 2}, {1, 2, 2, 1}};
        System.out.println(getMaxPathSumBrute(matrix));
        System.out.println(getMaxPathSumBetter(matrix));
        System.out.println(getMaxPathSumOptmised(matrix));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.MAXIMUM_PATH_SUM_IN_THE_MATRIX);
    }

    // TODO: Top Down Approach [Time Limit Exceeded]
    //  Time Complexity: O (n^2)
    public static int getMaxPathSumBrute(int[][] matrix) {
        int max = Integer.MIN_VALUE;
        for (int j = matrix[0].length - 1; j >= 0; j--) {
            int temp = getMaxPathSumBruteUtil(matrix, matrix.length - 1, j);
            max = Math.max(max, temp);
        }
        return max;
    }

    private static int getMaxPathSumBruteUtil(int[][] matrix, int row, int col) {
        if (row < 0 || col < 0 || col == matrix[0].length) {
            return Integer.MIN_VALUE;
        }
        if (row == 0 && col < matrix[0].length) {
            return matrix[row][col];
        }
        int up = getMaxPathSumBruteUtil(matrix, row - 1, col);
        int upRight = getMaxPathSumBruteUtil(matrix, row - 1, col + 1);
        int upLeft = getMaxPathSumBruteUtil(matrix, row - 1, col - 1);
        return matrix[row][col] + Math.max(up, Math.max(upLeft, upRight));
    }

    // TODO: Top Down Approach
    //  Time Complexity: O(n * m)
    //  Space Complexity: O(n * m)
    public static int getMaxPathSumBetter(int[][] matrix) {
        int[][] dp = new int[matrix.length][matrix[0].length];
        for (int[] arr : dp) {
            Arrays.fill(arr, -1);
        }
        int max = Integer.MIN_VALUE;
        for (int j = matrix[0].length - 1; j >= 0; j--) {
            int temp = getMaxPathSumBetterUtil(matrix, matrix.length - 1, j, dp);
            max = Math.max(max, temp);
        }
        return max;
    }
    private static int getMaxPathSumBetterUtil(int[][] matrix, int row, int col, int[][] dp) {
        if (row < 0 || col < 0 || col == matrix[0].length) {
            return Integer.MIN_VALUE;
        }
        if (row == 0 && col < matrix[0].length) {
            return dp[row][col] = matrix[row][col];
        }
        if (dp[row][col] != -1) {
            return dp[row][col];
        }
        int up = getMaxPathSumBetterUtil(matrix, row - 1, col, dp);
        int upRight = getMaxPathSumBetterUtil(matrix, row - 1, col + 1, dp);
        int upLeft = getMaxPathSumBetterUtil(matrix, row - 1, col - 1, dp);
        return dp[row][col] = matrix[row][col] + Math.max(up, Math.max(upLeft, upRight));
    }

    // TODO: DP Solution
    //  Time Complexity: O(n * m)
    //  Space Complexity: O(n * m)
    public static int getMaxPathSumOptmised(int[][] matrix) {
        int n = matrix.length;
        int m = matrix[0].length;
        int[][] dp = new int[n][m];
        for (int j = 0; j < m; j++) {
            dp[0][j] = matrix[0][j];
        }

        for (int row = 1; row < n; row++) {
            for (int col = 0; col < m; col++) {
                int up = dp[row - 1][col];
                int upLeft = Integer.MIN_VALUE;
                if (col - 1 >= 0) {
                    upLeft = dp[row - 1][col - 1];
                }
                int upRight = Integer.MIN_VALUE;
                if (col + 1 < m) {
                    upRight = dp[row - 1][col + 1];
                }
                dp[row][col] = matrix[row][col] + Math.max(up, Math.max(upLeft, upRight));
            }
        }
        int max = Integer.MIN_VALUE;
        for (int j = 0; j < m; j++) {
             max = Math.max(max, dp[n - 1][j]);
        }
        return max;
    }
}
