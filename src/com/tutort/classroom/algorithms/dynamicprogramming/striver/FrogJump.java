package com.tutort.classroom.algorithms.dynamicprogramming.striver;

import com.tutort.LeetCodeURLConstant;

import java.util.Arrays;

public class FrogJump {

    public static void main(String[] args) {
        int[] heights = {10, 20, 30, 10};
        System.out.println(frogJumpBrute(heights.length, heights));
        System.out.println(frogJumpBetter(heights.length, heights));
        System.out.println(frogJumpOptimised1(heights.length, heights));
        System.out.println(frogJumpOptimised2(heights.length, heights));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.FROG_JUMP);
    }

    // TODO: Recursive Approach
    //  Time Complexity: O (2^n)
    public static int frogJumpBrute(int n, int heights[]) {
        return frogJumpBruteUtil(heights, heights.length - 1);
    }

    private static int frogJumpBruteUtil(int heights[], int index) {
        if (index == 0) {
            return 0;
        }
        int jumpOne = frogJumpBruteUtil(heights, index - 1) + Math.abs(heights[index] - heights[index - 1]);
        int jumpTwo = Integer.MAX_VALUE;
        if (index > 1) {
            jumpTwo = frogJumpBruteUtil(heights, index - 2) + Math.abs(heights[index] - heights[index - 2]);
        }
        return Math.min(jumpOne, jumpTwo);
    }

    // TODO: Recursive Approach with Memoization
    //  Time Complexity: O (N)
    //  Space Complexity: O(N)
    public static int frogJumpBetter(int n, int heights[]) {
        int[] dp = new int[n];
        Arrays.fill(dp, -1);
        return frogJumpBetterUtil(heights, heights.length - 1, dp);
    }

    private static int frogJumpBetterUtil(int heights[], int index, int[] dp) {
        if (index == 0) {
            return 0;
        }
        if (dp[index] != -1) {
            return dp[index];
        }
        int jumpOne = frogJumpBetterUtil(heights, index - 1, dp) + Math.abs(heights[index] - heights[index - 1]);
        int jumpTwo = Integer.MAX_VALUE;
        if (index > 1) {
            jumpTwo = frogJumpBetterUtil(heights, index - 2, dp) + Math.abs(heights[index] - heights[index - 2]);
        }
        return dp[index] = Math.min(jumpOne, jumpTwo);
    }

    // TODO: DP Solution
    //  Time Complexity: O(N)
    //  Space Complexity: O(N)
    public static int frogJumpOptimised1(int n, int heights[]) {
        int[] dp = new int[n];
        dp[0] = 0;
        for (int index = 1; index < heights.length; index++) {
            int jumpOne = dp[index - 1] + Math.abs(heights[index] - heights[index - 1]);
            int jumpTwo = Integer.MAX_VALUE;
            if (index > 1) {
                jumpTwo = dp[index - 2] + Math.abs(heights[index] - heights[index - 2]);
            }
            dp[index] = Math.min(jumpOne, jumpTwo);
        }
        return dp[n - 1];
    }

    // TODO: DP Solution
    //  Time Complexity: O(N)
    //  Space Complexity: O(1)
    public static int frogJumpOptimised2(int n, int heights[]) {
        int[] dp = new int[n];
        dp[0] = 0;
        int curr = 0;
        int prev1 = 0;
        int prev2 = 0;
        for (int index = 1; index < heights.length; index++) {
            int jumpOne = prev1 + Math.abs(heights[index] - heights[index - 1]);
            int jumpTwo = Integer.MAX_VALUE;
            if (index > 1) {
                jumpTwo = prev2 + Math.abs(heights[index] - heights[index - 2]);
            }
            curr = Math.min(jumpOne, jumpTwo);
            prev2 = prev1;
            prev1 = curr;
        }
        return curr;
    }
}
