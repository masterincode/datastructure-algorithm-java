package com.tutort.classroom.algorithms.dynamicprogramming.striver;

import com.tutort.LeetCodeURLConstant;

import java.util.Arrays;

public class UniquePathsII {

    public static void main(String[] args) {
        int[][] arr1 = {{0, 0, 0}, {0, 1, 0}, {0, 0, 0}};
        System.out.println(uniquePathsWithObstaclesBrute(arr1));
        System.out.println(uniquePathsWithObstaclesBetter(arr1));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.UNIQUE_PATHS_II);
    }

    public static int uniquePathsWithObstaclesBrute(int[][] obstacleGrid) {
        return uniquePathsWithObstaclesBruteUtil(obstacleGrid, 0, 0);
    }

    private static int uniquePathsWithObstaclesBruteUtil(int[][] obstacleGrid, int i, int j) {
        int n = obstacleGrid.length;
        int m = obstacleGrid[0].length;
        if (i < n && j < m && obstacleGrid[i][j] == 1) {
            return 0;
        }
        if (i == (n - 1) && j == (m - 1))
            return 1;
        if (i >= n || j >= m) {
            return 0;
        }
        int downSide = uniquePathsWithObstaclesBruteUtil(obstacleGrid, i + 1, j);
        int rightSide = uniquePathsWithObstaclesBruteUtil(obstacleGrid, i, j + 1);
        return downSide + rightSide;
    }


    public static int uniquePathsWithObstaclesBetter(int[][] obstacleGrid) {
        int[][] dp = new int[obstacleGrid.length][obstacleGrid[0].length];
        for (int[] arr : dp) {
            Arrays.fill(arr, -1);
        }
        return uniquePathsWithObstaclesBetterUtil(obstacleGrid, 0, 0, dp);
    }

    private static int uniquePathsWithObstaclesBetterUtil(int[][] obstacleGrid, int i, int j, int[][] dp) {
        int n = obstacleGrid.length;
        int m = obstacleGrid[0].length;
        if (i < n && j < m && obstacleGrid[i][j] == 1) {
            return 0;
        }
        if (i == (n - 1) && j == (m - 1))
            return 1;
        if (i >= n || j >= m) {
            return 0;
        }
        if (dp[i][j] != -1) {
            return dp[i][j];
        }
        int downSide = uniquePathsWithObstaclesBetterUtil(obstacleGrid, i + 1, j, dp);
        int rightSide = uniquePathsWithObstaclesBetterUtil(obstacleGrid, i, j + 1, dp);
        return dp[i][j] = downSide + rightSide;
    }

}
