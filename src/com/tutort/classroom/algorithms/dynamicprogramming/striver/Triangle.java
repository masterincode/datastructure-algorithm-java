package com.tutort.classroom.algorithms.dynamicprogramming.striver;

import com.tutort.LeetCodeURLConstant;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Triangle {

    public static void main(String[] args) {
        List<List<Integer>> triangle = new ArrayList<>();
        triangle.add(new ArrayList<>(Arrays.asList(2)));
        triangle.add(new ArrayList<>(Arrays.asList(3, 4)));
        triangle.add(new ArrayList<>(Arrays.asList(6, 5, 7)));
        triangle.add(new ArrayList<>(Arrays.asList(4, 1, 8, 3)));
        System.out.println(minimumTotalBrute(triangle));
        System.out.println(minimumTotalBetter(triangle));
        System.out.println(minimumTotalOptimised(triangle));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.TRIANGLE);
    }

    // TODO: Top Down Approach [Time Limit Exceeded]
    //  Time Complexity: O (n^2)
    public static int minimumTotalBrute(List<List<Integer>> triangle) {
        return minimumTotalBruteUtil(triangle, 0, 0);
    }

    private static int minimumTotalBruteUtil(List<List<Integer>> triangle, int i, int j) {
        if (i == triangle.size() - 1) {
            return triangle.get(i).get(j);
        }
        int downSide = triangle.get(i).get(j) + minimumTotalBruteUtil(triangle, i + 1, j);
        int diagonal = triangle.get(i).get(j) + minimumTotalBruteUtil(triangle, i + 1, j + 1);
        return Math.min(downSide, diagonal);
    }

    // TODO: Top Down Approach
    //  Time Complexity: O(n * n)
    //  Space Complexity: O(n * n)
    public static int minimumTotalBetter(List<List<Integer>> triangle) {
        int[][] dp = new int[triangle.size()][triangle.size()];
        for (int[] arr : dp) {
            Arrays.fill(arr, -1);
        }
        return minimumTotalBetterUtil(triangle, 0, 0, dp);
    }

    private static int minimumTotalBetterUtil(List<List<Integer>> triangle, int i, int j, int[][] dp) {
        if (i == triangle.size() - 1) {
            return triangle.get(i).get(j);
        }
        if (dp[i][j] != -1) {
            return dp[i][j];
        }
        int downSide = triangle.get(i).get(j) + minimumTotalBetterUtil(triangle, i + 1, j, dp);
        int diagonal = triangle.get(i).get(j) + minimumTotalBetterUtil(triangle, i + 1, j + 1, dp);
        return dp[i][j] = Math.min(downSide, diagonal);
    }

    // TODO: DP Solution
    //  Time Complexity: O(n * n)
    //  Space Complexity: O(n * n)
    public static int minimumTotalOptimised(List<List<Integer>> triangle) {
        int n = triangle.size();
        int dp[][] = new int[n][n];
        for (int j = 0; j < n; j++) {
            dp[n - 1][j] = triangle.get(n - 1).get(j);
        }
        for (int i = n - 2; i >= 0; i--) {
            for (int j = i; j >= 0; j--) {
                int down = triangle.get(i).get(j) + dp[i + 1][j];
                int diagonal = triangle.get(i).get(j) + dp[i + 1][j + 1];
                dp[i][j] = Math.min(down, diagonal);
            }
        }
        return dp[0][0];
    }
}
