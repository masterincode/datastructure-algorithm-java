package com.tutort.classroom.algorithms.dynamicprogramming.striver;

import com.tutort.LeetCodeURLConstant;

import java.util.Arrays;

public class HouseRobberII {

    public static void main(String[] args) {
        System.out.println(houseRobberBrute(new int[]{1, 3, 2, 1}));
        System.out.println(houseRobberBetter(new int[]{1, 3, 2, 1}));
        System.out.println(robOptimised(new int[]{1, 3, 2, 1}));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.HOUSE_ROBBER_II);
    }

    // TODO: Recursive Approach
    //  Time Complexity: O (2^n)
    public static int houseRobberBrute(int[] valueInHouse) {
        if (valueInHouse.length == 1)
            return valueInHouse[0];
        if (valueInHouse.length == 2)
            return Math.max(valueInHouse[0], valueInHouse[1]);

        int excludeLastElement = houseRobberBruteBruteUtil(valueInHouse, 0, valueInHouse.length - 2);
        int excludeFirstElement = houseRobberBruteBruteUtil(valueInHouse, 1, valueInHouse.length - 1);
        return Math.max(excludeLastElement, excludeFirstElement);
    }

    private static int houseRobberBruteBruteUtil(int[] valueInHouse, int startIndex, int finalIndex) {
        if (startIndex == finalIndex) {
            return valueInHouse[startIndex];
        }
        if (finalIndex < startIndex) {
            return 0;
        }
        int include = valueInHouse[finalIndex] + houseRobberBruteBruteUtil(valueInHouse, startIndex, finalIndex - 2);
        int exclude = 0 + houseRobberBruteBruteUtil(valueInHouse, startIndex, finalIndex - 1);
        return Math.max(include, exclude);
    }


    // TODO: Recursive Approach with Memoization
    //  Time Complexity: O (n)
    //  Space Complexity: O (n^2)
    public static int houseRobberBetter(int[] valueInHouse) {
        if (valueInHouse.length == 1)
            return valueInHouse[0];
        if (valueInHouse.length == 2)
            return Math.max(valueInHouse[0], valueInHouse[1]);

        int[][] dp = new int[valueInHouse.length][2];
        for (int[] arr : dp) {
            Arrays.fill(arr, -1);
        }
        int excludeLastElement = houseRobberBruteBetterUtil(valueInHouse, 0, valueInHouse.length - 2, dp);
        int excludeFirstElement = houseRobberBruteBetterUtil(valueInHouse, 1, valueInHouse.length - 1, dp);
        return Math.max(excludeLastElement, excludeFirstElement);
    }

    private static int houseRobberBruteBetterUtil(int[] valueInHouse, int startIndex, int finalIndex, int[][] dp) {
        if (startIndex == finalIndex) {
            return dp[finalIndex][startIndex] = valueInHouse[startIndex];
        }
        if (finalIndex < startIndex) {
            return 0;
        }
        if (dp[finalIndex][startIndex] != -1) {
            return dp[finalIndex][startIndex];
        }
        int include = valueInHouse[finalIndex] + houseRobberBruteBetterUtil(valueInHouse, startIndex, finalIndex - 2, dp);
        int exclude = 0 + houseRobberBruteBetterUtil(valueInHouse, startIndex, finalIndex - 1, dp);
        return dp[finalIndex][startIndex] = Math.max(include, exclude);
    }

    // TODO: DP Solutions
    //  Time Complexity: O (n)
    public static int robOptimised(int[] nums) {
        if (nums.length == 1) {
            return nums[0];
        }
        int dp[][] = new int[nums.length + 1][2];
        int one = HouseRobberOne(nums, 1, nums.length - 1, dp, 0);
        int two = HouseRobberOne(nums, 2, nums.length, dp, 1);
        return Math.max(one, two);
    }

    public static int HouseRobberOne(int[] nums, int start, int end, int[][] dp, int idx) {
        dp[start][idx] = nums[start - 1];
        for (int i = start; i < end; i++) {
            dp[i + 1][idx] = Math.max(dp[i][idx], dp[i - 1][idx] + nums[i]);
        }
        return dp[end][idx];
    }


}
