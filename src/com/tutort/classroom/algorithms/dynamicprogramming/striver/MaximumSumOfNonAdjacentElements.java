package com.tutort.classroom.algorithms.dynamicprogramming.striver;

import com.tutort.LeetCodeURLConstant;

import java.util.ArrayList;
import java.util.Arrays;

public class MaximumSumOfNonAdjacentElements {

    public static void main(String[] args) {
        ArrayList arrayList = new ArrayList<>(Arrays.asList(1, 2, 3, 5, 4));
        System.out.println(maximumNonAdjacentSumBrute(arrayList));
        System.out.println(maximumNonAdjacentSumBetter(arrayList));
        System.out.println(maximumNonAdjacentSumOptmised1(arrayList));
        System.out.println(maximumNonAdjacentSumOptmised2(arrayList));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.MAXIMUM_SUM_OF_NON_ADJACENT_ELEMENTS);
    }

    // TODO: Recursive Approach
    //  Time Complexity: O (2^n)
    public static int maximumNonAdjacentSumBrute(ArrayList<Integer> nums) {
        return maximumNonAdjacentSumBruteUtil(nums, nums.size() - 1);
    }

    private static int maximumNonAdjacentSumBruteUtil(ArrayList<Integer> nums, int index) {
        if(index < 0) {
            return 0;
        }
        if(index == 0) {
            return nums.get(index);
        }
        int include = nums.get(index) + maximumNonAdjacentSumBruteUtil(nums, index - 2);
        int exclude = maximumNonAdjacentSumBruteUtil(nums, index - 1);
        return Math.max(include, exclude);
    }

    // TODO: Recursive Approach with Memoization
    //  Time Complexity: O (N)
    //  Space Complexity: O(N)
    public static int maximumNonAdjacentSumBetter(ArrayList<Integer> nums) {
        int[] dp = new int[nums.size()];
        Arrays.fill(dp, -1);
        return maximumNonAdjacentSumBetterUtil(nums, nums.size() - 1, dp);
    }

    private static int maximumNonAdjacentSumBetterUtil(ArrayList<Integer> nums, int index, int[] dp) {
        if(index < 0) {
            return 0;
        }
        if(index == 0) {
            return nums.get(index);
        }
        if(dp[index] != -1) {
            return dp[index];
        }

        int include = nums.get(index) + maximumNonAdjacentSumBetterUtil(nums, index - 2, dp);
        int exclude = maximumNonAdjacentSumBetterUtil(nums, index - 1, dp);
        return dp[index] = Math.max(include, exclude);
    }

    // TODO: DP Solution
    //  Time Complexity: O(N)
    //  Space Complexity: O(N)
    public static int maximumNonAdjacentSumOptmised1(ArrayList<Integer> nums) {
        int[] dp = new int[nums.size()];
        dp[0] = nums.get(0);
        for (int index = 1; index < nums.size(); index++) {
            int include = nums.get(index);
            if(index > 1) {
                include = include + dp[index - 2];
            }
            int exclude = dp[index - 1];
            dp[index] = Math.max(include, exclude);
        }
        return dp[nums.size() - 1];
    }

    // TODO: DP Solution
    //  Time Complexity: O(N)
    //  Space Complexity: O(1)
    public static int maximumNonAdjacentSumOptmised2(ArrayList<Integer> nums) {
        int prev1 = nums.get(0);
        int prev2 = 0;
        for (int index = 1; index < nums.size(); index++) {
            int include = nums.get(index);
            if(index > 1) {
                include = include + prev2;
            }
            int exclude = prev1;
            int curr = Math.max(include, exclude);
            prev2 = prev1;
            prev1 = curr;
        }
        return prev1;
    }
}
