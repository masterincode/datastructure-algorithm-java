package com.tutort.classroom.algorithms.dynamicprogramming;

public class SubsetProblem {

    public static void main(String[] args) {
        int[] set1 = new int[]{1, 3, 7, 6};
        System.out.println(subsetProblemBrute(set1, set1.length, 6));
        System.out.println(subsetProblemOptimised(set1, set1.length, 6));
    }

    // TODO: Recursive Approach
    //  Time Complexity: O (2^n)
    public static boolean subsetProblemBrute(int[] set, int n, int sum) {
        // Base Cases
        if (sum == 0)
            return true;
        if (n == 0 && sum > 0)
            return false;
        // If last element is greater than sum, then ignore it
        if (set[n - 1] > sum)
            return subsetProblemBrute(set, n - 1, sum);
       /* else, check if sum can be obtained by any of the following
          (a) including the last element
          (b) excluding the last element   */
        return subsetProblemBrute(set, n - 1, sum) || subsetProblemBrute(set, n - 1, sum - set[n - 1]);
    }

    // TODO: DP Solutions
    //  Time Complexity: O (n * sum)
    //  Space Complexity: n * sum
    public static boolean subsetProblemOptimised(int[] set, int n, int sum) {
        boolean[][] subset = new boolean[n + 1][sum + 1];
        // If sum is 0, then answer is true
        for (int i = 0; i <= n; i++) {
            subset[i][0] = true;
        }
        // If sum is not 0 and set is empty, then answer is false
        for (int i = 1; i <= sum; i++) {
            subset[0][i] = false;
        }
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= sum; j++) {
                if (j < set[i - 1]) {
                    subset[i][j] = subset[i - 1][j];
                }
                if (j >= set[i - 1]) {
                    subset[i][j] = subset[i - 1][j] || subset[i - 1][j - set[i - 1]];
                }
            }
        }
        return subset[n][sum];
    }

}
