package com.tutort.classroom.algorithms.dynamicprogramming;

import com.tutort.LeetCodeURLConstant;

public class UniquePaths {

    public static void main(String[] args) {
        System.out.println(uniquePathsBrute(3, 7));
        System.out.println(uniquePathsOptimised(3, 7));
        System.out.println(numberOfPaths(3,3));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.UNIQUE_PATHS_I);
    }

    // TODO: Top Down Approach
    //  Time Complexity: O (n^2)
    public static int uniquePathsBrute(int m, int n) {
        return uniquePathsBruteUtil(0, 0, n, m);
    }

    private static int uniquePathsBruteUtil(int i, int j, int n, int m) {
        if (i == (n - 1) && j == (m - 1))
            return 1;
        //This check is required to avoid IndexOutOfBoundException
        if (i >= n || j >= m)
            return 0;
        else
            return uniquePathsBruteUtil(i + 1, j, n, m) + uniquePathsBruteUtil(i, j + 1, n, m);
    }

    // TODO: Top Down Approach
    //  Time Complexity: O(n-1) or O(m-1)
    //  Space Complexity: O(1)
    public static int uniquePathsOptimised(int m, int n) {
        int N = n + m - 2;
        int r = m - 1;
        double res = 1;

        for (int i = 1; i <= r; i++)
            res = res * (N - r + i) / i;
        return (int) res;
    }

    // TODO: DP Solution
    //  Time Complexity: O(m * n)
    //  Space Complexity: O(m * n)
    public static int numberOfPaths(int m, int n) {
        if (m == 1 || n == 1)
            return 1;
        int[][] count = new int[m][n];
        for (int i = 0; i < m; i++)
            count[i][0] = 1; // Filling 1st column as 1
        for (int j = 0; j < n; j++)
            count[0][j] = 1; // Filling 1st row as 1
        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++)
                count[i][j] = count[i - 1][j] + count[i][j - 1];
        }
        return count[m - 1][n - 1];
    }
}