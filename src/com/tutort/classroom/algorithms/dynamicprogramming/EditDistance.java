package com.tutort.classroom.algorithms.dynamicprogramming;

import com.tutort.LeetCodeURLConstant;

import java.util.Arrays;

public class EditDistance {

    public static void main(String[] args) {
        System.out.println(minDistanceBrute("horse", "ros"));
        System.out.println(minDistanceBetter("horse", "ros"));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.EDIT_DISTANCE);
    }

    // TODO: Recursive Approach
    //  Time Complexity: O (2^n)
    public static int minDistanceBrute(String word1, String word2) {
        return minDistanceBruteUtil(word1, word2, word1.length() - 1, word2.length() - 1);
    }

    private static int minDistanceBruteUtil(String word1, String word2, int index1, int index2) {
        if (index1 < 0)
            return index2 + 1;
        if (index2 < 0)
            return index1 + 1;
        if (word1.charAt(index1) == word2.charAt(index2)) {
            return minDistanceBruteUtil(word1, word2, index1 - 1, index2 - 1);
        } else {
            int insert = minDistanceBruteUtil(word1, word2, index1, index2 - 1);
            int remove = minDistanceBruteUtil(word1, word2, index1 - 1, index2);
            int replace = minDistanceBruteUtil(word1, word2, index1 - 1, index2 - 1);
            return 1 + Math.min(replace, Math.min(insert, remove));
        }
    }

    // TODO: Recursive Approach with Memoization
    //  Time Complexity: O (N * M)
    //  Space Complexity: O(N * M) + O(N + M)
    public static int minDistanceBetter(String word1, String word2) {
        int m = word1.length();
        int n = word2.length();
        int[][] dp = new int[m][n];
        for (int[] arr : dp) {
            Arrays.fill(arr, -1);
        }
        return minDistanceBetterUtil(word1, word2, m - 1, n - 1, dp);
    }

    private static int minDistanceBetterUtil(String word1, String word2, int index1, int index2, int[][] dp) {
        if (index1 < 0)
            return index2 + 1;
        if (index2 < 0)
            return index1 + 1;
        if (dp[index1][index2] != -1) {
            return dp[index1][index2];
        }
        if (word1.charAt(index1) == word2.charAt(index2)) {
            return dp[index1][index2] = minDistanceBetterUtil(word1, word2, index1 - 1, index2 - 1, dp);
        } else {
            int insert = minDistanceBetterUtil(word1, word2, index1, index2 - 1, dp);
            int remove = minDistanceBetterUtil(word1, word2, index1 - 1, index2, dp);
            int replace = minDistanceBetterUtil(word1, word2, index1 - 1, index2 - 1, dp);
            return dp[index1][index2] = 1 + Math.min(replace, Math.min(insert, remove));
        }
    }

    // TODO: DP Solutions
    //  Time Complexity: O(N * M)
    //  Space Complexity: O(N * M)
    public static int minDistanceOptmised(String word1, String word2) {
        int n = word1.length();
        int m = word2.length();

        int[][] dp = new int[n + 1][m + 1];
        for (int i = 0; i <= n; i++) {
            dp[i][0] = i;
        }
        for (int j = 0; j <= m; j++) {
            dp[0][j] = j;
        }

        for (int i = 1; i < n + 1; i++) {
            for (int j = 1; j < m + 1; j++) {
                if (word1.charAt(i - 1) == word2.charAt(j - 1))
                    dp[i][j] = 0 + dp[i - 1][j - 1];

                else dp[i][j] = 1 + Math.min(dp[i - 1][j - 1], Math.min(dp[i - 1][j], dp[i][j - 1]));
            }
        }

        return dp[n][m];
    }
}
