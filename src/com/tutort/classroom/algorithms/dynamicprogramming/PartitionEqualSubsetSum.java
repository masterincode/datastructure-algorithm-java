package com.tutort.classroom.algorithms.dynamicprogramming;

import com.tutort.LeetCodeURLConstant;

public class PartitionEqualSubsetSum {

    public static void main(String[] args) {
        int[] set1 = new int[]{1, 5, 11, 5};
        int[] set2 = new int[]{1, 2, 3, 5};
        System.out.println(canPartitionBrute(set1));
        System.out.println(canPartitionOptimised(set1));
        System.out.println();
        System.out.println(canPartitionBrute(set2));
        System.out.println(canPartitionOptimised(set2));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.PARTITION_EQUAL_SUBSET_SUM);
    }

    // TODO: Recursive Approach
    //  Time Complexity: O (2^n)
    public static boolean canPartitionBrute(int[] nums) {
        int sum = 0;
        for (int val : nums) {
            sum = sum + val;
        }
        if (sum % 2 == 0) {
            return canPartitionBruteUtil(nums, nums.length, sum / 2);
        } else {
            return false;
        }
    }

    private static boolean canPartitionBruteUtil(int[] nums, int n, int sum) {
        // Base Cases
        if (sum == 0)
            return true;
        if (n == 0 && sum > 0)
            return false;
        // If last element is greater than sum, then ignore it
        if (nums[n - 1] > sum)
            return canPartitionBruteUtil(nums, n - 1, sum);
       /* else, check if sum can be obtained by any of the following
          (a) including the last element
          (b) excluding the last element   */
        return canPartitionBruteUtil(nums, n - 1, sum) || canPartitionBruteUtil(nums, n - 1, sum - nums[n - 1]);
    }

    // TODO: DP Solutions
    //  Time Complexity: O (n * sum)
    //  Space Complexity: n * sum
    public static boolean canPartitionOptimised(int[] nums) {
        int sum = 0;
        for (int val : nums) {
            sum = sum + val;
        }
        if(sum % 2 != 0) {
            return false;
        }
        sum = sum / 2;
        int n = nums.length;
        boolean[][] subset = new boolean[n + 1][sum + 1];
        // If sum is 0, then answer is true
        for (int i = 0; i <= n; i++) {
            subset[i][0] = true;
        }
        // If sum is not 0 and nums is empty, then answer is false
        for (int i = 1; i <= sum; i++) {
            subset[0][i] = false;
        }
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= sum; j++) {
                if (j < nums[i - 1]) {
                    subset[i][j] = subset[i - 1][j];
                }
                if (j >= nums[i - 1]) {
                    subset[i][j] = subset[i - 1][j] || subset[i - 1][j - nums[i - 1]];
                }
            }
        }
        return subset[n][sum];
    }
}
