package com.tutort.classroom.algorithms.dynamicprogramming;

import com.tutort.LeetCodeURLConstant;

public class ClimbingStairs {

    public static void main(String[] args) {
        System.out.println(climbStairsBruteForceAppraoch(3));
        System.out.println(climbStairsBetterApproach(3));
        System.out.println(climbStairsOptmisedApproach(3));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.CLIMBING_STAIRS);
    }

    // TODO: Top Down Approach
    //  Time Complexity: O (n^2)
    //  Space Complexity: O (n)
    public static int climbStairsBruteForceAppraoch(int n) {
        if (n == 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        }
        if (n == 2) {
            return 2;
        }
        return climbStairsBruteForceAppraoch(n - 1) + climbStairsBruteForceAppraoch(n - 2);
    }

    // TODO: Bottom UP Approach
    //  Time Complexity: O (n)
    //  Space Complexity: O (n)
    public static int climbStairsBetterApproach(int n) {
        if (n <= 1) {
            return 1;
        }
        int[] dp = new int[n + 1];
        dp[1] = 1;
        dp[2] = 2;
        for (int i = 3; i <= n; i++) {
            dp[i] = dp[i - 1] + dp[i - 2];
        }
        return dp[n];
    }

    // TODO: Bottom UP Approach
    //  Time Complexity: O(n)
    //  Space Complexity: O(1)
    // To calculate the new value we only leverage the previous two values. So we don't need to use an array to store all the previous values.
    public static int climbStairsOptmisedApproach(int n) {
        if (n <= 1) {
            return 1;
        }
        int prev1 = 1; //When 1 stair step is given then there is only one way i.e 1 Jump
        int prev2 = 2; //When 2 stair step is given then there are two ways
        // i.e (1 Jump -> 1 Jump) or
        // 2 Jump
        for (int i = 3; i <= n; i++) {
            int newValue = prev1 + prev2;
            prev1 = prev2;
            prev2 = newValue;
        }

        return prev2;
    }
}
