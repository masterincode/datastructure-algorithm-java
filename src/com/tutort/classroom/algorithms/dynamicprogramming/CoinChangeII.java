package com.tutort.classroom.algorithms.dynamicprogramming;

import com.tutort.LeetCodeURLConstant;

public class CoinChangeII {

    public static void main(String[] args) {

        int[] coins1 = new int[]{1, 2, 5};
        System.out.println(coinChange(coins1, 5));
        System.out.println(coinChangeOptimised(coins1, 5));
        System.out.println();
        int[] coins2 = new int[]{2};
        System.out.println(coinChange(coins2, 3));
        System.out.println(coinChangeOptimised(coins2, 3));
        System.out.println();
        int[] coins3 = new int[]{10};
        System.out.println(coinChange(coins3, 10));
        System.out.println(coinChangeOptimised(coins3, 10));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.COIN_CHANGE_II);
    }

    // TODO: Recursive Approach
    //  Time Complexity: O (2^n)
    public static int coinChange(int[] coins, int amount) {
        return coinChangeUtil(coins, coins.length - 1, amount);
    }

    private static int coinChangeUtil(int[] coins, int index, int amount) {
        if (index == 0) {
            if (amount % coins[index] == 0) {
                return 1;
            } else {
                return 0;
            }
        }
        /* Why, exclude has index - 1 and amount ?
         * Ans: If I am at current index and excluding the element then I have to move to index - 1 position in next iteration,
         * Since, I am excluding the element, there is no deduction in amount and due to this amount remains same. */
        int exclude = coinChangeUtil(coins, index - 1, amount);
        int include = 0;
        if (coins[index] <= amount) {
            /* Why, include has index and amount - coins[index] ?
             * Ans: If I am at current index and including the element then in next iteration also I can use same element, same for every iteration,
             * Since, I am including the element, there is deduction of coins[index] from total amount and due to this amount - coins[index] is written. */
            include = coinChangeUtil(coins, index, amount - coins[index]);
        }
        return exclude + include;
    }

    // TODO: DP Solutions..Why 2D array....coz we have 2 variables i.e  n and amount
    //  Time Complexity: O (n * amount)
    //  Space Complexity: n * amount
    public static int coinChangeOptimised(int[] coins, int amount) {
        int n = coins.length;
        int[][] dp = new int[n][amount + 1];
        // If amount is 0, then answer is 0
        for (int i = 0; i < n; i++) {
            dp[i][0] = 0;
        }
        for (int j = 0; j <= amount; j++) {
            if (j % coins[0] == 0)
                dp[0][j] = 1;
        }
        for (int i = 1; i < n; i++) { // Rows
            for (int j = 0; j <= amount; j++) { // Columns
                int exclude = dp[i - 1][j];
                int include = 0;
                if (j >= coins[i]) {
                    include = dp[i][j - coins[i]];
                }
                dp[i][j] = exclude + include;
            }
        }
        return dp[n - 1][amount];
    }
}
