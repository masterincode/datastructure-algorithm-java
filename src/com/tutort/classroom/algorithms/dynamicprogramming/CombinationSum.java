package com.tutort.classroom.algorithms.dynamicprogramming;

import com.tutort.LeetCodeURLConstant;

import java.util.ArrayList;
import java.util.List;

public class CombinationSum {
    public static void main(String[] args) {
        System.out.println(combinationSum(new int[]{2, 3, 6, 7}, 7));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.COMBINATION_SUM);
    }

    public static List<List<Integer>> combinationSum(int[] candidates, int target) {
        List<List<Integer>> result = new ArrayList<>();
        List<Integer> pick = new ArrayList<>();
        combinationSumUtil(candidates, result, pick, 0, target);
        return result;
    }

    public static void combinationSumUtil(int[] candidates, List<List<Integer>> result, List<Integer> pick, int index, int target) {
        if (index == candidates.length) {
            if (target == 0) {
                result.add(new ArrayList<>(pick));
                return;
            } else {
                return;
            }
        }
        // Include
        if(target >= candidates[index]) {
            pick.add(candidates[index]);
            combinationSumUtil(candidates, result, pick, index, target - candidates[index]);
            pick.remove((Integer) candidates[index]);
        }
        //Exclude
        combinationSumUtil(candidates, result, pick, index + 1, target);
    }


}
