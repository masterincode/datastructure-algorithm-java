package com.tutort.classroom.algorithms.dynamicprogramming;

import com.tutort.LeetCodeURLConstant;

public class MinimumPathSum {
    public static void main(String[] args) {
        System.out.println(minPathSumBrute(new int[][]{{1, 3, 1}, {1, 5, 1}, {4, 2, 1}}));
        System.out.println(minPathSumBrute(new int[][]{{1, 2, 3}, {4, 5, 6}}));
        System.out.println(minPathSumOptimised(new int[][]{{1, 3, 1}, {1, 5, 1}, {4, 2, 1}}));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.MINIMUM_PATH_SUM);
    }

    // TODO: Recursive Solution [Time Limit Exceeded]
    //  Time Complexity: O(m * n)
    //  Space Complexity: O(m * n)
    public static int minPathSumBrute(int[][] grid) {
        int m = grid.length;
        int n = grid[0].length;
        return minPathSumBruteUtil(m - 1, n - 1, grid);
    }

    private static int minPathSumBruteUtil(int m, int n, int[][] grid) {
        if(m == 0 && n == 0) {
            return grid[0][0];
        }
        if (m == 0) {
            return minPathSumBruteUtil(m, n-1, grid) + grid[m][n];
        }
        if (n == 0) {
            return minPathSumBruteUtil(m-1, n, grid) + grid[m][n];
        }
        int left = minPathSumBruteUtil(m - 1, n, grid);
        int right = minPathSumBruteUtil(m, n - 1, grid);
        int minValue = Math.min(left, right);
        return minValue + grid[m][n];
    }


    // TODO: DP Solution
    //  Time Complexity: O(m * n)
    //  Space Complexity: O(m * n)
    public static int minPathSumOptimised(int[][] grid) {

        int m = grid.length;
        int n = grid[0].length;

        int[][] dp = new int[m][n];
        dp[0][0] = grid[0][0];
        for (int i = 1; i < m; i++) {
            dp[i][0] = dp[i - 1][0] + grid[i][0];
        }
        for (int j = 1; j < n; j++) {
            dp[0][j] = dp[0][j - 1] + grid[0][j];
        }
        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                dp[i][j] = Math.min(dp[i - 1][j], dp[i][j - 1]) + grid[i][j];
            }
        }
        return dp[m - 1][n - 1];
    }
}
