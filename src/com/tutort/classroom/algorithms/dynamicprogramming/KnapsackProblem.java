package com.tutort.classroom.algorithms.dynamicprogramming;

import com.tutort.LeetCodeURLConstant;

public class KnapsackProblem {

    public static void main(String[] args) {
        int[] weight1 = new int[]{1, 2, 2};
        int[] values1 = new int[]{60, 100, 120};
        System.out.println(maxTotalWeightBrute(weight1, values1, weight1.length, 3));
        System.out.println(maxTotalWeightOptimised(weight1, weight1.length, values1, 3));

        int[] weight2 = new int[]{1, 2, 2};
        int[] values2 = new int[]{160, 100, 120};
        System.out.println(maxTotalWeightBrute(weight2, values2, weight2.length, 3));
        System.out.println(maxTotalWeightOptimised(weight2, weight2.length, values2, 3));

        int[] weight3 = new int[]{2, 3, 1, 2};
        int[] values3 = new int[]{6, 1, 3, 2};
        System.out.println(maxTotalWeightBrute(weight3, values3, weight3.length, 5));
        System.out.println(maxTotalWeightOptimised(weight3, weight3.length, values3, 5));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.KNAPSACK_PROBLEM);

    }

    // TODO: Recursive Approach
    //  Time Complexity: O (2^n) -> Time Limit Exceeded
    public static int maxTotalWeightBrute(int[] weight, int[] value, int n, int maxWeight) {
        // Base Cases
        if (maxWeight == 0)
            return 0;
        if (n == 0 && maxWeight > 0)
            return 0;
        // If last element is greater than maxWeight, then ignore it
        if (weight[n - 1] > maxWeight)
            return maxTotalWeightBrute(weight, value, n - 1, maxWeight);
       /* else, check max maxWeight can be obtained by the following
          (a) including the last element
          (b) excluding the last element   */
        int exclude = maxTotalWeightBrute(weight, value, n - 1, maxWeight);
        int include = value[n - 1] + maxTotalWeightBrute(weight, value, n - 1, maxWeight - weight[n - 1]);
        return Math.max(exclude, include);
    }

    // TODO: DP Solutions..Why 2D array....coz we have 2 variables i.e  n and maxWeight
    //  Time Complexity: O (n * maxWeight)
    //  Space Complexity: n * maxWeight
    public static int maxTotalWeightOptimised(int[] weight, int n, int[] value, int maxWeight) {
        int[][] dp = new int[n + 1][maxWeight + 1];

        // If maxWeight is 0, then answer is 0
        for (int i = 0; i <= n; i++) {
            dp[i][0] = 0;
        }
        // If maxWeight is not 0 and weight is empty, then answer is 0
        for (int i = 1; i <= maxWeight; i++) {
            dp[0][i] = 0;
        }
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= maxWeight; j++) {
                if (j < weight[i - 1]) {
                    dp[i][j] = dp[i - 1][j];
                }
                if (j >= weight[i - 1]) {
                    int exclude = dp[i - 1][j];
                    int include = value[i - 1] + dp[i - 1][j - weight[i - 1]];
                    dp[i][j] = Math.max(exclude, include);
                }
            }
        }
        return dp[n][maxWeight];
    }
}
