package com.tutort.classroom.algorithms.dynamicprogramming;

import com.tutort.LeetCodeURLConstant;

import java.util.Arrays;

public class HouseRobber {

    public static void main(String[] args) {
        System.out.println(robBrute(new int[]{1, 2, 3, 1}));
        System.out.println(robBrute(new int[]{2, 7, 9, 3, 1}));
        System.out.println(robBetter(new int[]{1, 2, 3, 1}));
        System.out.println(robBetter(new int[]{2, 7, 9, 3, 1}));
        System.out.println(robUsingDp(new int[]{1, 2, 3, 1}));
        System.out.println(robUsingDp(new int[]{2, 7, 9, 3, 1}));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.HOUSE_ROBBER_I);
    }

    // TODO: Recursive Approach
    //  Time Complexity: O (2^n)
    public static int robBrute(int[] nums) {
        return robBruteUtil(nums, nums.length - 1);
    }
    private static int robBruteUtil(int[] nums, int index) {
        if (index == 0) {
            return nums[index];
        }
        if (index < 0) {
            return 0;
        }
        int include = nums[index] + robBruteUtil(nums, index - 2);
        int exclude = 0 + robBruteUtil(nums, index - 1);
        return Math.max(include, exclude);
    }

    // TODO: Recursive Approach with Memoization
    //  Time Complexity: O (n)
    public static int robBetter(int[] nums) {
        int[] dp = new int[nums.length];
        Arrays.fill(dp, -1);
        return robBetterUtil(nums, nums.length - 1, dp);
    }
    private static int robBetterUtil(int[] nums, int index, int[] dp) {
        if (index == 0) {
            return nums[index];
        }
        if (index < 0) {
            return 0;
        }
        if(dp[index] != -1) {
            return dp[index];
        }
        int include = nums[index] + robBetterUtil(nums, index - 2, dp);
        int exclude = 0 + robBetterUtil(nums, index - 1, dp);
        return dp[index] =  Math.max(include, exclude);
    }

    // TODO: DP Solutions
    //  Time Complexity: O (n)
    public static int robUsingDp(int[] nums) {
        int[] dp = new int[nums.length];
        Arrays.fill(dp, -1);
        dp[0] = nums[0];
        for (int i = 1; i < nums.length; i++) {
            int include = nums[i];
            if(i > 1) {
                include = include + dp[i - 2];
            }
            int exclude = 0 + dp[i - 1];
            dp[i] =  Math.max(include, exclude);
        }
        return dp[nums.length - 1];
    }
}
