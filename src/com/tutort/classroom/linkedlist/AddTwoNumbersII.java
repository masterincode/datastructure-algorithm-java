package com.tutort.classroom.linkedlist;

import com.tutort.LeetCodeURLConstant;

public class AddTwoNumbersII {

    public static void main(String[] args) {
        int[] arr1 = {7,2,4,3};
        ListNodeInputData listNode1 = new ListNodeInputData();
        for (int value: arr1) {
            listNode1.push(value);
        }
        int[] arr2 = {5,6,4};
        ListNodeInputData listNode2 = new ListNodeInputData();
        for (int value: arr2) {
            listNode2.push(value);
        }
        System.out.println(addTwoNumbers(listNode1.head, listNode2.head));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.ADD_TWO_NUMBERS_II);
    }

    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode prevNode1 = null;
        ListNode prevNode2 = null;
        ListNode nextNode;

        while(l1 != null) {
            nextNode = l1.next;
            l1.next = prevNode1;
            prevNode1 = l1;
            l1 =  nextNode;
        }

        while(l2 != null) {
            nextNode = l2.next;
            l2.next = prevNode2;
            prevNode2 = l2;
            l2 = nextNode;
        }
        int sum = 0;
        int rem = 0;
        int carry = 0;
        ListNode head = new ListNode(0);
        ListNode tail = head;
        while(prevNode1 != null || prevNode2 != null || carry == 1) {
            if(prevNode1 != null) {
                sum = sum + prevNode1.val;
                prevNode1 = prevNode1.next;
            }
            if(prevNode2!= null) {
                sum = sum + prevNode2.val;
                prevNode2 = prevNode2.next;
            }
            if(carry == 1) {
                sum = sum + carry;
            }
            rem = sum % 10;
            carry = sum / 10;
            ListNode newNode = new ListNode(rem);
            tail.next = newNode;
            tail = newNode;
            sum = 0;
        }
        ListNode sumHead = head.next;
        ListNode prevNode3 = null;
        while(sumHead != null) {
            nextNode = sumHead.next;
            sumHead.next = prevNode3;
            prevNode3 = sumHead;
            sumHead = nextNode;
        }
        return prevNode3;
    }
}
