package com.tutort.classroom.linkedlist;


import com.tutort.LeetCodeURLConstant;

import java.util.HashMap;
import java.util.Map;

public class CopyListWithRandomPointer {

    public static void main(String[] args) {
        int[] arr1 = {1,2,3,4,5};
        NodeInputData listNode1 = new NodeInputData();
        for (int value: arr1) {
            listNode1.push(value);
        }
        System.out.println(copyRandomListAppraoch2(listNode1.head));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.COPY_LIST_WITH_RANDOM_POINTER);
    }

    //Time Complexity: O(n)
    //Space Complexity: n
    //Approach 1: Using HashMap
    public static Node copyRandomListAppraoch1(Node head) {
        Node currentHead = head;
        Node cloneHead = null;
        Node cloneTail = null;
        Map<Node,Node> map = new HashMap<>();

        //TODO: Traverse the original list and create clone list and store orginal node and clone node in hashmap
        while (currentHead != null) {
            Node newNode = new Node(currentHead.val);
            if(cloneHead == null) {
                cloneHead = newNode;
                cloneTail = newNode;
            } else {
                cloneTail.next = newNode;
                cloneTail = newNode;
            }
            map.put(currentHead, newNode);
            currentHead = currentHead.next;
        }

        //TODO: Set the random pointer in clone list
        currentHead = head;
        Node currentCloneHead = cloneHead;
        while (currentHead != null && currentCloneHead!= null) {
            currentCloneHead.random = map.get(currentHead.random);
            currentCloneHead = currentCloneHead.next;
            currentHead      = currentHead.next;
        }
        return cloneHead;
    }

    //Time Complexity: O(n)
    //Space Complexity: O(1)
    //Approach 2: No Extra Space
    public static Node copyRandomListAppraoch2(Node head) {
        if(head == null)
            return head;
        Node currentHead = head;
        while(currentHead != null ) {
            Node newNode = new Node(currentHead.val);
            Node nextNode = currentHead.next;
            currentHead.next = newNode;
            currentHead.next.next = nextNode;
            currentHead = nextNode;
        }
        currentHead = head;
        while(currentHead != null) {
            if(currentHead.next != null) {
                currentHead.next.random = (currentHead.random != null) ? currentHead.random.next : null;
            }
            currentHead = currentHead.next.next;
        }
        Node oddHead = head;
        Node evenTail = head.next;
        Node evenHead = evenTail;
        while (oddHead != null) {
            oddHead.next =oddHead.next.next;
            evenTail.next = (evenTail.next != null) ? evenTail.next.next  : evenTail.next;
            oddHead = oddHead.next;
            evenTail = evenTail.next;
        }
        return evenHead;
    }
}
