package com.tutort.classroom.linkedlist;

import com.tutort.LeetCodeURLConstant;

public class ConvertBinaryNumberInALinkedListToInteger {

    public static void main(String[] args) {
        int[] arr1 = {1,0,1};
        ListNodeInputData listNode1 = new ListNodeInputData();
        for (int value: arr1) {
            listNode1.push(value);
        }
        System.out.println(getDecimalValue(listNode1.head));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.CONVERT_BINARY_NUMBER_IN_A_LINKED_LIST_TO_INTEGER);
    }

    public static int getDecimalValue(ListNode head) {
        ListNode prevNode = null;
        ListNode nextNode;

        while(head !=null) {
            nextNode = head.next;
            head.next = prevNode;
            prevNode = head;
            head = nextNode;
        }
        int power = 0;
        int sum = 0;

        //prevNode is now new Head after reversing Linked List
        while(prevNode != null ) {
            sum = (int) (sum + prevNode.val * Math.pow(2,power));
            prevNode = prevNode.next;
            power++;
        }
        return sum;
    }
}
