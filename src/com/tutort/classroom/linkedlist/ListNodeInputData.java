package com.tutort.classroom.linkedlist;

public class ListNodeInputData {
    public ListNode head;
    public ListNode tail;

    public void push(int value) {
        ListNode newNode = new ListNode(value);
        if (head == null) {
            head = newNode;
            tail = newNode;
        } else {
            tail.next = newNode;
            tail = newNode;
        }
    }
}
