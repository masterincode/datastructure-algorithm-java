package com.tutort.classroom.linkedlist;

import com.tutort.LeetCodeURLConstant;

public class ReverseLinkedList {

    public static void main(String[] args) {
        int[] arr1 = {1,2,3,4,5};
        ListNodeInputData listNode1 = new ListNodeInputData();
        for (int value: arr1) {
            listNode1.push(value);
        }
        System.out.println(reverseList(listNode1.head));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.REVERSE_LINKED_LIST);
    }

    public static ListNode reverseList(ListNode head) {
        ListNode prevNode = null;
        ListNode nextNode;

        while(head !=null) {
            nextNode = head.next;
            head.next = prevNode;
            prevNode = head;
            head = nextNode;
        }
        return prevNode;
    }
}
