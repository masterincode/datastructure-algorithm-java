package com.tutort.classroom.linkedlist;

import com.tutort.LeetCodeURLConstant;

public class NthNodeFromEndOfLinkedList {
    public static void main(String[] args) {
        int[] arr1 = {1,2,3,4,5};
        ListNodeInputData listNode1 = new ListNodeInputData();
        for (int value: arr1) {
            listNode1.push(value);
        }
        System.out.println(removeNthFromEnd(listNode1.head,5));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.NTH_NODE_FROM_END_OF_LINKED_LIST);
    }

    // Time Complexity: O(n)
    public static ListNode removeNthFromEnd(ListNode head, int n) {
        ListNode start = new ListNode(0);
        start.next = head;
        ListNode fast = start;
        ListNode slow = start;
        // 0 -> 1 -> 2 -> 3 -> 4 -> 5 ->
        // s -> head
        //                     s    f
        for(int i = 1; i <= n; ++i) // pre increment is here
            fast = fast.next;

        while(fast.next != null)
        {
            fast = fast.next;
            slow = slow.next;
        }
        slow.next = slow.next.next;
        return start.next;
    }
}
