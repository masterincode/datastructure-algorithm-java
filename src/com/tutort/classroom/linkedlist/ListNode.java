package com.tutort.classroom.linkedlist;

public class ListNode {
     public int val;
     public ListNode next;
     public ListNode(int x) { val = x; }

     @Override
     public String toString() {
          return "ListNode{" +
                  "val=" + val +
                  ", next=" + next +
                  '}';
     }
}

class Node {
     int val;
     Node next;
     Node random;

     public Node(int val) {
          this.val = val;
          this.next = null;
          this.random = null;
     }

     @Override
     public String toString() {
          return "Node{" +
                  "val=" + val +
                  ", next=" + next +
                  ", random=" + random +
                  '}';
     }
}

class NodeInputData {
     Node head;
     Node tail;

     public void push(int value) {
          Node newNode = new Node(value);
          if(head == null) {
               head = newNode;
               tail = newNode;
          } else {
               tail.next = newNode;
               tail =  newNode;
          }
     }
}



