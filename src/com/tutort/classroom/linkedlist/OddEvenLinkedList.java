package com.tutort.classroom.linkedlist;

import com.tutort.LeetCodeURLConstant;

public class OddEvenLinkedList {

    public static void main(String[] args) {
            int[] arr = {1,2,3,4,5,6};
            ListNodeInputData listNodeInputData = new ListNodeInputData();
            for (int value: arr) {
                listNodeInputData.push(value);
            }
            System.out.println(optimisedApproach(listNodeInputData.head));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.ODD_EVEN_LINKED_LIST);
    }

    //Time Complexity: O(n)
    //Space Complexity: O(1)
    //TODO: Using 2 Separate List
    public static ListNode bruteForceApproach(ListNode head) {
        if(head==null || head.next==null) return head;

        ListNode oddHead = null, oddTail = null;
        ListNode evenHead = null, evenTail = null;
        ListNode curr = head;
        int i = 1;
        while(curr!=null){
            // generate the odd indices list
            if(i%2==1){
                if(oddHead==null){
                    oddHead = curr;
                    oddTail = curr;
                }
                else{
                    oddTail.next = curr;
                    oddTail = curr;
                }
            }
            // generate the even indices list
            else{
                if(evenHead==null){
                    evenHead = curr;
                    evenTail = curr;
                }
                else{
                    evenTail.next = curr;
                    evenTail = curr;
                }
            }
            curr = curr.next;
            i++;
        }

        evenTail.next = null;     // there should not be any ListNode after even tail
        oddTail.next  = evenHead;   // join even list after odd
        return oddHead;
    }


    public static ListNode optimisedApproach(ListNode head) {
        if(head==null) return head;

        ListNode odd = head;
        ListNode even = head.next;
        ListNode evenHead = even;

        while(even!=null && even.next!=null){
            odd.next = odd.next.next;
            odd = odd.next;

            even.next = even.next.next;
            even = even.next;
        }

        odd.next = evenHead;

        return head;
    }
}
