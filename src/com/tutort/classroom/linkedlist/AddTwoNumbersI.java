package com.tutort.classroom.linkedlist;

import com.tutort.LeetCodeURLConstant;

public class AddTwoNumbersI {
    public static void main(String[] args) {
        int[] arr1 = {9,9,9,9,9,9,9};
        ListNodeInputData listNode1 = new ListNodeInputData();
        for (int value: arr1) {
            listNode1.push(value);
        }
        int[] arr2 = {9,9,9,9};
        ListNodeInputData listNode2 = new ListNodeInputData();
        for (int value: arr2) {
            listNode2.push(value);
        }
        System.out.println(addTwoNumbers(listNode1.head, listNode2.head));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.ADD_TWO_NUMBERS);
    }

    // Time Complexity : O(max(N, M)) where N is length of l1 & M is length of l2
    // Space Complexity :- O(max(N,M))
    // TODO: max is function of Math.max(N, M)
    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode head = new ListNode(0);
        ListNode tail = head;
        ListNode c1 = l1;
        ListNode c2 = l2;
        int sum = 0;
        int carry = 0;
        int rem;

        while(c1 != null || c2 != null || carry != 0) {
            if(c1 != null) {
                sum = sum + c1.val;
                c1 = c1.next;
            }
            if(c2 != null) {
                sum = sum + c2.val;
                c2 =c2.next;
            }
            if (carry != 0) {
                sum = sum + carry;
            }
            rem = sum % 10;
            carry = sum / 10;
            ListNode newNode = new ListNode(rem);
            tail.next = newNode;
            tail = newNode;
            sum = 0; // Reset sum to 0
        }
        return head.next;
    }

}
