package com.tutort.classroom.linkedlist;

import com.udemy.coltsteele.datastructure.linkedList.singly.Node;
import com.udemy.coltsteele.datastructure.linkedList.singly.SinglyLinkedList;

public class LinkedListIsOfEvenOrOddLength {


    public static void main(String[] args) {
        int[] arr = {1,2,3,4,5};
        SinglyLinkedList singlyLinkedList = new SinglyLinkedList();
        for (int value: arr) {
            singlyLinkedList.push(value);
        }
        System.out.println(evenOrOddLength(singlyLinkedList.head));
        singlyLinkedList.traverse();
    }

    public static String evenOrOddLength(Node head) {
        Node fast = head;
        while(fast != null && fast.next !=null) {
            fast = fast.next.next;
        }
        if (fast == null) {
            return "even";
        }

        if(fast.next == null) {
            return "odd";
        }
        return null;
    }
}
