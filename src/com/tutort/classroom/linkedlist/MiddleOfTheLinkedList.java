package com.tutort.classroom.linkedlist;

import com.tutort.LeetCodeURLConstant;
import com.udemy.coltsteele.datastructure.linkedList.singly.Node;

public class MiddleOfTheLinkedList {
    public static void main(String[] args) {
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.MIDDLE_OF_THE_LINKED_LIST);
    }

    // Time Complexity: O(n)
    public static Node middleNode(Node head) {
        if(head == null) {
            return null;
        }
        Node p = head;
        Node q = head;

        while(q != null && q.next != null) {
            p = p.next;
            q = q.next.next;
        }
        return p;
    }
}
