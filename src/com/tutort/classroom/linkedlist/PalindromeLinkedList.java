package com.tutort.classroom.linkedlist;

import com.tutort.LeetCodeURLConstant;

import java.util.Stack;

public class PalindromeLinkedList {

    public static void main(String[] args) {
        int[] arr1 = {1, 2, 2, 1};
        ListNodeInputData listNode1 = new ListNodeInputData();
        for (int value : arr1) {
            listNode1.push(value);
        }
        System.out.println(isPalindrome(listNode1.head));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.PALINDROME_LINKED_LIST);
    }

    public static boolean isPalindrome(ListNode head) {

        boolean isPalindrome = false;
        Stack<Integer> stack = new Stack<>();
        ListNode currentNode = head;
        int length = 0;
        while (currentNode != null) {
            length++;
            currentNode = currentNode.next;
        }
        if(length == 1) {
            return true;
        }
        currentNode = head;
        int index = 0;
        while (currentNode != null) {
            if (index < length / 2) {
                stack.push(currentNode.val);
            } else if (length % 2 == 0 || index > length / 2) {
                if (stack.pop() == currentNode.val) {
                    isPalindrome = true;
                } else {
                    isPalindrome = false;
                    break;
                }
            }
            index++;
            currentNode = currentNode.next;
        }
        return isPalindrome;
    }
}
