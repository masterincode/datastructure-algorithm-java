package com.tutort.classroom.linkedlist;

import com.tutort.LeetCodeURLConstant;

import java.util.HashMap;
import java.util.Map;

public class DetectCycleInLinkedList {

    public static void main(String[] args) {
        int[] arr1 = {3,2,0,-4};
        ListNodeInputData listNode1 = new ListNodeInputData();
        for (int value: arr1) {
            listNode1.push(value);
        }
        listNode1.tail.next = listNode1.head.next;
        System.out.println(hasCycle(listNode1.head)); //true
        System.out.println(lengthOfCycle(listNode1.head)); // 3
        System.out.println(removeCycle(listNode1.head));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.LINKED_LIST_CYCLE);
    }

    public static boolean hasCycle(ListNode head) {
        ListNode fast = head;
        ListNode slow = head;

        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;

            if (slow == fast)
                return true;
        }
        return false;
    }

    public static int lengthOfCycle(ListNode head){
        ListNode fast = head;
        ListNode slow = head;
        boolean hasCycle = false;
        Map<Integer, Integer> map = new HashMap<>();
        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;

            if (slow == fast) {
               hasCycle = true;
            }
            if (hasCycle) {
                if(map.get(slow.val) == null) {
                    map.put(slow.val,1);
                } else {
                    return map.size();
                }
            }
        }
        return map.size();
    }

    public static ListNode removeCycle(ListNode head) {
        Map<Integer, Integer> map = new HashMap<>();
        ListNode fast = head;
        ListNode slow = null;
        while (fast !=null && fast.next != null) {
            if(map.get(fast.val) == null) {
                map.put(fast.val,1);
            } else {
                slow.next = null;
                break;
            }
            slow = fast;
            fast = fast.next;
        }
        return head;
    }
}
