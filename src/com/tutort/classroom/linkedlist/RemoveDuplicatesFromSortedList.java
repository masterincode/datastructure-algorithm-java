package com.tutort.classroom.linkedlist;

import com.tutort.LeetCodeURLConstant;

public class RemoveDuplicatesFromSortedList {

    public static void main(String[] args) {
        int[] arr1 = {1,2,2,3,3,3,4,4,4,4};
        ListNodeInputData listNode1 = new ListNodeInputData();
        for (int value: arr1) {
            listNode1.push(value);
        }
        System.out.println(deleteDuplicates(listNode1.head));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.REMOVE_DUPLICATES_FROM_SORTED_LIST);
    }

    public static ListNode deleteDuplicates(ListNode head) {
        ListNode current = head;
        while(current != null && current.next != null) {
            if(current.val == current.next.val) {
                current.next  = current.next.next;
            } else {
                current = current.next;
            }
        }
        return head;
    }
}
