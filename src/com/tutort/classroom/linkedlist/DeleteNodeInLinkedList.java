package com.tutort.classroom.linkedlist;

import com.tutort.LeetCodeURLConstant;

public class DeleteNodeInLinkedList {

    public static void main(String[] args) {
        int[] arr1 = {4,5,1,9};
        ListNodeInputData listNode1 = new ListNodeInputData();
        for (int value: arr1) {
            listNode1.push(value);
        }
        System.out.println("Initial List: "+listNode1.head);
        deleteNode(listNode1.head.next);
        System.out.println("Final List: "+listNode1.head);
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.DELETE_NODE_IN_LINKED_LIST);
    }

    public static void deleteNode(ListNode node) {
        System.out.println("Delete Node: "+node.val);
        node.val = node.next.val;
        node.next = node.next.next;
    }
}
