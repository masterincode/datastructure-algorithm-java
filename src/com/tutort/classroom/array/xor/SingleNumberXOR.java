package com.tutort.classroom.array.xor;

import com.tutort.LeetCodeURLConstant;

public class SingleNumberXOR {

    public static void main(String[] args) {
        System.out.println(singleNumber(new int[]{4,1,2,1,2}));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.SINGLE_NUMBER);
    }

    //Time complexity: O(n)
    //Space Complexity: O(1)
    public static int singleNumber(int[] nums) {
        int xorOfInput = 0;
        for (int i = 0; i < nums.length; i++) {
            if(i == 0) {
                xorOfInput = nums[i];
            } else {
                xorOfInput = xorOfInput ^ nums[i];
            }
        }
        return xorOfInput;
    }
}
