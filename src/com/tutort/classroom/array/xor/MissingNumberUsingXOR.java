package com.tutort.classroom.array.xor;

import com.tutort.LeetCodeURLConstant;

public class MissingNumberUsingXOR {

    public static void main(String[] args) {
        System.out.println(missingNumber(new int[]{2,1,3,0,5,6}));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.MISSING_NUMBER);
    }

    //Time complexity: O(n)
    //Space Complexity: O(1)
    public static int missingNumber(int[] nums) {
        int res = nums.length;
        int xorOfRange = 0;
        int xorOfInput = 0;

        for (int i = 1; i <= res; i++) {
            if(i == 1) {
                xorOfRange = i;
            } else {
                xorOfRange = xorOfRange ^ i;
            }
        }
        for (int i = 0; i < nums.length; i++) {
            if(i == 0) {
                xorOfInput = nums[i];
            } else {
                xorOfInput = xorOfInput ^ nums[i];
            }
        }
        res = xorOfInput ^ xorOfRange;
        return res;
    }
}
