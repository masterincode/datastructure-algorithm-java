package com.tutort.classroom.array.xor;

import com.tutort.LeetCodeURLConstant;

public class DecodeXORedArray {

    public static void main(String[] args) {
       for (int val : decode(new int[]{1,2,3}, 1))
           System.out.print("["+val+"]");
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.DECODE_XOR_ARRAY);
    }

    public static int[] decode(int[] encoded, int first) {

        int[] ans = new int[encoded.length + 1];
        ans[0] = first;
        int j;
        for(int i = 0; i < encoded.length; i++) {
            j = i + 1;
            if(i == 0) {
                ans[j] = first ^ encoded[i];
            } else {
                ans[j] = ans[j - 1] ^ encoded[i];
            }
        }
        return ans;
    }
}
