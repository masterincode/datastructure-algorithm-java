package com.tutort.classroom.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ZigZagArray {
    public static void main(String[] args) {
        List<Integer[]> testCases = new ArrayList<>();
        testCases.add(new Integer[]{1, 4, 3, 2}); //1, 4, 2, 3
        testCases.add(new Integer[]{4, 3, 7, 8, 6, 2, 1}); //3, 7, 4, 8, 2, 6, 1
        testCases.add(new Integer[]{5}); //5
        testCases.add(new Integer[]{1,2,3,4,5}); // 1, 3, 2, 5, 4
        testCases.add(new Integer[]{2,4,7,8,9,10}); //2, 7, 4, 9, 8, 10
        int test = 1;

        for (Integer[] testCase : testCases) {
            System.out.println("Test Cases: " + test);
            for (int value: optimisedApproach(testCase))
                System.out.print("["+value+"]");
            test++;
            System.out.println();
        }
    }

    //Time Complexity: O(n log n)
    //Space Complexity: O(n)
    public static Integer[] bruteForceApproach(Integer[] arr) {
        Arrays.sort(arr);
        Integer[] result = new Integer[arr.length];
        int length = (arr.length % 2 == 0) ? (arr.length/2) - 1 : arr.length/2;
        int j = arr.length - 1;
        int k = 0;
        for (int i = 0; i <= length; i++) {
                result[k++] = arr[i];
                if(k < result.length) {
                    result[k++] = arr[j];
                    j--;
                }
        }
        return result;
    }

    //Time Complexity: O(n)
    //Space Complexity: O(1)
    public static Integer[] optimisedApproach(Integer[] arr) {

        for (int i = 0; i < arr.length - 1; i++) {
            if(i % 2 == 0) { // for Even Index
                if(arr[i] > arr[i+1]) {
                    swapping(arr, i);
                }
            } else { // for Odd Index
                if(arr[i] < arr[i+1]) {
                    swapping(arr, i);
                }
            }
        }
        return arr;
    }

    private static void swapping(Integer[] arr, int i) {
        int temp = arr[i];
        arr[i] = arr[i+1];
        arr[i+1] = temp;
    }
}
