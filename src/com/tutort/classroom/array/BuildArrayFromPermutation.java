package com.tutort.classroom.array;

import com.tutort.LeetCodeURLConstant;

import java.util.ArrayList;
import java.util.List;

public class BuildArrayFromPermutation {

    public static void main(String[] args) {
        List<Integer[]> testCases = new ArrayList<>();
        testCases.add(new Integer[]{0,2,1,5,3,4});
        testCases.add(new Integer[]{5,0,1,2,3,4});
        int test = 1;

        for (Integer[] testCase : testCases) {
            System.out.println("Test Cases: " + test);
            for (int value: optimisedApproach(testCase))
                System.out.print("["+value+"]");
            test++;
            System.out.println();
        }
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.BUILD_ARRAY_FROM_PERMUTATION);
    }

    // Time Complexity : O(n)
    // Space Complexity : O(1)
    //TODO: https://www.youtube.com/watch?v=XblA2DKBOSI&t=733s&ab_channel=ProgrammersZone
    public static Integer[] optimisedApproach(Integer[] nums) {
        int n = nums.length;

        for (int i = 0; i < nums.length; i++) {
            nums[i] = n * (nums[nums[i]] % n) + nums[i]; //Formula to assign initial + final value at index i.
        }
        for (int j = 0; j < nums.length; j++) {
            nums[j] = nums[j] / n;
        }
        return nums;
    }

    // Time Complexity : O(n)
    // Space Complexity : O(n)
    public static int[] bruteForceApproach(int[] nums) {

        if(nums.length == 0) {
            return nums;
        }
        int[] ans = new int[nums.length];
        for(int i = 0; i < nums.length; i++) {
            ans[i] = nums[nums[i]];
        }

        return ans;
    }
}
