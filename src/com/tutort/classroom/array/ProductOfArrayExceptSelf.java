package com.tutort.classroom.array;

import com.tutort.LeetCodeURLConstant;

public class ProductOfArrayExceptSelf {
    public static void main(String[] args) {
        //I/P: {1,2,3,4} <=> O/P: 24,12,8,6
        //I/P: {-1,1,0,-3,3} <=> O/P: 0,0,9,0,0
        for (int ab: bruteForceApproach(new int[]{-1,1,0,-3,3})) {
            System.out.print(ab+",");
        }
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.PRODUCT_OF_ARRAY_EXCEPT_SELF);
    }

    // Time Complexity: O(n^2)
    // Time Complexity: n
    public static int[] bruteForceApproach(int[] nums) {
        if (nums.length == 0) {
            return nums;
        }
        if (nums.length == 1) {
            nums[0] = 1;
            return nums;
        }
        int product;
        int[] result = new int[nums.length];
        for(int i = 0; i < nums.length; i++) {
            product = 1;
            for(int j = 0; j < nums.length; j++) {
                if(i != j) {
                    product = product * nums[j];
                }
            }
            result[i] = product;
        }
        return result;
    }

    // Time Complexity: O(n)
    // Time Complexity: 2n coz we took 2 new arrays lhs and rhs
    public static int[] optimisedApproach(int[] nums) {

        if(nums.length == 0) {
            return nums;
        }
        if(nums.length == 1) {
            nums[0] = 1;
            return nums;
        }
        int[] lhs = new int[nums.length]; // lhs [1,1,2,6]
        int[] rhs = new int[nums.length]; // rhs [24,12,4,1]

        //Calculate L.H.S =  L.H.S[i-1] * nums[i-1]
        for(int i = 0; i < nums.length; i++) {
            if(i == 0)
                lhs[i] = 1;
            else
                lhs[i] = lhs[i-1] * nums[i-1];
        }

        //Calculate R.H.S = R.H.S[j + 1] * nums[j + 1]
        for(int j = nums.length - 1; j >= 0; j--) {
            if(j == nums.length - 1)
                rhs[j] = 1;
            else
                rhs[j] = rhs[j + 1] * nums[j + 1];
        }

        //Calculate result = L.H.S * R.H.S
        for(int k = 0; k < nums.length; k++) {
            nums[k] = lhs[k] * rhs[k];
        }
        return nums;

    }
}
