package com.tutort.classroom.array;

import java.util.*;

public class RightMaximumPart2 {

    public static void main(String[] args) {
        //TODO: Test Cases for Brute Force Approach and Optimized
        List<Integer[]> testCases1 = new ArrayList<>();
        testCases1.add(new Integer[]{16,17,4,3,5,2});  // 17, 17, 5, 5, 5, 2
        testCases1.add(new Integer[]{6,2,4,5,3,1});  // 6, 5, 5, 5, 3, 1
        testCases1.add(new Integer[]{0,-4,19,1,8,-2,-3,5}); // 19, 19, 19, 8, 8, 5, 5, 5
        testCases1.add(new Integer[]{5}); // -1,
        testCases1.add(new Integer[]{0,0,4,0,0,6}); // 6, 6, 6, 6, 6, 6
        int test1 = 1;

        for (Integer[] testCase : testCases1) {
            System.out.println("Test Cases for Brute Force Approach and Optimized: " + test1);
            for (Integer val: optimisedApproach2(testCase))
                System.out.print(val+", ");
            test1++;
            System.out.println();
        }
    }

    // Time Complexity : O(n^2)
    // Space Complexity : O(1)
    public static Integer[] bruteForceApproach(Integer[] arr) {
        if(arr.length == 0) {
            return arr;
        }
        if(arr.length == 1) {
            arr[0] = -1;
            return arr;
        }
        int max;
        for(int i = 0; i < arr.length; i++) {
            max = Integer.MIN_VALUE;
            for(int j = i; j < arr.length; j++) {
                if(arr[j] > max) {
                    max = arr[j];
                }
            }
            arr[i] = max;
        }
        return arr;
    }

    // Time Complexity : O(n)
    // Space Complexity : O(1)
    public static Integer[] optimisedApproach2(Integer[] arr) {
        if(arr.length == 0) {
            return arr;
        }
        if(arr.length == 1) {
            arr[0] = -1;
            return arr;
        }
        for(int i = arr.length - 2; i >= 0 ; i--) {
            if(arr[i+1] > arr[i]) {
                arr[i] = arr[i+1];
            }
        }
        return arr;
    }
}
