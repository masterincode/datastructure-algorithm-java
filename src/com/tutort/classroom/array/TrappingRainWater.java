package com.tutort.classroom.array;

import com.tutort.LeetCodeURLConstant;

public class TrappingRainWater {

    public static void main(String[] args) {
        System.out.println(optimisedApproach(new int[]{0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1}));
        System.out.println(optimisedApproach(new int[]{4, 2, 0, 3, 2, 5}));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.TRAPPING_RAIN_WATER);
    }

    // Time Complexity : O(n^2)
    // Space Complexity : O(1)
    public static int bruteForceApproach(int[] height) {
        int total = 0;
        int leftMax = Integer.MIN_VALUE;
        int rightMax = Integer.MIN_VALUE;

        for (int i = 0; i < height.length; i++) {

            //Find Left max
            for (int j = 0; j <= i; j++) {
                if (height[j] > leftMax) {
                    leftMax = height[j];
                }
            }
            //Find Right max
            for (int k = i; k < height.length; k++) {
                if (height[k] > rightMax) {
                    rightMax = height[k];
                }
            }
            total = total + (Math.min(leftMax, rightMax) - height[i]) * 1;
            leftMax = Integer.MIN_VALUE;
            rightMax = Integer.MIN_VALUE;
        }
        return total;
    }

    // Time Complexity : O(n)
    // Space Complexity : O(2n)
    public static int betterApproach(int[] height) {
        int total = 0;
        int[] leftMaxArray = new int[height.length];
        int[] rightMaxArray = new int[height.length];

        //Find Left Maximum array
        leftMaxArray[0] = height[0];
        for (int i = 1; i < height.length; i++) {
            leftMaxArray[i] = Math.max(leftMaxArray[i - 1], height[i]);
        }

        //Find Right Maximum array
        rightMaxArray[height.length - 1] = height[height.length - 1];
        for (int j = height.length - 2; j >= 0; j--) {
            rightMaxArray[j] = Math.max(height[j], rightMaxArray[j + 1]);
        }

        for (int k = 0; k < height.length; k++) {
            total = total + (Math.min(leftMaxArray[k], rightMaxArray[k]) - height[k]) * 1;
        }
        return total;
    }

    // Time Complexity : O(n)
    // Space Complexity : O(1)
    public static int optimisedApproach(int[] height) {
        int length = height.length;
        if (length <= 2)
            return 0;

        int maxLeft = height[0];
        int maxRight = height[length - 1];
        int trappedWater = 0;
        int left = 1;   //Left pointer
        int right = length - 2;    //Right pointer
        while (left <= right) {
            if (maxLeft < maxRight) {
                if (height[left] >= maxLeft) {
                    maxLeft = height[left];
                } else {
                    trappedWater = trappedWater + maxLeft - height[left];
                }
                left++;
            } else {
                if (height[right] > maxRight) {
                    maxRight = height[right];
                } else {
                    trappedWater = trappedWater + maxRight - height[right];
                }
                right--;
            }
        }
        return trappedWater;
    }
}
