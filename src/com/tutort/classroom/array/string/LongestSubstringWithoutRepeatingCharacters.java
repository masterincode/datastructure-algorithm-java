package com.tutort.classroom.array.string;

import com.tutort.LeetCodeURLConstant;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class LongestSubstringWithoutRepeatingCharacters {

    public static void main(String[] args) {
        System.out.println(lengthOfLongestSubstringBruteForce("pwwkew"));
        System.out.println(lengthOfLongestSubstringOptimisedApproach("pwwkew"));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.LONGEST_SUBSTRING_WITHOUT_REPEATING_CHARACTERS);
    }


    public static int lengthOfLongestSubstringBruteForce(String s) {
        if (s.length() == 0)
            return 0;
        int maxLength = Integer.MIN_VALUE;
        for (int i = 0; i < s.length(); i++) // outer loop for traversing the string
        {
            Set<Character> hashSet = new HashSet<>();
            for (int j = i; j < s.length(); j++) // nested loop for getting different string starting with str[i]
            {
                if (hashSet.contains(s.charAt(j))) // if element if found so mark it as ans and break from the loop
                {
                    maxLength = Math.max(maxLength, j - i);
                    break;
                }
                hashSet.add(s.charAt(j));
            }
        }
        return maxLength;
    }

    public static int lengthOfLongestSubstringOptimisedApproach(String s) {
        if(s == null || s.length() == 0) {
            return 0;
        }
        // 1. Two pointers at same index
        int leftIndex = 0, rightIndex = 0;
        int lengthOfString = s.length();
        int maxLength = Integer.MIN_VALUE;
        HashMap<Character, Integer> map = new HashMap<>();

        //2. Iterate rightIndex till last index of string
        while (rightIndex < lengthOfString) {

            if (map.containsKey(s.charAt(rightIndex))) {
                leftIndex = Math.max(map.get(s.charAt(rightIndex)) + 1, leftIndex);
            }

            map.put(s.charAt(rightIndex), rightIndex);

            // Reason, rightIndex - leftIndex + 1 is both pointer starting from index 0
            maxLength = Math.max(maxLength, rightIndex - leftIndex + 1);
            rightIndex++;
        }
        return maxLength;
    }
}
