package com.tutort.classroom.array.string;

import com.tutort.LeetCodeURLConstant;

public class ValidPalindrome {

    public static void main(String[] args) {
        System.out.println(isPalindromeApproach1("A1man, a plan, a canal: Panam1a"));
        System.out.println(isPalindromeApproach2("A1 man, a plan, a canal: Panam1a"));
        System.out.println(isPalindromeApproach3("A1 man, a plan, a canal: Panam1a"));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.VALID_PALINDROME);
    }

    public static boolean isPalindromeApproach1(String s) {
        String actual = s.toLowerCase().replaceAll("[^a-zA-Z0-9]", "");
        String rev = "";
        for(int i= actual.length()-1; i >=0; i--) {
            rev = rev + actual.charAt(i);
        }
        if(actual.equalsIgnoreCase(rev))
            return true;
        else
            return false;
    }

    // My Simple preferred Logic
    public static boolean isPalindromeApproach3(String s) {
        char[] arr = s.toCharArray();
        String result = "";
        String reverse = "";
        for(char ch : arr) {
            if(Character.isLetterOrDigit(ch)) {
                result = result + ch;
            }
        }
        for (int i = result.length() - 1; i >=0 ; i--) {
            reverse = reverse + result.charAt(i);
        }
        if(result.equalsIgnoreCase(reverse)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isPalindromeApproach2(String s) {
        int left = 0, right = s.length() - 1;
        while (left < right) {
            while (left < right && !Character.isLetterOrDigit(s.charAt(left))) {
                left++;
            }
            while (left < right && !Character.isLetterOrDigit(s.charAt(right))) {
                right--;
            }
            if(Character.toLowerCase(s.charAt(left)) != Character.toLowerCase(s.charAt(right))) {
                return false;
            }
            left++;
            right--;
        }
        return true;
    }
}
