package com.tutort.classroom.array.matrix;

public class DiagonalTraversal {

    public static void main(String[] args) {
        int[][] array = new int[5][5];

        for (int gap = 0; gap < array.length; gap++) {
            for (int row = 0, col = gap; col < array.length; row++, col++) {
                array[row][col] = 1;
            }
        }
    }
}
