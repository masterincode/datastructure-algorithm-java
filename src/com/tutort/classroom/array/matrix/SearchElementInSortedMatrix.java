package com.tutort.classroom.array.matrix;

public class SearchElementInSortedMatrix {

    public static void main(String[] args) {
        int[][] arr = {{1, 5, 9}, {10, 11, 12}, {13, 14, 15}};
        System.out.println(searchMatrix(arr, 6));
    }

    public static int searchMatrix(int[][] matrix, int target) {

//        if (matrix.length == 0)
//            return false;
        int n = matrix.length;
        int m = matrix[0].length;

        int low = 0;
        int high = (n * m) - 1;
        int res = matrix[(target - 1) / m][(target - 1) % m];

//        while (low <= high) {
//            int mid = (low + (high - low) / 2);

//            if (matrix[mid / m][mid % m] == target) {  // [mid / m] will give row, [mid % m] will give column
//                return true;
//            }
//            if (matrix[mid / m][mid % m] < target) {
//                low = mid + 1;
//            } else {
//                high = mid - 1;
//            }
//        }
        return res;
    }

//    public static boolean searchMatrix(int[][] matrix, int target) {
//
//        if (matrix.length == 0)
//            return false;
//        int n = matrix.length;
//        int m = matrix[0].length;
//
//        int low = 0;
//        int high = (n * m) - 1;
//
//        while (low <= high) {
//            int mid = (low + (high - low) / 2);
//            if (matrix[mid / m][mid % m] == target) {  // [mid / m] will give row, [mid % m] will give column
//                return true;
//            }
//            if (matrix[mid / m][mid % m] < target) {
//                low = mid + 1;
//            } else {
//                high = mid - 1;
//            }
//        }
//        return false;
//    }
}
