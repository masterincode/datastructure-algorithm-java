package com.tutort.classroom.array.matrix;

import com.tutort.LeetCodeURLConstant;

import java.util.Arrays;

public class FloodFill {

    public static void main(String[] args) {
        int[][] image = {{1,1,1},{1,1,0},{1,0,1}};
        for (int[] op: floodFill(image, 1 , 1 , 2))
            System.out.println(Arrays.toString(op));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.FLOOD_FILL);
    }

//    Time Complexity: O(N), where N is the number of pixels in the image. We might process every pixel.
//    Space Complexity: O(N), the size of the implicit call stack when calling dfs.
    public static int[][] floodFill(int[][] image, int sr, int sc, int color) {
        int original = image[sr][sc];
        floodFillUtil(image, sr, sc, color, original);
        return image;
    }

    public static void floodFillUtil(int[][] image, int sr, int sc, int color, int original) {
        if(sr < 0 || sc < 0 || sr == image.length || sc == image[0].length || image[sr][sc] != original || image[sr][sc] == color) {
            return;
        }

        image[sr][sc] = color;
        floodFillUtil(image, sr + 1, sc, color, original); // Down
        floodFillUtil(image, sr, sc - 1, color, original); // Left
        floodFillUtil(image, sr, sc + 1, color, original); // Right
        floodFillUtil(image, sr - 1, sc, color, original); // Up
    }
}
