package com.tutort.classroom.array;

import com.tutort.LeetCodeURLConstant;

public class ConcatenationOfArray {

    public static void main(String[] args) {
        System.out.print("[");
        for (int val: getConcatenation(new int[]{1,3,2,1})) {
            System.out.print(val+",");
        }
        System.out.print("]");
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.CONCATENATION_OF_ARRAY);
    }

    public static int[] getConcatenation(int[] nums) {
        int[] ans = new int[nums.length * 2];

        if(nums == null || nums.length == 0) {
            return ans;
        }
        int n = nums.length;
        for(int i = 0; i < nums.length; i++) {
            ans[i] = nums[i];
            ans[i + n] = nums[i];
        }
        return ans;
    }
}
