package com.tutort.classroom.array;

import com.tutort.LeetCodeURLConstant;

import java.util.*;

public class RotateArrayByKTimes {

    public static void main(String[] args) {

        // Please note most of the problems rotate from left to right by K times
        // But in leetcode it has asked to rotate from Right to Left by K times
        rotateUsingArrayDeque(new int[]{1, 2, 3, 4, 5, 6, 7}, 3); // O/P: [5, 6, 7, 1, 2, 3, 4]
        rotateUsingArrayDeque(new int[]{-1, -100, 3, 99}, 2);
        rotateUsingArrayDeque(new int[]{1, 2}, 3);
        System.out.println();
        rotateOptimisedApproach(new int[]{1, 2, 3, 4, 5, 6, 7}, 3);
        rotateOptimisedApproach(new int[]{1, 2}, 3);
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.ROTATE_ARRAY_BY_K_TIMES);

    }

    // Time Complexity: O (n)
    // Space Complexity: O (1)
    // Link: https://www.youtube.com/watch?v=ENcnXXiRT6E&t=316s&ab_channel=HelloWorld
    public static void rotateOptimisedApproach(int[] nums, int k) {
        int n = nums.length;

        // It will handle case when you have array length of 7 but rotate 10 times
        // After rotation of 7 times, again the array will come to initial position, so now only rotate 3 times more
        k = k % n;
        //         0  1  2  3  4  5  6
        //        [1, 2, 3, 4, 5, 6, 7]  n = 7, k = 3
        //case1:   s        e
        //case2:               s     e
        //case3:   s                 e
        reverseArray(nums, 0, n - k - 1);  //case1:
        reverseArray(nums, n - k, n - 1);   //case2:
        reverseArray(nums, 0, n - 1);     //case3:
        System.out.println("rotateOptimisedApproach: " + Arrays.toString(nums));
    }

    public static void reverseArray(int[] nums, int start, int end) {
        // Swapping Logic is Used
        while (start < end) {
            int temp = nums[start];
            nums[start] = nums[end];
            nums[end] = temp;
            start++;
            end--;
        }
    }


    // Time Complexity: O (n^2)
    // Space Complexity: O (1)
    public static void rotateBruteForceApproach(int[] nums, int k) {
        System.out.println("Before: " + Arrays.toString(nums));
        int count = 1;
        while (count <= k) {
            rotateElementBy1(nums);
            count++;
        }
        System.out.println("After: " + Arrays.toString(nums));

    }

    private static void rotateElementBy1(int[] nums) {
        int temp1 = 0; // 7
        int temp2 = 0; // 7
        for (int i = 0; i < nums.length; i++) {
            if (i == 0) {
                temp1 = nums[i];
                nums[i] = nums[nums.length - 1];
            } else {
                temp2 = nums[i];
                nums[i] = temp1;
                temp1 = temp2;
            }
        }
    }


    public static void rotateUsingArrayDeque(int[] nums, int k) {

        ArrayDeque<Integer> queue = new ArrayDeque<>();
        for (int val : nums) {
            queue.add(val);
        }

        int count = 1;
        while (count <= k) {
            int a = queue.removeLast(); // O (1)
            queue.addFirst(a); // O (1)
            count++;
        }

        for (int i = 0; i < nums.length; i++) {
            nums[i] = queue.pop();
        }
        System.out.println(Arrays.toString(nums));
    }
}
