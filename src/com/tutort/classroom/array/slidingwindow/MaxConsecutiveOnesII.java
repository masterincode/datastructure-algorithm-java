package com.tutort.classroom.array.slidingwindow;

public class MaxConsecutiveOnesII {

    public static void main(String[] args) {
        System.out.println(optimizedAppraoch(new int[] {0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1})); //5
        System.out.println(optimizedAppraoch(new int[] {1, 0, 1, 1, 1, 0, 0, 1})); //5
        System.out.println(optimizedAppraoch(new int[] {0, 1, 0, 1, 0, 1, 0, 1})); //3
        System.out.println(optimizedAppraoch(new int[] {0, 1, 1, 1, 1, 1, 0, 1, 0, 1})); //7
    }


    public static int optimizedAppraoch(int[] nums) {
        int k = 1; //Number of flipping allowed
        int start = -1;
        int max = Integer.MIN_VALUE;
        int zeroCount = 0;

        for (int end = 0; end < nums.length; end++) {
            if(nums[end] == 0) {
                zeroCount++;
            }
            if(zeroCount > k) {
                start++;
                if(nums[start] == 0) {
                    zeroCount--;
                }
            }
            max = Math.max(end - start, max);
        }
        return max;
    }
}
