package com.tutort.classroom.array.slidingwindow;

import com.tutort.LeetCodeURLConstant;

public class MinimumSizeSubarraySum {

    public static void main(String[] args) {
        System.out.println(minSubArrayLengthBruteForceApproach(7, new int[]{2, 3, 1, 2, 4, 3}));
        System.out.println(minSubArrayLengthOptmisedApproach(7, new int[]{2, 3, 1, 2, 4, 3}));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.MINIMUM_SIZE_SUBARRAY_SUM);
    }

    // Time Complexity: O (n^2)
    // Space Complexity: O (n)
    public static int minSubArrayLengthBruteForceApproach(int target, int[] nums) {
        int n = nums.length;
        if (n == 0)
            return 0;
        int minLength = Integer.MAX_VALUE;
        int[] prefixSum = new int[n];
        prefixSum[0] = nums[0];

        for (int i = 1; i < n; i++)
            prefixSum[i] = prefixSum[i - 1] + nums[i];

        for (int i = 0; i < n; i++) {
            for (int j = i; j < n; j++) {
                int sum = prefixSum[j] - prefixSum[i] + nums[i];
                if (sum >= target) {
                    minLength = Math.min(minLength, (j - i + 1));
                    break; //Found the smallest subarray with sum>=target starting with index i, hence move to next index
                }
            }
        }
        return (minLength != Integer.MAX_VALUE) ? minLength : 0;
    }

    // Time Complexity: O (n)
    // Space Complexity: O (n)
    public static int minSubArrayLengthOptmisedApproach(int target, int[] nums) {
        int n = nums.length;
        int minLength = Integer.MAX_VALUE;
        int left = 0;
        int right = 0;
        int sum = 0;

        while (right < n) {
            sum = sum + nums[right];

            while (sum >= target) {
                minLength = Math.min(minLength, right - left + 1);
                sum = sum - nums[left];
                left++;
            }
            right++;
        }

        return (minLength != Integer.MAX_VALUE) ? minLength : 0;
    }


}
