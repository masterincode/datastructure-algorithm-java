package com.tutort.classroom.array.slidingwindow;

import com.tutort.LeetCodeURLConstant;

public class MaxConsecutiveOnesI {

    public static void main(String[] args) {
        System.out.println(findMaxConsecutiveOnes(new int[]{1,1,0,1,1,1}));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.MAX_CONSECUTIVE_ONES);
    }

    public static int findMaxConsecutiveOnes(int[] nums) {
        int max = Integer.MIN_VALUE;
        int start = 0, end = 0;
        for(int i = 0; i < nums.length; i++) {
            if(nums[i] == 1) {
                end++;
            } else {
                start = end;
                end = 0;
            }
            if(start > max ) {
                max = start;
            }
            if(end > max) {
                max =  end;
            }
        }
        return max;
    }
}
