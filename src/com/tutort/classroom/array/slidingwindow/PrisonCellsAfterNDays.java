package com.tutort.classroom.array.slidingwindow;

import com.tutort.LeetCodeURLConstant;

public class PrisonCellsAfterNDays {
    public static void main(String[] args) {
        for (int val: prisonAfterNDaysApproach2(new int[]{0,1,0,1,1,0,0,1}, 7))
            System.out.print(val+",");
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.PRISON_CELLS_AFTER_N_DAYS);
    }

    //Time Complexity: O(n^2)
    //Space Complexity: n
    public static int[] prisonAfterNDaysApproach2(int[] cells, int n) {
        n = n % 14 == 0? 14 : n % 14;
        for(int i = 0; i < n; i++)
        {
            int[] next = new int[cells.length];
            for(int j = 1; j < cells.length-1; j++)
            {
                if(cells[j-1]==cells[j+1])
                {
                    next[j]=1;
                }
                else
                {
                    next[j]=0;
                }
            }
            cells = next;
        }
        return cells;
    }

    //TODO: My Logic but facing Issue of Time Limit Exceeded
    public static int[] prisonAfterNDaysAppraoch1(int[] cells, int n) {
        int[] result =  new int[cells.length];
        int i = -1;
        int day = 1;
        n = n % 14 == 0? 14 : n % 14;
        while (i < cells.length && day <= n) {
            i++;
            if(i == 0 || i == cells.length - 1) {
                result[i] = 0;
            } else {
                if(cells[i - 1] == 0 && cells[i + 1] == 0) {
                    result[i] = 1;
                } else if(cells[i - 1] == 1 && cells[i + 1] == 1) {
                    result[i] = 1;
                } else {
                    result[i] = 0;
                }
            }
            if(i == cells.length - 1) {
                for (int j = 0; j < result.length; j++) {
                    cells[j] = result[j];
                }
                day++;
                i = -1;
            }
        }
        return result;
    }

}
