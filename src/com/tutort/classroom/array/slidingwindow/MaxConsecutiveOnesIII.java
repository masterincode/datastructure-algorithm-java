package com.tutort.classroom.array.slidingwindow;

import com.tutort.LeetCodeURLConstant;

public class MaxConsecutiveOnesIII {
    public static void main(String[] args) {
        System.out.println(optimizedAppraoch(new int[] {1,1,1,0,0,0,1,1,1,1,0}, 2)); //6
        System.out.println(optimizedAppraoch(new int[] {0,0,1,1,0,0,1,1,1,0,1,1,0,0,0,1,1,1,1}, 3)); //10
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.MAX_CONSECUTIVE_ONES_III);
    }

    public static int bruteForceApproach(int[] nums, int k) {
        int start = -1;
        int max = Integer.MIN_VALUE;
        int zeroCount = 0;

        for (int end = 0; end < nums.length; end++) {
            if(nums[end] == 0) {
                zeroCount++;
            }
            while (zeroCount > k) {
                start++;
                if(nums[start] == 0) {
                    zeroCount--;
                }
            }
            max = Math.max(end - start, max);
        }
        return max;
    }

    public static int optimizedAppraoch(int[] nums, int k) {
        int start = -1;
        int max = Integer.MIN_VALUE;
        int zeroCount = 0;

        for (int end = 0; end < nums.length; end++) {
            if(nums[end] == 0) {
                zeroCount++;
            }
            if(zeroCount > k) {
                start++;
                if(nums[start] == 0) {
                    zeroCount--;
                }
            }
            max = Math.max(end - start, max);
        }
        return max;
    }

}
