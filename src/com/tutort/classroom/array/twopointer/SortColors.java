package com.tutort.classroom.array.twopointer;

import com.tutort.LeetCodeURLConstant;

import java.util.Arrays;

public class SortColors {

    public static void main(String[] args) {
        int[] nums = new int[]{2,0,2,1,1,0};
        sortColors(nums);
        System.out.println(Arrays.toString(nums));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.SORT_COLORS);
    }

//    Approach 1: Arrays.sort();   Time complexity: O(n log n)
//    Approach 2: Count No of 0, 1 and 2 and then put into array accordingly. Time complexity: O(n)
//    Approach 3: Use 3 pointers Low, Mid and High [Dutch National Flag Alogrithm]

    //Time Complexity: O(n)
    //Space Complexity: O(1)
    public static void sortColors(int[] nums) {
        int low = 0;
        int high = nums.length - 1;
        int mid = 0, temp;
        while (mid <= high) {
            switch (nums[mid]) {
                case 0: {
                    temp = nums[low];
                    nums[low] = nums[mid];
                    nums[mid] = temp;
                    low++;
                    mid++;
                    break;
                }
                case 1:
                    mid++;
                    break;
                case 2: {
                    temp = nums[mid];
                    nums[mid] = nums[high];
                    nums[high] = temp;
                    high--;
                    break;
                }
            }
        }
    }
}
