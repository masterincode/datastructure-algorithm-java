package com.tutort.classroom.array;

public class ProductOfAnElementInArray {
    public static void main(String[] args) {
        for (int ab: productArray(new int[]{2, 3, 4, 5, 6})) {
            System.out.print(ab+",");
        }
    }

    public static int[] productArray(int[] arr) {

        if(arr.length == 0 || arr.length == 1) {
            return arr;
        }
        int temp = 0;
        int product = 0;
        for(int i = 0; i < arr.length; i++) {
            if(i == 0) {
                temp = arr[i]; // temp = 2
                arr[i] = arr[i] * arr[i+1]; //
            } else if(i == arr.length - 1) {
                arr[i] = temp * arr[i]; // 5 * 6
            } else {
                product = temp * arr[i+1]; // product = 4 * 6 = 24
                temp = arr[i]; // temp = 5
                arr[i] = product; // arr[i] = 24
            }
        }
        return arr;
    }
}
