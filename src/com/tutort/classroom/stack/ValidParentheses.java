package com.tutort.classroom.stack;

import com.tutort.LeetCodeURLConstant;

import java.util.Stack;

public class ValidParentheses {
    public static void main(String[] args) {
        System.out.println(isValid("[{(A+B)}]"));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.VALID_PARENTHESES);
    }

    public static boolean isValid(String s) {
        Stack<Character> stack = new Stack<>();
        for(int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if(ch == '(' || ch == '[' || ch == '{')
                stack.push(ch);
            else if(stack.empty())
                return false;
            else if(ch == ')' && stack.pop() != '(')
                return false;
            else if(ch == ']' && stack.pop() != '[')
                return false;
            else if(ch == '}' && stack.pop() != '{')
                return false;
        }
        return stack.empty();
    }
}
