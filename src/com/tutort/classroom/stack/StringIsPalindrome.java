package com.tutort.classroom.stack;

import java.util.Stack;

public class StringIsPalindrome {

    public static void main(String[] args) {
        System.out.println(stringIsPalindromeOptimisedAppraoch("aabaa"));
        System.out.println(stringIsPalindromeOptimisedAppraoch("aabbaa"));
        System.out.println(stringIsPalindromeOptimisedAppraoch("aabca"));
    }

    // TODO:
    // Approach 1: Reverse the string , T.C: O (n)

    // Approach 2: Use 2 pointers
    // Time Complexity: O (n)
    public static boolean stringIsPalindromeBruteForceAppraoch(String str) {

        int i = 0;
        int j = str.length() - 1;
        boolean isPalindrome = false;
        while (i < j) {
            if (str.charAt(i) == str.charAt(j)) {
                isPalindrome = true;
            } else {
                isPalindrome = false;
            }
            i++;
            j--;
        }
        return isPalindrome;
    }

    // Approach 3: Use stack
    // Time Complexity: O (n/2)
    public static boolean stringIsPalindromeOptimisedAppraoch(String str) {

        boolean isPalindrome = false;
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < str.length(); i++) {
            if (i < str.length() / 2) {
                stack.push(str.charAt(i));
            } else if (str.length() % 2 == 0 || i > str.length() / 2) {
                if (stack.pop() == str.charAt(i)) {
                    isPalindrome = true;
                } else {
                    isPalindrome = false;
                    break;
                }
            }
        }
        return isPalindrome;
    }

}
