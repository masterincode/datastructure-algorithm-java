package com.tutort.classroom.stack;

import com.tutort.LeetCodeURLConstant;

import java.util.Stack;

public class EvaluateReversePolishNotation {


    public static void main(String[] args) {
        System.out.println(evalRPN(new String[]{"2", "1", "+", "3", "*"}));
        System.out.println(evalRPN(new String[]{"4","13","5","/","+"}));
        System.out.println(evalRPN(new String[]{"10","6","9","3","+","-11","*","/","*","17","+","5","+"}));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.EVALUATE_REVERSE_POLISH_NOTATION);
    }

    public static int evalRPN(String[] tokens) {
        Stack<Integer> stack = new Stack<>();

        for (int i = 0; i < tokens.length; i++) {
            int second;
            int first;
            String ch = tokens[i];
            if (ch.equals("+")) {
                stack.push(stack.pop() + stack.pop());
            } else if (ch.equals("-")) {
                second = stack.pop();
                first = stack.pop();
                stack.push(first - second);
            } else if (ch.equals("/")) {
                 second = stack.pop();
                 first = stack.pop();
                stack.push( first / second);
            } else if (ch.equals("*")) {
                stack.push(stack.pop() * stack.pop());
            } else {
                stack.push(Integer.valueOf(ch));
            }
        }
        return stack.pop();
    }
}
