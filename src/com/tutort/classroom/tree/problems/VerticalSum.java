package com.tutort.classroom.tree.problems;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.*;
class Pair {
    TreeNode node;
    int distance;
    Pair(TreeNode node, int distance) {
        this.node = node;
        this.distance = distance;
    }
}
public class VerticalSum {

    public static void main(String[] args) {
        Tree tree = new Tree();
        Integer arr1[] = {50, 60, 65, 55, 40, 35, 45, 30, 37, 42, 48};
        Arrays.stream(arr1).forEach(value -> {
            tree.insert(value);
        });
        System.out.println(tree.root);
        System.out.println("Vertical Sum using PreOrder: " + verticalSum(tree.root));
        System.out.println("Vertical Sum using LOT:      " +verticalSumUsingLevelOrderTraversal(tree.root));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.VERTICAL_SUM);
    }

    //==========================Pre Order Traversal====================================================
    static Map<Integer, Integer> map = new TreeMap<>();
    public static ArrayList<Integer> verticalSum(TreeNode root) {
        verticalSumUsingPreOrder(root, 0);
        ArrayList<Integer> list = new ArrayList<>();
        map.forEach((key, value) -> list.add(value));
        return list;
    }
    public static void verticalSumUsingPreOrder(TreeNode root, int distance) {
    // 1. Base Condition
        if (root == null) {
            return;
        }
    // 2. Process
        Integer sum = map.get(distance);
        if (sum == null) {
            map.put(distance, root.val);
        } else {
            map.put(distance, sum + root.val);
        }
    // 3. Pre Order
        verticalSumUsingPreOrder(root.left, distance - 1);
        verticalSumUsingPreOrder(root.right, distance + 1);
    }
    //=========================Level Order Traversal===========================================
    public static ArrayList<Integer> verticalSumUsingLevelOrderTraversal(TreeNode root) {
        Queue<Pair> queue = new ArrayDeque<>();
        Map<Integer, Integer> map = new TreeMap<>();
        queue.add(new Pair(root, 0));
        while (!queue.isEmpty()) {
            Pair current = queue.poll();
            // Process
            Integer sum = map.get(current.distance);
            if(sum == null) {
                map.put(current.distance, current.node.val);
            } else {
                map.put(current.distance, sum + current.node.val);
            }
            // Level order traversal
            if (current.node.left != null) {
                queue.add(new Pair(current.node.left, current.distance - 1));
            }
            if (current.node.right != null) {
                queue.add(new Pair(current.node.right, current.distance + 1));
            }
        }
        ArrayList<Integer> list = new ArrayList<>();
        map.forEach((key, value) -> list.add(value));
        return list;
    }
}
