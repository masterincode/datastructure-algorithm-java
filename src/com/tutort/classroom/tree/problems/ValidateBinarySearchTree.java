package com.tutort.classroom.tree.problems;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.Arrays;

public class ValidateBinarySearchTree {

    public static void main(String[] args) {
        Tree tree = new Tree();
        Integer arr1[] = {50, 60, 65, 55, 40, 35, 45, 30, 37};
        Arrays.stream(arr1).forEach(value -> tree.insert(value));
        System.out.println("Level: Traversal: " + isValidBST(tree.root));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.VALIDATE_BINARY_SEARCH_TREE);
    }

    public static boolean isValidBST(TreeNode root) {
        return isValidBST(root, Long.MIN_VALUE, Long.MAX_VALUE);
    }

    public static boolean isValidBST(TreeNode root, long minVal, long maxVal) {
        if (root == null)
            return true;
        if (root.val >= maxVal || root.val <= minVal)
            return false;
        return isValidBST(root.left, minVal, root.val) &&
                isValidBST(root.right, root.val, maxVal);
    }
}
