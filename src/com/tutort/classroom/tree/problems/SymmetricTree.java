package com.tutort.classroom.tree.problems;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.tree.common.TreeNode;

public class SymmetricTree {

    public static void main(String[] args) {
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.SYMMETRIC_TREE);
    }

    public static boolean isSymmetric(TreeNode root) {
        if(root == null) {
            return false;
        }
        return isIsomorphic(root.left, root.right);
    }

    private static boolean isIsomorphic(TreeNode n1, TreeNode n2){
        if(n1 == null && n2 == null)
            return true;
        if(n1 != null && n2 == null)
            return false;
        if(n1 == null && n2 != null)
            return false;
        return (n1.val == n2.val) &&
                isIsomorphic(n1.left,n2.right) &&
                isIsomorphic(n1.right, n2.left);
    }
}
