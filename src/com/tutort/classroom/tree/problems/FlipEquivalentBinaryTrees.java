package com.tutort.classroom.tree.problems;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.tree.common.TreeNode;

public class FlipEquivalentBinaryTrees {

    public static void main(String[] args) {
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.FLIP_EQUIVALENT_BINARY_TREES);
    }

    public boolean flipEquiv(TreeNode root1, TreeNode root2) {

        if (root1 == null && root2 == null)
            return true;
        if (root1 == null || root2 == null)
            return false;

        return   (root1.val == root2.val) &&
                 ((flipEquiv(root1.left, root2.left) && flipEquiv(root1.right, root2.right) ||  //Logic of Identical trees
                 (flipEquiv(root1.left, root2.right) && flipEquiv(root1.right, root2.left))));  //Logic of Isomorphic(Mirror) trees
    }
}
