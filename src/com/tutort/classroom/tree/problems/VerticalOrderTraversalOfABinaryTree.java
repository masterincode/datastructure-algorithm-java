package com.tutort.classroom.tree.problems;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.*;
import java.util.stream.Collectors;

class XandYpair {
    public int xDistance;
    public int yDistance;
    public TreeNode node;

    XandYpair(TreeNode node, int xDistance, int yDistance) {
        this.node = node;
        this.xDistance = xDistance;
        this.yDistance = yDistance;
    }
}
public class VerticalOrderTraversalOfABinaryTree {

    public static void main(String[] args) {
        Tree tree1 = new Tree();
        Integer arr1[] = {50, 40, 35, 45, 60, 55, 65};
        Arrays.stream(arr1).forEach(value -> tree1.insert(value));
        System.out.println("Horizontal Distance: " + verticalTraversal(tree1.root));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.VERTICAL_ORDER_TRAVERSAL_OF_A_BINARY_TREE);
    }

    public static List<List<Integer>> verticalTraversal(TreeNode root) {
        Queue<XandYpair> queue = new ArrayDeque<>();
        Map<Integer, List<XandYpair>> map = new TreeMap<>();
        queue.add(new XandYpair(root, 0,0));
        Comparator<XandYpair> comparator = (a,b) -> {
            if (a.yDistance == b.yDistance && a.node.val < b.node.val) {
                return -1;
            } else if(a.yDistance == b.yDistance && a.node.val > b.node.val) {
                return 1;
            } else {
                return 0;
            }
        };
        while (!queue.isEmpty()) {
            XandYpair current = queue.poll();
            if (map.containsKey(current.xDistance)) {
                List<XandYpair> existingList = map.get(current.xDistance);
                existingList.add(new XandYpair(current.node, current.xDistance, current.yDistance));
                Collections.sort(existingList, comparator);
            } else {
                List<XandYpair> newList = new ArrayList<>();
                newList.add(new XandYpair(current.node, current.xDistance, current.yDistance));
                map.put(current.xDistance, newList);
            }
            if (current.node.left != null) {
                queue.add(new XandYpair(current.node.left, current.xDistance - 1, current.yDistance + 1));
            }
            if (current.node.right != null) {
                queue.add(new XandYpair(current.node.right, current.xDistance + 1, current.yDistance + 1));
            }
        }
        List<List<Integer>> answerList = new ArrayList<>();
        map.forEach((key,value) -> {
            List<Integer> rowList = value.stream().map(pair -> pair.node.val).collect(Collectors.toList());
            answerList.add(rowList);
        });
        return answerList;
    }
}
