package com.tutort.classroom.tree.problems;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.Arrays;

public class InvertBinaryTree {

    public static void main(String[] args) {
        Tree tree1 = new Tree();
        Integer arr1[] = {50, 60, 65, 55, 40, 35, 45, 30, 37, 42, 48};
        Arrays.stream(arr1).forEach(value -> tree1.insert(value));
        System.out.println("isIdentical: " + invertTree(tree1.root));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.INVERT_BINARY_TREE);
    }

    public static TreeNode invertTree(TreeNode root) {
        if(root == null) {
            return root;
        }
        // 1. swapping right node to left node and vice versa
        TreeNode temp = root.left;
        root.left = root.right;
        root.right = temp;

        // 2. recursion call
        invertTree(root.left);
        invertTree(root.right);
        return root;
    }
}
