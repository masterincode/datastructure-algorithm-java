package com.tutort.classroom.tree.problems;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.*;

public class MaximumLevelSumOfABinaryTree {

    public static void main(String[] args) {
        Tree tree = new Tree();
        Integer arr1[] = {50, 60, 65, 55, 40, 35, 45, 30, 37, 42, 48};
        Arrays.stream(arr1).forEach(value -> {
            tree.insert(value);
        });
        System.out.println("Max Level Sum using LOT: " + maxLevelSum(tree.root));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.MAXIMUM_LEVEL_SUM);
    }

    // Time Complexity: O(n)
    // Space Complexity: 2n
    public static int maxLevelSum(TreeNode root) {
        Map<Integer, Integer> map = new HashMap<>();
        Queue<TreeNode> queue = new ArrayDeque<>();
        TreeNode dummy = new TreeNode(0);
        queue.add(root);
        queue.add(dummy);
        int level = 1;
        int maxValue = Integer.MIN_VALUE;
        int maxLevel = -1;
        while (!queue.isEmpty()) {
            TreeNode current = queue.poll();
            if (current == dummy && !queue.isEmpty()) {
                level++;
                queue.add(dummy);
            } else {
                Integer sum = map.get(level);
                if (sum == null) {
                    map.put(level, current.val);
                } else {
                    map.put(level, sum + current.val);
                }
                if (current.left != null) {
                    queue.add(current.left);
                }
                if (current.right != null) {
                    queue.add(current.right);
                }
            }
        }
        for (Map.Entry<Integer, Integer> mapping : map.entrySet()) {
            if (mapping.getValue() > maxValue) {
                maxValue = mapping.getValue();
                maxLevel = mapping.getKey();
            }
        }
        return maxLevel;
    }
}
