package com.tutort.classroom.tree.problems;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.Arrays;

public class BinaryTreeToDoublyLinkedList {

    public static void main(String[] args) {
        Tree tree = new Tree();
        Integer arr1[] = {50, 60, 65, 55, 40, 35, 45, 30, 37};
        Arrays.stream(arr1).forEach(value -> tree.insert(value));
        System.out.println("Level: Traversal: " + bToDLL(tree.root));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.BINARY_TREE_TO_DOUBLY_LINKED_LIST);
    }

    static TreeNode head = null;
    static TreeNode prev = null;
    public static TreeNode bToDLL(TreeNode root)
    {
        convertToDll(root);
        return head;
    }

    private static void convertToDll(TreeNode root) {
        if(root == null) {
            return;
        }
        bToDLL(root.left);
        if(prev == null) {
            head = root;
        } else {
            root.left = prev;
            prev.right = root;
        }
        prev = root;
        bToDLL(root.right);
    }
}
