package com.tutort.classroom.tree.problems;

import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.Arrays;

public class SumOfTress {
    public static void main(String[] args) {
        Tree tree = new Tree();
        Integer arr1[] = {50, 60, 65, 55, 40, 35, 45, 30, 37, 42, 48};
        Arrays.stream(arr1).forEach(value -> {
            tree.insert(value);
        });
        System.out.println(tree.root);
        sumOfTree(tree.root);
        System.out.println();
        System.out.println(tree.root);

    }

    public static int sumOfTree(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int ls = sumOfTree(root.left);//Calling LST
        int rs = sumOfTree(root.right);//Calling RST
        root.val = ls + rs + root.val;//Processing
        return root.val;
    }
}
