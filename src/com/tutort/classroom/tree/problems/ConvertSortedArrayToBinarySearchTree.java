package com.tutort.classroom.tree.problems;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

public class ConvertSortedArrayToBinarySearchTree {

    public static void main(String[] args) {
        Tree tree = new Tree();
        int arr1[] = {50, 60, 65, 55, 40, 35, 45, 30, 37};
        System.out.println("Level: Traversal: " + sortedArrayToBST(arr1));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.CONVERT_SORTED_ARRAY_TO_BINARY_SEARCH_TREE);
    }

    public static TreeNode sortedArrayToBST(int[] nums) {
        if (nums.length == 0) {
            return null;
        }
        return createBinarySearchTree(nums, 0, nums.length - 1);
    }

    private static TreeNode createBinarySearchTree(int[] nums, int start, int end) {
        if (start > end) {
            return null;
        }
        int mid = (start + end) / 2;
        TreeNode newNode = new TreeNode(nums[mid]);
        newNode.left = createBinarySearchTree(nums, start, mid - 1);
        newNode.right = createBinarySearchTree(nums, mid + 1, end);
        return newNode;
    }
}
