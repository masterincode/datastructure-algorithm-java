package com.tutort.classroom.tree.theory;

import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.Arrays;

public class IdenticalTrees {

    public static void main(String[] args) {
        Tree tree1 = new Tree();
        Integer arr1[] = {50, 60, 65, 55, 40, 35, 45, 30, 37, 42, 48};
        Arrays.stream(arr1).forEach(value -> {
            tree1.insert(value);
        });
        Tree tree2 = new Tree();
        Integer arr2[] = {50, 60, 65, 55, 40, 35, 45, 30, 37, 42, 48};
        Arrays.stream(arr2).forEach(value -> {
            tree2.insert(value);
        });
        System.out.println("isIdentical: " + isIdentical(tree1.root, tree2.root));
    }

    public static boolean isIdentical(TreeNode root1, TreeNode root2) {
        if (root1 == null && root2 == null)
            return true;
        if (root1 == null || root2 == null)
            return false;

        //PreOrder
//        if (root1.value == root2.value) {
//            if (isIdentical(root1.left, root2.left)) {
//                if (isIdentical(root1.right, root2.right)) {
//                    return true;
//                }
//            }
//        }
        //PostOrder
        return                         (root1.val == root2.val) &&
               isIdentical(root1.left,root2.left)   &&    isIdentical(root1.right, root2.right);
//        return false;
    }

}
