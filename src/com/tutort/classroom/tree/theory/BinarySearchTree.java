package com.tutort.classroom.tree.theory;

import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.*;

public class BinarySearchTree {

    public static void main(String[] args) {
        Tree tree = new Tree();
        Integer arr1[] = {50, 60, 65, 55, 40, 35, 45, 30, 37, 42, 48};
        Arrays.stream(arr1).forEach(value -> {
            tree.insert(value);
        });
        System.out.println("Level: Traversal: " + levelOrderTraversal(tree.root));
        System.out.println();
        System.out.println("Level of Given Node: " + levelOfGivenNode(tree.root, 48));
        System.out.print("In Order Recursion: ");inOrderRecursion(tree.root);
        System.out.println();
        System.out.println("In Order Iterative: "+inOrderIterative(tree.root));

        System.out.print("Pre Order Recursion: ");preOrderRecursion(tree.root);
        System.out.println();
        System.out.println("Pre Order Iterative: "+preOrderIterative(tree.root));

        System.out.print("Post Order Recursion: ");postOrderRecursion(tree.root);
        System.out.println();
        System.out.println("Post Order Iterative: "+postOrderIterative(tree.root));
        System.out.println("Size of tree: " + sizeOfTree(tree.root));
    }

    //Level Order Traversal also called as BFS
    public static List<Integer> levelOrderTraversal(TreeNode root) {
        if (root == null) {
            return null;
        }
        //TODO new PriorityQueue will throw exception : TreeNode cannot be cast to java.lang.Comparable
        Queue<TreeNode> queue = new ArrayDeque<>();
        List<Integer> output = new ArrayList<>();
        queue.add(root);
        while (!queue.isEmpty()) {
            TreeNode node = queue.poll();
            output.add(node.val);
            if (node.left != null) {
                queue.add(node.left);
            }
            if (node.right != null) {
                queue.add(node.right);
            }
        }
        return output;
    }
// ===========================PreOrder, InOrder and PostOrder using Recursion=============================
    public static void inOrderRecursion(TreeNode root) {
        if (root == null) {
            return;
        }
        inOrderRecursion(root.left); //Calling LST
        System.out.print(root.val + ", "); //Processing
        inOrderRecursion(root.right); //Calling RST
    }

    public static void preOrderRecursion(TreeNode root) {
        if (root == null) {
            return;
        }
        System.out.print(root.val + ", ");//Processing
        preOrderRecursion(root.left); //Calling LST
        preOrderRecursion(root.right);//Calling RST
    }

    public static void postOrderRecursion(TreeNode root) {
        if (root == null) {
            return;
        }
        postOrderRecursion(root.left); //Calling LST
        postOrderRecursion(root.right);//Calling RST
        System.out.print(root.val + ", ");//Processing
    }
// ===========================PreOrder, InOrder and PostOrder using Iterative=============================
    public static List<Integer> inOrderIterative(TreeNode root) {
       Stack<TreeNode> stack = new Stack<>();
       List<Integer> result =  new ArrayList<>();
       TreeNode current = root;
       while (true) {
           while (current != null) {
               stack.push(current);
               current = current.left;
           }
           if(stack.isEmpty()) {
               break;
           }
           current = stack.pop();
           result.add(current.val);
           current = current.right;
       }
       return result;
    }

    public static List<Integer> preOrderIterative(TreeNode root) {
        Stack<TreeNode> stack = new Stack<>();
        List<Integer> result =  new ArrayList<>();
        TreeNode current = root;
        while (true) {
            while (current != null) {
                stack.push(current);
                result.add(current.val);
                current = current.left;
            }
            if(stack.isEmpty()) {
                break;
            }
            current = stack.pop();
            current = current.right;
        }
        return result;
    }

    public static List<Integer> postOrderIterative(TreeNode root) {
        Stack<TreeNode> stack = new Stack<>();
        List<Integer> result =  new ArrayList<>();
        TreeNode current = root;
        TreeNode previous = null;
        while(true) {
            while(current != null) {
                stack.push(current);
                current = current.left;
            }
            if(stack.isEmpty()) {
                break;
            }
            while (current == null && !stack.isEmpty()) {
                current = stack.peek();
                if(current.right == null || current.right == previous) {
                    result.add(current.val);
                    stack.pop();
                    previous = current;
                    current = null;
                } else {
                    current = current.right;
                }
            }
        }
        return result;
    }

// =======================================================================================================
    //It has PostOrder Approach
    public static int sizeOfTree(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int ls = sizeOfTree(root.left);//Calling LST
        int rs = sizeOfTree(root.right);//Calling RST
        int currentSize = ls + rs + 1;//Processing
        return currentSize;
    }

    public static int levelOfGivenNode(TreeNode root, int target) {
        if (root == null)
            return -1;
        return levelNodeUtil(root, target, 0);
    }

    private static int levelNodeUtil(TreeNode root, int target, int level) {
        if (root == null) {
            return -1;
        }
        if (target == root.val) {
            return level;
        }
        int ls = levelNodeUtil(root.left, target, level + 1);
        if (ls != -1) {
            return ls;
        }
        int rs = levelNodeUtil(root.right, target, level + 1);
        if (rs != -1) {
            return rs;
        }
        return -1;
    }

}
