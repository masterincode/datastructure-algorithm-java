package com.tutort.classroom.tree.theory;

import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.Arrays;

public class IsomorphicTrees {

    public static void main(String[] args) {
        Tree tree1 = new Tree();
        Integer arr1[] = {50, 60, 65, 55, 40, 35, 45, 30, 37, 42, 48};
        Arrays.stream(arr1).forEach(value -> {
            tree1.insert(value);
        });
        Tree tree2 = new Tree();
        Integer arr2[] = {50, 60, 65, 55, 40, 35, 45, 30, 37, 42, 48};
        Arrays.stream(arr2).forEach(value -> {
            tree2.insert(value);
        });
        System.out.println("isIdentical: " + isIsomorphic(tree1.root, tree2.root));
    }
    public static boolean isIsomorphic(TreeNode n1, TreeNode n2){
        if(n1 == null && n2 == null)
            return true;
        if(n1 != null && n2 == null)
            return false;
        if(n1 == null && n2 != null)
            return false;

        return                      (n1.val == n2.val) &&
             isIsomorphic(n1.left,n2.right)   &&     isIsomorphic(n1.right, n2.left);
    }
}
