package com.tutort.classroom.tree.theory;

import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.*;

public class RightView {

    public static void main(String[] args) {
        Tree tree1 = new Tree();
        Integer arr1[] = {50, 40, 35, 45, 43, 60, 55, 65, 58, 57};
        Arrays.stream(arr1).forEach(value -> tree1.insert(value));
        System.out.println("left View Using Iterative: " + leftViewUsingIterative(tree1.root));
        System.out.print("left View Using Resursion: ");leftViewUsingRecursion(tree1.root, 0);
    }

    public static List<Integer> leftViewUsingIterative(TreeNode root) {
        if (root == null) {
            return null;
        }
        Queue<TreeNode> queue = new ArrayDeque<>();
        TreeNode dummy = new TreeNode(0);
        queue.add(root);
        queue.add(dummy);
        List<Integer> ans = new ArrayList<>();
        ans.add(root.val);
        while (!queue.isEmpty()) {
            TreeNode temp = queue.poll();
            if (temp == dummy && !queue.isEmpty()) {
                queue.add(dummy);
                ans.add(queue.peek().val);
            } else {
        //In Right view, We will insert right node first followed by left node.
                if (temp.right != null) {
                    queue.add(temp.right);
                }
                if (temp.left != null) {
                    queue.add(temp.left);
                }
            }
        }
        return ans;
    }

    static int maxLevel = -1; //***
    //It uses preOrder Traversal
    public static void leftViewUsingRecursion(TreeNode root, int level) {
        //1. Base Condition
        if (root == null) {
            return;
        }
        //2. Process
        if (maxLevel < level) {
            System.out.print(root.val +", ");
            maxLevel = level;
        }
        //2. Level find Logic is used
        //In Right view, We will pass right node first followed by left node.
        leftViewUsingRecursion(root.right, level + 1);
        leftViewUsingRecursion(root.left, level + 1);
        return;
    }
}
