package com.tutort.classroom.tree.theory;

import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.*;

public class HorizontalDistance {
    public static void main(String[] args) {
        Tree tree1 = new Tree();
        Integer arr1[] = {50, 40, 35, 45, 60, 55, 65};
        Arrays.stream(arr1).forEach(value -> tree1.insert(value));
        System.out.print("Horizontal Distance: ");
        horizontalDistance(tree1.root, 0);
        System.out.println(map);
    }

    static Map<Integer, List<Integer>> map = new HashMap<>();

    //Horizontal distance using PreOrder Traversal
    public static void horizontalDistance(TreeNode root, int distance) {
    // 1. Base Condition
        if (root == null) {
            return;
        }
    // 2. Process
        List<Integer> node = map.get(distance);
        if (node == null) {
            node = new ArrayList<>();
            node.add(root.val);
            map.put(distance, node);
        } else {
            node.add(root.val);
            map.put(distance, node);
        }
    // PreOrder traversal
        horizontalDistance(root.left, distance - 1);
        horizontalDistance(root.right, distance + 1);
        return;
    }
}
