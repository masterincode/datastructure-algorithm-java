package com.tutort.classroom.tree.theory;

import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.*;

public class TopView {

    //TODO: Top View uses Horizontal Distance Logic
    public static void main(String[] args) {
        Tree tree1 = new Tree();
        Integer arr1[] = {50, 40, 35, 45, 60, 55, 65};
        Arrays.stream(arr1).forEach(value -> tree1.insert(value));
        System.out.print("Horizontal Distance: ");
        horizontalDistance(tree1.root, 0, 0);
        System.out.println(map);
    }

    static Map<Integer, Map<Integer,Integer>> map = new HashMap<>();

    public static void horizontalDistance(TreeNode root, int distance, int level) {
        if (root == null) {
            return;
        }
        Map<Integer,Integer> node = map.get(distance);
        if (node == null) {
            node = new HashMap<>();
            node.put(root.val, level);
            map.put(distance, node);
        } else {
            if(node.get(root.val) != null && level < node.get(root.val)) {
                node.put(root.val,level);
                map.put(distance, node);
            }

        }
        horizontalDistance(root.left, distance - 1, level + 1);
        horizontalDistance(root.right, distance + 1, level + 1);
        return;
    }
}
