package com.tutort.classroom.tree.theory;

import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Queue;

public class DeepestNode {
    public static void main(String[] args) {
        Tree tree = new Tree();
        Integer arr1[] = {50, 60, 65, 55, 40, 35, 45, 30, 37, 42, 48};
        Arrays.stream(arr1).forEach(value -> tree.insert(value));
        System.out.println("Level: Traversal: " + levelOrderTraversal(tree.root));
    }

    public static TreeNode levelOrderTraversal(TreeNode root) {
        if (root == null) {
            return root;
        }
        //TODO new PriorityQueue will throw exception : TreeNode cannot be cast to java.lang.Comparable
        Queue<TreeNode> queue = new ArrayDeque<>();
        TreeNode dummy = new TreeNode(0);
        TreeNode lastNode = null;
        int level = 0;
        queue.add(root);
        queue.add(dummy);
        while (!queue.isEmpty()) {
            TreeNode temp = queue.poll();
            if(dummy != temp) {
                lastNode = temp;
            }
            if (dummy == temp && !queue.isEmpty()) {
                level++;
                queue.add(dummy);
            } else {
                if (temp.left != null) {
                    queue.add(temp.left);
                }
                if (temp.right != null) {
                    queue.add(temp.right);
                }
            }
        }
        return lastNode;
    }
}
