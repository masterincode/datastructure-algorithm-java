package com.tutort.classroom.tree.theory;


import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Queue;

public class LevelOrderTraversal {

    public static void main(String[] args) {
        Tree tree = new Tree();
        Integer arr1[] = {50, 60, 65, 55, 40, 35, 45, 30, 37, 42, 48};
        Arrays.stream(arr1).forEach(value -> tree.insert(value));
        System.out.println("Level: Traversal: " + levelOrderTraversal(tree.root, 50));
        System.out.println("Level: Traversal: " + levelOrderTraversal(tree.root, 40));
        System.out.println("Level: Traversal: " + levelOrderTraversal(tree.root, 55));
        System.out.println("Level: Traversal: " + levelOrderTraversal(tree.root, 37));
    }

    public static int levelOrderTraversal(TreeNode root, int target) {
        if (root == null) {
            return 0;
        }
        //TODO new PriorityQueue will throw exception : TreeNode cannot be cast to java.lang.Comparable
        Queue<TreeNode> queue = new ArrayDeque<>();
        TreeNode dummy = new TreeNode(0);
        int level = 0;
        queue.add(root);
        queue.add(dummy);
        while (!queue.isEmpty()) {
            TreeNode temp = queue.poll();
            if (target == temp.val) {
                return level;
            }
            if (dummy == temp) {
                level++;
                queue.add(dummy);
            } else {
                if (temp.left != null) {
                    queue.add(temp.left);
                }
                if (temp.right != null) {
                    queue.add(temp.right);
                }
            }
        }
        return -1;
    }
}
