package com.tutort.classroom.tree.common;

public class Tree {
    public TreeNode root;
    public boolean insert(int value) {
        TreeNode newNode = new TreeNode(value);
        //1. Check for root null
        if (this.root == null) {
            this.root = newNode;
        }
        TreeNode current = this.root; //2. Current Node to traverse tree.
        while (true) {
            //3. Check for same value
            if (value == current.val)
                return false;
            //4. Check for value < Left
            if (value < current.val) {
                if (current.left == null) {
                    current.left = newNode;
                    return true;
                }
                current = current.left;
                //5. Check for value > Right
            } else {
                if (current.right == null) {
                    current.right = newNode;
                    return true;
                }
                current = current.right;
            }
        }
    }
}
