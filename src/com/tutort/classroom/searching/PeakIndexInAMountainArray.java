package com.tutort.classroom.searching;

import com.tutort.LeetCodeURLConstant;

public class PeakIndexInAMountainArray {

    public static void main(String[] args) {
        System.out.println(peakIndexInMountainArray(new int[]{0, 10, 5, 2}));
        System.out.println(peakIndexInMountainArray(new int[]{0, 2, 4, 10, 9, 8, 6, 7, 5, 3, 1}));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.PEAK_INDEX_IN_A_MOUNTAIN_ARRAY);
    }

    public static int peakIndexInMountainArray(int[] arr) {
        int lowIndex = 0;
        int highIndex = arr.length - 1;
        while (lowIndex < highIndex) {
            int midIndex = (lowIndex + highIndex) / 2;
            if (arr[midIndex] > arr[midIndex + 1]) {
                highIndex = midIndex;
            }
            if (arr[midIndex] < arr[midIndex + 1]) {
                lowIndex = midIndex + 1;
            }
        }
        return lowIndex;
    }
}
