package com.tutort.classroom.searching;

import com.tutort.LeetCodeURLConstant;

public class BinarySearchIterative {

    public static void main(String[] args) {
        System.out.println(search(new int[]{-1, 0, 3, 5, 9, 12}, 9));
        System.out.println(search(new int[]{-1, 0, 3, 5, 9, 12}, 2));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.BINARY_SEARCH);
    }

    public static int search(int[] nums, int target) {
        int lowIndex = 0;
        int highIndex = nums.length - 1;
        int midIndex;

        while (lowIndex <= highIndex) {

            midIndex = (lowIndex + highIndex) / 2;

            if (nums[midIndex] == target) {
                return midIndex;

            } else if (target > nums[midIndex]) {
                lowIndex = midIndex + 1;
            } else {
                highIndex = midIndex - 1;
            }
        }
        return -1;
    }
}
