package com.tutort.classroom.searching;

import com.tutort.LeetCodeURLConstant;

import java.util.Arrays;

public class FindFirstAndLastPositionOfElementInSortedArray {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(searchRange(new int[]{5, 7, 7, 8, 8, 10}, 8)));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.FIND_FIRST_AND_LAST_POSITION_OF_ELEMENT_IN_SORTED_ARRAY);
    }

    public static int[] searchRange(int[] nums, int target) {
        int[] result = new int[2];
        result[0] = findFirst(nums, target);
        result[1] = findLast(nums, target);
        return result;
    }

    private static int findFirst(int[] nums, int target) {
        int index = -1;
        int lowIndex = 0;
        int highIndex = nums.length - 1;
        while (lowIndex <= highIndex) {
            int midIndex = (lowIndex + highIndex) / 2;
            if (nums[midIndex] == target)
                index = midIndex;
            if (target <= nums[midIndex] ) {
                highIndex = midIndex - 1;
            } else {
                lowIndex = midIndex + 1;
            }
        }
        return index;
    }

    private static int findLast(int[] nums, int target) {
        int index = -1;
        int low = 0;
        int high = nums.length - 1;
        while (low <= high) {
            int mid = (low + high) / 2;
            if (nums[mid] <= target) {
                low = mid + 1;
            } else {
                high = mid - 1;
            }
            if (nums[mid] == target)
                index = mid;
        }
        return index;
    }
}
