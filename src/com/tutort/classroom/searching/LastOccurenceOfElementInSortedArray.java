package com.tutort.classroom.searching;

public class LastOccurenceOfElementInSortedArray {

    public static void main(String[] args) {
        System.out.println(search(new int[]{1, 2, 3, 4, 4, 4, 5, 6, 7, 8}, 4));
        System.out.println(search(new int[]{1, 2, 5, 6, 7, 8}, 6));
        System.out.println(search(new int[]{1, 1, 1, 2, 3}, 1));
        System.out.println(search(new int[]{6, 7, 8, 8, 8}, 8));
    }

    public static int search(int[] nums, int target) {
        int index = -1;
        int lowindex = 0;
        int highIndex = nums.length - 1;
        while (lowindex <= highIndex) {
            int mid = (lowindex + highIndex) / 2;
            if (nums[mid] == target) {
                index = mid;
            }
            if (nums[mid] <= target) {
                lowindex = mid + 1;
            } else {
                highIndex = mid - 1;
            }
        }
        return index;
    }
}
