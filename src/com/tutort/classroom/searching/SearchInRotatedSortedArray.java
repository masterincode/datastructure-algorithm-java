package com.tutort.classroom.searching;

import com.tutort.LeetCodeURLConstant;

public class SearchInRotatedSortedArray {

    public static void main(String[] args) {
        System.out.println(search(new int[]{4, 5, 6, 7, 0, 1, 2}, 0));
        System.out.println(search(new int[]{11, 13, 15, 1, 3, 5, 7, 9}, 3));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.SEARCH_IN_ROTATED_SORTED_ARRAY);
    }

    public static int search(int[] nums, int target) {
        int lowIndex = 0, highIndex = nums.length - 1, midIndex;
        while (lowIndex <= highIndex) {
            midIndex = (lowIndex + highIndex) / 2;
            if (target == nums[midIndex]) {
                return midIndex;
            }
            if (nums[lowIndex] <= nums[midIndex]) {
                if (target >= nums[lowIndex] && target < nums[midIndex]) {
                    highIndex = midIndex - 1;
                } else {
                    lowIndex = midIndex + 1;
                }
            } else {
                if (target > nums[midIndex] && target <= nums[highIndex]) {
                    lowIndex = midIndex + 1;
                } else {
                    highIndex = midIndex - 1;
                }
            }
        }
        return -1;
    }
}
