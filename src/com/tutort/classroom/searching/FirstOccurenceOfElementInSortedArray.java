package com.tutort.classroom.searching;

public class FirstOccurenceOfElementInSortedArray {

    public static void main(String[] args) {
        System.out.println(search(new int[]{1, 2, 3, 4, 4, 4, 5, 6, 7, 8}, 4));
        System.out.println(search(new int[]{1, 2, 3, 4, 5, 6, 7, 8}, 6));
        System.out.println(search(new int[]{1, 1, 1, 1, 1, 1, 2, 3}, 1));
        System.out.println(search(new int[]{6, 7, 8, 8, 8}, 8));
    }

    public static int search(int[] nums, int target) {
        int index = -1;
        int lowIndex = 0;
        int highIndex = nums.length - 1;
        while (lowIndex <= highIndex) {
            int midIndex = (lowIndex + highIndex) / 2;
            if (nums[midIndex] == target) {
                index = midIndex;
            }
            if (target <= nums[midIndex] ) {
                highIndex = midIndex - 1;
            } else {
                lowIndex = midIndex + 1;
            }
        }
        return index;
    }
}
