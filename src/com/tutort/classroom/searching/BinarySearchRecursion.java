package com.tutort.classroom.searching;

public class BinarySearchRecursion {

    public static void main(String[] args) {
        int[] nums = {2, 5, 6, 8, 9, 10};
        System.out.println(binarySearch(nums, 5));
        System.out.println(binarySearch(nums, 10));
        System.out.println(binarySearch(nums, 11));
    }

    public static int binarySearch(int[] nums, int target) {
        int left = 0;
        int right = nums.length - 1;
        return binarySearchUtil(nums, left, right, target);
    }

    public static int binarySearchUtil(int[] nums, int left, int right, int target) {
        if (left > right) {
            return -1;
        }
        int mid = (left + right) / 2;
        if (nums[mid] == target) {
            return mid;
        } else if (target < nums[mid]) {
            return binarySearchUtil(nums, left, mid - 1, target);
        } else {
            return binarySearchUtil(nums, mid + 1, right, target);
        }
    }
}
