package com.tutort;

import java.awt.*;
import java.net.URI;
import java.util.Scanner;

public class LeetCodeURLConstant {

    public static final String FIZZ_BUZZ = "https://leetcode.com/problems/fizz-buzz/";
    public static final String REVERSE_VOWELS_OF_A_STRING = "https://leetcode.com/problems/reverse-vowels-of-a-string/";
    public static final String MERGE_SORTED_ARRAY_IN_FIRST_ARRAY = "https://leetcode.com/problems/merge-sorted-array/";
    public static final String RICHEST_CUSTOMER_WEALTH = "https://leetcode.com/problems/richest-customer-wealth/";
    public static final String RUNNING_SUM_OF_1D_ARRAY = "https://leetcode.com/problems/running-sum-of-1d-array/";
    public static final String MINIMUM_ABSOLUTE_DIFFERENCE = "https://leetcode.com/problems/minimum-absolute-difference/";
    public static final String THREE_CONSECUTIVE_ODDS = "https://leetcode.com/problems/three-consecutive-odds/";
    public static final String MAJORITY_ELEMENT_IN_ARRAY = "https://leetcode.com/problems/majority-element/";
    public static final String MAJORITY_ELEMENT_IN_ARRAY_PART_2 = "https://leetcode.com/problems/majority-element-ii/submissions/";
    public static final String MOVE_ZEROES = "https://leetcode.com/problems/move-zeroes/";
    public static final String JEWELS_AND_STONES = "https://leetcode.com/problems/jewels-and-stones/";
    public static final String TRANSPOSE_MATRIX = "https://leetcode.com/problems/transpose-matrix/";
    public static final String HAPPY_NUMBERS = "https://leetcode.com/problems/happy-number/";
    public static final String POWER_OF_TWO = "https://leetcode.com/problems/power-of-two/";
    public static final String VALID_ANAGRAM = "https://leetcode.com/problems/valid-anagram/";
    public static final String VALID_PALINDROME = "https://leetcode.com/problems/valid-palindrome/";
    public static final String FIND_DIFF_IN_WORD = "https://leetcode.com/problems/find-the-difference/";
    public static final String FIND_THIRD_MAX_NUMBER = "https://leetcode.com/problems/third-maximum-number/";
    public static final String ADD_DIGITS = "https://leetcode.com/problems/add-digits/";
    public static final String UGLY_NUMBER = "https://leetcode.com/problems/ugly-number/";
    public static final String SUM_OF_DIGITS_OF_STRING_AFTER_CONVERT = "https://leetcode.com/problems/sum-of-digits-of-string-after-convert/";
    public static final String PRODUCT_OF_ARRAY_EXCEPT_SELF = "https://leetcode.com/problems/product-of-array-except-self/";
    public static final String TWO_SUM = "https://leetcode.com/problems/two-sum/";
    public static final String TWO_SUM_PART_2 = "https://leetcode.com/problems/two-sum-ii-input-array-is-sorted/";
    public static final String PASCALS_TRIANGLE = "https://leetcode.com/problems/pascals-triangle/";
    public static final String PASCALS_TRIANGLE_PART_2 = "https://leetcode.com/problems/pascals-triangle-ii/";
    public static final String BEST_TIME_TO_BUY_AND_SELL_STOCK = "https://leetcode.com/problems/best-time-to-buy-and-sell-stock/";
    public static final String BEST_TIME_TO_BUY_AND_SELL_STOCK_PART_2 = "https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/";
    public static final String BUILD_ARRAY_FROM_PERMUTATION = "https://leetcode.com/problems/build-array-from-permutation/";
    public static final String KIDS_WITH_THE_GREATEST_NUMBER_OF_CANDIES = "https://leetcode.com/problems/kids-with-the-greatest-number-of-candies/";
    public static final String RIGHT_MAXIMUM = "https://leetcode.com/problems/replace-elements-with-greatest-element-on-right-side/";
    public static final String TRAPPING_RAIN_WATER = "https://leetcode.com/problems/trapping-rain-water/";
    public static final String THREE_SUM = "https://leetcode.com/problems/3sum/";
    public static final String THREE_SUM_CLOSET = "https://leetcode.com/problems/3sum-closest/submissions/";
    public static final String FOUR_SUM = "https://leetcode.com/problems/4sum/";
    public static final String CONCATENATION_OF_ARRAY = "https://leetcode.com/problems/concatenation-of-array/";
    public static final String LONGEST_SUBSTRING_WITHOUT_REPEATING_CHARACTERS = "https://leetcode.com/problems/longest-substring-without-repeating-characters/";
    public static final String MAX_CONSECUTIVE_ONES = "https://leetcode.com/problems/max-consecutive-ones/";
    public static final String MAX_CONSECUTIVE_ONES_III = "https://leetcode.com/problems/max-consecutive-ones-iii/";
    public static final String ROTATE_IMAGE = "https://leetcode.com/problems/rotate-image/";
    public static final String ROTATE_ARRAY_BY_K_TIMES = "https://leetcode.com/problems/rotate-array/";
    public static final String CONTAINS_DUPLICATE_I = "https://leetcode.com/problems/contains-duplicate/";
    public static final String CONTAINS_DUPLICATE_II = "https://leetcode.com/problems/contains-duplicate-ii/";
    public static final String SUMMARY_RANGES = "https://leetcode.com/problems/summary-ranges/";
    public static final String RANGE_SUM_QUERY_IMMUTABLE = "https://leetcode.com/problems/range-sum-query-immutable/";
    public static final String RANGE_SUM_QUERY_2D_IMMUTABLE = "https://leetcode.com/problems/range-sum-query-2d-immutable/";
    public static final String REMOVE_ELEMENT = "https://leetcode.com/problems/remove-element/";
    public static final String INTERSECTION_OF_TWO_ARRAYS_I = "https://leetcode.com/problems/intersection-of-two-arrays/";
    public static final String INTERSECTION_OF_TWO_ARRAYS_II = "https://leetcode.com/problems/intersection-of-two-arrays-ii/";
    public static final String NEXT_GREATER_ELEMENT_I = "https://leetcode.com/problems/next-greater-element-i/";
    public static final String NEXT_GREATER_ELEMENT_II = "https://leetcode.com/problems/next-greater-element-ii/";
    public static final String NEXT_GREATER_ELEMENT_III = "https://leetcode.com/problems/next-greater-element-iii/";
    public static final String REDUCE_ARRAY_SIZE_TO_THE_HALF = "https://leetcode.com/problems/reduce-array-size-to-the-half/";
    public static final String RANK_TEAMS_BY_VOTES = "https://leetcode.com/problems/rank-teams-by-votes/";
    public static final String MINIMUM_SIZE_SUBARRAY_SUM = "https://leetcode.com/problems/minimum-size-subarray-sum/";
    public static final String SORT_COLORS = "https://leetcode.com/problems/sort-colors/";

    //Maths
    public static final String PRISON_CELLS_AFTER_N_DAYS = "https://leetcode.com/problems/prison-cells-after-n-days/";

    //Bits Manipulation
    public static final String SINGLE_NUMBER = "https://leetcode.com/problems/single-number/";
    public static final String MISSING_NUMBER = "https://leetcode.com/problems/missing-number/";
    public static final String DECODE_XOR_ARRAY = "https://leetcode.com/problems/decode-xored-array/";

    //Linked List
    public static final String MIDDLE_OF_THE_LINKED_LIST = "https://leetcode.com/problems/middle-of-the-linked-list/";
    public static final String NTH_NODE_FROM_END_OF_LINKED_LIST = "https://leetcode.com/problems/remove-nth-node-from-end-of-list/";
    public static final String ODD_EVEN_LINKED_LIST = "https://leetcode.com/problems/odd-even-linked-list/";
    public static final String ADD_TWO_NUMBERS = "https://leetcode.com/problems/add-two-numbers/";
    public static final String DELETE_NODE_IN_LINKED_LIST = "https://leetcode.com/problems/delete-node-in-a-linked-list/";
    public static final String REMOVE_DUPLICATES_FROM_SORTED_LIST = "https://leetcode.com/problems/remove-duplicates-from-sorted-list/";
    public static final String REVERSE_LINKED_LIST = "https://leetcode.com/problems/reverse-linked-list/";
    public static final String CONVERT_BINARY_NUMBER_IN_A_LINKED_LIST_TO_INTEGER = "https://leetcode.com/problems/convert-binary-number-in-a-linked-list-to-integer/";
    public static final String ADD_TWO_NUMBERS_II = "https://leetcode.com/problems/add-two-numbers-ii/";
    public static final String LINKED_LIST_CYCLE = "https://leetcode.com/problems/linked-list-cycle/";
    public static final String COPY_LIST_WITH_RANDOM_POINTER = "https://leetcode.com/problems/copy-list-with-random-pointer/";
    public static final String REMOVE_ZERO_SUM_CONSECUTIVE_NODES_FROM_LINKED_LIST = "https://leetcode.com/problems/remove-zero-sum-consecutive-nodes-from-linked-list/";
    public static final String REMOVE_LINKED_LIST_ELEMENTS = "https://leetcode.com/problems/remove-linked-list-elements/";
    public static final String MERGE_TWO_SORTED_LISTS = "https://leetcode.com/problems/merge-two-sorted-lists/";
    public static final String INTERSECTION_OF_TWO_LINKED_LISTS = "https://leetcode.com/problems/intersection-of-two-linked-lists/";
    public static final String PALINDROME_LINKED_LIST = "https://leetcode.com/problems/palindrome-linked-list/";

    //Recursion
    public static final String FIND_THE_WINNER_OF_THE_CIRCULAR_GAME = "https://leetcode.com/problems/find-the-winner-of-the-circular-game/";

    //Tree
    public static final String INVERT_BINARY_TREE = "https://leetcode.com/problems/invert-binary-tree/";
    public static final String SYMMETRIC_TREE = "https://leetcode.com/problems/symmetric-tree/";
    public static final String FLIP_EQUIVALENT_BINARY_TREES = "https://leetcode.com/problems/flip-equivalent-binary-trees/";
    public static final String POPULATING_NEXT_RIGHT_POINTERS_IN_EACH_NODE_I = "https://leetcode.com/problems/populating-next-right-pointers-in-each-node/";
    public static final String POPULATING_NEXT_RIGHT_POINTERS_IN_EACH_NODE_II = "https://leetcode.com/problems/populating-next-right-pointers-in-each-node-ii/";
    public static final String VERTICAL_ORDER_TRAVERSAL_OF_A_BINARY_TREE = "https://leetcode.com/problems/vertical-order-traversal-of-a-binary-tree/";
    public static final String VERTICAL_SUM = "https://practice.geeksforgeeks.org/problems/vertical-sum/1";
    public static final String MAXIMUM_LEVEL_SUM = "https://leetcode.com/problems/maximum-level-sum-of-a-binary-tree/";
    public static final String BINARY_TREE_POSTORDER_TRAVERSAL = "https://leetcode.com/problems/binary-tree-postorder-traversal/";
    public static final String BINARY_TREE_INORDER_TRAVERSAL = "https://leetcode.com/problems/binary-tree-inorder-traversal/";
    public static final String MAXIMUM_DEPTH_OF_BINARY_TREE = "https://leetcode.com/problems/maximum-depth-of-binary-tree/";
    public static final String MINIMUM_DEPTH_OF_BINARY_TREE = "https://leetcode.com/problems/minimum-depth-of-binary-tree/";
    public static final String BINARY_TREE_LEVEL_ORDER_TRAVERSAL = "https://leetcode.com/problems/binary-tree-level-order-traversal/";
    public static final String AVERAGE_OF_LEVELS_IN_BINARY_TREE = "https://leetcode.com/problems/average-of-levels-in-binary-tree/";
    public static final String DIAMETER_OF_BINARY_TREE = "https://leetcode.com/problems/diameter-of-binary-tree/";
    public static final String PATH_SUM_I = "https://leetcode.com/problems/path-sum/";
    public static final String PATH_SUM_II = "https://leetcode.com/problems/path-sum-ii/";
    public static final String PATH_SUM_III = "https://leetcode.com/problems/path-sum-iii/";
    public static final String VALIDATE_BINARY_SEARCH_TREE = "https://leetcode.com/problems/validate-binary-search-tree/";
    public static final String CONVERT_SORTED_ARRAY_TO_BINARY_SEARCH_TREE = "https://leetcode.com/problems/convert-sorted-array-to-binary-search-tree/";
    public static final String BINARY_TREE_TO_DOUBLY_LINKED_LIST = "https://practice.geeksforgeeks.org/problems/binary-tree-to-dll/1#";
    public static final String BINARY_TREE_ZIGZAG_LEVEL_ORDER_TRAVERSAL = "https://leetcode.com/problems/binary-tree-zigzag-level-order-traversal/";
    public static final String SUM_ROOT_TO_LEAF_NUMBERS = "https://leetcode.com/problems/sum-root-to-leaf-numbers/";
    public static final String DISTRIBUTE_COINS_IN_BINARY_TREE = "https://leetcode.com/problems/distribute-coins-in-binary-tree/";
    public static final String LONGEST_UNIVALUE_PATH = "https://leetcode.com/problems/longest-univalue-path/";
    public static final String SUBTREE_OF_ANOTHER_TREE = "https://leetcode.com/problems/subtree-of-another-tree/";
    public static final String MAXIMUM_WIDTH_OF_BINARY_TREE = "https://leetcode.com/problems/maximum-width-of-binary-tree/";
    public static final String SEARCH_IN_A_BINARY_SEARCH_TREE = "https://leetcode.com/problems/search-in-a-binary-search-tree/";
    public static final String INSERT_INTO_A_BINARY_SEARCH_TREE = "https://leetcode.com/problems/insert-into-a-binary-search-tree/";
    public static final String BALANCE_A_BINARY_SEARCH_TREE = "https://leetcode.com/problems/balance-a-binary-search-tree/";
    public static final String BINARY_SEARCH_TREE_TO_GREATER_SUM_TREE = "https://leetcode.com/problems/binary-search-tree-to-greater-sum-tree/";
    public static final String LOWEST_COMMON_ANCESTOR_OF_A_BINARY_SEARCH_TREE = "https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-search-tree/";
    public static final String RECOVER_BINARY_SEARCH_TREE = "https://leetcode.com/problems/recover-binary-search-tree/";
    public static final String CONSTRUCT_BINARY_SEARCH_TREE_FROM_PREORDER_TRAVERSAL = "https://leetcode.com/problems/construct-binary-search-tree-from-preorder-traversal/";
    public static final String MAXIMUM_SUM_BST_IN_BINARY_TREE = "https://leetcode.com/problems/maximum-sum-bst-in-binary-tree/";

    //Priority Queue
    public static final String MINIMUM_COST_OF_ROPES = "https://practice.geeksforgeeks.org/problems/minimum-cost-of-ropes-1587115620/1#";
    public static final String KTH_LARGEST_ELEMENT_IN_AN_ARRAY = "https://leetcode.com/problems/kth-largest-element-in-an-array/";
    public static final String LAST_STONE_WEIGHT = "https://leetcode.com/problems/last-stone-weight/";
    public static final String KTH_LARGEST_ELEMENT_IN_A_STREAM = "https://leetcode.com/problems/kth-largest-element-in-a-stream/";
    public static final String K_CLOSEST_POINTS_TO_ORIGIN = "https://leetcode.com/problems/k-closest-points-to-origin/";
    public static final String SORT_CHARACTERS_BY_FREQUENCY = "https://leetcode.com/problems/sort-characters-by-frequency/";
    public static final String TOP_K_FREQUENT_ELEMENTS = "https://leetcode.com/problems/top-k-frequent-elements/";
    public static final String KTH_SMALLEST_ELEMENT_IN_A_SORTED_MATRIX = "https://leetcode.com/problems/kth-smallest-element-in-a-sorted-matrix/";
    public static final String TOP_K_FREQUENT_WORDS = "https://leetcode.com/problems/top-k-frequent-words/";
    public static final String FIND_K_PAIRS_WITH_SMALLEST_SUMS = "https://leetcode.com/problems/find-k-pairs-with-smallest-sums/";
    public static final String MERGE_K_SORTED_LISTS = "https://leetcode.com/problems/merge-k-sorted-lists/";
    public static final String FIND_K_TH_SMALLEST_PAIR_DISTANCE = "https://leetcode.com/problems/find-k-th-smallest-pair-distance/";
    public static final String FIND_MEDIAN_FROM_DATA_STREAM = "https://leetcode.com/problems/find-median-from-data-stream/";
    public static final String FIND_THE_KTH_SMALLEST_SUM_OF_A_MATRIX_WITH_SORTED_ROWS = "https://leetcode.com/problems/find-the-kth-smallest-sum-of-a-matrix-with-sorted-rows/";
    public static final String CAR_POOLING = "https://leetcode.com/problems/car-pooling/";

    //Stack
    public static final String VALID_PARENTHESES = "https://leetcode.com/problems/valid-parentheses/";
    public static final String EVALUATE_REVERSE_POLISH_NOTATION = "https://leetcode.com/problems/evaluate-reverse-polish-notation/";

    //Searching
    public static final String BINARY_SEARCH = "https://leetcode.com/problems/binary-search/";
    public static final String FIND_FIRST_AND_LAST_POSITION_OF_ELEMENT_IN_SORTED_ARRAY = "https://leetcode.com/problems/find-first-and-last-position-of-element-in-sorted-array/";
    public static final String PEAK_INDEX_IN_A_MOUNTAIN_ARRAY = "https://leetcode.com/problems/peak-index-in-a-mountain-array/";
    public static final String SEARCH_IN_ROTATED_SORTED_ARRAY = "https://leetcode.com/problems/search-in-rotated-sorted-array/";

    //Dynamic Programming
    public static final String CLIMBING_STAIRS = "https://leetcode.com/problems/climbing-stairs/";
    public static final String FROG_JUMP = "https://www.codingninjas.com/codestudio/problems/frog-jump_3621012";
    public static final String HOUSE_ROBBER_I = "https://leetcode.com/problems/house-robber/";
    public static final String HOUSE_ROBBER_II = "https://leetcode.com/problems/house-robber-ii/";
    public static final String MAXIMUM_SUM_OF_NON_ADJACENT_ELEMENTS = "https://www.codingninjas.com/codestudio/problems/maximum-sum-of-non-adjacent-elements_843261";
    public static final String UNIQUE_PATHS_I = "https://leetcode.com/problems/unique-paths/";
    public static final String UNIQUE_PATHS_II = "https://leetcode.com/problems/unique-paths-ii/";
    public static final String MINIMUM_PATH_SUM = "https://leetcode.com/problems/minimum-path-sum/";
    public static final String KNAPSACK_PROBLEM = "https://www.codingninjas.com/codestudio/problems/0-1-knapsack_920542";
    public static final String COIN_CHANGE_I = "https://leetcode.com/problems/coin-change/";
    public static final String COIN_CHANGE_II = "https://leetcode.com/problems/coin-change-2/";
    public static final String PARTITION_EQUAL_SUBSET_SUM = "https://leetcode.com/problems/partition-equal-subset-sum/";
    public static final String COMBINATION_SUM = "https://leetcode.com/problems/combination-sum/";
    public static final String LONGEST_COMMON_SUBSEQUENCE = "https://leetcode.com/problems/longest-common-subsequence/";
    public static final String PALINDROMIC_SUBSTRINGS = "https://leetcode.com/problems/palindromic-substrings/";
    public static final String LONGEST_PALINDROMIC_SUBSTRING = "https://leetcode.com/problems/longest-palindromic-substring/";
    public static final String LONGEST_PALINDROMIC_SUBSEQUENCE = "https://leetcode.com/problems/longest-palindromic-subsequence/";
    public static final String MAXIMUM_SUBARRAY = "https://leetcode.com/problems/maximum-subarray/";
    public static final String MAXIMUM_PRODUCT_SUBARRAY = "https://leetcode.com/problems/maximum-product-subarray/";
    public static final String LONGEST_COMMON_SUBSTRING = "https://www.codingninjas.com/codestudio/problems/longest-common-substring_1235207";
    public static final String EDIT_DISTANCE = "https://leetcode.com/problems/edit-distance/";
    public static final String DELETE_OPERATION_FOR_TWO_STRINGS = "https://leetcode.com/problems/delete-operation-for-two-strings/";
    public static final String LONGEST_INCREASING_SUBSEQUENCE = "https://leetcode.com/problems/longest-increasing-subsequence/";
    public static final String BEST_TIME_TO_BUY_AND_SELL_STOCK_III = "https://leetcode.com/problems/best-time-to-buy-and-sell-stock-iii/";
    public static final String BEST_TIME_TO_BUY_AND_SELL_STOCK_WITH_COOLDOWN = "https://leetcode.com/problems/best-time-to-buy-and-sell-stock-with-cooldown/";
    public static final String DECODE_WAYS = "https://leetcode.com/problems/decode-ways/";
    public static final String TRIANGLE =  "https://leetcode.com/problems/triangle/";
    public static final String MAXIMUM_PATH_SUM_IN_THE_MATRIX = "https://www.codingninjas.com/codestudio/problems/maximum-path-sum-in-the-matrix_797998";

    //Graph
    public static final String FLOOD_FILL = "https://leetcode.com/problems/flood-fill/";
    public static final String NUMBER_OF_ISLANDS = "https://leetcode.com/problems/number-of-islands/";
    public static final String MAX_AREA_OF_ISLAND = "https://leetcode.com/problems/max-area-of-island/";
    public static final String ROTTING_ORANGES = "https://leetcode.com/problems/rotting-oranges/";
    public static final String SNAKES_AND_LADDERS = "https://leetcode.com/problems/snakes-and-ladders/";
    public static final String WORD_LADDER_I = "https://leetcode.com/problems/word-ladder/";
    public static final String STEPS_BY_KNIGHT = "https://practice.geeksforgeeks.org/problems/steps-by-knight5927/1";
    public static final String ALIEN_DICTIONARY = "https://practice.geeksforgeeks.org/problems/alien-dictionary/1";
    public static final String CLONE_GRAPH = "https://leetcode.com/problems/clone-graph/";
    public static final String COURSE_SCHEDULE = "https://leetcode.com/problems/course-schedule/";

    //Backtracking
    public static final String PERMUTATIONS_OF_A_GIVEN_NUMBER = "https://leetcode.com/problems/permutations/";
    public static final String PERMUTATIONS_OF_A_GIVEN_STRING = "https://practice.geeksforgeeks.org/problems/permutations-of-a-given-string2041/1";
    public static final String N_QUEENS = "https://leetcode.com/problems/n-queens/";
    public static final String SUDOKU_SOLVER = "https://leetcode.com/problems/sudoku-solver/";
    public static final String M_COLORING_PROBLEM = "https://practice.geeksforgeeks.org/problems/m-coloring-problem-1587115620/1";
    public static final String RAT_IN_A_MAZE_PROBLEM_I = "https://practice.geeksforgeeks.org/problems/rat-in-a-maze-problem/1";


    public static void openLeetCodeUrl(String url) {
        try{
            System.out.println();
            System.out.println("Do u want open Leetcode URL: Y / N ?");
            String response = new Scanner(System.in).nextLine();
            if("y".equalsIgnoreCase(response)) {
                Desktop.getDesktop().browse(new URI(url));
            } else if ("".equals(response)) {
                openLeetCodeUrl(url);
            }
        } catch (Exception e) {
            System.out.println("Exception occcured while opening the URL: "+e.getMessage());
        }
    }
}
