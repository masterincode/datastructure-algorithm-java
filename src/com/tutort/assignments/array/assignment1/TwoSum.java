package com.tutort.assignments.array.assignment1;

import com.tutort.LeetCodeURLConstant;

import java.util.*;

public class TwoSum {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(optimizedApproach(new int[]{3, 2, 3}, 6)));
        System.out.println(Arrays.toString(optimizedApproach(new int[]{3, 2, 4}, 6)));
        System.out.println(optimizedApproachUsingSet(new int[]{2,4,5,6,3,7,9}, 10));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.TWO_SUM);
    }

    //Time complexity: O(n^2)
    //Space Complexity: O(1)
    public static int[] bruteForceApproach(int[] nums, int target) {

        if (nums != null) {
            if (nums.length == 0)
                return nums;
            int[] result = new int[2];

            for (int i = 0; i < nums.length; i++) {
                for (int j = i + 1; j < nums.length; j++) {

                    if (nums[i] + nums[j] == target) {
                        result[0] = i;
                        result[1] = j;
                        return result;
                    }
                }
            }
            return result;
        }
        return null;
    }

    //Time complexity: O(n)
    //Space Complexity: O(n)
    public static int[] optimizedApproach(int[] nums, int target) {
        int[] result = new int[2];
        Map<Integer, Integer> map = new HashMap<>();

        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(target - nums[i])) {
                result[0] = i;
                result[1] = map.get(target - nums[i]);
                return result;
            }
            map.put(nums[i], i);
        }
        return result;
    }

    //Time complexity: O(n)
    //Space Complexity: O(n)
    public static Set<List<Integer>> optimizedApproachUsingSet(int[] nums, int target) {

        Set<Integer> set = new HashSet<>();
        Set<List<Integer>> result = new HashSet<>();

        for (int i = 0; i < nums.length; i++) {
            if (set.contains(target - nums[i])) {
                List<Integer> row = new ArrayList<>();
                row.add(nums[i]);
                row.add(target - nums[i]);
                result.add(row);
            }
            set.add(nums[i]);
        }
        return result;
    }
}
