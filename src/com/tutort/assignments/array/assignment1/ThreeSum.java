package com.tutort.assignments.array.assignment1;

import com.tutort.LeetCodeURLConstant;

import java.util.*;

public class ThreeSum {
    public static void main(String[] args) {
        System.out.print(optimisedApproach(new int[]{-1,0,1,2,-1,-4})); // [[-1, -1, 2], [-1, 0, 1]]
//        System.out.print(threeSum(new int[]{0,0,0}));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.THREE_SUM);
    }

    //Time Complexity: O(n^3)
    //Space Complexity: O(n)
    public static List<List<Integer>> bruteForceApproach(int[] nums) {
        List<List<Integer>> resultList = new ArrayList<>();
        List<Integer> list;
        Set<List<Integer>> setList = new HashSet<>();

        for (int i = 0; i < nums.length - 2; i++) {
            for (int j = i + 1; j < nums.length - 1 ; j++) {
                for (int k = j + 1; k < nums.length; k++) {
                    if(nums[i] + nums[j] + nums[k] == 0) {
                        list = new ArrayList<>();
                        list.add(nums[i]);
                        list.add(nums[j]);
                        list.add(nums[k]);
                        Collections.sort(list);
                        setList.add(list);
                    }
                }
            }
        }
        setList.stream().forEach( numList -> {
            resultList.add(numList);
        });
        return resultList;
    }

    //Time Complexity: O(n^2)
    //Space Complexity: O(1) : Ignore output extra space used
    //TODO: https://www.youtube.com/watch?v=E7Ie6OlQgN4&t=72s&ab_channel=PrakashShukla
    public static List<List<Integer>> optimisedApproach(int[] nums) {
        List<List<Integer>> result = new ArrayList();
        Arrays.sort(nums); //O (n log n)
        for(int i=0;i<nums.length;i++){
            if(i==0 || nums[i-1]!=nums[i]){
                twoSumSorted(i+1,nums.length-1,nums,0-nums[i], result);
            }
        }
        return result;
    }


    private static void twoSumSorted(int i,int j,int[] nums,int target, List<List<Integer>> result){
        int a1 = nums[i-1];
        while(i<j){ //search space
            if(nums[i]+nums[j] < target){
                i++;
            }else if(nums[i]+nums[j] > target){
                j--;
            }else{
                List<Integer> list = new ArrayList();
                list.add(a1);
                list.add(nums[i]);
                list.add(nums[j]);
                result.add(list);
                //duplicate b
                while(i < j && nums[i] == nums[i+1])
                    i++;
                //duplicate c
                while(i < j && nums[j] == nums[j-1])
                    j--;

                i++;j--;
            }
        }
    }
}
