package com.tutort.assignments.array.assignment1;

import com.tutort.LeetCodeURLConstant;

import java.util.*;

public class FourSum {

    public static void main(String[] args) {
        System.out.println(optimisedApproach(new int[]{1,0,-1,0,-2,2}, 0));
        System.out.println(optimisedApproach(new int[]{2,2,2,2,2}, 8));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.FOUR_SUM);
    }

    //Time complexity: O(n log n + n^3 * log n)
    //Space Complexity: O(n)
    public static List<List<Integer>> bruteForceApproach(int[] nums, int target) {
        Set<List<Integer>> resultSet = new HashSet<>();
        List<Integer> list;
        Arrays.sort(nums);// O(n log n)
        for (int i = 0; i < nums.length; i++) { // O(n)
            for (int j = i + 1; j < nums.length; j++) { // O(n)
                for (int k = j + 1; k < nums.length; k++) { // O(n)
                    // nums[i] + nums[j] + nums[k] + value = target;
                    int value = target - nums[i] - nums[j] - nums[k];
                    int index = Arrays.binarySearch(nums, k+1, nums.length, value); // log n
                    if (index > -1) {
                        list =  new ArrayList<>();
                        list.add(nums[i]);
                        list.add(nums[j]);
                        list.add(nums[k]);
                        list.add(nums[index]);
                        resultSet.add(list);
                    }
                }
            }
        }
        return new ArrayList<>(resultSet);
    }

    // Time Complexity: O (n Log n + n^3)
    // Space Complexity: 1
    public static List<List<Integer>> optimisedApproach(int[] nums, int target) {
        ArrayList<List<Integer>> resultList = new ArrayList<List<Integer>>();
        if (nums == null || nums.length == 0)
            return resultList;
        int length = nums.length;
        Arrays.sort(nums); // O(n log n)
        for (int i = 0; i < length; i++) { // O(n)
            for (int j = i + 1; j < length; j++) { // O(n)
                int twoSumTarget = target - nums[i] - nums[j];
                int front = j + 1;
                int back = length - 1;

                while(front < back) { // O(n)
                    int twoSum = nums[front] + nums[back];
                    if (twoSum < twoSumTarget)
                        front++;
                    else if (twoSum > twoSumTarget)
                        back--;
                    else {
                        List<Integer> quad = new ArrayList<>();
                        quad.add(nums[i]);
                        quad.add(nums[j]);
                        quad.add(nums[front]);
                        quad.add(nums[back]);
                        resultList.add(quad);
                        // Processing the duplicates of number 3
                        while (front < back && nums[front] == quad.get(2))
                            front++;
                        // Processing the duplicates of number 4
                        while (front < back && nums[back] == quad.get(3))
                            back--;
                    }
                }
                // Processing the duplicates of number 2
                while(j + 1 < length && nums[j + 1] == nums[j])
                    j++;
            }
            // Processing the duplicates of number 1
            while (i + 1 < length && nums[i + 1] == nums[i])
                i++;
        }
        return resultList;
    }
}
