package com.tutort.assignments.array.assignment1;

import com.tutort.LeetCodeURLConstant;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PascalsTriangle {

    public static void main(String[] args) {
        System.out.println(generate(1));
        System.out.println(generate(3));
        System.out.println(generate(5));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.PASCALS_TRIANGLE);
    }

    //Time complexity: O(n^2)
    //Space Complexity: O(n)
    public static List<List<Integer>> generate(int numRows) {
        List<List<Integer>> resultList = new ArrayList<>();
        List<Integer> list;
        List<Integer> nums;
        int sum;
        if (numRows == 0) {
            return resultList;
        }
        resultList.add(new ArrayList<>(Arrays.asList(1))); // INSERT first row
        for (int i = 1; i < numRows; i++) {
            list = new ArrayList<>();
            nums = resultList.get(i - 1);
            list.add(1); // Each row Inserting first column as 1
            for (int j = 1; j < resultList.size(); j++) {
                sum = nums.get(j - 1) + nums.get(j);
                list.add(sum);
            }
            list.add(1); // Each row Inserting Last column as 1
            resultList.add(list);
        }
        return resultList;
    }
}
