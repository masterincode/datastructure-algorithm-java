package com.tutort.assignments.array.assignment1;

import com.tutort.LeetCodeURLConstant;

import java.util.*;

public class MajorityElementPart2 {
    //TODO : Majority element is always greater than (arr.length/2)
    public static void main(String[] args) {
        int[] arr = {1,2};
        System.out.println(optimisedApproach2(arr));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.MAJORITY_ELEMENT_IN_ARRAY_PART_2);
    }

    //Time complexity : O(n^2)
    //Space complexity : O(n)
    public static List<Integer> bruteForceApproach1(int[] nums) {
        List<Integer> list = new ArrayList<>();
        if(nums == null || nums.length == 0) {
            return list;
        }
        if(nums.length == 1) {
            list.add(nums[0]);
            return list;
        }
        int value;
        int count;
        int times = nums.length/3;
        for(int i = 0; i < nums.length; i++) {
            value = nums[i];
            count = 1;
            if(count > times) {
                list.add(value);
            }
            for(int j = i + 1; j < nums.length; j++) {
                if (value == nums[j]) {
                    count++;
                }
            }
            if(count > times) {
                if(!list.contains(value)) {
                    list.add(value);
                }
            }
        }
        return list;
    }


    //Time complexity : O(n)
    //Space complexity : O(n)
    //TODO: Using Hashing Time complexity is improved
    public static List<Integer> optimisedApproach1(int[] nums) {
        List<Integer> list = new ArrayList<>();
        Map<Integer, Integer> map = new HashMap<>();
        if (nums == null || nums.length == 0) {
            return list;
        }
        if (nums.length == 1) {
            list.add(nums[0]);
            return list;
        }
        Integer count;
        for(int value: nums) {
            count = map.get(value);
            if(count == null) {
                map.put(value, 1);
            } else {
                count++;
                map.put(value, count);
            }
        }
        map.forEach((key, val) -> {
            if(val > nums.length / 3) {
                list.add(key);
            }
        });
        return list;
    }

    //Time complexity : O(n)
    //Space complexity : O(1)
    //TODO: Using Moore's Voting Algorithm
    public static List<Integer> optimisedApproach2(int[] nums) {
        List<Integer> list = new ArrayList<>();
        if (nums == null || nums.length == 0) {
            return list;
        }
        if (nums.length == 1) {
            list.add(nums[0]);
            return list;
        }
        int candidate1 = -1;
        int voteCount1 = 0;

        int candidate2 = -1;
        int voteCount2 = 0;

        for(int i = 0; i < nums.length; i++) {
            if(nums[i] == candidate1) {
                voteCount1++;
            } else if(nums[i] == candidate2) {
                voteCount2++;
            } else if(voteCount1 == 0) {
                candidate1 = nums[i];
                voteCount1++;
            } else if(voteCount2 == 0) {
                candidate2 = nums[i];
                voteCount2++;
            } else {
                voteCount1--;
                voteCount2--;
            }
        }

        voteCount1 = 0;
        voteCount2 = 0;
        for (int value: nums) {
            if(value == candidate1) {
                voteCount1++;
            } else if(value == candidate2) {
                voteCount2++;
            }
        }
        if (voteCount1 > nums.length/3)
            list.add(candidate1);
        if (voteCount2 > nums.length/3)
            list.add(candidate2);
        return list;
    }
}
