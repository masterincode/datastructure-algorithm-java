package com.tutort.assignments.array.assignment1;

import com.tutort.LeetCodeURLConstant;

import java.util.Arrays;

public class ThreeSumClosest {

    public static void main(String[] args) {
        System.out.println(optimisedApproach(new int[] {-1,2,1,-4}, 1));
        System.out.println(optimisedApproach(new int[] {0,0,0}, 1));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.THREE_SUM_CLOSET);
    }

    //Time Complexity: O(n^3)
    //Space Complexity: O(n)
    public static int bruteForceApproach(int[] nums, int target) {
        int result = nums[0] + nums[1] + nums[nums.length - 1];
        int sum = 0;
        for (int i = 0; i < nums.length-2; i++) {
            for (int j = i + 1; j < nums.length-1; j++) {
                for (int k = j + 1; k < nums.length; k++) {
                    sum = nums[i] + nums[j] + nums[k];
                    if (Math.abs(sum - target) < Math.abs(result - target)) {
                        result = sum;
                    }
                }
            }
        }
       return sum;
    }

    //Time Complexity: O(n^2)
    //Space Complexity: O(n)
    public static int optimisedApproach(int[] num, int target) {
        int result = num[0] + num[1] + num[num.length - 1];
        Arrays.sort(num);
        for (int i = 0; i < num.length - 2; i++) {
            int start = i + 1;
            int end   = num.length - 1;
            while (start < end) {
                int sum = num[i] + num[start] + num[end];
                if (sum > target) {
                    end--;
                } else {
                    start++;
                }
                if (Math.abs(sum - target) < Math.abs(result - target)) {
                    result = sum;
                }
            }
        }
        return result;
    }
}
