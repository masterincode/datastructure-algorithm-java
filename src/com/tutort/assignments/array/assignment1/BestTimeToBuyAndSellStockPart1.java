package com.tutort.assignments.array.assignment1;

import com.tutort.LeetCodeURLConstant;

public class BestTimeToBuyAndSellStockPart1 {

    public static void main(String[] args) {
        int[] prices = {7,1,5,3,6,4};
        BestTimeToBuyAndSellStockPart1 shares = new BestTimeToBuyAndSellStockPart1();
        System.out.println(shares.maxProfitDp(prices));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.BEST_TIME_TO_BUY_AND_SELL_STOCK);
    }
    //BruteForceApproach  Note: Time Limit Exceeded
    //Time Complexity: O(n^2)
    //Space Complexity: O(1)
    public int maxProfitBruteForceApproach(int[] prices) {

        if(prices.length == 0 || prices.length == 1)
            return 0;
        int maxProfit = 0;
        int profit = 0;
        for(int i=0 ; i < prices.length ; i++) {
            for(int j = i+1; j < prices.length ; j++) {
                profit = prices[j] - prices[i];
                if(profit > maxProfit) {
                    maxProfit = profit;
                }
            }
        }
        return maxProfit;
    }

    //OptimisedApproach
    //Time Complexity: O(n)
    //Space Complexity: O(1)
    //Runtime : 1 ms
    //Memory : 51.9 MB
    public int maxProfitOptimisedApproach(int[] prices) {
        int minprice = Integer.MAX_VALUE;
        int maxprofit = 0;
        for (int i = 0; i < prices.length; i++) {
            if (prices[i] < minprice)
                minprice = prices[i];
            if (prices[i] - minprice > maxprofit)
                maxprofit = prices[i] - minprice;
        }
        return maxprofit;

    }

    // TODO: DP Solution https://www.youtube.com/watch?v=excAOvwF_Wk&ab_channel=takeUforward
    //Time Complexity: O(n)
    //Space Complexity: O(1)
    public int maxProfitDp(int[] prices) {
        int minprice = prices[0];
        int maxprofit = 0;
        for (int i = 1; i < prices.length; i++) {
           int cost = prices[i] - minprice;
           maxprofit = Math.max(maxprofit, cost);
           minprice = Math.min(minprice, prices[i]);
        }
        return maxprofit;
    }
}
