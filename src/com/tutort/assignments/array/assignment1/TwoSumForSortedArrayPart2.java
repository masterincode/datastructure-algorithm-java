package com.tutort.assignments.array.assignment1;

import com.tutort.LeetCodeURLConstant;

import java.util.HashMap;
import java.util.Map;

public class TwoSumForSortedArrayPart2 {
    public static void main(String[] args) {
        for (int ab : optimizedApproach2(new int[]{3, 2, 3}, 6)) {
            System.out.print(ab + ",");
        }
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.TWO_SUM_PART_2);
    }

    //Time complexity: O(n)
    //Space Complexity: O(n)
    public static int[] optimizedApproach2(int[] numbers, int target) {
        int n = numbers.length;
        Map<Integer, Integer> map = new HashMap<>();
        int[] result = new int[2];
        for (int i = 0; i < n; i++) {
            map.put(numbers[i], i);
        }

        for (int i = 0; i < n - 1; i++)
            if (map.containsKey(target - numbers[i])) {
                result[0] = i + 1;
                result[1] = map.get(target - numbers[i]) + 1;
                return result;
            }
        return result;
    }

    //Time complexity: O(n)
    //Space Complexity: O(1)
    //Algorithm: Sliding Window
    public static int[] optimizedApproach1(int[] nums, int target) {
        int[] result = new int[2];
        int j = 0, sum = 0;
        int windowSize = 1;
        for (int i = 0; i < nums.length; i++) {

            j = windowSize + i;
            if (j == nums.length) {
                windowSize++;
                i = 0;
                j = windowSize + i;
            }
            sum = nums[i] + nums[j];
            if (sum == target) {
                result[0] = i;
                result[1] = j;
                return result;
            }

        }
        return result;
    }

    //Time complexity: O(n^2)
    //Space Complexity: O(1)
    public static int[] bruteForceApproach(int[] nums, int target) {

        if (nums != null) {
            if (nums.length == 0)
                return nums;
            int[] result = new int[2];

            for (int i = 0; i < nums.length; i++) {
                for (int j = i + 1; j < nums.length; j++) {

                    if (nums[i] + nums[j] == target) {
                        result[0] = i;
                        result[1] = j;
                        return result;
                    }
                }
            }
            return result;
        }
        return null;
    }
}
