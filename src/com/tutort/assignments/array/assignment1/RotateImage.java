package com.tutort.assignments.array.assignment1;

import com.tutort.LeetCodeURLConstant;

public class RotateImage {

    public static void main(String[] args) {
        int[][] matrix = {{1,2,3},{4,5,6},{7,8,9}};
        int[][] list = optimizedAppraoch(matrix);
        System.out.print("[");
        for (int[] row : list) {
            System.out.print("[");
            for (int col: row) {
                System.out.print(col+",");
            }
            System.out.print("]");
        }
        System.out.print("]");
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.ROTATE_IMAGE);
    }

    // Time Complexity: O(n^2)
    // Space Complexity: n
    public static int[][] bruteForceApproach(int[][] matrix) {
        int length = matrix.length;
        int[][] result = new int[length][length]; //Extra Space taken
        int resultRow = length - 1;
        for(int row = 0; row < length; row++) {
            for(int col = 0; col < length; col++) {
                result[col][resultRow] = matrix[row][col];
            }
            resultRow--;
        }
        return result;
    }

    // Time Complexity: O(n^2)
    // Space Complexity: 1
    public static int[][] optimizedAppraoch(int[][] matrix) {
        for(int row = 0; row < matrix.length; row++) {
            for(int col = row; col < matrix[0].length; col++) {
                int temp = matrix[row][col];
                matrix[row][col] = matrix[col][row];
                matrix[col][row] = temp;
            }
        }
        for (int row = 0; row < matrix.length; row++) {
            int left = 0;
            int right = matrix.length - 1;
            while (left < right) {
                int temp           = matrix[row][right];
                matrix[row][right] = matrix[row][left];
                matrix[row][left]  = temp;
                left++;
                right--;
            }
        }
        return matrix;
    }
}
