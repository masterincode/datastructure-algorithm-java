package com.tutort.assignments.array.assignment2;

import com.tutort.LeetCodeURLConstant;

public class RangeSumQueryImmutable {

    public static void main(String[] args) {
        NumArray numArray = new NumArray(new int[]{-4,-5});
        System.out.print(numArray.optimisedApproach(0,0)+", ");
        System.out.print(numArray.optimisedApproach(1,1)+", ");
        System.out.print(numArray.optimisedApproach(0,1)+", ");
        System.out.print(numArray.optimisedApproach(1,1)+", ");
        System.out.print(numArray.optimisedApproach(0,0));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.RANGE_SUM_QUERY_IMMUTABLE);
    }
    static class NumArray {
        int[] nums;
        int[] prefixSum;
        int sum = 0;
        public NumArray(int[] nums) {
            this.nums = nums;
            this.prefixSum =  new int[nums.length];
            for (int i = 0; i < nums.length; i++) {
                sum = sum + nums[i];
                this.prefixSum[i] = sum;
            }
        }

        public int bruteForceApproach(int left, int right) {
            int sum = 0;
            for(int i = left; i <= right; i++) {
                sum = sum + this.nums[i];
            }
            return sum;
        }

        public int optimisedApproach(int left, int right) {
           return left-1 >= 0 ? this.prefixSum[right] - this.prefixSum[left-1] : this.prefixSum[right];
        }
    }
}
