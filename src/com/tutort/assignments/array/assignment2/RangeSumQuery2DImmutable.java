package com.tutort.assignments.array.assignment2;

import com.tutort.LeetCodeURLConstant;

public class RangeSumQuery2DImmutable {

    public static void main(String[] args) {
        NumMatrix numArray = new NumMatrix(new int[][]{{3, 0, 1, 4, 2}, {5, 6, 3, 2, 1}, {1, 2, 0, 1, 5}, {4, 1, 0, 1, 7}, {1, 0, 3, 0, 5}});
        System.out.print(numArray.sumRegion(2,1,4,3)+", ");
        System.out.print(numArray.sumRegion(1,1,2,2)+", ");
        System.out.print(numArray.sumRegion(1,2,2,4)+", ");
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.RANGE_SUM_QUERY_2D_IMMUTABLE);
    }

    static class NumMatrixBruteForceApproach {

        int[][] matrix;
        public NumMatrixBruteForceApproach(int[][] matrix) {
            this.matrix = matrix;
        }

        public int bruteForceApproach(int row1, int col1, int row2, int col2) {
            int sum = 0;
            for(int i = row1; i <= row2; i++) {
                for(int j = col1; j <= col2; j++) {
                    sum = sum + this.matrix[i][j];
                }
            }
            return sum;
        }
    }

    // For Understanding watch video: https://www.youtube.com/watch?v=rkLDDxOcJxU&ab_channel=AlgorithmsMadeEasy
    // Code taken from: https://leetcode.com/problems/range-sum-query-2d-immutable/discuss/1204168/JS-Python-Java-C%2B%2B-or-Easy-4-Rectangles-DP-Solution-w-Explanation
    static class NumMatrix {
        int[][] prefixSum;
        public NumMatrix(int[][] M) {
            int ylen = M.length + 1, xlen = M[0].length + 1;
            prefixSum = new int[ylen][xlen];
            for (int i = 1; i < ylen; i++)
                for (int j = 1; j < xlen; j++)
                    prefixSum[i][j] = M[i-1][j-1] + prefixSum[i-1][j] + prefixSum[i][j-1] - prefixSum[i-1][j-1];
        }

        public int sumRegion(int row1, int col1, int row2, int col2) {
            return (prefixSum[row2+1][col2+1] - prefixSum[row2+1][col1] - prefixSum[row1][col2+1] + prefixSum[row1][col1]);
        }
    }

}
