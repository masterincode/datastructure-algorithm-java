package com.tutort.assignments.array.assignment2;

import com.tutort.LeetCodeURLConstant;

import java.util.Arrays;

public class RankTeamsByVotes {
    public static void main(String[] args) {
        System.out.println(rankTeams(new String[]{"ABC","ACB","ABC","ACB","ACB"}));
        System.out.println(rankTeams(new String[]{"WXYZ","XYZW"}));
        System.out.println(rankTeams(new String[]{"ZMNAGUEDSJYLBOPHRQICWFXTVK"}));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.RANK_TEAMS_BY_VOTES);
    }

    // Time Complexity: O(26 + n * c + 26 * log 26 * c + c) = O(n).
    // Space Complexity: n
    public static String rankTeams(String[] votes) {
        int len = votes[0].length();
        int[][] map = new int[26][len + 1];
        for(int i = 0; i < 26; i++){
            map[i][len] = i;
        }
        for(int i = 0; i < votes.length; i++){
            String s = votes[i];
            for(int j = 0; j < len; j++){
                map[s.charAt(j) - 'A'][j]++;
            }
        }
        Arrays.sort(map, (a, b) ->{
            for(int i = 0; i < len; i++){
                if(a[i] < b[i]) {
                    return 1;
                }
                if(a[i] > b[i]) {
                    return -1;
                }
            }
            return 0;
        });
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < len; i++){
            sb.append((char)('A' + map[i][len]));
        }
        return sb.toString();
    }
}
