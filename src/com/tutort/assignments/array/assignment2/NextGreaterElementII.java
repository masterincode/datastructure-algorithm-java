package com.tutort.assignments.array.assignment2;

import com.tutort.LeetCodeURLConstant;

import java.util.Stack;

public class NextGreaterElementII {

    public static void main(String[] args) {
        for (int val : nextGreaterElements(new int[]{1, 2, 1}))
            System.out.print(val + ", ");
        System.out.println();
        for (int val : nextGreaterElements(new int[]{1, 2, 3, 4, 3}))
            System.out.print(val + ", ");
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.NEXT_GREATER_ELEMENT_II);
    }

    // Time Complexity: O(n)
    // Space Complexity: n
    //Reference: https://leetcode.com/problems/next-greater-element-ii/discuss/1449789/Java-Damn-Easy-to-Understand
    public static int[] nextGreaterElements(int[] nums) {
        Stack<Integer> stack = new Stack<>();
        for (int i = nums.length - 1; i >= 0; i--) {
            stack.push(nums[i]);
        }

        int greater[] = new int[nums.length];
        for (int i = nums.length - 1; i >= 0; i--) {
            while (!stack.isEmpty() && stack.peek() <= nums[i]) {
                stack.pop();
            }
            greater[i] = stack.empty() ? -1 : stack.peek();
            stack.push(nums[i]);
        }

        return greater;
    }
}
