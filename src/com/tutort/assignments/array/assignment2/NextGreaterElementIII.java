package com.tutort.assignments.array.assignment2;

import com.tutort.LeetCodeURLConstant;

import java.util.Arrays;

public class NextGreaterElementIII {

    public static void main(String[] args) {
        System.out.println(optimisedAppraoch(12));
        System.out.println(optimisedAppraoch(45132));
        System.out.println(optimisedAppraoch(1234));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.NEXT_GREATER_ELEMENT_III);
    }

    // Time Complexity: O(n)
    // Space Complexity: 1
    public static int optimisedAppraoch(int n) {
        String str = Integer.toString(n);
        char [] arr = str.toCharArray();
        // To find the dip             //45132 - At pos: of 1 dip occurs
        int i = arr.length - 2;
        while(i >= 0 && arr[i] >= arr[i + 1]){
            i--;
        }
        if(i == -1)
            return -1;
        //To Find the index of greater element than at idx i
        int j = arr.length - 1;
        while(arr[i] >= arr[j]){
            j--;
        }
        //Swaping
        char temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;          //45231
        Arrays.sort(arr, i+1, arr.length);           //45213
        //Result Concat in String
        String res = new String(arr);
        // To long
        long sol = Long.parseLong(res);
        if(sol <= Integer.MAX_VALUE)
            return (int) sol;
        return -1;
    }

    // Time Complexity: O(n^2)
    // Space Complexity: 1
    public static int bruteForceApproach(int n) {
        String value = String.valueOf(n);
        char[] nums = value.toCharArray();
        int j = nums.length - 1;
        char temp;
        int index = 0;
        boolean found = false;
        for (int i = nums.length - 2; i >= 0 ; i--) {
            if(nums[i] < nums[i+1]) {
                while (j >= 0) {
                    if (nums[j] > nums[i]) {
                        temp = nums[j];
                        nums[j] = nums[i];
                        nums[i] = temp;
                        index = i + 1;
                        i = -1;
                        break;
                    }
                }
                found = true;
            }
        }
        if(!found) {
            return -1;
        }
        int i = index;
        j = nums.length - 1;
        while (i < j) {
            temp = nums[i];
            nums[i] = nums[j];
            nums[j] = temp;
            i++;
            j--;
        }
        String s = new String(nums);
        return Integer.valueOf(s);
    }
}
