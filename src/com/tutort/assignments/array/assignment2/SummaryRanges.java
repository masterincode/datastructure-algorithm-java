package com.tutort.assignments.array.assignment2;

import com.tutort.LeetCodeURLConstant;

import java.util.ArrayList;
import java.util.List;

public class SummaryRanges {

    public static void main(String[] args) {
        System.out.println(summaryRanges(new int[]{0,1,2,4,5,7}));
        System.out.println(summaryRanges(new int[]{0,2,3,4,6,8,9}));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.SUMMARY_RANGES);
    }

    public static List<String> summaryRanges(int[] nums) {
        ArrayList<String> list=new ArrayList<>();
        for(int end = 0; end < nums.length; end++){
            int start = nums[end];
            while(end+1 < nums.length && nums[end]+1 == nums[end+1])
                end++;

            if(start != nums[end]){
                list.add(""+start+"->"+nums[end]);
            }
            else{
                list.add(""+start);
            }
        }
        return list;
    }
}
