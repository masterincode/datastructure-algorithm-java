package com.tutort.assignments.array.assignment2;

import com.tutort.LeetCodeURLConstant;

import java.util.HashSet;
import java.util.Set;

public class ContainsDuplicateI {

    public static void main(String[] args) {
        System.out.println(optimisedApproach(new int[]{1,2,3,1}));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.CONTAINS_DUPLICATE_I);
    }

    //BruteForceApproach
    //Time Complexity: O(n^2)
    // Time Limit Exceeded
    public static boolean bruteForceApproach(int[] nums) {
        boolean flag = false;
        if(nums.length == 1)
            return false;

        for(int i=0 ; i < nums.length ; i++) {
            for(int j = i+1; j<nums.length ; j++) {
                if(nums[i] == nums[j])
                    flag = true;
            }
        }
        return flag;
    }

    //Time Complexity: O(n)
    //Space Complexity: O(n)
    public static boolean optimisedApproach(int[] nums) {
        Set<Integer> set = new HashSet<>();
        for(int val: nums) {
            set.add(val);
        }
        if(nums.length == set.size())
            return false;
        else
            return true;
    }
}
