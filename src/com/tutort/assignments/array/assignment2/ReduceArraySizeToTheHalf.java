package com.tutort.assignments.array.assignment2;

import com.tutort.LeetCodeURLConstant;

import java.util.*;

//Reduce Array Size to The Half
public class ReduceArraySizeToTheHalf {

    public static void main(String[] args) {
        System.out.println(optmisedAppraoch(new int[]{3,3,3,3,5,5,5,2,2,7}));
        System.out.println(optmisedAppraoch(new int[]{7,7,7,7,7,7}));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.REDUCE_ARRAY_SIZE_TO_THE_HALF);
    }

    // Time Complexity: O (n log n)
    // Space Complexity: 2n
    public static int optmisedAppraoch(int[] arr) {
        if(arr == null || arr.length == 0) {
            return 0;
        }
        Map<Integer, Integer> map = new HashMap<>();
        List<Integer> list = new ArrayList<>();
        Integer frequency;
        for (int val: arr) {
            frequency =  map.get(val);
           if(frequency == null){
               map.put(val,1);
           } else {
               map.put(val,++frequency);
           }
        }
        map.forEach((key, value) -> list.add(value));
        Collections.sort(list); // O(n log n)
        Collections.reverse(list); //O (n)
        int count = 0;
        int sum = 0;
        for (int i = 0; i < list.size(); i++) {
            sum = sum + list.get(i);
            count++;
            if (sum >= arr.length/2) {
                return count;
            }
        }
        return -1;
    }
}
