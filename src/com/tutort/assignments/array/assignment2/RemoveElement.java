package com.tutort.assignments.array.assignment2;

import com.tutort.LeetCodeURLConstant;

public class RemoveElement {

    public static void main(String[] args) {
        System.out.println(removeElementAppraoch(new int[]{3,2,2,3},3));
        System.out.println(removeElementAppraoch(new int[]{0,1,2,2,3,0,4,2},2));
        System.out.println(removeElementAppraoch(new int[]{3,3},3));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.REMOVE_ELEMENT);
    }

    public static int removeElementAppraoch(int[] nums, int val) {
        int start = 0;
        int length = 0;

        for(int i = 0; i < nums.length; i++) {
            if(nums[i] != val) {
                nums[start] = nums[i];
                start++;
                length++;
            }
        }
        return length;
    }
}
