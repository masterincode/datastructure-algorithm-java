package com.tutort.assignments.array.assignment2;

import com.tutort.LeetCodeURLConstant;

import java.util.HashMap;
import java.util.Map;

public class ContainsDuplicateII {
    public static void main(String[] args) {
        System.out.println(optimizedApproach(new int[]{1,2,3,1}, 3));
        System.out.println(optimizedApproach(new int[]{1,0,1,1}, 1));
        System.out.println(optimizedApproach(new int[]{1,2,3,1,2,3}, 2));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.CONTAINS_DUPLICATE_II);
    }

    //Time Complexity: O(n^2)
    //Space Complexity: 1
    // Time Limit Exceeded
    public static boolean bruteForceApproach(int[] nums, int k) {
        boolean hasDuplicate = false;
        for (int i = 0; i < nums.length; i++) {
            for (int j = i+1; j < nums.length; j++) {
                if(nums[i] == nums[j]) {
                    if(Math.abs(i - j) <= k) {
                        hasDuplicate = true;
                    }
                }
            }
        }
        return hasDuplicate;
    }

    //Time Complexity: O(n)
    //Space Complexity: n
    public static boolean optimizedApproach(int[] nums, int k) {
        boolean hasDuplicate = false;
        Map<Integer,Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if(map.get(nums[i]) == null) {
                map.put(nums[i], i);
            } else {
                int index = map.get(nums[i]);
                if(Math.abs(index - i) <= k) {
                    hasDuplicate = true;
                } else {
                    map.put(nums[i], i);
                }
            }
        }
        return hasDuplicate;
    }
}
