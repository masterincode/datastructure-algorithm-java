package com.tutort.assignments.array.assignment2;

import com.tutort.LeetCodeURLConstant;

import java.util.HashMap;
import java.util.Stack;

public class NextGreaterElementI {

    public static void main(String[] args) {
        for (int val : nextGreaterElement(new int[]{4, 1, 2}, new int[]{1, 3, 4, 2}))
            System.out.print(val + ", ");
        System.out.println();
        for (int val : nextGreaterElement(new int[]{1, 3, 5, 2, 4}, new int[]{6, 5, 4, 3, 2, 1, 7}))
            System.out.print(val + ", ");
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.NEXT_GREATER_ELEMENT_I);
    }

    // Time Complexity: O(n^2)
    // Space Complexity: 1
    public static int[] bruteForceApproach(int[] nums1, int[] nums2) {
        int[] ans = new int[nums1.length];
        int index = 0;
        int element = 0;
        boolean isFound = false;
        for (int i = 0; i < nums1.length; i++) {
            isFound = false;
            for (int j = 0; j < nums2.length; j++) {
                if (nums1[i] == nums2[j]) {
                    isFound = true;
                    element = nums2[j];
                }
                if (isFound) {
                    if (j == nums2.length - 1) {
                        if (nums2[j] > element) {
                            ans[index++] = nums2[j];
                        } else {
                            ans[index++] = -1;
                        }
                        isFound = false;
                    } else {
                        if (nums2[j + 1] > element) {
                            ans[index++] = nums2[j + 1];
                            isFound = false;
                        }
                    }
                }
            }
        }
        return ans;
    }

    // Time Complexity: O(n)
    // Space Complexity: 2n
    //Reference: https://leetcode.com/problems/next-greater-element-i/discuss/1579935/Java-easy-solution-oror-Brute-and-optimal-oror-Stack
    public static int[] nextGreaterElement(int[] nums1, int[] nums2) {
        int[] ans = new int[nums1.length];

        Stack<Integer> stack = new Stack<>();
        HashMap<Integer, Integer> map = new HashMap<>();

        // find out all the next greater elements in nums2 array
        for (int num : nums2) {
            // if num is greater than top elements in stack then it is the next greater element in nums2
            while (!stack.isEmpty() && num > stack.peek()) {
                map.put(stack.pop(), num);
            }
            // then add num to stack
            stack.add(num);
        }

        int i = 0;
        for (int num : nums1) {
            ans[i++] = map.getOrDefault(num, -1);
        }

        return ans;
    }
}
