package com.tutort.assignments.array.assignment2;

import com.tutort.LeetCodeURLConstant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IntersectionOfTwoArraysII {

    public static void main(String[] args) {
        for (int val: intersection(new int[]{4,9,5},new int[]{9,4,9,8,4}))
            System.out.print(val+", ");
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.INTERSECTION_OF_TWO_ARRAYS_II);
    }

    // Time Complexity: O(n)
    // Space Complexity: 2n
    public static int[] intersection(int[] nums1, int[] nums2) {
        List<Integer> list = new ArrayList<>();
        Integer count;
        Map<Integer, Integer> map =  new HashMap<>();
        for (int i = 0; i < nums1.length; i++) {
            count = map.get(nums1[i]);
            if (count == null) {
                map.put(nums1[i], 1);
            } else {
                map.put(nums1[i], ++count);
            }
        }
        for (int i = 0; i < nums2.length; i++) {
            count = map.get(nums2[i]);
            // !list.contains(nums2[i]) is removed coz we need duplicate record as well.
//            if(count != null && count > 0 && !list.contains(nums2[i])) {
            if(count != null && count > 0) {
                    list.add(nums2[i]);
                    map.put(nums2[i], --count);
            }
        }
        int[] result =  new int[list.size()]; //This is result array
        for (int i = 0; i < list.size(); i++) {
            result[i] = list.get(i);
        }
        return result;
    }
}
