package com.tutort.assignments.tree.assignment1;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.Arrays;

public class LongestUnivaluePath {

    public static void main(String[] args) {
        Tree tree = new Tree();
        Integer arr1[] = {4, 2, 6, 1, 3, 5, 7};
        Arrays.stream(arr1).forEach(tree::insert);
        System.out.println(longestUnivaluePath(tree.root));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.LONGEST_UNIVALUE_PATH);
    }

    static int res = 0;
    public static int longestUnivaluePath(TreeNode root) {
        postOrderTraversal(root);
        return res;
    }

    // Time Complexity: O(n)
    // Space Complexity: 1
    private static int postOrderTraversal(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int leftPath = postOrderTraversal(root.left);
        int rightPath = postOrderTraversal(root.right);
        int leftCheck = 0;
        int rightCheck = 0;
        if (root.left != null && root.val == root.left.val) {
            leftCheck = leftPath + 1;
        }
        if (root.right != null && root.val == root.right.val) {
            rightCheck = rightPath + 1;
        }
        res = Math.max(res, leftCheck + rightCheck);
        return Math.max(leftCheck, rightCheck);
    }
}
