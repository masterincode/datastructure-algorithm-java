package com.tutort.assignments.tree.assignment1;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.Arrays;

public class DistributeCoinsInBinaryTree {

    public static void main(String[] args) {
        Tree tree = new Tree();
        Integer arr1[] = {4,2,6,1,3,5,7};
        Arrays.stream(arr1).forEach(tree::insert);
        System.out.println(distributeCoins(tree.root));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.DISTRIBUTE_COINS_IN_BINARY_TREE);
    }
    static int ans;
    public static int distributeCoins(TreeNode root) {
        ans = 0;
        dfs(root);
        return ans;
    }

    public static int dfs(TreeNode node) {
        if (node == null) return 0;
        int L = dfs(node.left);
        int R = dfs(node.right);
        ans = ans + Math.abs(L) + Math.abs(R);
        return node.val + L + R - 1;
    }
}
