package com.tutort.assignments.tree.assignment1;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.*;

class Pair {
    TreeNode node;
    int index;

    Pair(TreeNode node, int index) {
        this.index = index;
        this.node = node;
    }
}

public class MaximumWidthOfBinaryTree {

    public static void main(String[] args) {
        Tree tree = new Tree();
//        Integer arr1[] = {50, 40, 60, 65, 35, 45};
        Integer arr2[] = {50, 40, 60, 30, 20, 70, 65};
        Arrays.stream(arr2).forEach(value -> tree.insert(value));
        System.out.println(widthOfBinaryTreeAppraoch2(tree.root));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.MAXIMUM_WIDTH_OF_BINARY_TREE);
    }

    public static int widthOfBinaryTreeAppraoch2(TreeNode root) {
        if (root == null) return 0;
        int maxWidth = 0;
        Queue<Pair> queue = new ArrayDeque<>();
        queue.add(new Pair(root, 0));
        while (!queue.isEmpty()) {

            int size = queue.size();
            int minIndex = queue.peek().index;
            int firstIndex = 0, lastIndex = 0;

            for (int i = 0; i < size; i++) {
                int currentIndex = queue.peek().index - minIndex;
                TreeNode currentNode = queue.peek().node;
                queue.poll();
                if (i == 0) {
                    firstIndex = currentIndex;
                }
                if (i == size - 1) {
                    lastIndex = currentIndex;
                }
                if (currentNode.left != null)
                    queue.offer(new Pair(currentNode.left, currentIndex * 2 + 1));
                if (currentNode.right != null)
                    queue.offer(new Pair(currentNode.right, currentIndex * 2 + 2));
            }
            maxWidth = Math.max(maxWidth, lastIndex - firstIndex + 1);
        }
        return maxWidth;
    }

    //Time Limit Exceeded
    public static int widthOfBinaryTreeAppraoch1(TreeNode root) {
        if (root == null) {
            return 0;
        }
        Deque<TreeNode> deque = new LinkedList<>();
        deque.add(root);
        int maxWidth = 1;
        while (!deque.isEmpty()) {
            while (!deque.isEmpty() && deque.getFirst() == null) {
                deque.removeFirst();
            }
            while (!deque.isEmpty() && deque.getLast() == null) {
                deque.removeLast();
            }
            maxWidth = Math.max(maxWidth, deque.size());
            int length = deque.size();
            for (int i = 0; i < length; i++) {
                TreeNode current = deque.poll();
                if (current == null) {
                    deque.add(null);
                    deque.add(null);
                } else {
                    deque.add(current.left);
                    deque.add(current.right);
                }
            }
        }
        return maxWidth;
    }

}
