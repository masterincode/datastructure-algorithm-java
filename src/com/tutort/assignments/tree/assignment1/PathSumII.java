package com.tutort.assignments.tree.assignment1;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PathSumII {

    public static void main(String[] args) {
        Tree tree = new Tree();
        Integer arr1[] = {4,2,6,1,3,5,7};
        Arrays.stream(arr1).forEach(tree::insert);
        System.out.println(pathSum(tree.root, 9));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.PATH_SUM_II);
    }

    public static List<List<Integer>> pathSum(TreeNode root, int targetSum) {
        List<List<Integer>> answerList = new ArrayList<>();
        pathSumRecursion(root, targetSum, new ArrayList<>(), answerList);
        return answerList;
    }

    private static void pathSumRecursion(TreeNode root, int targetSum, List<Integer> rowList, List<List<Integer>> answerList) {
        if (root == null) {
            return;
        }
        rowList.add(root.val);
        if (root.left == null && root.right == null && targetSum == root.val) {
            List<Integer> temp = new ArrayList<>(rowList);
            answerList.add(temp);
        } else {
            pathSumRecursion(root.left, targetSum - root.val, rowList, answerList);
            pathSumRecursion(root.right, targetSum - root.val, rowList, answerList);
        }
        rowList.remove(rowList.size() - 1);
    }
}
