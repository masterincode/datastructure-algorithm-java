package com.tutort.assignments.tree.assignment1;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.*;

public class AverageOfLevelsInBinaryTree {

    public static void main(String[] args) {
        Tree tree = new Tree();
        Integer arr1[] = {50, 60, 65, 55, 40, 35, 45, 30, 37};
        Arrays.stream(arr1).forEach(value -> tree.insert(value));
        System.out.println("Level: Traversal: " + averageOfLevels(tree.root));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.AVERAGE_OF_LEVELS_IN_BINARY_TREE);
    }

    public static List<Double> averageOfLevels(TreeNode root) {
        if(root == null) {
            return new ArrayList<>();
        }
        Queue<TreeNode> queue = new ArrayDeque<>();
        Map<Integer, List<Double>> map = new TreeMap<>();
        TreeNode dummy = new TreeNode(0);
        queue.add(root);
        queue.add(dummy);
        int level = 0;
        while (!queue.isEmpty()) {
            TreeNode current = queue.poll();
            if(current == dummy) {
                level++;
                if(queue.isEmpty()) {
                    break;
                }
                queue.add(dummy);
            } else {
                if(map.containsKey(level)) {
                    List<Double> existingList = map.get(level);
                    existingList.add((double) current.val);
                    map.put(level, existingList);
                } else {
                    List<Double> newList = new ArrayList<>();
                    newList.add((double) current.val);
                    map.put(level, newList);
                }
            }
            if(current.left != null) {
                queue.add(current.left);
            }
            if(current.right != null) {
                queue.add(current.right);
            }

        }
        List<Double> answerList = new ArrayList<>();
        for(List<Double> list: map.values()) {
           double sum = 0;
           for (Double val: list) {
               sum = sum + val;
           }
           answerList.add(sum/list.size());
        }
        return answerList;
    }


}
