package com.tutort.assignments.tree.assignment1;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class PathSumIII {

    public static void main(String[] args) {
        Tree tree = new Tree();
        Integer arr1[] = {4, 2, 6, 1, 3, 5, 7};
        Arrays.stream(arr1).forEach(tree::insert);
        System.out.println(pathSum(tree.root, 9));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.PATH_SUM_III);
    }

    static int total = 0;

    public static int pathSum(TreeNode root, int sum) {
        if (root == null) {
            return 0;
        }
        Map<Integer, Integer> map = new HashMap<>();
        map.put(0, 1);
        total = 0;
        findPathSum(root, 0, sum, map);
        return total;
    }

    private static void findPathSum(TreeNode current, int sum, int target, Map<Integer, Integer> map) {
        if (current == null) {
            return;
        }
        sum = sum + current.val;
        if (map.containsKey(sum - target)) {
            total = total + map.get(sum - target);
        }
        map.put(sum, map.getOrDefault(sum, 0) + 1);
        findPathSum(current.left, sum, target, map);
        findPathSum(current.right, sum, target, map);
        map.put(sum, map.get(sum) - 1);
        return;
    }
}
