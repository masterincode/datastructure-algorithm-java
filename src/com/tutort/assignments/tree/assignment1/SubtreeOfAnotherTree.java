package com.tutort.assignments.tree.assignment1;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.tree.common.TreeNode;

public class SubtreeOfAnotherTree {

    public static void main(String[] args) {
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.SUBTREE_OF_ANOTHER_TREE);
    }

    public boolean isSubtree(TreeNode root, TreeNode subRoot) {
        if (root == null) {
            return false;
        }
        //Finding subRoot tree in subtree of root(starting from root), if found then return true.
        if (isSubtreeFound(root, subRoot)) {
            return true;
        }
        //Else, finding subRoot tree in left and right subtrees of root.
        return isSubtree(root.left, subRoot) || isSubtree(root.right, subRoot);
    }

    public boolean isSubtreeFound(TreeNode root, TreeNode subRoot) {
        //If root is null and subRoot is null, then it will return true, else if only one of them is null, then it will return false.
        if (root == null || subRoot == null) {
            return root == subRoot;
        }
        //If the value of root and subRoot are not equal, then simply return false.
        if (root.val != subRoot.val) {
            return false;
        }
        //If the value of root and subRoot are equal, then check if their left and right nodes are equal or not.
        return isSubtreeFound(root.left, subRoot.left) && isSubtreeFound(root.right, subRoot.right);
    }
}
