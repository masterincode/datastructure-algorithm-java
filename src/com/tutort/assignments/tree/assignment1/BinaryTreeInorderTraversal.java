package com.tutort.assignments.tree.assignment1;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BinaryTreeInorderTraversal {
    public static void main(String[] args) {
        Tree tree = new Tree();
        Integer arr1[] = {50, 60, 55};
        Arrays.stream(arr1).forEach(value -> {
            tree.insert(value);
        });
        System.out.println(inorderTraversal(tree.root));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.BINARY_TREE_INORDER_TRAVERSAL);
    }

    public static List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> ans = new ArrayList<>();
        addInOrderValues(root, ans);
        return ans;
    }

    private static void addInOrderValues(TreeNode root, List<Integer> ans) {
        if(root == null) {
            return;
        }
        addInOrderValues(root.left, ans);
        ans.add(root.val);
        addInOrderValues(root.right, ans);
    }
}
