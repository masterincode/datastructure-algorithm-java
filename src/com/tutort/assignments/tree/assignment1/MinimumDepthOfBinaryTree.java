package com.tutort.assignments.tree.assignment1;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.Arrays;

public class MinimumDepthOfBinaryTree {

    public static void main(String[] args) {
        Tree tree = new Tree();
        Integer arr1[] = {50, 60, 65, 55, 40, 35, 45, 30, 37, 42, 48};
        Arrays.stream(arr1).forEach(value -> tree.insert(value));
        System.out.println(minDepth(tree.root));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.MINIMUM_DEPTH_OF_BINARY_TREE);
    }

    public static int minDepth(TreeNode root) {
        //base case
        if (root == null)
            return 0;
        //if there is only right child get depth of it
        if (root.left == null)
            return minDepth(root.right) + 1;
            //similarly if there is only left child  get depth of it
        else if (root.right == null)
            return minDepth(root.left) + 1;
        //in case there are both  get the min of both
        return Math.min(minDepth(root.right), minDepth(root.left)) + 1;
    }
}
