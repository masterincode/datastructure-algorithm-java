package com.tutort.assignments.tree.assignment1;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.Arrays;

public class SumRootToLeafNumbers {

    public static void main(String[] args) {
        Tree tree = new Tree();
        Integer arr1[] = {4, 2, 6, 1, 3, 5, 7};
        Arrays.stream(arr1).forEach(tree::insert);
        System.out.println(sumNumbers(tree.root));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.SUM_ROOT_TO_LEAF_NUMBERS);
    }

    static int total;
    public static int sumNumbers(TreeNode root) {
        total = 0;
        sumNumbersRecursion(root, 0);
        return total;
    }

    private static void sumNumbersRecursion(TreeNode root, int sum) {
        if (root == null)
            return;
        sum = sum * 10 + root.val;
        if (root.left == null && root.right == null) {
            total = total + sum;
            return;
        }
        sumNumbersRecursion(root.left, sum);
        sumNumbersRecursion(root.right, sum);
    }
}
