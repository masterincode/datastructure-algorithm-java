package com.tutort.assignments.tree.assignment1;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.tree.common.Node;

public class PopulatingNextRightPointersInEachNodeII {

    public static void main(String[] args) {
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.POPULATING_NEXT_RIGHT_POINTERS_IN_EACH_NODE_II);
    }
    public Node connect(Node root) {
        if (root == null) {
            return null;
        }
        if (root.left != null) {
            if (root.right != null) {
                root.left.next = root.right;
            } else {
                Node nxt = findNext(root.next);
                root.left.next = nxt;
            }
        }
        if (root.right != null) {
            Node nxt = findNext(root.next);
            root.right.next = nxt;
        }
        // this is a key: build the connection from right to left.
        // you can change it from left to right, see what will happend. lol
        connect(root.right);
        connect(root.left);
        return root;
    }

    //this helper function likes to operate a linkedlist
    private Node findNext(Node root) {
        while (root != null) {
            if (root.left != null) {
                return root.left;
            }
            if (root.right != null) {
                return root.right;
            }
            root = root.next;
        }
        return root;
    }
}
