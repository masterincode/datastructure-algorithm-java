package com.tutort.assignments.tree.assignment1;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.*;

public class BinaryTreeLevelOrderTraversal {

    public static void main(String[] args) {
        Tree tree = new Tree();
        Integer arr1[] = {50, 60, 65, 55, 40, 35, 45, 30, 37};
        Arrays.stream(arr1).forEach(value -> tree.insert(value));
        System.out.println("Level: Traversal: " + levelOrder(tree.root));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.BINARY_TREE_LEVEL_ORDER_TRAVERSAL);
    }

    public static List<List<Integer>> levelOrder(TreeNode root) {
        if(root == null) {
            return new ArrayList<>();
        }
        Queue<TreeNode> queue = new ArrayDeque<>();
        Map<Integer, List<Integer>> map = new TreeMap<>();
        TreeNode dummy = new TreeNode(0);
        queue.add(root);
        queue.add(dummy);
        int level = 0;
        while (!queue.isEmpty()) {
            TreeNode current = queue.poll();
            if(current == dummy) {
                level++;
                if(queue.isEmpty()) {
                    break;
                }
                queue.add(dummy);
            } else {
                if(map.containsKey(level)) {
                    List<Integer> existingList = map.get(level);
                    existingList.add(current.val);
                    map.put(level, existingList);
                } else {
                    List<Integer> newList = new ArrayList<>();
                    newList.add(current.val);
                    map.put(level, newList);
                }
            }
            if(current.left != null) {
                queue.add(current.left);
            }
            if(current.right != null) {
                queue.add(current.right);
            }

        }
        List<List<Integer>> answerList = new ArrayList<>();
        map.forEach((key,value) -> answerList.add(value));
        return answerList;
    }
}
