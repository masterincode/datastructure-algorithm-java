package com.tutort.assignments.tree.assignment1;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.Arrays;

public class DiameterOfBinaryTree {

    public static void main(String[] args) {
        Tree tree = new Tree();
        Integer arr1[] = {50, 60, 65, 55, 40, 35, 45, 30, 37, 42, 48};
        Arrays.stream(arr1).forEach(value -> tree.insert(value));
        System.out.println(diameterOfBinaryTree(tree.root));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.DIAMETER_OF_BINARY_TREE);
    }

    public static int diameterOfBinaryTree(TreeNode root) {
        int[] diameter = new int[1];
        calculateHeight(root, diameter);
        return diameter[0];
    }

    private static int calculateHeight(TreeNode root, int[] diameter) {
        if (root == null) {
            return 0;
        }
        int lh = calculateHeight(root.left, diameter);
        int rh = calculateHeight(root.right, diameter);
        diameter[0] = Math.max(diameter[0], lh + rh);
        return Math.max(lh, rh) + 1;
    }

}
