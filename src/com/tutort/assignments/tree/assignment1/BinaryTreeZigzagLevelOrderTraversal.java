package com.tutort.assignments.tree.assignment1;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.*;

public class BinaryTreeZigzagLevelOrderTraversal {

    public static void main(String[] args) {
        Tree tree = new Tree();
        Integer[] arr1 = {50, 60, 65, 55, 40, 35, 45, 30, 37};
        Arrays.stream(arr1).forEach(tree::insert);
        System.out.println("Level: Traversal: " + levelOrder_Approach2(tree.root));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.BINARY_TREE_ZIGZAG_LEVEL_ORDER_TRAVERSAL);
    }

    public static List<List<Integer>> levelOrder_Approach2(TreeNode root) {
        TreeNode current = root;
        List<List<Integer>> answerList = new ArrayList<>();
        if (current == null)
            return answerList;
        Stack<TreeNode> stack1 = new Stack<>();
        stack1.push(root);
        Stack<TreeNode> stack2 = new Stack<>();

        while (!stack1.isEmpty() || !stack2.isEmpty()) {
            List<Integer> rowList = new ArrayList<>();
            //1. Stack 1
            while (!stack1.isEmpty()) {
             // Pop out from Stack 1
                current = stack1.pop();
                rowList.add(current.val);

                if (current.left != null)
                  //Push into Stack 2
                    stack2.push(current.left);

                if (current.right != null)
                 // Push into Stack 2
                    stack2.push(current.right);
            }
            answerList.add(rowList);
            rowList = new ArrayList<>();
            // Stack 2
            while (!stack2.isEmpty()) {
              // Pop out from Stack 2
                current = stack2.pop();
                rowList.add(current.val);

                if (current.right != null)
                 // Push into Stack 2
                    stack1.push(current.right);

                if (current.left != null)
                 // Push into Stack 2
                    stack1.push(current.left);
            }
            if (!rowList.isEmpty())
                answerList.add(rowList);
        }
        return answerList;
    }

    public static List<List<Integer>> levelOrder_Approach1(TreeNode root) {
        if (root == null) {
            return new ArrayList<>();
        }
        Queue<TreeNode> queue = new ArrayDeque<>();
        Map<Integer, List<Integer>> map = new TreeMap<>();
        TreeNode dummy = new TreeNode(0);
        queue.add(root);
        queue.add(dummy);
        int level = 0;
        while (!queue.isEmpty()) {
            TreeNode current = queue.poll();
            if (current == dummy) {
                level++;
                if (queue.isEmpty()) {
                    break;
                }
                queue.add(dummy);
            } else {
                if (map.containsKey(level)) {
                    List<Integer> existingList = map.get(level);
                    existingList.add(current.val);
                    map.put(level, existingList);
                } else {
                    List<Integer> newList = new ArrayList<>();
                    newList.add(current.val);
                    map.put(level, newList);
                }
            }
            if (current.left != null) {
                queue.add(current.left);
            }
            if (current.right != null) {
                queue.add(current.right);
            }

        }
        List<List<Integer>> answerList = new ArrayList<>();
        map.forEach((key, value) -> {
            if (key % 2 != 0) {
                Collections.reverse(value);
            }
            answerList.add(value);
        });
        return answerList;
    }

}
