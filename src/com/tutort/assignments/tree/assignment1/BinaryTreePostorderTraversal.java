package com.tutort.assignments.tree.assignment1;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BinaryTreePostorderTraversal {
    public static void main(String[] args) {
        Tree tree = new Tree();
        Integer arr1[] = {50, 60, 55};
        Arrays.stream(arr1).forEach(value -> {
            tree.insert(value);
        });
        System.out.println(postorderTraversal(tree.root));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.BINARY_TREE_POSTORDER_TRAVERSAL);
    }

    public static List<Integer> postorderTraversal(TreeNode root) {
        List<Integer> ans = new ArrayList<>();
        addPostOrderValues(root, ans);
        return ans;
    }

    private static void addPostOrderValues(TreeNode root, List<Integer> ans) {
        if(root == null) {
            return;
        }
        addPostOrderValues(root.left, ans);
        addPostOrderValues(root.right, ans);
        ans.add(root.val);
    }

}
