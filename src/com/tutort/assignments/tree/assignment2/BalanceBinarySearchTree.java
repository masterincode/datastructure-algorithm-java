package com.tutort.assignments.tree.assignment2;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BalanceBinarySearchTree {

    public static void main(String[] args) {
        Tree tree = new Tree();
        Integer arr1[] = {4, 2, 7, 1 , 3};
        Arrays.stream(arr1).forEach(tree::insert);
        System.out.println(balanceBST(tree.root));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.BALANCE_A_BINARY_SEARCH_TREE);
    }

    public static TreeNode balanceBST(TreeNode root) {
        List<Integer> nums = new ArrayList<>();
        inOrderTraversal(root, nums);
        return sortedArrayToBST(nums);
    }

    public static TreeNode sortedArrayToBST(List<Integer> nums) {
        if (nums.size() == 0) {
            return null;
        }
        return createBinarySearchTree(nums, 0, nums.size() - 1);
    }

    private static TreeNode createBinarySearchTree(List<Integer> nums, int start, int end) {
        if (start > end) {
            return null;
        }
        //Pre Order
        int mid = (start + end) / 2;
        TreeNode newNode = new TreeNode(nums.get(mid));
        newNode.left = createBinarySearchTree(nums, start, mid - 1);
        newNode.right = createBinarySearchTree(nums, mid + 1, end);
        return newNode;
    }

    private static void inOrderTraversal(TreeNode root, List<Integer> nums) {
        if(root == null) {
            return;
        }
        inOrderTraversal(root.left, nums);
        nums.add(root.val); // Inorder
        inOrderTraversal(root.right, nums);
        return;
    }
}
