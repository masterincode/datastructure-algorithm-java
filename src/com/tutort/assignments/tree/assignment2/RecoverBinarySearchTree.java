package com.tutort.assignments.tree.assignment2;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.Arrays;

public class RecoverBinarySearchTree {

    public static void main(String[] args) {
        Tree tree = new Tree();
        Integer arr1[] = {1, 3, 2};
        Arrays.stream(arr1).forEach(tree::insert);
        recoverTree(tree.root);
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.RECOVER_BINARY_SEARCH_TREE);
    }

    private static TreeNode first;
    private static TreeNode prev;
    private static TreeNode middle;
    private static TreeNode last;

    public static void recoverTree(TreeNode root) {
        first = middle = last = null;
        prev = new TreeNode(Integer.MIN_VALUE); // -2789456123
        inorder(root);
        if (first != null && last != null) {
            int t = first.val;
            first.val = last.val;
            last.val = t;
        } else if (first != null && middle != null) {
            int t = first.val;
            first.val = middle.val;
            middle.val = t;
        }
    }

    private static void inorder(TreeNode root) {
        if (root == null)
            return;
        inorder(root.left);
        if (prev != null && (root.val < prev.val)) {
            // If this is first violation, mark these two nodes as
            // 'first' and 'middle'
            if (first == null) {
                first = prev;
                middle = root;
            }
            // If this is second violation, mark this node as last
            else last = root;
        }
        // Mark this node as previous
        prev = root;
        inorder(root.right);
    }
}
