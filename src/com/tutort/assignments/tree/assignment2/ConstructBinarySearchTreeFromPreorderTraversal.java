package com.tutort.assignments.tree.assignment2;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

public class ConstructBinarySearchTreeFromPreorderTraversal {

    public static void main(String[] args) {
        Tree tree = new Tree();
        int arr1[] = {4, 2, 7, 1 , 3};
        System.out.println(bstFromPreorder(arr1));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.CONSTRUCT_BINARY_SEARCH_TREE_FROM_PREORDER_TRAVERSAL);
    }

    public static TreeNode bstFromPreorder(int[] preorder) {
        return bstFromPreorder(preorder, Integer.MAX_VALUE, new int[]{0});
    }

    private static TreeNode bstFromPreorder(int[] nums, int upperBound, int[] i) {
        if(i[0] == nums.length || nums[i[0]] > upperBound) {
            return null;
        }
        TreeNode root = new TreeNode(nums[i[0]++]);
        root.left = bstFromPreorder(nums, root.val, i);
        root.right = bstFromPreorder(nums, upperBound, i);
        return root;
    }
}
