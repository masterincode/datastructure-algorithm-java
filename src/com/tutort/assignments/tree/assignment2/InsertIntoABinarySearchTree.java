package com.tutort.assignments.tree.assignment2;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.Arrays;

public class InsertIntoABinarySearchTree {

    public static void main(String[] args) {
        Tree tree = new Tree();
        Integer arr1[] = {4, 2, 7, 1 , 3};
        Arrays.stream(arr1).forEach(tree::insert);
        System.out.println(insertIntoBST(tree.root, 5));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.INSERT_INTO_A_BINARY_SEARCH_TREE);
    }

    public static TreeNode insertIntoBST(TreeNode root, int val) {
        TreeNode newNode =  new TreeNode(val);
        if(root == null) {
            root = newNode;
            return root;
        }
        TreeNode current = root;
        while (true) {
            if(val == current.val) {
                return root;
            }
            if(val < current.val) {
                if(current.left == null) {
                    current.left = newNode;
                    return root;
                }
                current = current.left;
            } else {
                if(current.right == null) {
                    current.right = newNode;
                    return root;
                }
                current = current.right;
            }
        }
    }
}
