package com.tutort.assignments.tree.assignment2;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.Arrays;

public class BinarySearchTreeToGreaterSumTree {

    public static void main(String[] args) {
        Tree tree = new Tree();
        Integer arr1[] = {4, 2, 7, 1 , 3};
        Arrays.stream(arr1).forEach(tree::insert);
        System.out.println(bstToGst(tree.root));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.BINARY_SEARCH_TREE_TO_GREATER_SUM_TREE);
    }

    static int sum = 0;
    public static TreeNode bstToGst(TreeNode root) {
        if(root == null) {
            return null;
        }
        bstToGst(root.right);
        sum = sum + root.val;
        root.val = sum;
        bstToGst(root.left);
        return root;
    }
}
