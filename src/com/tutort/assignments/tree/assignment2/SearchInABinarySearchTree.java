package com.tutort.assignments.tree.assignment2;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.Arrays;

public class SearchInABinarySearchTree {

    public static void main(String[] args) {
        Tree tree = new Tree();
        Integer arr1[] = {4, 2, 7, 1 , 3};
        Arrays.stream(arr1).forEach(tree::insert);
        System.out.println(searchBST(tree.root, 2));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.SEARCH_IN_A_BINARY_SEARCH_TREE);
    }

    public static TreeNode searchBST(TreeNode root, int val) {
        if(root == null) {
            return null;
        }
        if(root.val == val) {
            return root;
        }
        if(val < root.val) {
            return searchBST(root.left, val);
        } else {
            return searchBST(root.right, val);
        }
    }
}
