package com.tutort.assignments.tree.assignment2;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.Arrays;

public class LowestCommonAncestorBinarySearchTree {

    public static void main(String[] args) {
        Tree tree = new Tree();
        Integer arr1[] = {4, 2, 7, 1 , 3};
        Arrays.stream(arr1).forEach(tree::insert);
        System.out.println(lowestCommonAncestor(tree.root, tree.root.left , tree.root.right));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.LOWEST_COMMON_ANCESTOR_OF_A_BINARY_SEARCH_TREE);
    }

    public static TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if(root == null || root == p || root == q) {
            return root;
        }
        TreeNode left = lowestCommonAncestor(root.left, p, q);
        TreeNode right = lowestCommonAncestor(root.right, p, q);
        if(left == null) {
            return right;
        } else if(right == null) {
            return left;
        } else {
            return root;
        }

    }
}
