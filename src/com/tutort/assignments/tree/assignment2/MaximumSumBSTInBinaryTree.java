package com.tutort.assignments.tree.assignment2;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.tree.common.Tree;
import com.tutort.classroom.tree.common.TreeNode;

import java.util.Arrays;

public class MaximumSumBSTInBinaryTree {

    public static void main(String[] args) {
        Tree tree = new Tree();
        int arr1[] = {4, 3, 1, 2};
        Arrays.stream(arr1).forEach(tree::insert);
        System.out.println(maxSumBST(tree.root));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.MAXIMUM_SUM_BST_IN_BINARY_TREE);
    }

    static int maxSum = 0;
    public static int maxSumBST(TreeNode root) {
        findMaxSum(root);
        return maxSum;
    }

    private static NodeWrapper findMaxSum(TreeNode root) {
        if (root == null) {
            return null;
        }

        NodeWrapper leftTree = findMaxSum(root.left);
        NodeWrapper rightTree = findMaxSum(root.right);

        NodeWrapper currentNode = new NodeWrapper(root);
        if (leftTree != null) {
            currentNode.sum += leftTree.sum;
            currentNode.max = Math.max(root.val, leftTree.max);
            currentNode.min = Math.min(root.val, leftTree.min);
            currentNode.validBST = leftTree.validBST && leftTree.max < root.val;
        }
        if (rightTree != null) {
            currentNode.sum += rightTree.sum;
            currentNode.max = Math.max(currentNode.max, rightTree.max);
            currentNode.min = Math.min(currentNode.min, rightTree.min);
            currentNode.validBST &= rightTree.validBST && rightTree.min > root.val;
        }

        if (currentNode.validBST) {
            maxSum = Math.max(maxSum, currentNode.sum);
        }
        return currentNode;
    }

    static class NodeWrapper {
        final TreeNode node;
        int sum;
        int max;
        int min;
        boolean validBST = true;
        public NodeWrapper(TreeNode node) {
            this.node = node;
            this.sum = node.val;
            this.max = node.val;
            this.min = node.val;
        }
    }
}
