package com.tutort.assignments.priorityqueue;

import com.tutort.LeetCodeURLConstant;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;

public class FindTheKthSmallestSumOfMatrixWithSortedRows {

    public static void main(String[] args) {
        System.out.println(kthSmallest(new int[][]{{1, 3, 11}, {2, 4, 6}}, 5));
        System.out.println(kthSmallest(new int[][]{{1, 3, 11}, {2, 4, 6}}, 9));
        System.out.println(kthSmallest(new int[][]{{1, 10, 10}, {1, 4, 5}, {2, 3, 6}}, 7));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.FIND_THE_KTH_SMALLEST_SUM_OF_A_MATRIX_WITH_SORTED_ROWS);
    }

    public static int kthSmallest(int[][] mat, int k) {
        int[] row = mat[0];

        for (int i = 1; i < mat.length; i++)
            row = kthSmallestUtil(row, mat[i], k);
        return row[k - 1];
    }

    public static int[] kthSmallestUtil(int[] nums1, int[] nums2, int k) {
        PriorityQueue<List<Integer>> minHeapPQ = new PriorityQueue<>((a, b) -> (a.get(0) + a.get(1)) - (b.get(0) + b.get(1)));
        List<Integer> result = new ArrayList<>();
        if (nums1.length == 0 || nums2.length == 0 || k == 0)
            return result.stream().mapToInt(i -> i).toArray();
        for (int i = 0; i < nums1.length && i < k; i++) {
            minHeapPQ.add(Arrays.asList(nums1[i], nums2[0], 0));
        }
        while (k-- > 0 && !minHeapPQ.isEmpty()) {
            List<Integer> current = minHeapPQ.poll();
            result.add(current.get(0) + current.get(1));
            int nums2Index = current.get(2);
            if (nums2Index < nums2.length - 1) {
                minHeapPQ.add(Arrays.asList(current.get(0), nums2[nums2Index + 1], nums2Index + 1));
            }
        }
        return result.stream().mapToInt(i -> i).toArray();
    }
}
