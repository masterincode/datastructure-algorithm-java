package com.tutort.assignments.priorityqueue;

import com.tutort.LeetCodeURLConstant;

import java.util.*;

public class TopKFrequentWords {

    public static void main(String[] args) {
        System.out.println(topKFrequent(new String[]{"i", "love", "leetcode", "i", "love", "coding"}, 2));
        System.out.println(topKFrequent(new String[]{"the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"}, 4));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.TOP_K_FREQUENT_WORDS);
    }

    public static List<String> topKFrequent(String[] words, int k) {
        Map<String, Integer> hashMap = new HashMap<>();
        for (String value : words) {
            hashMap.put(value, hashMap.getOrDefault(value, 0) + 1);
        }
        Comparator<String> comp = (a, b) -> hashMap.get(b) != hashMap.get(a) ? hashMap.get(b) - hashMap.get(a) : a.compareTo(b);
        PriorityQueue<String> maxHeapPQ = new PriorityQueue<>(comp);
        maxHeapPQ.addAll(hashMap.keySet());
        List<String> result = new ArrayList<>();
        int pollTimes = 0;
        while (!maxHeapPQ.isEmpty() && pollTimes != k) {
            result.add(maxHeapPQ.poll());
            pollTimes++;
        }
        return result;
    }
}
