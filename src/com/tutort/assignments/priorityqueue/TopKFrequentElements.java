package com.tutort.assignments.priorityqueue;

import com.tutort.LeetCodeURLConstant;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class TopKFrequentElements {

    public static void main(String[] args) {
        int[] arr = {1, 2};
        System.out.println(Arrays.toString(topKFrequent(arr, 2)));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.TOP_K_FREQUENT_ELEMENTS);
    }

    public static int[] topKFrequent(int[] nums, int k) {
        Map<Integer, Integer> hashMap = new HashMap<>();
        for (int value : nums) {
            hashMap.put(value, hashMap.getOrDefault(value, 0) + 1);
        }

        PriorityQueue<Integer> maxHeapPQ = new PriorityQueue<>((a, b) -> (hashMap.get(b) - hashMap.get(a)));
        maxHeapPQ.addAll(hashMap.keySet());

        int[] result = new int[k];
        int index = 0;
        int pollTimes = 0;
        while (!maxHeapPQ.isEmpty() && pollTimes != k) {
            result[index] = maxHeapPQ.poll();
            index++;
            pollTimes++;
        }
        return result;
    }
}
