package com.tutort.assignments.priorityqueue;

import com.tutort.LeetCodeURLConstant;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;

public class FindKPairsWithSmallestSums {

    public static void main(String[] args) {
        System.out.println(kSmallestPairsOptimisedApproach(new int[]{1, 7, 11}, new int[]{2, 4, 6}, 3));
        System.out.println(kSmallestPairsOptimisedApproach(new int[]{1, 1, 2}, new int[]{1, 2, 3}, 2));
        System.out.println(kSmallestPairsOptimisedApproach(new int[]{1, 2}, new int[]{3}, 3));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.FIND_K_PAIRS_WITH_SMALLEST_SUMS);
    }

    // Time Complexity: O(k log k) since minHeapPQ.size <= k and we do at most k loop.
    // Space Complexity: n
    public static List<List<Integer>> kSmallestPairsOptimisedApproach(int[] nums1, int[] nums2, int k) {
        PriorityQueue<List<Integer>> minHeapPQ = new PriorityQueue<>((a, b) -> (a.get(0) + a.get(1)) - (b.get(0) + b.get(1)));
        List<List<Integer>> result = new ArrayList<>();
        if (nums1.length == 0 || nums2.length == 0 || k == 0)
            return result;
        for (int i = 0; i < nums1.length && i < k; i++) {
            minHeapPQ.add(Arrays.asList(nums1[i], nums2[0], 0));
        }
        while (k-- > 0 && !minHeapPQ.isEmpty()) {
            List<Integer> current = minHeapPQ.poll();
            result.add(Arrays.asList(current.get(0), current.get(1)));
            int nums2Index = current.get(2);
            if (current.get(2) < nums2.length - 1) {
                minHeapPQ.add(Arrays.asList(current.get(0), nums2[nums2Index + 1], nums2Index + 1));
            }
        }
        return result;

    }

    // Time Complexity: O (n^2)
    // Space Complexity: n
    // TODO: Time Limit Exceeded
    public static List<List<Integer>> kSmallestPairsBruteForceApproach(int[] nums1, int[] nums2, int k) {
        PriorityQueue<List<Integer>> minHeapPQ = new PriorityQueue<>((a, b) -> (b.get(0) + b.get(1)) - (a.get(0) + a.get(1)));
        for (int i = 0; i < nums1.length; i++) {
            for (int j = 0; j < nums2.length; j++) {
                minHeapPQ.add(Arrays.asList(nums1[i], nums2[j]));
                if (minHeapPQ.size() > k) {
                    minHeapPQ.poll();
                }
            }
        }
        return new ArrayList<>(minHeapPQ);
    }
}
