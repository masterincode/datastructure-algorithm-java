package com.tutort.assignments.priorityqueue;

import com.tutort.LeetCodeURLConstant;

import java.util.PriorityQueue;

public class KthLargestElementInAStream {

    public static void main(String[] args) {
        KthLargestElementInAStream kthLargestElementInAStream = new KthLargestElementInAStream(2, new int[] {0});
        System.out.println(kthLargestElementInAStream.add(-1));   // return 4
        System.out.println(kthLargestElementInAStream.add(1));   // return 5
        System.out.println(kthLargestElementInAStream.add(-2));  // return 5
        System.out.println(kthLargestElementInAStream.add(-4));   // return 8
        System.out.println(kthLargestElementInAStream.add(3));   // return 8
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.KTH_LARGEST_ELEMENT_IN_A_STREAM);
    }

    int k;
    PriorityQueue<Integer> minHeapPQ;
    public KthLargestElementInAStream(int k, int[] nums) {
        this.k = k;
        minHeapPQ = new PriorityQueue<>();
        for (int value: nums) {
            this.add(value);
        }
    }

    public int add(int val) {
        minHeapPQ.add(val);
        if(minHeapPQ.size() > this.k) {
            minHeapPQ.remove(); //Removes the head of this queue
        }
        return minHeapPQ.peek();
    }
}
