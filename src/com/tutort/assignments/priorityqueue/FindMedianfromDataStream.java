package com.tutort.assignments.priorityqueue;

import com.tutort.LeetCodeURLConstant;

import java.util.Collections;
import java.util.PriorityQueue;

public class FindMedianfromDataStream {

    public static void main(String[] args) {
        FindMedianfromDataStream medianFinder = new FindMedianfromDataStream();
        medianFinder.addNum(1);    // arr = [1]
        medianFinder.addNum(2);    // arr = [1, 2]
        System.out.println(medianFinder.findMedian()); // return 1.5 (i.e., (1 + 2) / 2)
        medianFinder.addNum(3);    // arr[1, 2, 3]
        System.out.println(medianFinder.findMedian()); // return 2.0
        medianFinder.addNum(4);    // arr[1, 2, 3, 4]
        System.out.println(medianFinder.findMedian()); // return 2.5
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.FIND_MEDIAN_FROM_DATA_STREAM);
    }

    PriorityQueue<Integer> minHeapPQ = new PriorityQueue();
    PriorityQueue<Integer> maxHeapPQ = new PriorityQueue(1000, Collections.reverseOrder());

    // Adds a number into the data structure.
    public void addNum(int num) {
        maxHeapPQ.add(num);
        minHeapPQ.add(maxHeapPQ.poll());
        if (maxHeapPQ.size() < minHeapPQ.size()) {
            maxHeapPQ.add(minHeapPQ.poll());
        }
    }
    // Returns the median of current data stream
    public double findMedian() {
        if (maxHeapPQ.size() == minHeapPQ.size())
            return (maxHeapPQ.peek() + minHeapPQ.peek()) / 2.0;
        else
            return maxHeapPQ.peek();
    }
}
