package com.tutort.assignments.priorityqueue;

import com.tutort.LeetCodeURLConstant;

import java.util.Collections;
import java.util.PriorityQueue;

public class LastStoneWeight {

    public static void main(String[] args) {
        int[] arr = new int[]{1};
        System.out.println(lastStoneWeight(arr));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.LAST_STONE_WEIGHT);
    }

    public static int lastStoneWeight(int[] stones) {
        PriorityQueue<Integer> maxHeap = new PriorityQueue<>(Collections.reverseOrder());
        for (int value : stones) {
            maxHeap.add(value);
        }
        while (maxHeap.size() != 1) {
            int first = maxHeap.poll();
            int sec = maxHeap.poll();
            int diff = Math.abs(first - sec);
            maxHeap.add(diff);
        }
        return maxHeap.peek();
    }
}
