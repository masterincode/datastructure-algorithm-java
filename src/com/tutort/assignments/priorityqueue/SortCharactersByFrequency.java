package com.tutort.assignments.priorityqueue;


import com.tutort.LeetCodeURLConstant;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class SortCharactersByFrequency {

    public static void main(String[] args) {
        System.out.println(frequencySortOptimisedApproach("Aabb"));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.SORT_CHARACTERS_BY_FREQUENCY);
    }

    // Time Complexity:
    // Space Complexity: 2n
    public static String frequencySortOptimisedApproach(String s) {
        // Count the occurence on each character
        HashMap<Character, Integer> hashMap = new HashMap<>();
        for (char ch : s.toCharArray()) {
            hashMap.put(ch, hashMap.getOrDefault(ch, 0) + 1);
        }

        // Build maxHeapPQ
        PriorityQueue<Character> maxHeapPQ = new PriorityQueue<>((a, b) -> (hashMap.get(b) - hashMap.get(a)));
        maxHeapPQ.addAll(hashMap.keySet());

        // Build string
        StringBuilder sb = new StringBuilder();
        while (!maxHeapPQ.isEmpty()) {
            char ch = maxHeapPQ.poll();
            for (int i = 0; i < hashMap.get(ch); i++) {
                sb.append(ch);
            }
        }
        return sb.toString();
    }

    // Time Complexity:
    // Space Complexity: 2n
    public static String frequencySortBruteForceApproach(String s) {
        PriorityQueue<AlphabetFrequency> maxHeapPQ = new PriorityQueue<>(Collections.reverseOrder());
        Map<Character, AlphabetFrequency> map = new HashMap<>();
        Integer count = null;
        for (int i = 0; i < s.length(); i++) {
            if (map.get(s.charAt(i)) == null) {
                map.put(s.charAt(i), new AlphabetFrequency(1, s.charAt(i)));
            } else {
                count = map.get(s.charAt(i)).freq;
                map.put(s.charAt(i), new AlphabetFrequency(++count, s.charAt(i)));
            }
        }
        map.forEach((key, value) -> {
            maxHeapPQ.add(value);
        });
        String result = "";
        while (!maxHeapPQ.isEmpty()) {
            AlphabetFrequency alpha = maxHeapPQ.poll();
            for (int i = 1; i <= alpha.freq; i++) {
                result = result + alpha.ch;
            }
        }
        return result;
    }
}

class AlphabetFrequency implements Comparable<AlphabetFrequency> {
    public int freq;
    public char ch;

    public AlphabetFrequency(int freq, char ch) {
        this.freq = freq;
        this.ch = ch;
    }

    @Override
    public int compareTo(AlphabetFrequency o) {
        return (this.freq > o.freq) ? 1 : (this.freq < o.freq) ? -1 : 0;
    }
}
