package com.tutort.assignments.priorityqueue;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.linkedlist.ListNode;
import com.tutort.classroom.linkedlist.ListNodeInputData;

import java.util.Arrays;
import java.util.PriorityQueue;

public class MergeKSortedLists {

    public static void main(String[] args) {
        int[] arr1 = {1, 4, 5};
        ListNodeInputData listNode1 = new ListNodeInputData();
        for (int value : arr1) {
            listNode1.push(value);
        }
        int[] arr2 = {1, 3, 4};
        ListNodeInputData listNode2 = new ListNodeInputData();
        for (int value : arr2) {
            listNode2.push(value);
        }

        int[] arr3 = {2, 6};
        ListNodeInputData listNode3 = new ListNodeInputData();
        for (int value : arr3) {
            listNode3.push(value);
        }
        ListNode[] lists = {listNode1.head, listNode2.head, listNode3.head};
        System.out.println(Arrays.toString(lists));
        ListNode result = mergeKLists(lists);
        while (result != null) {
            System.out.print(result.val+" ->");
            result = result.next;
        }
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.MERGE_K_SORTED_LISTS);
    }


    public static ListNode mergeKLists(ListNode[] lists) {

        if (lists == null || lists.length == 0)
            return null;

        PriorityQueue<ListNode> minHeapPQ = new PriorityQueue<>((a, b) -> a.val - b.val);
        ListNode dummy = new ListNode(0);
        ListNode tail = dummy;
        for (ListNode node : lists) {
            if (node != null) {
                minHeapPQ.add(node);
            }
        }
        while (!minHeapPQ.isEmpty()) {
            ListNode newNode = minHeapPQ.poll();
            tail.next = newNode;
            tail = tail.next;

            if (tail.next != null)
                minHeapPQ.add(tail.next);
        }
        return dummy.next;
    }
}
