package com.tutort.assignments.priorityqueue;

import com.tutort.LeetCodeURLConstant;

import java.util.Arrays;

public class KthSmallestElementInSortedMatrix {

    public static void main(String[] args) {
        int[][] arr = {{1, 5, 9}, {10, 11, 13}, {12, 13, 15}};
        System.out.println(kthSmallest(arr, 8));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.KTH_SMALLEST_ELEMENT_IN_A_SORTED_MATRIX);
    }


    //O(Range * nlogn)
    //Range is Mat[row-1][col-1] - Mat[0][0].
    //n is total elements in matrix.
    //n rows are traversed inside which binary search of O(log n) is done so nlogn
    // TODO: https://www.youtube.com/watch?v=LkrsdWa69_Q&t=789s&ab_channel=ShashwatTiwari
    public static int kthSmallest(int[][] matrix, int k) {
        if (matrix == null) {
            return 0;
        }
        int row = matrix.length;
        int col = matrix[0].length;
        int startVal = matrix[0][0];
        int endVal = matrix[row - 1][col - 1];
        int midVal;

        while (startVal <= endVal) {
            midVal = (endVal + startVal) / 2;
            int ans = 0;
            /* "ans" store krega ki pure matrix me kitni values choti hai ya equal hai "midVal" variable se.
               "low" variable store kr rha hai each row me kitne <= "midVal" values hai and "ans" store kr rha hai ki sare rows
                ka add krke that's means pure matrix me kitne values <=midVal hai.
            * */
            for (int i = 0; i < row; i++) {
                int low = 0, high = col - 1;
                while (low <= high) {
                    int mid = low + (high - low) / 2;
                    if (matrix[i][mid] <= midVal) {
                        low = mid + 1;
                    } else {
                        high = mid - 1;
                    }
                }
                ans = ans + low;
            }
            if (ans < k) {
                startVal = midVal + 1;
            } else {
                endVal = midVal - 1;
            }
        }
        return startVal;
    }

    // Time Complexity: O (n^2)
    // Sapce Complexity: n^2
    public int kthSmallestBruteForceApproach(int[][] matrix, int k) {
        int n = matrix.length;
        int[] arr = new int[n * n];
        int index = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arr[index++] = matrix[i][j];
            }
        }
        Arrays.sort(arr);
        return arr[k - 1];
    }
}
