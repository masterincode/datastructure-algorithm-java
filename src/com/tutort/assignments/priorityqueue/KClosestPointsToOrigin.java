package com.tutort.assignments.priorityqueue;


import com.tutort.LeetCodeURLConstant;

import java.util.Arrays;
import java.util.PriorityQueue;

public class KClosestPointsToOrigin {

    public static void main(String[] args) {
        int[][] points = {{3, 3}, {5, -1}, {-2, 4}};
        System.out.println(Arrays.toString(kClosest(points, 2)));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.K_CLOSEST_POINTS_TO_ORIGIN);
    }

    //TODO: Here they are asking closest distance means => small distance
    // Due to which here maxHeapPQ will be used
    public static int[][] kClosest(int[][] points, int k) {

        PriorityQueue<ClosestDistance> maxHeapPQ = new PriorityQueue<>();
        for (int i = 0; i < points.length; i++) {
            double first = Math.pow(points[i][0], 2);
            double sec = Math.pow(points[i][1], 2);
            double squareRoot = Math.sqrt(first + sec);
            maxHeapPQ.add(new ClosestDistance(squareRoot, points[i][0], points[i][1]));
            if (maxHeapPQ.size() > k) {
                maxHeapPQ.poll();
            }
        }
        int[][] result = new int[k][2];
        for (int i = 0; i < result.length; i++) {
            ClosestDistance closestDistance = maxHeapPQ.poll();
            result[i][0] = closestDistance.xAxis;
            result[i][1] = closestDistance.yAxis;
        }
        return result;
    }
}

class ClosestDistance implements Comparable<ClosestDistance> {
    public double distance;
    public int xAxis;
    public int yAxis;

    public ClosestDistance(double distance, int xAxis, int yAxis) {
        this.distance = distance;
        this.xAxis = xAxis;
        this.yAxis = yAxis;
    }

    @Override
    public int compareTo(ClosestDistance o) {
        if (this.distance < o.distance) {
            return 1;
        } else if (this.distance > o.distance) {
            return -1;
        } else {
            return 0;
        }
    }
}

