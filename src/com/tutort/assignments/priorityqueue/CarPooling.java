package com.tutort.assignments.priorityqueue;

import com.tutort.LeetCodeURLConstant;

import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;
import java.util.stream.Collectors;

public class CarPooling {

    public static void main(String[] args) {
        System.out.println(carPooling(new int[][]{{2, 1, 5}, {3, 3, 7}}, 5));
        System.out.println(carPooling(new int[][]{{2, 1, 5}, {3, 3, 7}}, 4));
        System.out.println(carPooling(new int[][]{{2, 1, 3}, {2, 3, 7}}, 4));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.CAR_POOLING);
    }

    public static boolean carPooling(int[][] trips, int capacity) {
        Arrays.sort(trips, (a, b) -> a[1] - b[1]);  // sort array from start time in ascending order
        PriorityQueue<List<Integer>> pq = new PriorityQueue<>((a, b) -> a.get(2) - b.get(2));  // sort PriorityQueue from end time in ascending order.

        for (int[] trip : trips) {
            while (!pq.isEmpty() && pq.peek().get(2) <= trip[1]) {
                capacity = capacity + pq.poll().get(0);
            }
            List<Integer> tripList = Arrays.stream(trip).boxed().collect(Collectors.toList());
            pq.add(tripList);
            capacity = capacity - trip[0];
            if (capacity < 0)
                return false;
        }
        return true;
    }
}
