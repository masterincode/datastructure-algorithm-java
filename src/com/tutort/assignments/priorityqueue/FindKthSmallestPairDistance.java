package com.tutort.assignments.priorityqueue;

import com.tutort.LeetCodeURLConstant;

import java.util.Arrays;
import java.util.PriorityQueue;

public class FindKthSmallestPairDistance {

    public static void main(String[] args) {
        System.out.println(smallestDistancePair(new int[]{1, 3, 1}, 1));
        System.out.println(smallestDistancePair(new int[]{1, 14, 11, 15, 6}, 6));
        System.out.println(smallestDistancePair(new int[]{1, 6, 1}, 3));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.FIND_K_TH_SMALLEST_PAIR_DISTANCE);
    }

    public static int smallestDistancePair(int[] nums, int k) {
        Arrays.sort(nums);
        // 1, 6, 11, 14, 15
        int minDistance = 0; //(1, 1) => 1 - 1
        int maxDistance = nums[nums.length - 1] - nums[0]; // (1, 14) => 14 - 1
        while (minDistance < maxDistance) {
            int midDistance = (minDistance + maxDistance) / 2;

            //Sliding window
            int count = 0, left = 0;
            for (int right = 0; right < nums.length; right++) {
                while (nums[right] - nums[left] > midDistance) {
                    left++;
                }
                count = count + right - left;
            }
            //count = number of pairs with distance <= midDistance
            if (count >= k)
                maxDistance = midDistance;
            else
                minDistance = midDistance + 1;
        }
        return minDistance;
    }

    // Time Complexity: O (n^2)
    // Space Complexity: n
    // TODO: Time Limit Exceeded
    public static int smallestDistancePairBruteForceApproach(int[] nums, int k) {

        PriorityQueue<Integer> maxHeapPQ = new PriorityQueue<>((a, b) -> b - a);
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                maxHeapPQ.add(Math.abs(nums[i] - nums[j]));
                if (maxHeapPQ.size() > k) {
                    maxHeapPQ.poll();
                }
            }
        }
        return maxHeapPQ.peek();
    }
}
