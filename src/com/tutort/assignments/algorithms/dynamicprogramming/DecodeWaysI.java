package com.tutort.assignments.algorithms.dynamicprogramming;

import com.tutort.LeetCodeURLConstant;

public class DecodeWaysI {

    public static void main(String[] args) {
        System.out.println(numDecodingsBrute("1206"));
        System.out.println(numDecodingsBetter("1206"));
        System.out.println(numDecodingsOptimised("1206"));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.DECODE_WAYS);
    }

    // TODO: Recursive Approach
    //  Time Complexity: O (2^n)
    public static int numDecodingsBrute(String s) {
        return numDecodingsBruteUtil(s, 0);
    }

    private static int numDecodingsBruteUtil(String s, int index) {
        if (index == s.length()) {
            return 1;
        }
        if (s.charAt(index) == '0') {
            return 0;
        }

        if (index == s.length() - 1) {
            return 1;
        }
        int ans = numDecodingsBruteUtil(s, index + 1);
        if (Integer.parseInt(s.substring(index, index + 2)) <= 26) {
            ans = ans + numDecodingsBruteUtil(s, index + 2);
        }
        return ans;
    }

    // TODO: Recursive Approach with Memoization
    //  Time Complexity: O (N)
    //  Space Complexity: O(N)
    public static int numDecodingsBetter(String s) {
        int[] dp = new int[s.length() + 1];
        return numDecodingBetterUtil(s, 0, dp);
    }

    private static int numDecodingBetterUtil(String s, int index, int[] dp) {
        if (index == s.length()) {
            return 1;
        }
        if (s.charAt(index) == '0') {
            return 0;
        }
        if (index == s.length() - 1) {
            return 1;
        }
        if (dp[index] > 0)
            return dp[index];

        int ans = numDecodingBetterUtil(s, index + 1, dp);
        if (Integer.parseInt(s.substring(index, index + 2)) <= 26) {
            ans = ans + numDecodingBetterUtil(s, index + 2, dp);
        }
        dp[index] = ans;
        return ans;
    }

    // TODO: DP Solutions
    //  Time Complexity: O (N)
    //  Space Complexity: O(N)
    public static int numDecodingsOptimised(String s) {
        int[] dp = new int[s.length() + 1];
        dp[0] = 1;
        dp[1] = s.charAt(0) == '0' ? 0 : 1; //if there is only char and if it's not 0 then 1. Base case

        for (int i = 2; i < dp.length; i++) {
            if (s.charAt(i - 1) != '0') {
                dp[i] = dp[i] + dp[i - 1];
            }
            int twoDigit = Integer.parseInt(s.substring(i - 2, i));
            if (twoDigit >= 10 && twoDigit <= 26) {
                dp[i] = dp[i] + dp[i - 2];
            }
        }
        return dp[s.length()];
    }
}
