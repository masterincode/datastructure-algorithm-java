package com.tutort.assignments.algorithms.dynamicprogramming;

import com.tutort.LeetCodeURLConstant;

public class PalindromicSubstrings {

    public static void main(String[] args) {
        System.out.println(countSubstrings("aaa"));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.PALINDROMIC_SUBSTRINGS);
    }

    public static int countSubstrings(String s) {
        int length = s.length();
        boolean[][] dp = new boolean[length][length];
        int count = 0;

        for (int gap = 0; gap < dp.length; gap++) {
            for (int row = 0, col = gap; col < dp.length; row++, col++) {
                if (gap == 0) {
                    dp[row][col] = true;
                } else if (gap == 1) {
                    if (s.charAt(row) == s.charAt(col)) {
                        dp[row][col] = true;
                    } else {
                        dp[row][col] = false;
                    }
                } else {
                    if (s.charAt(row) == s.charAt(col) && dp[row + 1][col - 1]) {
                        dp[row][col] = true;
                    } else {
                        dp[row][col] = false;
                    }
                }
                if(dp[row][col]) {
                    count++;
                }
            }
        }
        return count;
    }


}
