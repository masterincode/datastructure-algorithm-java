package com.tutort.assignments.algorithms.dynamicprogramming;

import com.tutort.LeetCodeURLConstant;

import java.util.Arrays;

public class LongestIncreasingSubsequence {

    public static void main(String[] args) {
        System.out.println(lengthOfLISBrute(new int[]{10, 9, 2, 5, 3, 7, 101, 18}));
        System.out.println(lengthOfLISBetter(new int[]{10, 9, 2, 5, 3, 7, 101, 18}));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.LONGEST_INCREASING_SUBSEQUENCE);
    }


    // TODO: Recursive Approach
    //  Time Complexity: O (2^n)
    public static int lengthOfLISBrute(int[] nums) {
        return lengthOfLISBruteUtil(nums, 0, -1);
    }

    private static int lengthOfLISBruteUtil(int[] nums, int index, int prevIndex) {
        if (index == nums.length) {
            return 0;
        }
        int include = 0;
        int exclude = 0 + lengthOfLISBruteUtil(nums, index + 1, prevIndex);

        // prevIndex == -1 , it means it does not have previous element, then I can include current element.
        if (prevIndex == -1 || nums[index] > nums[prevIndex]) {
            include = 1 + lengthOfLISBruteUtil(nums, index + 1, index);
        }
        return Math.max(include, exclude);
    }

    // TODO: Recursive Approach with Memoization
    //  Time Complexity: O (N * N)
    //  Space Complexity: O(N * N) + O(N)
    public static int lengthOfLISBetter(int[] nums) {
        int[][] dp =  new int[nums.length][nums.length];
        for (int[] arr: dp)
            Arrays.fill(arr, -1);
        return lengthOfLISBetterUtil(nums, 0, -1, dp);
    }

    private static int lengthOfLISBetterUtil(int[] nums, int index, int prevIndex, int[][] dp) {
        if (index == nums.length) {
            return 0;
        }
        if(dp[index][prevIndex + 1] != -1) {
            return dp[index][prevIndex + 1];
        }
        int include = 0;
        int exclude = 0 + lengthOfLISBetterUtil(nums, index + 1, prevIndex, dp);

        // prevIndex == -1 , it means it does not have previous element, then I can include current element.
        if (prevIndex == -1 || nums[index] > nums[prevIndex]) {
            include = 1 + lengthOfLISBetterUtil(nums, index + 1, index, dp);
        }
        return dp[index][prevIndex + 1] = Math.max(include, exclude);
    }
}
