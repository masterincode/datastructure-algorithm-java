package com.tutort.assignments.algorithms.dynamicprogramming;

import com.tutort.LeetCodeURLConstant;

import java.util.Arrays;

public class BestTimeToBuyAndSellStockWithCooldown {

    public static void main(String[] args) {
        int[] prices = {7, 1, 5, 3, 6, 4};
        System.out.println(maxProfitBrute(prices));
        System.out.println(maxProfitBetter(prices));
        System.out.println(maxProfitOptimised(prices));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.BEST_TIME_TO_BUY_AND_SELL_STOCK_WITH_COOLDOWN);
    }

    // TODO: Recursion Solution
    //Time Complexity: O(2 ^ n)
    //Space Complexity: O(1)
    public static int maxProfitBrute(int[] prices) {
        return maxProfitBruteUtil(prices, 0, 0);
    }

    private static int maxProfitBruteUtil(int[] prices, int index, int buy) {
        int include;
        int exclude;
        if (index >= prices.length) { // here >=
            return 0;
        }
        int profit = 0;
        if (buy == 0) { // We can buy the stock
            include = -prices[index] + maxProfitBruteUtil(prices, index + 1, 1);
            exclude = maxProfitBruteUtil(prices, index + 1, 0);
            profit = Math.max(include, exclude);
        }
        if (buy == 1) { // We can sell the stock
            include = prices[index] + maxProfitBruteUtil(prices, index + 2, 0); //Here + 2
            exclude = maxProfitBruteUtil(prices, index + 1, 1);
            profit = Math.max(include, exclude);
        }
        return profit;
    }

    // TODO: Recursion Solution with memoization
    //Time Complexity: O(n ^ 2)
    //Space Complexity: O(n ^ 2) + O (n)
    public static int maxProfitBetter(int[] prices) {
        int[][] dp = new int[prices.length][prices.length];
        for (int[] arr : dp)
            Arrays.fill(arr, -1);
        return maxProfitBetterUtil(prices, 0, 0, dp);
    }

    private static int maxProfitBetterUtil(int[] prices, int index, int buy, int[][] dp) {
        int include;
        int exclude;
        if (index >= prices.length) {
            return 0;
        }
        if (dp[index][buy] != -1) {
            return dp[index][buy];
        }
        int profit = 0;
        if (buy == 0) { // We can buy the stock
            include = -prices[index] + maxProfitBetterUtil(prices, index + 1, 1, dp);
            exclude = maxProfitBetterUtil(prices, index + 1, 0, dp);
            profit = Math.max(include, exclude);
        }
        if (buy == 1) { // We can sell the stock
            include = prices[index] + maxProfitBetterUtil(prices, index + 2, 0, dp); //Here + 2
            exclude = maxProfitBetterUtil(prices, index + 1, 1, dp);
            profit = Math.max(include, exclude);
        }
        return dp[index][buy] = profit;
    }

    // TODO: Recursion Solution with memoization
    //Time Complexity: O(n ^ 2)
    //Space Complexity: O(n ^ 2)
    public static int maxProfitOptimised(int[] prices) {
        int include;
        int exclude;
        int n = prices.length;
        int dp[][] = new int[n + 2][2]; // size will n + 2
        // As dp array is intialized to 0, we have already covered the base case
        int profit = 0;
        for (int ind = n - 1; ind >= 0; ind--) {
            for (int buy = 0; buy <= 1; buy++) {
                if (buy == 0) {// We can buy the stock
                    include = -prices[ind] + dp[ind + 1][1];
                    exclude =  dp[ind + 1][0];
                    profit = Math.max(include, exclude);
                }
                if (buy == 1) {// We can sell the stock
                    include = prices[ind] + dp[ind + 2][0]; //Here + 2
                    exclude =  dp[ind + 1][1];
                    profit = Math.max(include, exclude);
                }
                dp[ind][buy] = profit;
            }
        }
        return dp[0][0];
    }
}
