package com.tutort.assignments.algorithms.dynamicprogramming;

import com.tutort.LeetCodeURLConstant;

import java.util.Arrays;

public class BestTimeToBuyAndSellStockPart3 {
    public static void main(String[] args) {
        int[] prices = {3, 3, 5, 0, 0, 3, 1, 4};
        System.out.println(maxProfitBrute(prices));
        System.out.println(maxProfitBetter(prices));
        System.out.println(maxProfitOptimised(prices));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.BEST_TIME_TO_BUY_AND_SELL_STOCK_III);
    }

    // TODO: Recursion Solution
    //Time Complexity: O(2 ^ n)
    //Space Complexity: O(1)
    public static int maxProfitBrute(int[] prices) {
        return maxProfitBruteUtil(prices, 0, 0, 2);
    }

    private static int maxProfitBruteUtil(int[] prices, int index, int buy, int txnLimit) {
        int include;
        int exclude;
        if (txnLimit == 0) {
            return 0;
        }
        if (index == prices.length) {
            return 0;
        }
        int profit = 0;
        if (buy == 0) { // We can buy the stock
            include = -prices[index] + maxProfitBruteUtil(prices, index + 1, 1, txnLimit);
            exclude = maxProfitBruteUtil(prices, index + 1, 0, txnLimit);
            profit = Math.max(include, exclude);
        }
        if (buy == 1) { // We can sell the stock
            include = prices[index] + maxProfitBruteUtil(prices, index + 1, 0, txnLimit - 1);
            exclude = maxProfitBruteUtil(prices, index + 1, 1, txnLimit);
            profit = Math.max(include, exclude);
        }
        return profit;
    }

    // TODO: Recursion Solution with memoization
    //Time Complexity: O(n ^ 2)
    //Space Complexity: O(n ^ 2) + O (n)
    public static int maxProfitBetter(int[] prices) {
        int[][][] dp = new int[prices.length][2][3];
        for (int[][]  x: dp) {
            for (int[] y: x) {
                Arrays.fill(y, -1);
            }
        }
        return maxProfitBetterUtil(prices, 0, 0, dp, 2);
    }

    private static int maxProfitBetterUtil(int[] prices, int index, int buy, int[][][] dp, int txnLimit) {
        int include;
        int exclude;
        if (txnLimit == 0) {
            return 0;
        }
        if (index == prices.length) {
            return 0;
        }
        if (dp[index][buy][txnLimit] != -1) {
            return dp[index][buy][txnLimit];
        }
        int profit = 0;
        if (buy == 0) { // We can buy the stock
            include = -prices[index] + maxProfitBetterUtil(prices, index + 1, 1, dp, txnLimit);
            exclude = maxProfitBetterUtil(prices, index + 1, 0, dp, txnLimit);
            profit = Math.max(include, exclude);
        }
        if (buy == 1) { // We can sell the stock
            include = prices[index] + maxProfitBetterUtil(prices, index + 1, 0, dp, txnLimit - 1);
            exclude = maxProfitBetterUtil(prices, index + 1, 1, dp, txnLimit);
            profit = Math.max(include, exclude);
        }
        return dp[index][buy][txnLimit] = profit;
    }

    // TODO: Recursion Solution with memoization
    //Time Complexity: O(n ^ 2)
    //Space Complexity: O(n ^ 2)
    public static int maxProfitOptimised(int[] prices) {
        int include;
        int exclude;
        int n = prices.length;
        int dp[][][] = new int[n + 1][2][3];
        // As dp array is intialized to 0, we have already covered the base case
        int profit = 0;
        for (int ind = n - 1; ind >= 0; ind--) {
            for (int buy = 0; buy <= 1; buy++) {
                for (int cap = 1; cap <=2 ; cap++) {
                    if (buy == 0) {// We can buy the stock
                        exclude = dp[ind + 1][0][cap];
                        include = -prices[ind] + dp[ind + 1][1][cap];
                        profit = Math.max(include, exclude);
                    }
                    if (buy == 1) {// We can sell the stock
                        exclude = dp[ind + 1][1][cap];
                        include = prices[ind] + dp[ind + 1][0][cap-1];
                        profit = Math.max(include, exclude);
                    }
                    dp[ind][buy][cap] = profit;
                }
            }
        }
        return dp[0][0][2];
    }

}
