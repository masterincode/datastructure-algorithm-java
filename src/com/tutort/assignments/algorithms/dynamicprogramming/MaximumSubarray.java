package com.tutort.assignments.algorithms.dynamicprogramming;

import com.tutort.LeetCodeURLConstant;

public class MaximumSubarray {

    public static void main(String[] args) {
        System.out.println(maxSubArray(new int[] {-2,1,-3,4,-1,2,1,-5,4}));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.MAXIMUM_SUBARRAY);
    }

    // Time Complexity: O (n)
    public static int maxSubArray(int[] nums) {
        int sum = nums[0];
        int maxSum = nums[0];
        for (int i = 1; i < nums.length; i++) {

            // If sum is already +ve then add new value i.e nums[i]
            if(sum >= 0) {
                sum = sum + nums[i];
            } else {
                sum = nums[i]; // If sum is -ve then start with new value i.e nums[i]
            }

            // Find max sum
            if(sum > maxSum) {
                maxSum = sum;
            }
        }
        return maxSum;
    }
}
