package com.tutort.assignments.linkedlist.assignment1;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.linkedlist.ListNode;
import com.tutort.classroom.linkedlist.ListNodeInputData;

public class RemoveLinkedListElements {

    public static void main(String[] args) {
        int[] arr1 = {1,2,6,3,4,5,6};
        ListNodeInputData listNode1 = new ListNodeInputData();
        for (int value: arr1) {
            listNode1.push(value);
        }
        System.out.println(bruteForceApproach(listNode1.head, 6));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.REMOVE_LINKED_LIST_ELEMENTS);
    }

    // Time Complexity: O (n)
    // Space Complexity: 1
    public static ListNode bruteForceApproach(ListNode head, int val) {
        if(head == null) {
            return head;
        }
        ListNode fast = head;
        ListNode dummy = new ListNode(0);
        ListNode slow = dummy;
        dummy.next = fast;
        while(fast != null) {
            if(fast.val == val) {
                slow.next = fast.next;
            } else {
                slow = fast;
            }
            fast = fast.next;
        }
        return dummy.next;
    }
}
