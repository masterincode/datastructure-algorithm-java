package com.tutort.assignments.linkedlist.assignment1;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.linkedlist.ListNode;
import com.tutort.classroom.linkedlist.ListNodeInputData;

import java.util.HashMap;
import java.util.Map;

public class RemoveZeroSumConsecutiveNodesFromLinkedList {

    public static void main(String[] args) {
        int[] arr1 = {1,2,-3,3,1};
        ListNodeInputData listNode1 = new ListNodeInputData();
        for (int value: arr1) {
            listNode1.push(value);
        }
        System.out.println(optimizedApproach(listNode1.head));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.REMOVE_ZERO_SUM_CONSECUTIVE_NODES_FROM_LINKED_LIST);
    }

    // Time Complexity: O (n * m)
    // Space Complexity: n
    public static ListNode bruteForceApproach(ListNode head) {
        ListNode dummy = new ListNode(0);
        dummy.next = head;
        Map<Integer, ListNode> map = new HashMap<>();
        int prefixSum = 0;
        map.put(prefixSum, dummy);
        ListNode current = head;
        while (current != null) {
            prefixSum = prefixSum + current.val;
            if(map.get(prefixSum) != null) {
                //remove entries
                ListNode removeNode = map.get(prefixSum).next;
                int sum = prefixSum;
                while (removeNode != current) {
                    sum = sum + removeNode.val;
                    map.remove(sum);
                    removeNode = removeNode.next;
                }
                //draw link
                map.get(prefixSum).next = current.next;
            } else {
                map.put(prefixSum, current);
            }
            current = current.next;
        }
        return dummy.next;
    }

    // Time Complexity: O (n)
    // Space Complexity: n
    public static ListNode optimizedApproach(ListNode head) {
        ListNode dummy = new ListNode(0);
        dummy.next = head;
        int prefixSum = 0;
        Map<Integer, ListNode> map = new HashMap<>();
        map.put(prefixSum, dummy);
        ListNode current = head;
        while (current != null) {
            prefixSum = prefixSum + current.val;
            map.put(prefixSum, current);
            current = current.next;
        }
        prefixSum = 0;
        current = dummy; // Key Note
        while (current != null) {
            prefixSum = prefixSum + current.val;
            current.next = map.get(prefixSum).next;
            current = current.next;
        }
        return dummy.next;
    }
}
