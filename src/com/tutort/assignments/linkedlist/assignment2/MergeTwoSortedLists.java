package com.tutort.assignments.linkedlist.assignment2;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.linkedlist.ListNode;
import com.tutort.classroom.linkedlist.ListNodeInputData;

public class MergeTwoSortedLists {

    public static void main(String[] args) {
        int[] arr1 = {1,2,4};
        ListNodeInputData listNode1 = new ListNodeInputData();
        for (int value: arr1) {
            listNode1.push(value);
        }
        int[] arr2 = {1,3,4};
        ListNodeInputData listNode2 = new ListNodeInputData();
        for (int value: arr2) {
            listNode2.push(value);
        }
        System.out.println(optimisedAppraoch(listNode1.head, listNode2.head));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.MERGE_TWO_SORTED_LISTS);
    }

    //Time complexity: O(n1 + n2)
    //Space complexity: O(n1 + n2)
    //TODO: BruteForceAppraoch will be to use newNode everytime to create new LinkedList

    //Time complexity: O(n1 + n2)
    //Space complexity: 1
    public static ListNode optimisedAppraoch(ListNode list1, ListNode list2) {
        //1. Null Check
        if(list1 == null) {
            return list2;
        }
        if (list2 == null) {
            return list1;
        }
        //2. Assign list1 to small head value
        if (list1.val > list2.val) {
            ListNode temp = list1;
            list1 = list2;
            list2 = temp;
        }
        //3. result head;
        ListNode resultHead = list1;
        while (list1 != null && list2 != null) {
            ListNode temp = null;
            while (list1 != null && list1.val <= list2.val) {
                temp = list1;
                list1 = list1.next;
            }
            temp.next = list2;
            //4. Swapping
            ListNode swap = list1;
            list1 = list2;
            list2 = swap;
        }
        return resultHead;
    }
}
