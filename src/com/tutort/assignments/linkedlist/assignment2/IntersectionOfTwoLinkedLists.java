package com.tutort.assignments.linkedlist.assignment2;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.linkedlist.ListNode;
import com.tutort.classroom.linkedlist.ListNodeInputData;

import java.util.HashMap;
import java.util.Map;

public class IntersectionOfTwoLinkedLists {

    public static void main(String[] args) {
        int[] arr1 = {4,1,8,4,5};
        ListNodeInputData listNode1 = new ListNodeInputData();
        for (int value: arr1) {
            listNode1.push(value);
        }
        int[] arr2 = {5,6,1};
        ListNodeInputData listNode2 = new ListNodeInputData();
        for (int value: arr2) {
            listNode2.push(value);
        }
        listNode2.tail.next = listNode1.head.next.next;
        System.out.println(optimisedApproach(listNode1.head, listNode2.head));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.INTERSECTION_OF_TWO_LINKED_LISTS);
    }

    //Time Complexity: O(n + m)
    //Space Complexity: n
    public static ListNode usingHashMap(ListNode headA, ListNode headB) {
        Map<ListNode, ListNode> map = new HashMap<>();
        while(headA != null) {
            map.put(headA,headA);
            headA = headA.next;
        }

        while(headB != null) {
            ListNode curr= map.get(headB);
            if(curr != null) {
                return curr;
            }
            headB = headB.next;
        }
        return null;
    }

    //Time Complexity: O(n + m)
    //Space Complexity: 1
    public static ListNode optimisedApproach(ListNode headA, ListNode headB) {
        ListNode d1 = headA;
        ListNode d2 = headB;

        while (true) {
            if(d1 == d2) {
                return d1;
            } else if(d1 == null && d2 != null) {
                d1 = headB;
            } else if(d2 == null && d1 != null) {
                d2 = headA;
            } else {
                d1 = d1.next;
                d2 = d2.next;
            }
        }
    }
}
