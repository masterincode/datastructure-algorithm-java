package com.tutort.assignments.linkedlist.assignment2;

import com.tutort.LeetCodeURLConstant;
import com.tutort.classroom.linkedlist.ListNode;
import com.tutort.classroom.linkedlist.ListNodeInputData;

public class LinkedListCycle {
    public static void main(String[] args) {
        int[] arr1 = {3,2,0,-4};
        ListNodeInputData listNode1 = new ListNodeInputData();
        for (int value: arr1) {
            listNode1.push(value);
        }
        listNode1.tail.next = listNode1.head.next;
        System.out.println(hasCycle(listNode1.head)); //true
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.LINKED_LIST_CYCLE);
    }

    public static boolean hasCycle(ListNode head) {

        // 1. Starting point will be same for both pointers
        ListNode fast = head;
        ListNode slow = head;

        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;

            if (slow == fast)
                return true;
        }
        return false;
    }
}
