package com.tutort.assignments.warmup.assignment2;

import com.tutort.LeetCodeURLConstant;

public class RichestCustomerWealth {
    public static void main(String[] args) {
        int[][] arr = {{1,2,3},{3,2,1}};
        System.out.println(maximumWealth(arr));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.RICHEST_CUSTOMER_WEALTH);
    }

    public static int maximumWealth(int[][] accounts) {

        int max = Integer.MIN_VALUE;
        int sum = 0;
        //             i < 2
        for(int i = 0; i < accounts.length; i++) {
            sum = 0;
            //             J < 3
            for(int j = 0 ; j < accounts[i].length; j++) {
                //    = 3 + 3
                sum = sum + accounts[i][j]; // sum = 6
            }
            if(sum > max) {
                max = sum; // max = 6
            }
        }
        return max;
    }
}
