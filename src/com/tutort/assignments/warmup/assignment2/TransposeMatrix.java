package com.tutort.assignments.warmup.assignment2;

import com.tutort.LeetCodeURLConstant;

import java.util.ArrayList;
import java.util.List;

public class TransposeMatrix {

    public static void main(String[] args) {
        List<Integer[][]> testCases = new ArrayList<>();
        testCases.add(new Integer[][]{{1,2,3},{4,5,6},{7,8,9}});
//        testCases.add(new Integer[][]{{1,2,3},{4,5,6}});
        int test = 1;

        for (Integer[][] testCase : testCases) {
            System.out.println("Test Cases: " + test);
            for(Integer[] row: optimizedApproach(testCase)) {
                System.out.print("[");
                for (Integer column: row) {
                    System.out.print(""+column+",");
                }
                System.out.print("]");
            }
            System.out.println();
            test++;
            System.out.println();
        }
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.TRANSPOSE_MATRIX);
    }
    //   i = 0       1       2
    //  [[1,2,3],[4,5,6],[7,8,9]]
    //    j

    // Time Complexity: O(n^2)
    // Space Complexity: n
    public static Integer[][] bruteForceApproach(Integer[][] matrix) {
        int rowLength = matrix.length;
        int colLength = matrix[0].length;
        Integer[][] result = new Integer[colLength][rowLength]; //Extra Space Taken
        for(int i = 0; i < matrix.length; i++) {
            for(int j = 0; j < matrix[i].length; j++) {
                result[j][i] = matrix[i][j];
            }
        }
        return result;
    }

    // Time Complexity: O(n^2)
    // Space Complexity: 1
    public static Integer[][] optimizedApproach(Integer[][] matrix) {
        for(int i = 0; i < matrix.length; i++) {
            for(int j = i; j < matrix[0].length; j++) {
                int temp = matrix[i][j];
                matrix[i][j] = matrix[j][i];
                matrix[j][i] = temp;
            }
        }
        return matrix;
    }
}
