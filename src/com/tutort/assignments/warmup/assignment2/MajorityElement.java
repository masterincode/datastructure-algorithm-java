package com.tutort.assignments.warmup.assignment2;

import com.tutort.LeetCodeURLConstant;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class MajorityElement {

    //TODO : Majority element is always greater than (arr.length/2)
    public static void main(String[] args) {
        int[] arr = {2,2,1,1,1,2,2};
        System.out.println(optimisedApproach1(arr));
        System.out.println(optimizedApproach2(arr));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.MAJORITY_ELEMENT_IN_ARRAY);
    }

    //Time complexity : O(n^2)
    //Space complexity : O(1)
    //TODO: Nested Loops and Count
    private static int bruteForceApproach1(int[] arr) {
        int count = 0;
        int majorityElement = 0;
        for(int i = 0; i < arr.length; i++) {
            majorityElement = arr[i];
            count = 1;
            for(int j = i+1; j < arr.length; j++) {
                if(majorityElement == arr[j]) {
                    count++;
                }
            }
            if(count > (arr.length/2)) {
                return majorityElement;
            }
        }
        return majorityElement;
    }

    //Time complexity : O(n log n)
    //Space complexity : O(1)
    //TODO: Sorting and count
    private static int bruteForceApproach2(int[] arr) {
        Arrays.sort(arr);
        int majorityElement = arr[0]; //majorityElement = 1
        int count = 1;
        for(int i = 1; i < arr.length; i++) {
            // arr[i] = 2
            if(majorityElement == arr[i]) {
                count++; // count = 2
            } else {
                majorityElement = arr[i];
                count = 1;
            }
            if(count > (arr.length/2)) {
                return majorityElement;
            }
        }
        return majorityElement;
    }

    //Time complexity : O(n)
    //Space complexity : O(n)
    //TODO: Using Hashing Time complexity is improved
    public static int optimisedApproach1(int[] nums) {

        if (nums.length == 0) return 0;

        Map<Integer, Integer> map = new HashMap<>();
        Integer count;
        //Duplicate count in array
        for (int val : nums) {
            count = map.get(val);
            if (count == null) {
                map.put(val, 1);
            } else {
                count++;
                map.put(val, count);
            }
        }
        AtomicInteger maxkey = new AtomicInteger();
        map.forEach((key, value) -> {
            if(value > nums.length/2) {
                maxkey.set(key);
            }
        });
        return maxkey.get();
    }

    //Most Optimised Approached
    //Time complexity : O(n)
    //Space complexity : O(1)
    //TODO: Boyer-Moore Voting Algorithm
    public static int optimizedApproach2(int[] nums) {
        int candidate = nums[0];
        int voteCount = 1;

        for(int i = 1; i < nums.length; i++) {
           if(nums[i] == candidate) {
               voteCount++;
           } else {
               voteCount--;
           }
           if(voteCount == 0) {
               candidate = nums[i];
               voteCount++;
           }
        }
        return candidate;
    }

}
