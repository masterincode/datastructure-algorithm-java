package com.tutort.assignments.warmup.assignment2;

import com.tutort.LeetCodeURLConstant;

import java.util.*;

public class MinimumAbsoluteDifferenceInArray {
    public static void main(String[] args) {
        List<Integer[]> testCases = new ArrayList<>();
        testCases.add(new Integer[]{4,2,1,3});
        testCases.add(new Integer[]{1,3,6,10,15});
        testCases.add(new Integer[]{3,8,-10,23,19,-4,-14,27});
        int test = 1;

        for (Integer[] testCase : testCases) {
            System.out.println("Test Cases: " + test);
            System.out.println(optimizedApproach_2(testCase));
            test++;
            System.out.println();
        }
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.MINIMUM_ABSOLUTE_DIFFERENCE);
    }

    public static List<List<Integer>> optimizedApproach_2(Integer[] arr) {

        List<List<Integer>> resultList = new ArrayList<>();
        List<Integer> pair;
        Arrays.sort(arr);
        int i = 0;
        int diff = 0;
        int minDIff = Integer.MAX_VALUE;
        while(i < arr.length - 2) {
            diff = Math.abs(arr[i] - arr[i+1]);
            if(diff < minDIff) {
                minDIff = diff;
            }
            i++;
        }
        int m  = 0;
        int n = m + 1;
        int length = arr.length;
        // 1,2,3,4
        //       m n
        while(m < length - 1 && n < length) {
            diff = Math.abs(arr[m] - arr[n]);
            if(diff == minDIff) {
                pair = new ArrayList<>();
                pair.add(arr[m]);
                pair.add(arr[n]);
                resultList.add(pair);
            }
            m++;
            n++;
        }
        return resultList;
    }

    public static List<List<Integer>> bruteForceApproach_1(Integer[] arr) {

        List<List<Integer>> resultList = new ArrayList<>();
        List<Integer> pair;
        Map<Integer,List<Integer>> map = new TreeMap<>();
        int diff = 0;
        int minDiff = Integer.MAX_VALUE;

        int m = 0;
        int n = 1;

        //Find the min diff value
        //  [3,8,-10,23,19,-4,-14,27]
        //                         m  n
        while(m < arr.length && n < arr.length) {
            diff = arr[m] - arr[n]; // diff = 23 - (19) => 4
            if(diff < 0) {
                diff = diff * -1; // diff = 5
            }
            if(diff < minDiff) {
                minDiff = diff; //minDiff = 4
            }
            n++;
            if(n == arr.length) {
                m ++;
                n = m + 1;
            }
        }
        //  [3,8,-10,23,19,-4,-14,27]
        //           i  j
        int i = 0;
        int j = i +1;

        // Approach 1 : Using While loop
        while(i < arr.length && j < arr.length) {
            if(minDiff == (arr[i]- arr[j]) || -minDiff == (arr[i]- arr[j])) {
                pair = new ArrayList<>();
                if(arr[i] < arr[j]) { // -10 < -14
                    pair.add(arr[i]); //pair [1,3]
                    pair.add(arr[j]);
                } else {
                    pair.add(arr[j]); //pair [19, 23]
                    pair.add(arr[i]);
                }
                map.put(arr[i] + arr[j], pair); // -24 -> {-14,-10}, 42 -> {19, 23}, 50 -> {23,27}
            }
            j++;
            if(j == arr.length) {
                i++;
                j = i + 1;
            }
        }

//        Approach 2 : Using for loop
//        for(int i = 0; i < arr.length; i++) {
//            for(int j = i+1; j < arr.length; j++ ) {
//                //  4  == 23 - (19)                    -4  == 3 - 8
//                if(minDiff == (arr[i]- arr[j]) || -minDiff == (arr[i]- arr[j])) {
//                    pair = new ArrayList<>();
//                    if(arr[i] < arr[j]) { // -10 < -14
//                        pair.add(arr[i]); //pair [1,3]
//                        pair.add(arr[j]);
//                    } else {
//                        pair.add(arr[j]); //pair [19, 23]
//                        pair.add(arr[i]);
//                    }
//                    map.put(arr[i] + arr[j], pair); // -24 -> {-14,-10}, 42 -> {19, 23}, 50 -> {23,27}
//                }
//            }
//        }

        map.forEach((key, value) -> {
            resultList.add(value);
        });
        return resultList;

    }
}
