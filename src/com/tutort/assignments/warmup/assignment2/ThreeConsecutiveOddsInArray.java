package com.tutort.assignments.warmup.assignment2;

import com.tutort.LeetCodeURLConstant;

import java.util.ArrayList;
import java.util.List;

public class ThreeConsecutiveOddsInArray {

    public static void main(String[] args) {
        List<Integer[]> testCases = new ArrayList<>();
        testCases.add(new Integer[]{4,2,5,1,3});
        testCases.add(new Integer[]{1,3,6,10,15});
        testCases.add(new Integer[]{1,2,34,3,4,5,7,23,12});
        int test = 1;

        for (Integer[] testCase : testCases) {
            System.out.println("Test Cases: " + test);
            System.out.println(threeConsecutiveOdds(testCase));
            test++;
            System.out.println();
        }
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.THREE_CONSECUTIVE_ODDS);
    }

    public static boolean threeConsecutiveOdds(Integer[] arr) {
        int odds = 0;
        //1,2,34,3,4,5,7,23,12
        for(int i = 0; i < arr.length ; i++) {

            if(arr[i] % 2 != 0) {
                odds++;
                if(odds == 3) {
                    return true;
                }
            } else {
                odds = 0;
            }
        }
        return false;
    }
}
