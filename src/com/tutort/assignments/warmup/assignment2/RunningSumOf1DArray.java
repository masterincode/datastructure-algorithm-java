package com.tutort.assignments.warmup.assignment2;

import com.tutort.LeetCodeURLConstant;

import java.util.ArrayList;
import java.util.List;

public class RunningSumOf1DArray {

    public static void main(String[] args) {
        List<Integer[]> testCases = new ArrayList<>();
        testCases.add(new Integer[]{1,2,3,4});
        testCases.add(new Integer[]{1,1,1,1,1});
        testCases.add(new Integer[]{5});
        testCases.add(new Integer[]{});
        testCases.add(new Integer[]{3,1,2,10,1});
        int test = 1;

        for (Integer[] testCase : testCases) {
            System.out.println("Test Cases: " + test);
            for (int value: runningSum(testCase))
                System.out.print("["+value+"]");
            test++;
            System.out.println();
        }
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.RUNNING_SUM_OF_1D_ARRAY);
    }

    public static int[] runningSum(Integer[] nums) {

        int length = nums.length;
        int[] result = new int[length];
        int sum = 0;
        for(int i = 0; i < length ; i++) {
            //  sum = 6 + 4;
            sum = sum + nums[i];// sum = 6
            result[i] = sum; // result[1,3,6,10]
        }
        return result;
    }
}
