package com.tutort.assignments.warmup.assignment2;

import com.tutort.LeetCodeURLConstant;

import java.util.HashSet;
import java.util.Set;

public class JewelsAndStones {
    public static void main(String[] args) {
        String jewels1 = "aA", stones1 = "aAAbbbb";
        String jewels2 = "z", stones2 = "ZZ";
        System.out.println(optimisedApproach(jewels1, stones1));
        System.out.println();
        System.out.println(optimisedApproach(jewels2, stones2));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.JEWELS_AND_STONES);
    }

    // Time Complexity : O(n^2)
    // Sapce Complexity : O(n) + O(m)
    public static int bruteForceApproach(String jewels, String stones) {

        char[] jewelsChar = jewels.toCharArray();
        char [] stonesChar = stones.toCharArray();
        int count = 0;
        for(int i = 0; i < jewelsChar.length; i++) {
            for(int j = 0; j < stonesChar.length; j++) {
                if(jewelsChar[i] == stonesChar[j]) {
                    count++;
                }
            }
        }
        return count;
    }

    // Time Complexity : O(n) + O(m)  => O(n)
    // Sapce Complexity : O(n) + O(m)
    public static int optimisedApproach(String jewels, String stones){
        char [] stonesChar = stones.toCharArray();
        Set<Character> set = new HashSet<>();
        for(Character ch : jewels.toCharArray()) {
            set.add(ch);
        }
        int count = 0;
        for(int i = 0; i < stonesChar.length; i++) {
            if(set.contains(stonesChar[i])) {
                count++;
            }
        }
        return count;
    }
}
