package com.tutort.assignments.warmup.assignment2;

import com.tutort.LeetCodeURLConstant;

public class MoveZeroesToEnd {

    public static void main(String[] args) {
        int[] arr1 = {0,1,0,3,12};
        MoveZeroesToEnd moveZeroesToEnd = new MoveZeroesToEnd();
        System.out.println("=====================BruteForceApproach======================");
        for(int v: moveZeroesToEnd.moveZeroesBruteForceApproach(arr1))
            System.out.print(v+ " ");
        System.out.println();
        int[] arr2 = {0,1,0,3,12};
        System.out.println("=====================OptimisedApproach======================");
        for(int v: moveZeroesToEnd.moveZeroesOptimisedApproach(arr2))
            System.out.print(v+ " ");
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.MOVE_ZEROES);
    }

    //Time Complexity: O(n)
    //Space Complexity: O(n)
    public int[]  moveZeroesBruteForceApproach(int[] nums) {
        if(nums.length == 0)
            return nums;
        int newIndex = 0;
        int[] arr = new int[nums.length];

        for(int i=0; i < nums.length ; i++) {
            if(nums[i] != 0) {
                arr[newIndex] = nums[i];
                newIndex++;
            }
        }
        for(int j=newIndex; j < arr.length; j++) {
            arr[j] = 0;
        }
        for(int k=0; k < nums.length; k++ ) {
            nums[k] = arr[k];
        }
        return nums;
    }

    //OptimisedApproach
    //Time Complexity: O(n)
    //Space Complexity: O(1)  Note: Only space complexity is improved
    public int[] moveZeroesOptimisedApproach(int[] nums) {
        if(nums.length == 0)
            return nums;
        int newIndex = 0;

        for(int i=0; i < nums.length ; i++) {
            if(nums[i] != 0) {
                nums[newIndex] = nums[i];
                newIndex++;
            }
        }
        for(int j=newIndex; j < nums.length; j++) {
            nums[j] = 0;
        }
        return nums;
    }

    //Time Complexity: O(n)
    //Space Complexity: O(1)  Note: Only space complexity is improved
    public void moveZeroesOptimised_Approach_2(int[] nums) {

        int i = 0;
        int j = 0;
        int temp;
        while(j < nums.length) {

            if(nums[j] == 0) {
                j++;
            } else  {
                temp = nums[i];
                nums[i] = nums[j];
                nums[j] = temp;
                i++;
                j++;
            }
        }
    }
}


