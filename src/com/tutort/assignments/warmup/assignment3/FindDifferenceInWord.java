package com.tutort.assignments.warmup.assignment3;

import com.tutort.LeetCodeURLConstant;

import java.util.List;
import java.util.stream.Collectors;

public class FindDifferenceInWord {

    public static void main(String[] args) {
        System.out.println(findTheDifference_Approach_1("abcd","abcde"));
        System.out.println(findTheDifference_Approach_1("a","aa"));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.FIND_DIFF_IN_WORD);
    }

    public static char findTheDifference_Approach_2(String s, String t) {
        List<Character> initialList = s.chars().mapToObj(e -> (char)e).collect(Collectors.toList());
        List<Character> finalList = t.chars().mapToObj(e -> (char)e).collect(Collectors.toList());
        initialList.forEach( c -> finalList.remove(c));
        return finalList.get(0);
    }

    public static char findTheDifference_Approach_1(String s, String t) {
        int initialSum = 0;
        int finalSum = 0;
        for (char val1: s.toCharArray()) {
            initialSum = initialSum + val1;
        }
        for (char val2: t.toCharArray()) {
            finalSum = finalSum + val2;
        }
        int diff = finalSum - initialSum;
        char letter = (char) diff;
        return letter;

    }

}
