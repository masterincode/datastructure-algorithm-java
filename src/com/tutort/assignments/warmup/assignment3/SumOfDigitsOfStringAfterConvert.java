package com.tutort.assignments.warmup.assignment3;

import com.tutort.LeetCodeURLConstant;

public class SumOfDigitsOfStringAfterConvert {
    public static void main(String[] args) {
        System.out.println(sumOfDigitsOfStringAfterConvert("zbax",2));
        System.out.println(sumOfDigitsOfStringAfterConvert("iiii",1));
        System.out.println(sumOfDigitsOfStringAfterConvert("leetcode",2));
        System.out.println(sumOfDigitsOfStringAfterConvert("zbax",2));
        System.out.println(sumOfDigitsOfStringAfterConvert("fleyctuuajsr",5)); //TODO: Use Case of Length 20
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.SUM_OF_DIGITS_OF_STRING_AFTER_CONVERT);

    }

    public static int sumOfDigitsOfStringAfterConvert(String s, int k) {

        int value = 0;
        //TODO: To handle one use case where realValue length will be 20
        //for that only we are using String Builder
        StringBuilder realValue = new StringBuilder();

        for(int i = 0; i < s.length(); i++ ) {
            value = (s.charAt(i) - 'a') + 1;
            realValue.append(value);
        }
        String result = realValue.toString();
        int counter = 0;
        int sum = 0;
        while (result.length() > 0){
            for(int i = 0; i < result.length(); i++) {
                sum = sum + Integer.valueOf(result.charAt(i)-'0');
            }
            counter++;
            if (counter == k) {
                return sum;
            }
            result = String.valueOf(sum);
            sum = 0;
        }
        return sum;
    }
}
