package com.tutort.assignments.warmup.assignment3;

import com.tutort.LeetCodeURLConstant;

public class UglyNumber {
    public static void main(String[] args) {
        System.out.println(isUgly(6));
        System.out.println(isUgly(1));
        System.out.println(isUgly(14));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.UGLY_NUMBER);
    }

    public static boolean isUgly(int n) {
        int[] prime = {2,3,5};
        int i = 0;
        if(n == 1) {
            return true;
        }
        if(n == 0) {
            return false;
        }
        /*  n = 6;          n = 14
        *   6  | 3 | 3 |    14 | 7 | 7 | 7
        *  --- |---|---|   ----|---|---|---
        *   2  | 2 | 3 |    2  | 2 | 3 | 5
        *   return true;     return false;
        * */
        while(i < prime.length) {
            if(n % prime[i] == 0) {
                n = n / prime[i];
                i = 0;
                if(n == 1) {
                    return true;
                }
            } else {
                i++;
            }
        }
        return false;
    }
}
