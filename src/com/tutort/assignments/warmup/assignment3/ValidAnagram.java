package com.tutort.assignments.warmup.assignment3;

import com.tutort.LeetCodeURLConstant;

import java.util.Arrays;

public class ValidAnagram {

    public static void main(String[] args) {
        System.out.println(isAnagram_Appraoch_2("anagram", "nagaram"));
        System.out.println(isAnagram_Appraoch_2("rat", "car"));
        System.out.println(isAnagram("anagram", "nagaram"));
        System.out.println(isAnagram("rat", "car"));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.VALID_ANAGRAM);
    }

    //TODO: Verification using For Loop
    // Time Complexity: O (n log n)
    // Space Complexity: 2n
    public static boolean isAnagram_Appraoch_1(String s, String t) {

        char[] firstChar = s.toCharArray();
        char[] secondChar = t.toCharArray();
        if (firstChar.length != secondChar.length) {
            return false;
        }
        Arrays.sort(firstChar);
        Arrays.sort(secondChar);
        boolean flag = false;
        for (int i = 0; i < firstChar.length; i++) {
            if (firstChar[i] == secondChar[i]) {
                flag = true;
            } else {
                flag = false;
                return flag;
            }
        }
        return flag;
    }

    //TODO: Verification using String
    // Time Complexity: O (n log n)
    // Space Complexity: 2n
    public static boolean isAnagram_Appraoch_2(String s, String t) {

        char[] firstChar = s.toCharArray();
        char[] secondChar = t.toCharArray();
        if (firstChar.length != secondChar.length) {
            return false;
        }
        Arrays.sort(firstChar);
        Arrays.sort(secondChar);

        String first = new String(firstChar);
        String second = new String(secondChar);
        if (first.equalsIgnoreCase(second))
            return true;
        else
            return false;

    }

    // Time Complexity: O (n)
    // Space Complexity: 1 [due to fix size of 26]
    public static boolean isAnagram(String s, String t) {

        int[] hash = new int[26];

        for (int i = 0; i < s.length(); i++) {
            int index = getIndex(s.charAt(i));
            hash[index]++;
        }

        for (int i = 0; i < t.length(); i++) {
            int index = getIndex(t.charAt(i));
            hash[index]--;
        }

        for (int i = 0; i < hash.length; i++) {
            if (hash[i] != 0) {
                return false;
            }
        }
        return true;

    }

    private static int getIndex(char ch) {
        return ch - 'a';
    }
}
