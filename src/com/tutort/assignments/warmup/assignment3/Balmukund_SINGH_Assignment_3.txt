1. Happy Number (https://leetcode.com/problems/happy-number/)
Soltions:
public static boolean isHappy(int n) {
        int sum = 0;
        int rem = 0;
        boolean isHappy = false;
        while(n > 0) {
            rem = n % 10;
            sum = sum + rem * rem;
            n  = n / 10;

            if(sum == 1 && n == 0) {
                isHappy = true;
                return isHappy;
            } else if(n == 0 && sum > 6) {
                n = sum;
                sum = 0;
            } else if(n == 0 && sum < 7) {
                isHappy = false;
                return isHappy;
            }
        }
        return isHappy;
}

2. Power of Two (https://leetcode.com/problems/power-of-two/)
Solutions:
public static boolean isPowerOfTwo(int n) {
        if(n == 1)
            return true;
        long value = 1;
        while(value <= n) {
            value = value * 2;
            if(value == n) {
                return true;
            }
        }
        return false;
}

3. Valid Anagram (https://leetcode.com/problems/valid-anagram/)
Solutions:
public static boolean isAnagram_Appraoch_1(String s, String t) {

        char[] firstChar = s.toCharArray();
        char[] secondChar = t.toCharArray();
        if(firstChar.length != secondChar.length) {
            return false;
        }
        Arrays.sort(firstChar);
        Arrays.sort(secondChar);
        boolean flag = false;
        for(int i = 0; i < firstChar.length ; i++) {
            if(firstChar[i] == secondChar[i]) {
                flag = true;
            } else {
                flag = false;
                return flag;
            }
        }
        return flag;
}

4. Ugly Number (https://leetcode.com/problems/ugly-number/)
Solutions:
public boolean isUgly(int n) {
        int[] prime = {2,3,5};
        int i = 0;
        if(n == 1) {
            return true;
        }
        if(n == 0) {
            return false;
        }
       while(i < prime.length) {
            if(n % prime[i] == 0) {
                n = n / prime[i];
                i = 0;
                if(n == 1) {
                    return true;
                }
            } else {
                i++;
            }
        }
        return false;
    }

5. Move Zeroes [https://leetcode.com/problems/move-zeroes/]
class Solution {
    public void moveZeroes(int[] nums) {
      if(nums.length == 0)
            return;
        int newIndex = 0;
        for(int i=0; i < nums.length ; i++) {
            if(nums[i] != 0) {
                nums[newIndex] = nums[i];
                newIndex++;
            }
        }
        for(int j=newIndex; j < nums.length; j++) {
            nums[j] = 0;
        }
        return;
    }
}

6. Reverse Vowels of a String (https://leetcode.com/problems/reverse-vowels-of-a-string/)
Solutions:
class Solution {
   public String reverseVowels(String s) {
      char[] arr = new char[0];
        if(s != null) {
            arr = s.toCharArray();
        }

        if (arr.length == 0) {
            return s;
        }
        if (arr.length == 1) {
            return s;
        }
      char temp;
      int i = 0;
      int j = arr.length - 1;

      while (i < j) {
          //arr[i] = a
          //arr[j] = e
          if (isVowels(arr[i]) && isVowels(arr[j])) {
              temp = arr[i];   //temp = a
              arr[i] = arr[j]; //arr[i] = f
              arr[j] = temp;   //arr[j] = a
              i++;
              j--;
          }
          if (!isVowels(arr[i])) {
              i++;
          }
          if (!isVowels(arr[j])) {
              j--;
          }
      }
      String newString = new String(arr);
      return newString;
    }

     private static  boolean isVowels(char a) {
        return (a=='a' || a=='e' ||a=='i' ||a=='o' ||a=='u' || a=='A' || a=='E' ||a=='I' ||a=='O' ||a=='U');
    }
}

7. Third Maximum Number (https://leetcode.com/problems/third-maximum-number/)
Solutions:
public int thirdMax(int[] arr) {

        if(arr.length == 0) {
            return 0;
        }
        Integer firstMax = null;
        Integer secondMax = null;
        Integer thirdMax = null;
        for(Integer value: arr) {
            if(value.equals(firstMax) || value.equals(secondMax) || value.equals(thirdMax))
                continue;

            if(firstMax == null || value > firstMax) {
                thirdMax = secondMax;
                secondMax = firstMax;
                firstMax = value;
            } else if(secondMax == null || value > secondMax) {
                thirdMax = secondMax;
                secondMax = value;
            } else if(thirdMax == null || value > thirdMax) {
                thirdMax = value;
            }
        }
        return thirdMax == null ? firstMax : thirdMax;
}


8. Find the Difference (https://leetcode.com/problems/find-the-difference/)
Solutions:
 public char findTheDifference(String s, String t) {

        int initialSum = 0;
        int finalSum = 0;
        for (char val1: s.toCharArray()) {
            initialSum = initialSum + val1;
        }
        for (char val2: t.toCharArray()) {
            finalSum = finalSum + val2;
        }
        int diff = finalSum - initialSum;
        char letter = (char) diff;
        return letter;
 }


9. Add Digits (https://leetcode.com/problems/add-digits/)
Solutions:
 public static int addDigits_Approach_2(int num) {
        if(num == 0 || num == 1) {
            return num;
        }
        int value = 1 + (num - 1) % 9;
        return value;
}

10. Sum of Digits of String After Convert (https://leetcode.com/problems/sum-of-digits-of-string-after-convert/)
Solutions:
public int getLucky(String s, int k) {
      int value = 0;
        StringBuilder realValue = new StringBuilder();

        for(int i = 0; i < s.length(); i++ ) {
            value = (s.charAt(i) - 'a') + 1;
            realValue.append(value);
        }
        int counter = 0;
        int sum = 0;
        while (realValue.length() > 0){
            for(int i = 0; i < realValue.length(); i++) {
                sum = sum + Integer.valueOf(realValue.charAt(i)-'0');
            }
            counter++;
            if (counter == k) {
                return sum;
            }
            realValue = realValue.replace(0, realValue.length(), String.valueOf(sum));
            sum = 0;
        }
        return sum;
 }
