package com.tutort.assignments.warmup.assignment3;

import com.tutort.LeetCodeURLConstant;

public class AddDigits {
    public static void main(String[] args) {
        System.out.println(addDigits_Approach_2(38));
        System.out.println(addDigits_Approach_2(0));
        System.out.println(addDigits_Approach_2(100));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.ADD_DIGITS);
    }

    public static int addDigits_Approach_2(int num) {
        if(num == 0 || num == 1) {
            return num;
        }
        int value = 1 + (num - 1) % 9;
        return value;
    }

    //TODO: Could you do it without any loop/recursion in O(1) runtime?
    public static int addDigits_Approach_1(int num) {
        int rem = 0;
        int sum = 0;
        while(num > 0) {
            rem = num % 10;
            sum = sum + rem;
            num = num / 10;
            if(num == 0 && sum > 9) {
                num = sum;
                sum = 0;
            } else if(num == 0 && sum < 10){
                return sum;
            }
        }
        return sum;
    }
}
