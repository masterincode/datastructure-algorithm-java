package com.tutort.assignments.warmup.assignment3;

import com.tutort.LeetCodeURLConstant;

import java.util.ArrayList;
import java.util.List;

public class ThirdMaximumNumber {

    public static void main(String[] args) {
        List<Integer[]> testCases = new ArrayList<>();
        testCases.add(new Integer[]{2,1,3,4,5,6});
        testCases.add(new Integer[]{12,11,11,4,5,6});
        testCases.add(new Integer[]{2,1});
        testCases.add(new Integer[]{1,1,2});
        testCases.add(new Integer[]{1,2,-2147483648});
        testCases.add(new Integer[]{-2147483648,1,1});
        testCases.add(new Integer[]{1,-2147483648,2});

        int test = 1;

        for (Integer[] testCase : testCases) {
            System.out.println("Test Cases: " + test);
            System.out.println(thirdMaximumNumber(testCase));
            System.out.println();
        }
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.FIND_THIRD_MAX_NUMBER);
    }

    public static int thirdMaximumNumber(Integer[] arr) {
        if(arr.length == 0) {
            return 0;
        }
        Integer firstMax = null;
        Integer secondMax = null;
        Integer thirdMax = null;
        for(Integer value: arr) {
            if(value.equals(firstMax) || value.equals(secondMax) || value.equals(thirdMax))
                continue;

            if(firstMax == null || value > firstMax) {
                thirdMax = secondMax;
                secondMax = firstMax;
                firstMax = value;
            } else if(secondMax == null || value > secondMax) {
                thirdMax = secondMax;
                secondMax = value;
            } else if(thirdMax == null || value > thirdMax) {
                thirdMax = value;
            }
        }
        return thirdMax == null ? firstMax : thirdMax;
    }
}
