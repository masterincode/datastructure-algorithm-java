package com.tutort.assignments.warmup.assignment3;

import com.tutort.LeetCodeURLConstant;

public class PowerOfTwo {
    public static void main(String[] args) {
        System.out.println(isPowerOfTwo(1073741825));
        System.out.println(isPowerOfTwo(16));
        System.out.println(isPowerOfTwo(3));
        System.out.println(isPowerOfTwo(1));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.POWER_OF_TWO);
    }

    //TODO:  Could you solve it without loops/recursion?
    public static boolean isPowerOfTwo(int n) {
        if(n == 1)
            return true;
        long value = 1;
        while(value <= n) {
            value = value * 2;
            if(value == n) {
                return true;
            }
        }
        return false;
    }
}
