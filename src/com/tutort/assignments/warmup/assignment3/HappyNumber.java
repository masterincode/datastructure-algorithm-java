package com.tutort.assignments.warmup.assignment3;

import com.tutort.LeetCodeURLConstant;

import java.util.HashSet;
import java.util.Set;

public class HappyNumber {

    public static void main(String[] args) {
        System.out.println(optimisedAppraoch(6));
        System.out.println(optimisedAppraoch(7));
        System.out.println(optimisedAppraoch(649));
        System.out.println(optimisedAppraoch(1111111));
        LeetCodeURLConstant.openLeetCodeUrl(LeetCodeURLConstant.HAPPY_NUMBERS);
    }

    // Time Complexity : O(n)
    // Space Complexity : O(n)
    public static boolean bruteForceAppraoch(int n) {
        Set<Integer> set = new HashSet<>();
        int sum = 0;
        int rem;
        while(n > 0) {
            rem = n % 10;
            sum = sum + rem * rem;
            n = n / 10;
            if(n == 0) {
                if(sum == 1) {
                    return true;
                } else if(set.contains(sum)) {
                    return false;
                } else {
                    set.add(sum);
                    n = sum;
                    sum = 0;
                }
            }
        }
        return false;
    }

    // Time Complexity : O(n)
    // Space Complexity : O(1)
    public static boolean optimisedAppraoch(int n) {
        int sum = 0;
        int rem;
        boolean isHappy = false;
        while(n > 0) {
            rem = n % 10;
            sum = sum + rem * rem;
            n  = n / 10;

            //TODO: From [1 - 9] which is equivalent to sum < 10, only 1 and 7 are happy numbers
            if(n == 0) {
                if(sum == 1 || sum == 7) {
                    isHappy = true;
                    return isHappy;
                } else if(sum != 1 && sum != 7 && sum < 10) {
                    isHappy = false;
                    return isHappy;
                } else {
                    n = sum;
                    sum = 0;
                }
            }

        }
        return isHappy;
    }


}
