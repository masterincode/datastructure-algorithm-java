package com.tutort.assignments.warmup.assignment1;

public class FindNumberOfElementLessOrEqualToX {

    public static void main(String[] args) {
       int A[] = {1, 2, 4, 5, 8, 10};
       int X = 9;
       int B[] = {1, 2, 2, 2, 5, 7, 9};
       int Y = 2;
        System.out.println(findNumberOfElementLessOrEqualToX(A,X));
        System.out.println(findNumberOfElementLessOrEqualToX(B,Y));
    }

    public static int findNumberOfElementLessOrEqualToX(int[] arr, int x) {
        if(arr.length == 0) {
            return 0;
        }
        int count = 0;
        int i = 0;
        while(i < arr.length) {
            if(arr[i] <= x) {
                count++;
            }
            i++;
        }
        return count;
    }
}
