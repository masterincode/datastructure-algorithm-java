package com.tutort.assignments.warmup.assignment1;

public class PerfectOrNotPerfectArray {

    public static void main(String[] args) {
        int Arr[] = {1, 2, 3, 2, 1};
        int Brr[] = {1, 2, 3, 4, 5};
        perfectOrNotPerfectArray(Arr);
        perfectOrNotPerfectArray(Brr);
    }

    public static void perfectOrNotPerfectArray(int[] arr) {
        if(arr.length == 0) {
            return;
        }
        int j = arr.length -1;
        int i = 0;
        boolean isPerfect = false;
        int[] result = new int[arr.length];
        while (i < result.length && j >= 0) {
            result[i] = arr[j];
            i++;
            j--;
        }
        for (int m = 0; m < arr.length; m++) {
            if(arr[m] == result[m]) {
                isPerfect = true;
            } else {
                isPerfect = false;
            }
        }
        if(isPerfect) {
            System.out.println("PERFECT");
        } else {
            System.out.println("NOT PERFECT");
        }
    }

}
