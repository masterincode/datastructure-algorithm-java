Q1. Given an array of N integers. Your task is to print the sum of all of the integers.
Solution:
public static void printSumOfAllIntegers(int[] arr) {
        int sum = 0;
        for(int val: arr) {
            sum = sum + val;
        }
        System.out.println(sum);
}

Q2. Given an array A[] of N integers and an index Key. Your task is to print the element present at index key in the array.
Solution:
public static void printElementAtIndexKey(int[] arr, int key) {
        if (arr.length == 0) {
            return;
        }
        if(key < 0 || key >= arr.length) {
            return;
        }
        System.out.println(arr[key]);
}

Q3. Given an sorted array A of size N. Find number of elements which are less than or equal to given element X.
Solution:
public static int findNumberOfElementLessOrEqualToX(int[] arr, int x) {
        if(arr.length == 0) {
            return 0;
        }
        int count = 0;
        int i = 0;
        while(i < arr.length) {
            if(arr[i] <= x) {
                count++;
            }
            i++;
        }
        return count;
}

Q4. You are given an array A of size N. You need to print elements of A in alternate order (starting from index 0).
Solution:
public static void printAlternateNumber(int[] arr) {
        if(arr.length == 0) {
            return;
        }
        int i = 0;
        while(i < arr.length) {
            if(i % 2 == 0) {
                System.out.println(arr[i]);
            }
            i++;
        }
 }

Q5. Given an array Arr of N positive integers. Your task is to find the elements whose value is equal to that of its index value ( Consider 1-based indexing ).
Solution:
public static void findElementWhoseValueIsSameAsIndex(int[] arr) {
        if(arr.length == 0) {
            return;
        }
        int i = 0;
        while(i < arr.length) {
            if(arr[i] == i+1) {
                System.out.println(arr[i]);
            }
            i++;
        }
}

Q6. Given an array of size N and you have to tell whether the array is perfect or not. An array is said
to be perfect if it's reverse array matches the original array. If the array is perfect then print
"PERFECT" else print "NOT PERFECT".
Solution:
public static void perfectOrNotPerfectArray(int[] arr) {
        if(arr.length == 0) {
            return;
        }
        int j = arr.length -1;
        int i = 0;
        boolean isPerfect = false;
        int[] result = new int[arr.length];
        while (i < result.length && j >= 0) {
            result[i] = arr[j];
            i++;
            j--;
        }
        for (int m = 0; m < arr.length; m++) {
            if(arr[m] == result[m]) {
                isPerfect = true;
            } else {
                isPerfect = false;
            }
        }
        if(isPerfect) {
            System.out.println("PERFECT");
        } else {
            System.out.println("NOT PERFECT");
        }
 }

 Q7. Given an array of length N, at each step it is reduced by 1 element. In the first step the maximum
 element would be removed, while in the second step minimum element of the remaining array would
 be removed, in the third step again the maximum and so on. Continue this till the array contains only 1
 element. And find the final element remaining in the array.
Solution:
public static int removeMinAndMax(Integer[] arr) {
         int index = 0;
         Arrays.sort(arr);
         if(arr.length % 2 == 0) {
             index = (arr.length / 2 ) - 1;
         } else {
             index = arr.length / 2;
         }
         return arr[index];
 }

Q8. Given an array of N distinct elements, the task is to find all elements in array except two greatest
elements in sorted order.
Solution:
public static void printAllElementExceptTwoGreatestElement(int[] arr) {

        int max = Integer.MIN_VALUE;
        int secMax = Integer.MIN_VALUE;
        int resultIndex = 0;
        int[] result = new int[arr.length-2];

        for(int i = 0; i < arr.length; i++) {
            if(arr[i] > max) {
                max = arr[i];
            }
            if(arr[i] > secMax && arr[i] < max) {
                secMax = arr[i];
            }
        }
        for (int j = 0; j < arr.length; j++) {
            if(arr[j] != secMax && arr[j] != max) {
                result[resultIndex] = arr[j];
                resultIndex++;
            }
        }
        Arrays.sort(result);
        for (int val: result) {
            System.out.print(val+",");
        }
 }

Q9. Write a program to find the sum of the given series 1+2+3+ . . . . . .(N terms)
Solution:
public static int findSumSeriseOfGivenNumber(int number){
        return (number * (number + 1))/2;
}


Q10. Given a number N. Your task is to check whether it is fascinating or not.
Fascinating Number: When a number(should contain 3 digits or more) is multiplied by 2 and 3 ,and
when both these products are concatenated with the original number, then it results in all digits from 1
to 9 present exactly once.
Solution:
public static boolean facinatingNumber(int number) {

    String twice  = String.valueOf(number * 2);
    String thrice = String.valueOf(number * 3);
    String result = number + twice + thrice;
    char[] resultChar = result.toCharArray();
    Arrays.sort(resultChar);
    String newResult = new String(resultChar);
    String defaultResult = "123456789";
    if(defaultResult.equalsIgnoreCase(newResult)) {
        return true;
    } else
        return false;
}

Bonus Question
Given an array of even size N, task is to find minimum value that can be added to an element so that
array become balanced. An array is balanced if the sum of the left half of the array elements is equal
to the sum of right half.
Solution:
public static int balancedArray(int[] arr) {
    int middle = arr.length/2;
    int firstHalf = 0;
    int secondHalf = 0;

    for(int i = 0; i < arr.length; i++) {

        if(i < middle) {
            firstHalf = firstHalf + arr[i];
        } else {
            secondHalf = secondHalf + arr[i];
        }
    }
    if(firstHalf - secondHalf >= 0) {
        return firstHalf - secondHalf;
    } else {
        return secondHalf - firstHalf;
    }
}














