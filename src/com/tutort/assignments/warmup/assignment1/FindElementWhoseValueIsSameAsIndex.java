package com.tutort.assignments.warmup.assignment1;

public class FindElementWhoseValueIsSameAsIndex {

    public static void main(String[] args) {
        int A[] = {15, 2, 45, 12, 7};
        int B[] = {1};
        findElementWhoseValueIsSameAsIndex(A);
        findElementWhoseValueIsSameAsIndex(B);
    }

    public static void findElementWhoseValueIsSameAsIndex(int[] arr) {
        if(arr.length == 0) {
            return;
        }
        int i = 0;
        while(i < arr.length) {
            if(arr[i] == i+1) {
                System.out.println(arr[i]);
            }
            i++;
        }
    }
}
