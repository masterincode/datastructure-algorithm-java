package com.tutort.assignments.warmup.assignment1;

public class PrintElementAtIndexKey {

    public static void main(String[] args) {
        int[] testCase1 = {10, 20, 30, 40, 50};
        int key1 = 2;
        int[] testCase2 = {10, 20, 30, 40, 50, 60, 70};
        int key2 = 4;
        printElementAtIndexKey(testCase1,key1);
        printElementAtIndexKey(testCase2,key2);
    }

    public static void printElementAtIndexKey(int[] arr, int key) {
        if (arr.length == 0) {
            return;
        }
        if(key < 0 || key >= arr.length) {
            return;
        }
        System.out.println(arr[key]);
    }

}
