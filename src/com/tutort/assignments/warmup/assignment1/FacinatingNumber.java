package com.tutort.assignments.warmup.assignment1;

import java.util.Arrays;

public class FacinatingNumber {

    public static void main(String[] args) {
        System.out.println(facinatingNumber(192));
        System.out.println(facinatingNumber(853));
    }

    public static boolean facinatingNumber(int number) {

        String twice  = String.valueOf(number * 2);
        String thrice = String.valueOf(number * 3);
        String result = number + twice + thrice;
        char[] resultChar = result.toCharArray();
        Arrays.sort(resultChar);
        String newResult = new String(resultChar);
        String defaultResult = "123456789";
        if(defaultResult.equalsIgnoreCase(newResult)) {
            return true;
        } else
            return false;
    }
}
