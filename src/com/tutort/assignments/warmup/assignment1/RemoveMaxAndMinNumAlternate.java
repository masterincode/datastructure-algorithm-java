package com.tutort.assignments.warmup.assignment1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RemoveMaxAndMinNumAlternate {
    public static void main(String[] args) {

        //TODO: Test Cases for Brute Force Approach and Optimized
        List<Integer[]> testCases1 = new ArrayList<>();
        testCases1.add(new Integer[]{7,2,3,4,9,5});
        testCases1.add(new Integer[]{7,8,3,9,2,4,5});
        testCases1.add(new Integer[]{0,-4,19,1,8,-2,-3,5});
        testCases1.add(new Integer[]{5});
        int test1 = 1;

        for (Integer[] testCase : testCases1) {
            System.out.println("Test Cases for Brute Force Approach and Optimized: " + test1);
            System.out.print(bruteForceApproach(testCase));
            test1++;
            System.out.println();
        }

        //TODO: Test Cases for Approach 3
        List<List<Integer>> testCases = new ArrayList<>();
        testCases.add(Stream.of(7, 8, 3, 4, 2, 9, 5).collect(Collectors.toList()));
        testCases.add(Stream.of(8, 1, 2, 9, 4, 3, 7, 5).collect(Collectors.toList()));
        testCases.add(Stream.of(2,10,7,5,4,1,8,6).collect(Collectors.toList()));
        testCases.add(Stream.of(0,-4,19,1,8,-2,-3,5).collect(Collectors.toList()));
        int test = 1;

//        for (List<Integer> testCase : testCases) {
//            System.out.println("Test Cases for Approach 3: " + test);
//            System.out.print(removeMaxAndMinNumAlternate(testCase));
//            test++;
//            System.out.println();
//        }
    }

    //TODO: Approach 1: Using swapping
    public static int bruteForceApproach(Integer[] arr) {
        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
        int maxIndex = 0;
        int minIndex = 0;
        int length = arr.length;
        boolean isMaxNumber = true;
        int temp = 0;
        // 7 2 3 4 9 5
        // 4 5| 3 7 2 9
        // n
        //    1 < 5
        while(1 < length) {
            //TODO: Reset values after each loop
            max = Integer.MIN_VALUE;
            min = Integer.MAX_VALUE;
            maxIndex = 0;
            minIndex = 0;
            if(isMaxNumber) {
                for (int m = 0; m < length; m++) {
                    if(arr[m] > max) {
                        max = arr[m]; // max = 5
                        maxIndex = m; // maxIndex = 1
                    }
                }
                temp = arr[maxIndex]; // temp = 5
                arr[maxIndex] = arr[length-1]; // arr[maxIndex] = 5
                arr[length-1] = temp; // arr[length-1] = 5
                length--; // length = 1
                isMaxNumber = false;
            } else {
                for(int n = 0; n < length; n++) {
                    if(arr[n] < min) {
                        min = arr[n]; // min = 3
                        minIndex = n; // minIndex = 2
                    }
                }
                temp = arr[minIndex]; // temp = 3
                arr[minIndex] = arr[length-1]; // arr[minIndex] = 3
                arr[length-1] = temp; // arr[length-1] = 3
                length--; // length = 4
                isMaxNumber = true;
            }
        }
        return arr[length-1];
    }

    //TODO: Approach 2: Using sorting and formula
    public static int optimisedApproach(Integer[] arr) {
        int index = 0;
        Arrays.sort(arr);
        if(arr.length % 2 == 0) {
            index = (arr.length / 2 ) - 1;
        } else {
            index = arr.length / 2;
        }
        return arr[index];
    }

    //TODO: Approach 2: Using Collection Framework
    public static int removeMaxAndMinNumAlternate(List<Integer> list) {

        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
        int maxIndex = 0;
        int minIndex = 0;

        int evenOdd = 1;

        while(list.size() > 1) {
            max = Integer.MIN_VALUE;
            maxIndex = 0;

            min = Integer.MAX_VALUE;
            minIndex = 0;

            //Dry Run:  7, 8, 3, 4, 2, 9, 5
            if(evenOdd % 2 != 0) {
                for(int i=0; i < list.size(); i++) {
                    //value = 7
                    if(list.get(i) > max) {
                        max = list.get(i); //max = 9
                        maxIndex = i;
                    }
                }
                list.remove(maxIndex);
            }
            if(evenOdd % 2 == 0) {
                for(int i=0; i < list.size(); i++) {
                    //value = 7
                    if (list.get(i) < min) {
                        min = list.get(i); //min = 4
                        minIndex = i;
                    }
                }
                list.remove(minIndex);
            }
            evenOdd++;
        }
        return list.get(0);
    }
}
