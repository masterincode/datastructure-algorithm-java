package com.tutort.assignments.warmup.assignment1;

import java.util.Arrays;

public class PrintAllElementExceptTwoGreatestElement {

    public static void main(String[] args) {
        int a[] = {2, 8, 7, 1, 5};
        int b[] = {7, -2, 3, 4, 9, -1};
        printAllElementExceptTwoGreatestElement(a);
        System.out.println("=====================");
        printAllElementExceptTwoGreatestElement(b);
    }

    public static void printAllElementExceptTwoGreatestElement(int[] arr) {

        int max = Integer.MIN_VALUE;
        int secMax = Integer.MIN_VALUE;
        int resultIndex = 0;
        int[] result = new int[arr.length-2];

        //TODO: to fins max and min logic is not accpeting all test case.
        for(int i = 0; i < arr.length; i++) {
            if(arr[i] > max) {
                max = arr[i];
            }
            if(arr[i] > secMax && arr[i] < max) {
                secMax = arr[i];
            }
        }
        for (int j = 0; j < arr.length; j++) {
            if(arr[j] != secMax && arr[j] != max) {
                result[resultIndex] = arr[j];
                resultIndex++;
            }
        }
        Arrays.sort(result);
        for (int val: result) {
            System.out.print(val+",");
        }
    }
}


