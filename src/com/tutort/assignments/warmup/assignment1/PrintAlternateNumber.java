package com.tutort.assignments.warmup.assignment1;

public class PrintAlternateNumber {

    public static void main(String[] args) {
        int A[] = {1, 2, 3, 4};
        int B[] = {1, 2, 3, 4, 5};
        System.out.println("Test Case 1");
        printAlternateNumber(A);
        System.out.println();
        System.out.println("Test Case 2");
        printAlternateNumber(B);
    }

    public static void printAlternateNumber(int[] arr) {
        if(arr.length == 0) {
            return;
        }
        int i = 0;
        while(i < arr.length) {
            if(i % 2 == 0) {
                System.out.println(arr[i]);
            }
            i++;
        }
    }

}
