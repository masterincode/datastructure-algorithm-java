package com.tutort.assignments.warmup.assignment1;

public class FindSumSeriseOfGivenNumber {
    public static void main(String[] args) {
        System.out.println(findSumSeriseOfGivenNumber(1));
        System.out.println(findSumSeriseOfGivenNumber(5));
    }

    public static int findSumSeriseOfGivenNumber(int number){
        return (number * (number + 1))/2;
    }
}
