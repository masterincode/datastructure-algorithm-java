package com.tutort.assignments.warmup.assignment1;

public class PrintSumOfIntegers {

    public static void main(String[] args)  {
        int[] testCase1 = {1, 2, 3, 4};
        int[] testCase2 = {5, 8, 3, 10, 22, 45};
        printSumOfAllIntegers(testCase1);
        printSumOfAllIntegers(testCase2);
    }

    public static void printSumOfAllIntegers(int[] arr) {
        int sum = 0;
        for(int val: arr) {
            sum = sum + val;
        }
        System.out.println(sum);
    }

}
