package com.tutort.assignments.warmup.assignment1;

public class BalancedArray {

    public static void main(String[] args) {
        int arr[] = {1, 5, 3, 2};
        int brr[] = { 1, 2, 1, 2, 1, 3 };
        System.out.println(balancedArray(arr));
        System.out.println(balancedArray(brr));
    }

    public static int balancedArray(int[] arr) {
        int middle = arr.length/2;
        int firstHalf = 0;
        int secondHalf = 0;

        for(int i = 0; i < arr.length; i++) {

            if(i < middle) {
                firstHalf = firstHalf + arr[i];
            } else {
                secondHalf = secondHalf + arr[i];
            }
        }
        return Math.abs(firstHalf - secondHalf);
    }
}
